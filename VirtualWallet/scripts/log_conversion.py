import datetime
import json
import glob
import os
import re

def convert_to_json(input_files, output_file):
    log_entries = []

    for input_file in input_files:
        with open(input_file, 'r') as f:
            log_data = f.read()

        log_lines = log_data.strip().split('\n')

        for log_line in log_lines:
            try:
                log_entry = json.loads(log_line)
                log_entries.append(log_entry)
            except json.JSONDecodeError:
                print(f"Failed to parse JSON: {log_line}")

    with open(output_file, 'w') as f:
        json.dump(log_entries, f, indent=4)

def generate_formatted_log_filename():
    today = datetime.datetime.now().strftime("%Y-%m-%d")
    pattern = re.compile(r"formatted-log-{}-(\d+)\.json".format(today))
    existing_numbers = []

    for filename in os.listdir("."):
        match = pattern.match(filename)
        if match:
            existing_number = int(match.group(1))
            existing_numbers.append(existing_number)

    if existing_numbers:
        next_number = max(existing_numbers) + 1
        return f"formatted-log-{today}-{next_number}.json"

    return f"formatted-log-{today}.json"

if __name__ == "__main__":
    logs_directory = os.path.join(os.getcwd(), "VirtualWallet.Api", "Logs")
    input_log_files = glob.glob(os.path.join(logs_directory, "log-*.txt"))
    output_json_file = os.path.join(logs_directory, generate_formatted_log_filename())
    convert_to_json(input_log_files, output_json_file)
