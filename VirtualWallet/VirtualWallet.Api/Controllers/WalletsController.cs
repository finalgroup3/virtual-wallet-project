﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VirtualWallet.Api.Constants;
using VirtualWallet.Api.Filters.Authentication;
using VirtualWallet.Features.Constants;
using VirtualWallet.Models;
using VirtualWallet.Models.Wallets.Requests;
using VirtualWallet.Models.Wallets.Responses;
using static VirtualWallet.Features.GetWalletFeature;
using static VirtualWallet.Features.Users.ChangeWalletBalanceFeature.ChangeWalletBalanceFeature;
using static VirtualWallet.Features.Wallets.ChangeCurrency.ChangeCurrencyFeature;

namespace VirtualWallet.Api;

[ApiController]
[Route("api/virtual-wallet/users/{userId}/wallets")]
[Authorize]
public class WalletsController : ControllerBase
{
    private readonly IMediator _mediator;

    public WalletsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPut("balance")]
    public async Task<IActionResult> ChangeWalletBalaceAsync(
        [FromBody] ChangeWalletBalanceRequest request,
        [FromRoute] int userId)
    {
        var finReq = request with { UserId = userId };
        var response = await _mediator.Send(new ChangeWalletBalanceCommand(finReq));
        return Ok(response);
    }

    [HttpPut(ApiRoutes.ChangeWalletCurrency, Name = HttpMethodNames.ChangeWalletCurrency)]
    [InvalidCredentialsFilter]
    [ProducesResponseType(typeof(ChangeCurrencyResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status401Unauthorized)]
    public async Task<IActionResult> ChangeWalletCurrency(
        [FromRoute] int userId,
        [FromBody] ChangeCurrencyRequest request)
    {
        var response = await _mediator.Send(new ChangeCurrencyCommand(request with { UserId = userId}));

        return new CreatedAtActionResult(nameof(GetWalletAsync), nameof(WalletsController), new { userId }, response);
    }
    [HttpGet(Name = "GetWallet")]
    [ProducesResponseType(typeof(CreateWalletResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetWalletAsync(
        [FromRoute] int userId)
    {
        var response = await _mediator.Send(new GetWalletQuery(new GetWalletRequest(userId)));
        return Ok(response);
    }
}
