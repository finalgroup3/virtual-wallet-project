﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using VirtualWallet.Api.Constants;
using VirtualWallet.Api.Filters.Authentication;
using VirtualWallet.Features.Constants;
using VirtualWallet.Models.Authentication.Responses;
using VirtualWallet.Models.User.Requests;
using VirtualWallet.Models.User.Responses;
using static VirtualWallet.Features.Users.DeleteUser.DeleteUserFeature;
using static VirtualWallet.Features.GetAllUsersFeature;
using static VirtualWallet.Features.GetUserFeature;
using static VirtualWallet.Features.Users.BlockUser.BlockUserFeature;
using static VirtualWallet.Features.Users.ChangeEmail.ChangeEmailFeature;
using static VirtualWallet.Features.Users.ChangePhoneNumber.ChangePhoneNumberFeature;
using static VirtualWallet.Features.Users.ChangePhoneNumber.ChangePictureUrlFeature;
using static VirtualWallet.Features.Users.DemoteUser.DemoteUserFeature;
using static VirtualWallet.Features.Users.FilterUsers.FilterUsersFeature;
using static VirtualWallet.Features.Users.GetReceivedAmountLastMonth.GetStatisticsFeature;
using static VirtualWallet.Features.Users.GetUserProfile.GetUserProfileFeature;
using static VirtualWallet.Features.Users.GetUserProfileByUsername.GetUserProfileByUsernameFeature;
using static VirtualWallet.Features.Users.PromoteUser.PromoteUserFeature;
using static VirtualWallet.Features.Users.UnblockUser.UnblockUserFeature;

namespace VirtualWallet.Api.Controllers;

[ApiController]
[Route(ApiRoutes.UserBase)]
[Authorize]
public class UsersController : ControllerBase
{
    private readonly IMediator _mediator;

    public UsersController(IMediator mediator)
    {
        _mediator = mediator;

    }

    [HttpGet(ApiRoutes.IdIdentifier, Name = HttpMethodNames.GetUser)]
    [ProducesResponseType(typeof(UserResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetUserAsync([FromRoute] int id)
    {
        var request = new GetUserRequest(id);
        var response = await _mediator.Send(new GetUserCommand(request));

        return Ok(response);
    }

    [HttpGet(ApiRoutes.GetUserProfile, Name = HttpMethodNames.GetUserProfile)]
    [ProducesResponseType(typeof(UserProfileResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetUserProfileAsync([FromRoute] int id)
    {
        var request = new GetUserRequest(id);
        var response = await _mediator.Send(new GetUserProfileCommand(request));

        return Ok(response);
    }

    [HttpGet(ApiRoutes.GetAllUsers, Name = HttpMethodNames.GetAllUsers)]
    [ProducesResponseType(typeof(List<UserProfileResponse>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetAllUsersAsync()
    {
        var response = await _mediator.Send(new GetAllUsersQuery());

        return Ok(response);
    }

    [HttpGet(ApiRoutes.GetUserProfileByUsername, Name = HttpMethodNames.GetUserProfileByUsername)]
    [ProducesResponseType(typeof(UserProfileResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetUserProfileByUsernameAsync([FromRoute] string username)
    {
        var request = new GetUserByUsernameRequest(username);
        var response = await _mediator.Send(new GetUserProfileByUsernameCommand(request));

        return Ok(response);
    }

    [HttpGet(ApiRoutes.FilterUsers, Name = HttpMethodNames.FilterUsers)]
    [ProducesResponseType(typeof(UserFilterResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> FilterUsersAsync([FromQuery] FilterUsersRequest request)
    {
        var response = await _mediator.Send(new FilterUsersCommand(request));

        return Ok(response);
    }

    [Authorize]
    [HttpPut(ApiRoutes.ChangeEmail, Name = HttpMethodNames.ChangeEmail)]
    [InvalidTokenFilter]
    [ProducesResponseType(typeof(InvalidTokenErrorResponse), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ChangeEmailResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> ChangeEmailAsync([FromQuery] string token,
        [FromBody] ChangeEmailRequest request)
    {
        var response = await _mediator.Send(new ChangeEmailCommand(request, token));

        Response.Cookies.Delete("Cookie_JWT");

        return Ok(response);
    }

    [Authorize]
    [HttpPut(ApiRoutes.ChangePhoneNumber, Name = HttpMethodNames.ChangePhoneNumber)]
    [ProducesResponseType(typeof(ChangePhoneNumberResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> ChangePhoneNumberAsync([FromBody] ChangePhoneNumberBody number)
    {
        var loggedId = await GetLoggedUserId();
        var newPhoneNumber = number.NewPhoneNumber;
        var request = new ChangePhoneNumberRequest(newPhoneNumber, loggedId);
        var response = await _mediator.Send(new ChangePhoneNumberCommand(request));

        return Ok(response);
    }

    [Authorize]
    [HttpPut(ApiRoutes.ChangePictureUrl, Name = HttpMethodNames.ChangePictureUrl)]
    [ProducesResponseType(typeof(ChangePictureUrlResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> ChangePictureUrlAsync([FromBody] ChangePictureUrlBody url)
    {
        var loggedId = await GetLoggedUserId();
        var newPictureUrl = url.NewPictureUrl;
        var request = new ChangePictureUrlRequest(newPictureUrl, loggedId);
        var response = await _mediator.Send(new ChangePictureUrlCommand(request));

        return Ok(response);
    }

    [HttpGet(ApiRoutes.GetStatistiscs, Name = HttpMethodNames.GetStatistics)]
    [ProducesResponseType(typeof(GetStatisticsResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetStatisticsAsync(
        [FromRoute] int id,
        [FromQuery] DateTime startDate,
        DateTime endDate)
    {
        var response = await _mediator.Send(new GetReceivedAmountLastMonthQuery(
            new GetStatisticsRequest(
            id,
            startDate,
            endDate)));

        return Ok(response);
    }

    [Authorize(Roles = "Admin")]
    [HttpPut(ApiRoutes.BlockUser, Name = HttpMethodNames.BlockUser)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> BlockUserAsync([FromRoute] int id)
    {
        int loggedUserId = await GetLoggedUserId();
        var request = new BlockStatusRequest(id, loggedUserId);

        var response = await _mediator.Send(new BlockUserCommand(request));

        return Ok(response);
    }

    [Authorize(Roles = "Admin")]
    [HttpPut(ApiRoutes.UnblockUser, Name = HttpMethodNames.UnblockUser)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> UnblockUserAsync([FromRoute] int id)
    {
        int loggedUserId = await GetLoggedUserId();
        var request = new BlockStatusRequest(id, loggedUserId);

        var response = await _mediator.Send(new UnblockUserCommand(request));

        return Ok(response);
    }

    [Authorize(Roles = "Admin")]
    [HttpPut(ApiRoutes.DemoteUser, Name = HttpMethodNames.DemoteUser)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> DemoteUserAsync([FromRoute] int id)
    {
        int loggedUserId = await GetLoggedUserId();
        var request = new AdminStatusRequest(id, loggedUserId);

        var response = await _mediator.Send(new DemoteUserCommand(request));

        return Ok(response);
    }

    [Authorize(Roles = "Admin")]
    [HttpPut(ApiRoutes.PromoteUser, Name = HttpMethodNames.PromoteUser)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> PromoteUserAsync([FromRoute] int id)
    {
        int loggedUserId = await GetLoggedUserId();
        var request = new AdminStatusRequest(id, loggedUserId);

        var response = await _mediator.Send(new PromoteUserCommand(request));

        return Ok(response);
    }

    [HttpDelete(ApiRoutes.IdIdentifier, Name = HttpMethodNames.DeleteUser)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> DeleteUserAsync(
        [FromRoute] int id)
    {
        _ = await _mediator.Send(new DeleteUserCommand(new DeleteUserRequest(id)));

        return NoContent();
    }

    private async Task<int> GetLoggedUserId()
    {
        await Task.CompletedTask;

        var userIdentity = User.Identity as ClaimsIdentity;
        var authId = int.Parse(userIdentity?.FindFirst(ClaimTypes.NameIdentifier)?.Value!);
        return authId;
    }
}
