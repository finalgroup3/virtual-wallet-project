﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using VirtualWallet.Api.Constants;
using VirtualWallet.Api.Filters.Authentication;
using VirtualWallet.Features.Constants;
using VirtualWallet.Models.Transfer.Requests;
using VirtualWallet.Models.Transfer.Responses;
using static VirtualWallet.Features.Transfers.FilterTransfers.FilterTransfersFeature;
using static VirtualWallet.Features.Transfers.GetTransfer.GetTransferFeature;
using static VirtualWallet.Features.Transfers.TransferFunds.TransferFundsFeature;

namespace VirtualWallet.Api.Controllers;

[ApiController]
[Route(ApiRoutes.TransferBase)]
[Authorize]
public class TransfersController : Controller
{
    private readonly IMediator _mediator;
    public TransfersController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost(Name = HttpMethodNames.TransferFunds)]
    [UnverifiedFilter]
    [ProducesResponseType(typeof(TransferFundsResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status401Unauthorized)]
    public async Task<IActionResult> TransferFundsAsync(TransferRequestBody request)
    {
        int senderId = await GetUserIdFromToken();

        var response = await _mediator.Send(new TransferFundsCommand(new TransferFundsRequest(senderId, request.RecipientId, request.Amount)));

        return Ok(response);
    }

    [HttpGet(ApiRoutes.FilterTransfers, Name = HttpMethodNames.FilterTransfers)]
    [ProducesResponseType(typeof(FilterTransfersResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> FilterTransfersAsync([FromQuery] FilterTransfersRequest request)
    {
        var response = await _mediator.Send(new FilterTransfersCommand(request));

        return Ok(response);
    }

    [HttpGet(ApiRoutes.TransferIdentifier, Name = HttpMethodNames.Transfer)]
    [ProducesResponseType(typeof(TransferFundsResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetTransferAsync([FromRoute] int id)
    {
        var request = new TransferRequest(id);
        var response = await _mediator.Send(new GetTransferCommand(request));

        return Ok(response);
    }

    private async Task<int> GetUserIdFromToken()
    {
        await Task.CompletedTask;

        var userIdentity = User.Identity as ClaimsIdentity;
        return int.Parse(userIdentity?.FindFirst(ClaimTypes.NameIdentifier)?.Value!);
    }
}
