using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using VirtualWallet.Api.Constants;
using VirtualWallet.Api.Filters.Authentication;
using VirtualWallet.Features.Common.Interfaces.Abstractions;
using VirtualWallet.Features.Constants;
using VirtualWallet.Models;
using VirtualWallet.Models.Authentication.Requests;
using VirtualWallet.Models.Authentication.Responses;
using VirtualWallet.Models.User.Responses;
using static VirtualWallet.Features.Authentication.InitiateEmailChange.InitiateEmailChangeFeature;
using static VirtualWallet.Features.Authentication.Login.LoginFeature;
using static VirtualWallet.Features.Authentication.Register.RegisterFeature;
using static VirtualWallet.Features.Authentication.ResetPassword.ResetPasswordFeature;
using static VirtualWallet.Features.Authentication.Verify.VerifyUserFeature;
using static VirtualWallet.Features.Users.ForgotPassword.ForgotPasswordFeature;

namespace VirtualWallet.Api.Controllers;

[ApiController]
[Route(ApiRoutes.AuthBase)]
[AllowAnonymous]
public class AuthenticationController : ControllerBase
{
    private readonly IMediator _mediator;
    private readonly IEmailSender _emailSender;

    public AuthenticationController(IMediator mediator, IEmailSender emailSender)
    {
        _mediator = mediator;
        _emailSender = emailSender;
    }

    [HttpPost(ApiRoutes.Register, Name = HttpMethodNames.Register)]
    [ServiceFilter(typeof(RegisterRequestFilter))]
    [ProducesResponseType(typeof(RegisterSuccessResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> RegisterAsync(
        [FromBody] RegisterRequest request)
    {
        var response = await _mediator.Send(
            new RegisterCommand(request));

        var emailBody = EmailMessages.RegistrationBody.Replace("[link]", response.Data.VerificationLink);

        await _emailSender.SendEmailAsync(
            request.Email,
            EmailMessages.RegistrationSubject,
            emailBody);

        return Ok(response);
    }

    [HttpPost(ApiRoutes.Login, Name = HttpMethodNames.Login)]
    [InvalidCredentialsFilter]
    [ProducesResponseType(typeof(AuthenticationResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status401Unauthorized)]
    public async Task<IActionResult> LoginAsync(
        [FromBody] LoginRequest request)
    {
        var response = await _mediator.Send(
            new LoginCommand(request));

        Response.Cookies.Append("Cookie_JWT", response.Data.Token, new CookieOptions
        {
            SameSite = SameSiteMode.None
        });

        return Ok(response);
    }

    [HttpPost(ApiRoutes.Logout, Name = HttpMethodNames.Logout)]
    [ProducesResponseType(typeof(LogoutResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(LogoutResponse), StatusCodes.Status400BadRequest)]
    public IActionResult Logout()
    {
        // retrieve the cookie to a variable
        var cookie = Request.Cookies["Cookie_JWT"];
        if (cookie is null)
        {
            return BadRequest(new LogoutResponse(
                false,
                "Logout failed."));
        }
        // get the token from the cookie
        JwtSecurityToken? token = new JwtSecurityTokenHandler().ReadJwtToken(cookie);

        // check if token has expired or is null
        if (token.ValidTo < DateTime.UtcNow || token is null)
        {
            return BadRequest(new LogoutResponse(
                false,
                "Logout failed."));
        }

        Response.Cookies.Append("Cookie_JWT", "", new CookieOptions
        {
            Expires = DateTime.Now.AddDays(-1),
            HttpOnly = true,
            Secure = true,
            SameSite = SameSiteMode.Lax
        });

        return Ok(new LogoutResponse(true, AuthenticationLiterals.LogoutSuccess));
    }

    [HttpGet(ApiRoutes.Verify, Name = HttpMethodNames.VerifyEmail)]
    [InvalidTokenFilter]
    [ProducesResponseType(typeof(InvalidTokenErrorResponse), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(VerificationResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> VerifyEmailAsync([FromQuery] string token)
    {
        var response = await _mediator.Send(
                       new VerifyUserCommand(token), HttpContext.RequestAborted);

        return Ok(response);
    }

    [HttpPost(ApiRoutes.ForgotPassword, Name = HttpMethodNames.ForgotPassword)]
    [ProducesResponseType(typeof(ForgotPasswordResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordRequest request)
    {
        var response = await _mediator.Send(new ForgotPasswordCommand(request), HttpContext.RequestAborted);

        var emailBody = EmailMessages.ResetPasswordBody.Replace("[link]", response.ResetPasswordLink);

        await _emailSender.SendEmailAsync(
            request.Email,
            EmailMessages.ResetPasswordSubject,
            emailBody);

        return Ok(response);
    }
    
    [HttpPost(ApiRoutes.ResetPassword, Name = HttpMethodNames.ResetPassword)]
    [InvalidTokenFilter]
    
    [ProducesResponseType(typeof(InvalidTokenErrorResponse), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ResetPasswordResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> ResetPasswordAsync([FromQuery] string token,
        [FromBody] ResetPasswordRequest request)
    {
        var response = await _mediator.Send(new ResetPasswordCommand(request, token), HttpContext.RequestAborted);

        return Ok(response);
    }

    [Authorize]
	[HttpPost(ApiRoutes.InitiateEmailChange, Name = HttpMethodNames.InitiateEmailChange)]
	[ProducesResponseType(typeof(InitiateEmailChangeResponse), StatusCodes.Status200OK)]
	public async Task<IActionResult> InitiateEmailChange()
	{
        var email = await GetLoggedUserEmail();
        var loggedId = await GetLoggedUserId();
		var request = new InitiateEmailChangeRequest(email, loggedId);
		var response = await _mediator.Send(new InitiateEmailChangeCommand(request), HttpContext.RequestAborted);

		var emailBody = EmailMessages.ChangeEmailBody.Replace("[link]", response.ChangeEmailLink);

		await _emailSender.SendEmailAsync(
			request.Email,
			EmailMessages.ChangeEmailSubject,
			emailBody);

		return Ok(response);
	}    

	private async Task<int> GetLoggedUserId()
	{
        await Task.CompletedTask;

		var userIdentity = User.Identity as ClaimsIdentity;
		var authId = int.Parse(userIdentity?.FindFirst(ClaimTypes.NameIdentifier)?.Value!);
		return authId;
	}

	private async Task<string> GetLoggedUserEmail()
	{
        await Task.CompletedTask;

        var userIdentity = User.Identity as ClaimsIdentity;
		var email = userIdentity?.FindFirst(ClaimTypes.Email)?.Value!;
		return email;
	}
}