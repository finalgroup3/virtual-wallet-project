﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using VirtualWallet.Api.Constants;
using VirtualWallet.Api.Filters.Card;
using VirtualWallet.Features.Constants;
using VirtualWallet.Models.Cards.Requests;
using VirtualWallet.Models.Cards.Responses;
using static VirtualWallet.Features.Cards.DeleteCard.DeleteCardFeature;
using static VirtualWallet.Features.Cards.GetCardsOfUser.GetCardsOfUserFeature;
using static VirtualWallet.Features.CreateCardFeature;
using static VirtualWallet.Features.GetCardFeature;

namespace VirtualWallet.Api.Controllers;

[ApiController]
[Route(ApiRoutes.CardBase)]
[Authorize(Roles = "Unblocked")]
public class CardsController : ControllerBase
{
    private readonly IMediator _mediator;
    public CardsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost(Name = HttpMethodNames.CreateCard)]
	[ServiceFilter(typeof(CardCreateRequestFilter))]
	[ProducesResponseType(typeof(CardResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> CreateCardAsync([FromBody] CreateCardRequest request)
    {
        int userId = await GetUserIdFromToken();
        var response = await _mediator.Send(new CreateCardCommand(request, userId));

        return CreatedAtAction(nameof(GetCardByNumberAsync), new { number = response.Number }, response);
    }

    [HttpGet(ApiRoutes.CardsOfUser, Name = HttpMethodNames.GetCardsOfUser)]
    [ProducesResponseType(typeof(List<CardResponse>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetCardsOfUserAsync([FromRoute] int userId)
    {
        var response = await _mediator.Send(new GetCardsOfUserCommand(new GetCardsOfUserRequest(userId)));

        return Ok(response);
    }

    [HttpGet(ApiRoutes.NumberIdentifier, Name = HttpMethodNames.GetCard)]
    [ProducesResponseType(typeof(CardResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetCardByNumberAsync([FromRoute] int id)
    {
        var response = await _mediator.Send(new GetCardCommand(new GetCardRequest(id)));

        return Ok(response);
    }

    [HttpDelete(ApiRoutes.NumberIdentifier, Name = HttpMethodNames.DeleteCard)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> DeleteCardAsync(int id)
    {
        int authId = await GetUserIdFromToken();
        var request = new DeleteCardRequest(id, authId);

        _ = await _mediator.Send(new DeleteCardCommand(request));

        return NoContent();
    }
    private async Task<int> GetUserIdFromToken()
    {
        await Task.CompletedTask;

        var userIdentity = User.Identity as ClaimsIdentity;
        return int.Parse(userIdentity?.FindFirst(ClaimTypes.NameIdentifier)?.Value!);
    }
}