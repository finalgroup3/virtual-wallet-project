﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using VirtualWallet.Domain.Exceptions.Common;

namespace VirtualWallet.Api.Filters.Authentication;

public class InvalidCredentialsFilterAttribute : ExceptionFilterAttribute
{
    public override async Task OnExceptionAsync(ExceptionContext context)
    {
        Exception exception = context.Exception;

        if (exception is InvalidCredentialsException)
        {
            var problemDetails = new ProblemDetails
            {
                Type = "errors/invalid-credentials",
                Title = "Invalid Credentials",
                Status = 401,
                Detail = "The provided credentials were invalid. Check your email and/or password.",
                Instance = context.HttpContext.Request.Path
            };

            context.Result = new ObjectResult(problemDetails)
            {
                StatusCode = 401
            };
        }

        await base.OnExceptionAsync(context);
    }

}
