﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Models.Authentication.Responses;

namespace VirtualWallet.Api.Filters.Authentication;

public class InvalidTokenFilterAttribute : ExceptionFilterAttribute
{
    public override async Task OnExceptionAsync(ExceptionContext context)
    {
        Exception exception = context.Exception;

        var method = context.ActionDescriptor.DisplayName;

        if (exception is InvalidTokenException)
        {
            var errorResponse = new
            {
                Type = "errors/invalid-token",
                Success = false,
                Message = "Invalid or expired token."
            };

            var response = new InvalidTokenErrorResponse(
                errorResponse.Type,
                errorResponse.Success,
                errorResponse.Message);

            context.HttpContext.Response.ContentType = "application/problem+json";
            context.Result = new ObjectResult(response)
            {
                StatusCode = 400
            };
        }
        await base.OnExceptionAsync(context);
    }
}
