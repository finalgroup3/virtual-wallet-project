﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text.RegularExpressions;
using VirtualWallet.Features.Constants;
using VirtualWallet.Models.Authentication.Requests;

namespace VirtualWallet.Api.Filters.Authentication;

public partial class ResetPasswordFilter : IActionFilter
{
    private static readonly Regex _passRegex = PassRegex();
    public void OnActionExecuting(ActionExecutingContext context)
    {
        if (context.ActionArguments.TryGetValue("request", out var requestObj) && requestObj is ResetPasswordRequest request)
        {
            PasswordValidation(context, request);

            if (!context.ModelState.IsValid)
            {
                var problemDetails = new ValidationProblemDetails(context.ModelState)
                {
                    Type = "https://example.com/errors/validation-error",
                    Title = "One or more validation errors occurred.",
                    Status = StatusCodes.Status400BadRequest,
                    Detail = "Please refer to the errors property for additional details."
                };

                context.Result = new BadRequestObjectResult(problemDetails);
                return;
            }
            return;
        }
        context.Result = new BadRequestObjectResult("Invalid request");
    }

    private static void PasswordValidation(ActionExecutingContext context, ResetPasswordRequest request)
    {
        var password = request.Password;

        if (string.IsNullOrEmpty(password))
        {
            context.ModelState.AddModelError(Validation.Password, Validation.ErrorRequired(Validation.Password));
            return;
        }
        if (password.Length < Validation.PasswordMinLength || password.Length > Validation.PasswordMaxLength)
        {
            context.ModelState.AddModelError(Validation.Password, Validation.ErrorLength(Validation.Password, Validation.PasswordMinLength, Validation.PasswordMaxLength));
            return;
        }
        if (!_passRegex.IsMatch(password))
        {
            context.ModelState.AddModelError(Validation.Password, Validation.PasswordFormatError());
            return;
        }

        var confirmPassword = request.ConfirmPassword;

        if (!confirmPassword.Equals(password))
        {
            context.ModelState.AddModelError(Validation.Password, Validation.PasswordMatchError());
        }


    }
    public void OnActionExecuted(ActionExecutedContext context)
    {
        // No implementation
    }

    [GeneratedRegex(Validation.PasswordRegex)]
    private static partial Regex PassRegex();
}
