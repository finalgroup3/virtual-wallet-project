﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text.RegularExpressions;
using VirtualWallet.Features.Constants;
using VirtualWallet.Models;

namespace VirtualWallet.Api.Filters.Authentication;

public partial class RegisterRequestFilter : IActionFilter
{
    private static readonly Regex _passRegex = PassRegex();
    private static readonly Regex _emailRegex = EmailRegex();
    private static readonly Regex _phoneRegex = PhoneRegex();

    public void OnActionExecuting(ActionExecutingContext context)
    {
        if (context.ActionArguments.TryGetValue("request", out var requestObj) && requestObj is RegisterRequest request)
        {
            UsernameValidation(context, request);

            PasswordValidation(context, request);

            EmailValidation(context, request);

            PhoneNumberValidation(context, request);

            if (!context.ModelState.IsValid)
            {
                var problemDetails = new ValidationProblemDetails(context.ModelState)
                {
                    Type = "https://example.com/errors/validation-error",
                    Title = "One or more validation errors occurred.",
                    Status = StatusCodes.Status400BadRequest,
                    Detail = "Please refer to the errors property for additional details."
                };

                context.Result = new BadRequestObjectResult(problemDetails);
                return;
            }
            return;
        }
        context.Result = new BadRequestObjectResult("Invalid request");
    }

    private static void PhoneNumberValidation(ActionExecutingContext context, RegisterRequest request)
    {
        var phoneNumber = request.PhoneNumber;

        if (string.IsNullOrEmpty(phoneNumber))
        {
            context.ModelState.AddModelError(Validation.PhoneNumber, Validation.ErrorRequired(Validation.PhoneNumber));
            return;
        }

        if (phoneNumber.Length != Validation.PhoneNumberLength)
        {
            context.ModelState.AddModelError(Validation.PhoneNumber, Validation.PhoneNumberLengthError());
            return;
        }

        if (!_phoneRegex.IsMatch(phoneNumber))
        {
            context.ModelState.AddModelError(Validation.PhoneNumber, Validation.PhoneNumberDigitError());
        }
    }

    private static void EmailValidation(ActionExecutingContext context, RegisterRequest request)
    {
        if (string.IsNullOrEmpty(request.Email))
        {
            context.ModelState.AddModelError(Validation.Email, Validation.ErrorRequired(Validation.Email));
            return;
        }

        if (!_emailRegex.IsMatch(request.Email))
        {
            context.ModelState.AddModelError(Validation.Email, Validation.ErrorFormat(Validation.Email));
        }
    }

    private static void PasswordValidation(ActionExecutingContext context, RegisterRequest request)
    {
        var password = request.Password;

        if (string.IsNullOrEmpty(password))
        {
            context.ModelState.AddModelError(Validation.Password, Validation.ErrorRequired(Validation.Password));
            return;
        }
        if (password.Length < Validation.PasswordMinLength || password.Length > Validation.PasswordMaxLength)
        {
            context.ModelState.AddModelError(Validation.Password, Validation.ErrorLength(Validation.Password, Validation.PasswordMinLength, Validation.PasswordMaxLength));
            return;
        }
        if (!_passRegex.IsMatch(password))
        {
            context.ModelState.AddModelError(Validation.Password, Validation.PasswordFormatError());
        }
    }

    private static void UsernameValidation(ActionExecutingContext context, RegisterRequest request)
    {
        var username = request.Username;

        if (string.IsNullOrEmpty(username))
        {
            context.ModelState.AddModelError(Validation.Username, Validation.ErrorRequired(Validation.Username));
            return;
        }
        if (username.Length < Validation.UsernameMinLength || username.Length > Validation.UsernameMaxLength)
        {
            context.ModelState.AddModelError(Validation.Username, Validation.ErrorLength(Validation.Username, Validation.UsernameMinLength, Validation.UsernameMaxLength));
        }
    }

    public void OnActionExecuted(ActionExecutedContext context)
    {
        // No action needed after the request is executed
    }

    [GeneratedRegex(Validation.PasswordRegex)]
    private static partial Regex PassRegex();

    [GeneratedRegex(Validation.EmailRegex)]
    private static partial Regex EmailRegex();

    [GeneratedRegex(Validation.PhoneNumberRegex)]
    private static partial Regex PhoneRegex();
}