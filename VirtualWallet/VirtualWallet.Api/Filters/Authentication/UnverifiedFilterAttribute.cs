﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;

namespace VirtualWallet.Api.Filters.Authentication
{
    public class UnverifiedFilterAttribute : ExceptionFilterAttribute
    {
        public override async Task OnExceptionAsync(ExceptionContext context)
        {
            Exception exception = context.Exception;

            if (exception is UnverifiedAccountException)
            {
                var problemDetails = new ProblemDetails
                {
                    Type = "errors/unverified",
                    Title = "Unverified",
                    Status = 401,
                    Detail = "Your account hasn't been verified yet, please click on the link in your email.",
                    Instance = context.HttpContext.Request.Path
                };

                context.Result = new ObjectResult(problemDetails)
                {
                    StatusCode = 401
                };
            }

            await base.OnExceptionAsync(context);
        }
    }
}
