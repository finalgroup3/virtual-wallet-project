﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text.RegularExpressions;
using VirtualWallet.Features.Constants;
using VirtualWallet.Models.Cards.Requests;

namespace VirtualWallet.Api.Filters.Card;

public partial class CardCreateRequestFilter : IActionFilter
{

    private static readonly Regex _numberRegex = CardNumberRegex();
    private static readonly Regex _expiryRegex = ExpiryRegex();
    private static readonly Regex _cvvRegex = CVVRegex();

    public void OnActionExecuting(ActionExecutingContext context)
    {
        if (context.ActionArguments.TryGetValue("request", out var requestObj) && requestObj is CreateCardRequest request)
        {
            NameValidation(context, request);

            NumberValidation(context, request);

            ExpiryValidation(context, request);

            CVVValidation(context, request);

            if (!context.ModelState.IsValid)
            {
                var problemDetails = new ValidationProblemDetails(context.ModelState)
                {
                    Type = "https://example.com/errors/validation-error",
                    Title = "One or more validation errors occurred.",
                    Status = StatusCodes.Status400BadRequest,
                    Detail = "Please refer to the errors property for additional details."
                };

                context.Result = new BadRequestObjectResult(problemDetails);
                return;
            }
            return;
        }
        context.Result = new BadRequestObjectResult("Invalid request" + context.ModelState.ToString());
    }

    private static void NameValidation(ActionExecutingContext context, CreateCardRequest request)
    {
        var name = request.Name;

        if (string.IsNullOrEmpty(name))
        {
            context.ModelState.AddModelError(Validation.CardHolder, Validation.ErrorRequired(Validation.CardHolder));
            return;
        }

        if (name.Length < Validation.CardHolderMinLength || name.Length > Validation.CardHolderMaxLength)
        {
            context.ModelState.AddModelError(Validation.CardHolder, Validation.CardHolderLengthError());
            return;
        }

    }

    private static void NumberValidation(ActionExecutingContext context, CreateCardRequest request)
    {
        var number = request.Number;
        if (string.IsNullOrEmpty(number))
        {
            context.ModelState.AddModelError(Validation.CardNumber, Validation.ErrorRequired(Validation.CardNumber));
            return;
        }

        if (number.Length != Validation.CardNumberLength)
        {
            context.ModelState.AddModelError(Validation.CardNumber, Validation.CardNumberLengthError());
            return;
        }

        if (!_numberRegex.IsMatch(request.Number))
        {
            context.ModelState.AddModelError(Validation.CardNumber, Validation.CardNumberFormatError());
        }
    }

    private static void ExpiryValidation(ActionExecutingContext context, CreateCardRequest request)
    {
        var expiry = request.ExpirationDate;

        if (string.IsNullOrEmpty(expiry))
        {
            context.ModelState.AddModelError(Validation.Expiry, Validation.ErrorRequired(Validation.Expiry));
            return;
        }
        if (expiry.Length != Validation.ExpiryLength)
        {
            context.ModelState.AddModelError(Validation.Expiry, Validation.ExpiryLengthError());
            return;
        }
        if (!_expiryRegex.IsMatch(expiry))
        {
            context.ModelState.AddModelError(Validation.Expiry, Validation.ExpiryFormatError());
        }
    }

    private static void CVVValidation(ActionExecutingContext context, CreateCardRequest request)
    {
        var cvv = request.SecurityCode;

        if (string.IsNullOrEmpty(cvv))
        {
            context.ModelState.AddModelError(Validation.CVV, Validation.ErrorRequired(Validation.CVV));
            return;
        }
        if (cvv.Length != Validation.CVVLength)
        {
            context.ModelState.AddModelError(Validation.CVV, Validation.CVVLengthError());
        }
        if (!_cvvRegex.IsMatch(cvv))
        {
            context.ModelState.AddModelError(Validation.CVV, Validation.CVVFormatError());
        }
    }

    public void OnActionExecuted(ActionExecutedContext context)
    {
        // No action needed after the request is executed
    }

    [GeneratedRegex(Validation.CardNumberRegex)]
    private static partial Regex CardNumberRegex();

    [GeneratedRegex(Validation.ExpiryRegex)]
    private static partial Regex ExpiryRegex();

    [GeneratedRegex(Validation.CVVRegex)]
    private static partial Regex CVVRegex();
}
