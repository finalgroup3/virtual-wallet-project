using Microsoft.AspNetCore.Authentication.JwtBearer;
using Serilog;
using VirtualWallet.Api;
using VirtualWallet.Api.Middlewares;
using VirtualWallet.Api.OptionsSetup;
using VirtualWallet.Api.Services;
using VirtualWallet.Data;
using VirtualWallet.Features;
using VirtualWallet.Features.Common.Interfaces.Abstractions;
using VirtualWallet.Features.ExceptionHandlers;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllersWithViews();

builder.Services
    .AddData()
    .AddFeatures()
    .AddPresentation();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer();



builder.Services.ConfigureOptions<JwtOptionsSetup>();
builder.Services.ConfigureOptions<JwtBearerOptionsSetup>();
builder.Services.ConfigureOptions<AuthMessageSenderOptionsSetup>();

builder.Services.AddTransient<ExceptionHandlerFactory>();
builder.Services.AddTransient<EntityNotFoundExceptionHandler>();
builder.Services.AddTransient<DuplicateEntityExceptionHandler>();
builder.Services.AddTransient<GeneralExceptionHandler>();

builder.Services.AddTransient<GlobalExceptionHandlingMiddleware>();

builder.Host.UseSerilog((context, configuration) => configuration.ReadFrom.Configuration(context.Configuration));

builder.Services.AddScoped<ILinkService, LinkService>();
builder.Services.AddHttpContextAccessor();

builder.Services.AddTransient<IEmailSender, EmailService>();
builder.Services.AddHttpClient<IExchangeRateService, ExchangeRateService>();

builder.Services.AddCors(builder =>
{
    builder.AddPolicy("V8W", policy =>
    {
        policy.WithOrigins("http://localhost:3006")
            .AllowAnyHeader()
            .AllowAnyMethod()
            .AllowCredentials();
    });
});

var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseSwagger();
app.UseSwaggerUI();

app.UseSerilogRequestLogging();

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseCors("V8W");

app.UseAuthentication();
app.UseAuthorization();

app.UseMiddleware<GlobalExceptionHandlingMiddleware>();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
