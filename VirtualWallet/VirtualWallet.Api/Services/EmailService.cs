using Mailjet.Client;
using Mailjet.Client.Resources;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using VirtualWallet.Data;
using VirtualWallet.Features.Common.Interfaces.Abstractions;

namespace VirtualWallet.Api.Services;

internal sealed class EmailService : IEmailSender
{
    private readonly MailjetClient _mailjetClient;
    private readonly AuthMessageOptions _authOptions;
    private readonly ILogger<EmailService> _logger;

    public EmailService(IOptions<AuthMessageOptions> options, ILogger<EmailService> logger)
    {
        _authOptions = options.Value;
        _mailjetClient = new MailjetClient(_authOptions.ApiKey, _authOptions.ApiSecret);
        _logger = logger;
    }

    public async Task<bool> SendEmailAsync(string email, string subject, string message)
    {
        var request = new MailjetRequest
        {
            Resource = Send.Resource,
        }
        .Property(Send.FromEmail, _authOptions.SenderEmail)
        .Property(Send.FromName, _authOptions.SenderName)
        .Property(Send.Subject, subject)
        .Property(Send.HtmlPart, message)
        .Property(Send.Recipients, new JArray
        {
            new JObject
            {
                {"Email", email}
            }
        });

        try
        {
            var response = await _mailjetClient.PostAsync(request);
            return response.IsSuccessStatusCode;
        }
        catch (Exception ex)
        {
            // BUG: Figure out why this never throws exceptions even with an invalid email
            _logger.LogError(ex, "Error sending email");
            return false;
        }
    }
}