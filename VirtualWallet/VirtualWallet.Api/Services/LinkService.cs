using VirtualWallet.Features.Common.Interfaces.Abstractions;
using VirtualWallet.Models;

namespace VirtualWallet.Api.Services;

internal sealed class LinkService : ILinkService
{
    private readonly LinkGenerator _linkGenerator;
    private readonly IHttpContextAccessor _httpContextAccessor;

    public LinkService(LinkGenerator linkGenerator, IHttpContextAccessor httpContextAccessor)
    {
        _linkGenerator = linkGenerator;
        _httpContextAccessor = httpContextAccessor;
    }
    public Link Generate(string routeName, object? routeValue = null, string? rel = null, string? method = null)
    {
        HttpContext httpContext = _httpContextAccessor.HttpContext!;

        return new Link(
            _linkGenerator.GetUriByName(
                httpContext,
                routeName,
                routeValue)!,
                rel,
                method);
    }
}