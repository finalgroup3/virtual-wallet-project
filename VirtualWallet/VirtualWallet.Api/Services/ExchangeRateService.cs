﻿using VirtualWallet.Features;
using VirtualWallet.Models;

namespace VirtualWallet.Api;

public class ExchangeRateService : IExchangeRateService
{
    private readonly HttpClient _httpClient;
    private const string BASE_URL = "https://v6.exchangerate-api.com/v6/";
    private const string API_KEY = "6288a44edd99f941e5c45fa3";
    public ExchangeRateService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<ExchangeRateResponse> GetExchangeRateAsync(string baseCurrency, string targetCurrency, decimal amount)
    {
        var url = $"{BASE_URL}{API_KEY}/pair/{baseCurrency}/{targetCurrency}/{amount}";
        var response = await _httpClient.GetFromJsonAsync<ExchangeRateResponse>(url);
        return response!;
    }

    public async Task<List<ExchangeRateResponse>> GetBestExchangeRatesAsync(string baseCurrency, string[] targetCurrencies, decimal amount)
    {
        var exchangeRateResponses = new List<ExchangeRateResponse>();
        foreach (var targetCurrency in targetCurrencies)
        {
            var exchangeRateResponse = await GetExchangeRateAsync(baseCurrency, targetCurrency, amount);
            exchangeRateResponses.Add(exchangeRateResponse);
        }
        return exchangeRateResponses;
    }
}