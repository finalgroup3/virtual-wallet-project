using Microsoft.Extensions.Options;
using VirtualWallet.Data;

namespace VirtualWallet.Api.OptionsSetup;

public class AuthMessageSenderOptionsSetup : IConfigureOptions<AuthMessageOptions>
{
    private const string authMessageSectionName = "VirtualAuth";
    private readonly IConfiguration _configuration;

    public AuthMessageSenderOptionsSetup(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    public void Configure(AuthMessageOptions options)
    {
        _configuration.GetSection(authMessageSectionName).Bind(options);
    }
}