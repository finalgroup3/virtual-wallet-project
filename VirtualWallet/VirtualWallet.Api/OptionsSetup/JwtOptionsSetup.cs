using Microsoft.Extensions.Options;
using VirtualWallet.Data.Authentication;

namespace VirtualWallet.Api.OptionsSetup;

public class JwtOptionsSetup : IConfigureOptions<JwtOptions>
{
    private const string jwtSectionName = "Jwt";
    private readonly IConfiguration _configuration;

    public JwtOptionsSetup(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    public void Configure(JwtOptions options)
    {
        _configuration.GetSection(jwtSectionName).Bind(options);
    }
}