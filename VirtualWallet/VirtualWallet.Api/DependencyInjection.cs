﻿using Microsoft.OpenApi.Models;
using VirtualWallet.Api.Filters.Authentication;
using VirtualWallet.Api.Filters.Card;
using VirtualWallet.Data.Authentication;
using VirtualWallet.Features.Common.Interfaces.Authentication;

namespace VirtualWallet.Api;

public static class DependencyInjection
{
    public static IServiceCollection AddPresentation(this IServiceCollection services)
    {
        services
            .AddOurSwagger()
            .AddControllers();

        services.AddFilters();

        services.AddRazorPages();

        services.AddScoped<IJwtProvider, JwtProvider>();

        return services;
    }

    public static IServiceCollection AddFilters(this IServiceCollection services)
    {
        services.AddTransient<RegisterRequestFilter>();
        services.AddTransient<CardCreateRequestFilter>();

        return services;
    }

    public static IServiceCollection AddOurSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(c =>
   {
       c.SwaggerDoc("v1", new OpenApiInfo { Title = "VirtualWallet Api", Version = "v1.0.5" });

       var securityScheme = new OpenApiSecurityScheme
       {
           Name = "Authorization",
           Description = "Enter your JWT token",
           Type = SecuritySchemeType.Http,
           Scheme = "bearer",
           BearerFormat = "JWT",
           In = ParameterLocation.Header,
           Reference = new OpenApiReference
           {
               Type = ReferenceType.SecurityScheme,
               Id = "Bearer"
           }
       };

       c.AddSecurityDefinition("Bearer", securityScheme);

       c.AddSecurityRequirement(new OpenApiSecurityRequirement
       {
            {
                new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                    }
                },
                new string[] {}
            }
       });
   });
        return services;
    }

}