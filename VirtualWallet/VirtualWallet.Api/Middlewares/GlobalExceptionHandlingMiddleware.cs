using Microsoft.AspNetCore.Mvc;
using System.Net;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.ExceptionHandlers;

namespace VirtualWallet.Api.Middlewares;

public class GlobalExceptionHandlingMiddleware : IMiddleware
{
    private readonly ILogger<GlobalExceptionHandlingMiddleware> _logger;
    private readonly ExceptionHandlerFactory _exceptionHandlerFactory;

    public GlobalExceptionHandlingMiddleware(ILogger<GlobalExceptionHandlingMiddleware> logger, ExceptionHandlerFactory exceptionHandlerFactory)
    {
        _logger = logger;
        _exceptionHandlerFactory = exceptionHandlerFactory;
    }

    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        try
        {
            await next(context);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, ex.Message);

            var handler = _exceptionHandlerFactory.GetHandler(ex);
            handler?.HandleAsync(context, ex);
        }
    }

    private async Task InvalidRequestExceptionHandler(HttpContext context, InvalidRequestException e)
    {
        _logger.LogError(e, e.Message);

        if (!context.Response.HasStarted)
        {
            context.Response.StatusCode =
                StatusCodes.Status400BadRequest;

            var problemDetails = new ProblemDetails()
            {
                Status = context.Response.StatusCode,
                Type = "https://httpwg.org/specs/rfc9110.html#status.400",
                Title = "Bad Request",
                Detail = e.Message
            };

            problemDetails.Extensions.Add("error_code", object.Equals(e.ErrorCode, null) ? "bad_request" : e.ErrorCode);
            problemDetails.Extensions.Add("traceId", context.TraceIdentifier);
            problemDetails.Extensions["suggested_actions"] = new[] {
                new ErrorDetails
                {
                    Action = "Check your input",
                    Description = "Check if the operation you are trying to perform is correct"
                },

                new ErrorDetails
                {
                    Action = "Contact Support",
                    Description = "If you believe this is an error, please contact our support team for further assistance."
                },
                new ErrorDetails{
                    Action = "Check your URL",
                    Description = "Make sure the URL you provided is correct and contains no typos."
                },
                new ErrorDetails
                {
                    Action = "Explore the API documentation",
                    Description = "Refer to the API documentation to find the correct endpoint for the resource you're trying to access."
                }
            };

            context.Response.ContentType = "application/problem+json";
            await context.Response.WriteAsJsonAsync(problemDetails);
        }
    }

    private async Task UnverifiedAccountExceptionHandler(HttpContext context, UnverifiedAccountException e)
    {
        _logger.LogError(e, e.Message);

        if (!context.Response.HasStarted)
        {
            context.Response.StatusCode =
                StatusCodes.Status401Unauthorized;

            var problemDetails = new ProblemDetails()
            {
                Status = context.Response.StatusCode,
                Type = "https://datatracker.ietf.org/doc/html/rfc7235#section-3.1",
                Title = "Unverified",
                Detail = e.Message
            };

            problemDetails.Extensions.Add("error_code", object.Equals(e.ErrorCode, null) ? "unverified" : e.ErrorCode);
            problemDetails.Extensions.Add("traceId", context.TraceIdentifier);
            problemDetails.Extensions["suggested_actions"] = new[] {
                new ErrorDetails
                {
                    Action = "Check your input",
                    Description = "Check if the operation you are trying to perform is correct"
                },
                new ErrorDetails
                {
                    Action = "Check your e-mail",
                    Description = "If you have not received the verification e-mail yet check your Junk folder just in case."
                },
                new ErrorDetails
                {
                    Action = "Contact Support",
                    Description = "If you believe this is an error, please contact our support team for further assistance."
                },
                new ErrorDetails{
                    Action = "Check your URL",
                    Description = "Make sure the URL you provided is correct and contains no typos."
                },
                new ErrorDetails
                {
                    Action = "Explore the API documentation",
                    Description = "Refer to the API documentation to find the correct endpoint for the resource you're trying to access."
                }
            };

            context.Response.ContentType = "application/problem+json";
            await context.Response.WriteAsJsonAsync(problemDetails);
        }
    }

    private async Task UnverifiedRecipientExceptionHandler(HttpContext context, UnverifiedRecipinetException e)
    {
        _logger.LogError(e, e.Message);

        if (!context.Response.HasStarted)
        {
            context.Response.StatusCode =
                StatusCodes.Status401Unauthorized;

            var problemDetails = new ProblemDetails()
            {
                Status = context.Response.StatusCode,
                Type = "https://datatracker.ietf.org/doc/html/rfc7235#section-3.1",
                Title = "Unverified",
                Detail = e.Message
            };

            problemDetails.Extensions.Add("error_code", object.Equals(e.ErrorCode, null) ? "unverified" : e.ErrorCode);
            problemDetails.Extensions.Add("traceId", context.TraceIdentifier);
            problemDetails.Extensions["suggested_actions"] = new[] {
                new ErrorDetails
                {
                    Action = "Check your input",
                    Description = "Check if the operation you are trying to perform is correct"
                },
                new ErrorDetails
                {
                    Action = "Contact the recepient",
                    Description = "If you are in contact with the transfer recepient please let him know about the problem."
                },
                new ErrorDetails
                {
                    Action = "Contact Support",
                    Description = "If you believe this is an error, please contact our support team for further assistance."
                },
                new ErrorDetails{
                    Action = "Check your URL",
                    Description = "Make sure the URL you provided is correct and contains no typos."
                },
                new ErrorDetails
                {
                    Action = "Explore the API documentation",
                    Description = "Refer to the API documentation to find the correct endpoint for the resource you're trying to access."
                }
            };

            context.Response.ContentType = "application/problem+json";
            await context.Response.WriteAsJsonAsync(problemDetails);
        }
    }

    private async Task TransferNotFoundExceptionHandler(HttpContext context, TransferNotFoundException e)
    {
        _logger.LogError(e, e.Message);

        if (!context.Response.HasStarted)
        {
            context.Response.StatusCode =
                StatusCodes.Status404NotFound;

            var problemDetails = new ProblemDetails()
            {
                Status = context.Response.StatusCode,
                Type = "https://tools.ietf.org/html/rfc7231#section-6.5.4",
                Title = "Not_Found",
                Detail = e.Message
            };

            problemDetails.Extensions.Add("error_code", object.Equals(e.ErrorCode, null) ? "not_found" : e.ErrorCode);
            problemDetails.Extensions.Add("traceId", context.TraceIdentifier);
            problemDetails.Extensions["suggested_actions"] = new[] {

                new ErrorDetails
                {
                    Action = "Check your input",
                    Description = "Check if the operation you are trying to perform is correct"
                },
                new ErrorDetails
                {
                    Action = "Adjust your search parameters",
                    Description = "Your search parameters might be too narrow. Try broadening the filter constraints."
                },
                new ErrorDetails
                {
                    Action = "Contact Support",
                    Description = "If you believe this is an error, please contact our support team for further assistance."
                },
                new ErrorDetails{
                    Action = "Check your URL",
                    Description = "Make sure the URL you provided is correct and contains no typos."
                },
                new ErrorDetails
                {
                    Action = "Explore the API documentation",
                    Description = "Refer to the API documentation to find the correct endpoint for the resource you're trying to access."
                }
            };

            context.Response.ContentType = "application/problem+json";
            await context.Response.WriteAsJsonAsync(problemDetails);
        }
    }
}