﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using VirtualWallet.Data.Data;
using VirtualWallet.Data.Data.Persistence.Repositories;
using VirtualWallet.Features.Common.Interfaces.Persistence;

namespace VirtualWallet.Data;

public static class DependencyInjection
{
    public static IServiceCollection AddData(this IServiceCollection services)
    {
        services.AddPersistence();

        return services;
    }

    public static IServiceCollection AddPersistence(this IServiceCollection services)
    {
        services.AddDbContext<VirtualWalletDbContext>(options =>
        {
            var connectionString = @"Server=.;Database=VirtualWallet;Trusted_Connection=True;TrustServerCertificate=True";
            options.UseSqlServer(connectionString);
        });

        services.AddTransient<ICardRepository, CardRepository>();
        services.AddTransient<IUserRepository, UserRepository>();
        services.AddTransient<ITransferRepository, TransferRepository>();
        services.AddTransient<IWalletRepository, WalletRepository>();

        return services;
    }


}
