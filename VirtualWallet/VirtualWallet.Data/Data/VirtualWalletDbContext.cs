﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Net;
using System.Numerics;
using System.Security.Claims;
using System;
using VirtualWallet.Domain.CardEntity;
using VirtualWallet.Domain.TransferEntity;
using VirtualWallet.Domain.UserEntity;
using static System.Collections.Specialized.BitVector32;
using VirtualWallet.Domain.EntityTypes;

namespace VirtualWallet.Data.Data;

public class VirtualWalletDbContext : DbContext
{

    public DbSet<BankCard> BankCards { get; set; } = null!;
    public DbSet<Transfer> Transfers { get; set; } = null!;
    public DbSet<User> Users { get; set; } = null!;
    public DbSet<Wallet> Wallets { get; set; } = null!;
    public VirtualWalletDbContext(DbContextOptions<VirtualWalletDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(VirtualWalletDbContext).Assembly);
        base.OnModelCreating(modelBuilder);

        string encodedPassword = "$2a$11$pEstQGRsHmjdZMuCpGTS0OkigNJLQ1MKa0mWjUyXFB7SrvRE9v.Wy"; // 123 in plain text
        int userId = 1;
        int walletId = 1;        

        var users = new List<User>();
        var admin = User.CreateUser("Admin", encodedPassword, "admin@com", "0888123456");
        admin.SetId(userId++);
        admin.PromoteToAdmin();
        admin.Verify();
        admin.SetPhotoUrl("https://i.kym-cdn.com/photos/images/original/002/029/752/c73.jpg");
        users.Add(admin);

        var usernames = new string[]
        {
            "Red",
            "Widow",
            "Turbo",
            "Cuddlebug",
            "Thrill",
            "AsteroidZ",
            "An1a",
            "Bluelight",
            "Loaded",
            "Rant",
            "Diva",
            "Yukki",
            "Werice",
            "Perfect",
            "MissK",
            "Bubbles",
            "ChEk",
            "Done",
            "HugsForDrubs",
            "Pippen",
            "Aisha",
            "Protectshell",
            "Geez",
            "Monkey_see",
            "Tight",
            "Cutefest",
            "Prong",
            "Strongarmed",
            "Sovenance",
            "Angel",
            "Mad",
            "Dimple",
            "Dollspell",
            "State",
            "Vellojoy",
            "Eagle",
            "Brutal",
            "Solar",
            "Intella",
            "Rainblow",
            "Venereology",
            "Dogbone",
            "Grep",
            "Hawkeye",
            "Glue",
            "Princess",
            "L4Legit",
            "Peapod",
            "Networked",
            "Prep",
            "Sharken",
            "Curio",
            "Ender",
            "Icelolly",
            "Muffinhead",
            "Zyber",
            "Moonchild",
            "Precision",
            "Hydrospace",
            "Talon",
            "BattlerBeauty",
            "Delta",
            "Lens",
            "Spit",
            "Zoom",
            "Gothichic",
            "Purple",
            "Ahem",
            "Swag",
            "Cuteness",
            "SkillDevil",
            "Island",
            "Gran",
            "Cosmosaegis",
            "Dove",
            "Crybaby",
            "Cyberproof",
            "Dedicated",
            "Epic",
            "Secure",
            "Doppler",
            "Buddy",
            "FibBage",
            "Strawboy",
            "Piggy",
            "Homes",
            "JellyBott_om",
            "Marill",
            "Guaranteed",
            "Hardcover",
            "Warmblush",
            "Croncity",
            "Chub",
            "Paradise",
            "Complete",
            "Moonlight",
            "Unicorn",
            "Limonad",
            "Peak",
            "Sly",
            "Mercy",
            "Cutie",
            "Cyberrock",
            "Safenet",
            "Shine",
            "Night",
            "Task",
            "Septenary",
            "Queenberry",
            "DrawFul",
            "Proeyeroller",
            "Bad",
            "Grifen",
            "Secret",
            "Elite",
            "Guardsmark",
            "Pinkness",
            "Dark",
            "Blu",
            "Crashion",
            "Star",
            "Pink",
            "Kitten",
            "Xentrix",
            "Hug",
            "Pioneer",
            "Pro",
            "Alert",
            "Fittofuel"
        };
        var emails = new string[] { "@yahoo", "@google", "@abv", "@apple" };
        var domains = new string[] { ".com", ".net", ".eu", ".ru", ".bg"}; 
        var photos = new string[] 
        {
            "https://img.freepik.com/premium-photo/robots-robot-businessman-futuristic-interpretation-future-2025-illustration-my-collection_615910-5787.jpg",
            "https://img.freepik.com/free-photo/cyberpunk-man-warrior-portrait_23-2150712610.jpg",
            "https://img.freepik.com/free-icon/swat_318-210114.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/laptop_318-570955.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/cyberpunk-femme-fatale-with-neon-accents_928211-12934.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/hacker-operating-laptop-cartoon-vector-icon-illustration-technology-icon-concept-isolated_483187-381.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/anime-graphic-character-wearing-glasses-with-dark-hair-style-cyberpunk-vector-illustration-generated-ai_646737-1545.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/field-view_318-527134.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/cute-robot-holding-clipboard-cartoon-vector-icon-illustration-science-technology-icon-isolated_138676-5184.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/cute-cat-hacker-operating-laptop-cartoon-vector-icon-illustration-animal-technology-icon-isolated_138676-6824.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/young-anonymous-hacker-with-flat-design_23-2147895161.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/head-gamers-sport-logo-template_90549-136.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/hacker_318-701207.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/facial-recognition_318-915086.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/criminal_318-447553.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/referee_318-867154.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/facial-recognition_318-915087.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/facial-recognition_318-915116.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/woman_318-179001.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/robot_318-570451.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/superhero_318-677770.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/cyberpunk-woman-schematic-project-engineering-style_739548-276.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/artificial-intelligence-logo-minimalist-3d-ai-simple-white-background-ultra-high-quality_950002-49418.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/target_318-569409.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/computer-geek_818771-1146.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/clown_318-575400.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/hacker_318-660263.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/referee_318-887223.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/robot_318-301757.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/hair-salon_318-359143.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/close-up-person-with-colorful-face-black-background-generative-ai_902846-29918.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/robot_318-197202.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/asian-girl-with-neon-colors-cyberpunk-cybernetics-print-tshirts_975568-13.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/target_318-921588.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/robot-face-concept-illustration_114360-8237.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-photo/scene-professional-esports-gamer-profile-colored-with-red-blue-light-generative-ai_191095-1976.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/cute-shiba-inu-dog-robot-cartoon-vector-icon-illustration-animal-technology-icon-concept-isolated_138676-6620.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/young-girl-hood-sits-front-laptop-generative-ai_438099-11284.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/hacker_318-917225.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/robot_318-570496.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/robot_318-570460.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/woman_318-728560.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/there-is-digital-painting-boy-smiling-front-futuristic-background-generative-ai_900814-9252.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/man-with-shirt-that-says-he-is-man_410516-87609.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/rock-man_318-872059.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/burglar_318-699954.jpg",
            "https://img.freepik.com/free-icon/skier_318-887208.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/vector-illustration-about-art-people_975572-16384.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/driver_318-444150.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/digital-art-selected_662214-8860.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/wide-angle-photo-blonde-cyberpunk-girl-with-no-glowing-blue-eyes-ina-neon-city_812426-10196.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/cute-cat-hacker-operating-laptop-cartoon-vector-icon-illustration-animal-technology-icon-isolated_138676-6824.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/modern-people-avatar-casual-clothes-vector-cartoon-illustration-man-with-individual-face-hair-light-digital-frame-dark-blue-computer-picture-web-profile_107791-4257.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/modern-people-avatar-casual-clothes-vector-cartoon-illustration-man-with-individual-face-hair-light-digital-frame-dark-blue-computer-picture-web-profile_107791-4258.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/person-holding-smartphone-enjoying-social-media_931022-4873.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/there-is-red-flower-white-background-with-grunge-effect-generative-ai_958192-24840.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/modern-people-avatar-casual-clothes-vector-cartoon-illustration-woman-with-individual-face-hair-light-digital-frame-dark-blue-computer-picture-web-profile_107791-4256.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/ar-instagram-social-media-filter_23-2148402229.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/cartoon-teenager-addicted-internet-ai-generative_407474-9223.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/hand-drawn-people-talking-phone-illustration_23-2149826988.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/cartoon-girl-with-glasses-colorful-shirt-with-word-i-m_410516-83097.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/synthwave-cyberpunk-gamer-girl-logo-logotype-branding-girl-gaming-channel_120585-197.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/3d-center-icon-bubble-talk-white-background_978536-356.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/hacker-activity-illustrated_23-2148539280.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/portrait-handsome-young-man-black-leather-jacket-studio-shot_743855-27238.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/steal-data-concept_23-2148533147.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/futuristic-background-computer-gamer_23-2148219921.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/human-knowledge_23-2147504574.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/close-up-person-wearing-headphones-jacket-generative-ai_902049-30898.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/abstract-head-geometry-with-triangles-style-dark-black-magenta_921860-17671.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/there-is-cartoon-man-with-tie-suit-generative-ai_902846-30486.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/thief_318-856850.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/hacker-activity-concept_23-2148543435.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-psd/metaverse-technology-future-concept-vr-virtual-digital-reality-cyber-metaverse-simulation-innovation-connection-global-blockchain-experiences-network-futuristic-3d-rendering_596528-31.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/user-verification-unauthorized-access-prevention-private-account-authentication-cyber-security-people-entering-login-password-safety-measures_335657-8.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/colorful-instagram-filter-with-woman_52683-32105.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/robot-face-concept-illustration_114360-8207.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/colorful-illustration-successful-female-scientist_23-2148398922.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/gamer-cyborg-4-mascot-full-color-esport-logo-design_177315-624.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/witch_318-856810.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/target_318-666015.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/hacker-activity-illustrated-concept_23-2148539281.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/ai-cartoon-colorful_940072-87.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/head-cyborg-gamer-esport-logo_177315-336.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/planets-galaxy-mind-flat-line-concept-vector-spot-illustration-surreal-face-man-2d-cartoon-outline-character-white-web-ui-design-space-surrealism-male-editable-isolated-color-hero-image_151150-16402.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/modern-people-avatar-casual-clothes-vector-cartoon-illustration-man-with-individual-face-hair-light-digital-frame-dark-blue-computer-picture-web-profile_107791-4258.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/free-photo-man-portrait-with-blue-lights-visual-effects_927961-516.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/hacker-activity-concept_23-2148549186.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-photo/young-adult-portrait-glows-with-futuristic-technology-generated-by-ai_188544-36501.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/sport-background-aesthetic-desktop-wallpaper-8k-photography-background_882954-14592.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/hand-drawn-nft-style-ape-illustration_23-2149611054.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/samurai-cyborg-man-with-gun-cyberpunk-style-cartoon-character-illustration_132871-200.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-photo/woman-with-long-hair-yellow-hoodie-with-word-music-it_1340-39068.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/barbiecore-embracing-vibrant-glamour-playful-fashion-every-click-with-barbie-trens_978786-571.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/artificial-intelligence-cyborg-head-high-tech-future-design-lines-dots-blue-background_961925-405.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/cyberpunk-man-wallpaper_759095-32918.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/gamer-twitch-profile-picture_742173-3352.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/android-girl-icon-space-slot-game_175250-852.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/purple-poster-with-robot-head-word-ro_670382-82353.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/hand-drawn-flat-design-metaverse-illustration_23-2149259637.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/cute-spaceman-astronaut-wearing-jacket-cartoon-vector-icon-illustration-people-science-icon-isolated_138676-4823.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/cyberpunk-neon-background_550653-4402.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/male-portrait_1196-547.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/free-cyberpunk-neon-men-background_550653-4302.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/colorful-minimal-3d-uhd-4k-abstract-background_669273-1512.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-photo/rise-humanoids-with-advanced-headgear-generative-ai_8829-2877.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/realistic-halloween-background_52683-45459.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-photo/scene-professional-esports-gamer-profile-colored-with-red-blue-light-generative-ai_191095-1972.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-photo/tiger-with-cyberpunk-design-illustration_826849-604.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-photo/cartoon-character-with-handbag-sunglasses_71767-99.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/realistic-portrait-scifi-cyberpunk-men-cyber-suit-hightech-futuristic-man_861875-7883.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/cute-ninja-working-laptop-cartoon-vector-icon-illustration-people-technology-icon-concept-flat-cartoon-style_138676-2594.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/man-futuristic-glasses-with-word-cyberpunk-front_7023-24954.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/gradient-tech-metaverse-illustration_52683-79770.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/futuristic-cyber-tech-ai-minimalist-symbol_893012-26657.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/inspiring-linear-minimalist-modern-monochromatic-multimedia-muted-naturalistic-portrait_730620-9313.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/cute-indonesian-ghost-tuyul-vector-illustration_547110-122.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/cyberpunk-man-wallpaper_759095-32794.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/modern-line-icon-cute-character-smiling-bold-lines-solid-color-pixel-perfect-isolate-minimalistic_68067-5360.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/robot_318-570488.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/beautiful-man-against-background-lights-night-city-ai_101266-27267.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/man-with-glasses-shirt-that-says-word-it_410516-87463.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/hacker-mascot-sports-gaming-logo_382438-520.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/hacker_318-640438.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/man-black-wet-suit-is-standing-front-control-panel_900298-325.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/man-illustration_23-2147514469.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-icon/phone-call_318-376689.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/man-with-robot-face-that-has-yellow-eyes_81048-4578.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-photo/cyberpunk-woman-schematic-project-engineering-style_739548-261.jpg?size=626&ext=jpg",
            "https://img.freepik.com/free-vector/emotion-detection-abstract-concept-illustration-speech-emotional-state-recognition-emotion-detection-from-text-sensor-technology-machine-learning-ai-reading-face_335657-268.jpg?size=626&ext=jpg",
            "https://img.freepik.com/premium-vector/cyberpunk-character-cyborg-premium-vector_257245-513.jpg?size=626&ext=jpg"

        };
        var phoneNumberStart = "088";
        var random = new Random();

        foreach (var username in usernames)
        {
            int emailsIndex = random.Next(0, emails.Length);
            int domainsIndex = random.Next(0, domains.Length);
            string email = username + emails[emailsIndex] + domains[domainsIndex];
            string phoneNumber = phoneNumberStart + random.Next(3000000, 9999999);
            var user = User.CreateUser(
                username,
                encodedPassword,
                email,
                phoneNumber
                );
            string photo = photos[userId - 2];
            user.SetPhotoUrl(photo);
            user.SetId(userId++);           
            user.Verify();
           
            users.Add(user);
        }
        modelBuilder.Entity<User>().HasData(users);

        var wallets = new List<Wallet>();

        foreach (var user in users)
        {
            var wallet = Wallet.CreateWallet(
                user.Id,
                random.Next(10, 999) * 100,
                ((CurrencyType)random.Next(1, 16)).ToString());
            wallet.SetId(walletId++);
            wallets.Add(wallet);
        };
        modelBuilder.Entity<Wallet>().HasData(wallets);


        var transfers = new List<Transfer>();   
        for (int i = 1; i < users.Count*15; i++)
        {
            var senderId = random.Next(1, users.Count);
            var recepientId = random.Next(1, users.Count);
            while (senderId == recepientId)
            {
                recepientId = random.Next(users.Count);
            }

            decimal amount = random.Next(1, 99)*100;
            decimal totalTaken = amount * (decimal)1.005;
            decimal fees = totalTaken - amount;

            string currency = ((CurrencyType)random.Next(1, 16)).ToString();
            var transfer = Transfer.CreateTransfer(
                senderId,
                recepientId,
                amount,
                currency,
                amount,
                currency,
                totalTaken,
                fees
                );
            transfer.SetId(i);
            var minutes = random.Next(1, 131400);
            var date = DateTime.UtcNow.AddMinutes(-minutes);
            transfer.SetTransferDate(date);
            transfers.Add(transfer);
        }
        modelBuilder.Entity<Transfer>().HasData(transfers);



    }
}