namespace VirtualWallet.Data.Data.Configurations;
using VirtualWallet.Domain.UserEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class WalletConfigurations : IEntityTypeConfiguration<Wallet>
{
    public void Configure(EntityTypeBuilder<Wallet> builder)
    {
        ConfigureWalletTable(builder);
    }

    private static void ConfigureWalletTable(EntityTypeBuilder<Wallet> builder)
    {
        builder.ToTable("Wallets");

        builder.HasKey(w => w.Id);

        builder.Property(w => w.Id)
            .ValueGeneratedOnAdd();

        builder.Property(w => w.Balance)
        .HasColumnType("decimal(18,2)")
            .IsRequired();

        builder.Property(w => w.Currency)
            .IsRequired();

        builder.Property(w => w.UserId)
            .IsRequired();
    }
}