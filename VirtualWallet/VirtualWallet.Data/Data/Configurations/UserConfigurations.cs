﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using VirtualWallet.Domain.UserEntity;

namespace VirtualWallet.Data.Data.Configurations;

public class UserConfigurations : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        ConfigureUserTable(builder);
        ConfigureUserReceivedTransfersTable(builder);
        ConfigureUserSentTransfersTable(builder);
        ConfigureUserBankCardsTable(builder);
    }
    private static void ConfigureUserSentTransfersTable(EntityTypeBuilder<User> builder)
    {
        builder.HasMany(c => c.TransfersSent)
            .WithOne(t => t.Sender)
            .HasForeignKey(t => t.SenderId)
            .OnDelete(DeleteBehavior.NoAction);

        builder.Navigation(c => c.TransfersSent)
            .UsePropertyAccessMode(PropertyAccessMode.Field);
    }

    private static void ConfigureUserReceivedTransfersTable(EntityTypeBuilder<User> builder)
    {
        builder.HasMany(c => c.TransfersReceived)
            .WithOne(t => t.Recipient)
            .HasForeignKey(t => t.RecipientId)
            .OnDelete(DeleteBehavior.NoAction);

        builder.Navigation(c => c.TransfersReceived)
            .UsePropertyAccessMode(PropertyAccessMode.Field);
    }

    private static void ConfigureUserBankCardsTable(EntityTypeBuilder<User> builder)
    {
        builder.HasMany(u => u.BankCards)
        .WithOne(c => c.User)
        .OnDelete(DeleteBehavior.NoAction)
        .HasForeignKey(c => c.UserId);

        builder.Navigation(u => u.BankCards)
        .UsePropertyAccessMode(PropertyAccessMode.Field);
    }

    private static void ConfigureUserTable(EntityTypeBuilder<User> builder)
    {
        builder.ToTable("Users");

        builder.HasKey(u => u.Id);

        builder.Property(u => u.Id)
        .ValueGeneratedOnAdd();

        builder.Property(u => u.Username)
        .IsRequired()
        .HasMaxLength(20);

        builder.HasIndex(u => u.Username)
        .IsUnique();

        builder.Property(u => u.Email)
        .IsRequired();

        builder.HasIndex(u => u.Email)
        .IsUnique();

        builder.Property(u => u.Password)
        .IsRequired();

        builder.Property(u => u.PhoneNumber)
        .IsRequired()
        .HasMaxLength(10);

        builder.HasIndex(u => u.PhoneNumber)
        .IsUnique();

        builder.HasOne(u => u.Wallet)
        .WithOne(w => w.User)
        .HasForeignKey<Wallet>(w => w.UserId)
        .OnDelete(DeleteBehavior.NoAction);

        builder.Property(u => u.IsAdmin);

        builder.Property(u => u.IsBlocked);

        builder.Property(u => u.IsDeleted);

        builder.Property(u => u.IsVerified);

        builder.Property(u => u.VerificationToken);

        builder.Property(u => u.PhotoUrl)
        .IsRequired(false);

        builder.Property(u => u.ResetPasswordToken)
            .IsRequired(false);
    }
}