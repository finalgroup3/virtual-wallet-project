using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VirtualWallet.Domain.TransferEntity;

namespace VirtualWallet.Data.Data.Configurations;

public class TransferConfigurations : IEntityTypeConfiguration<Transfer>
{
    public void Configure(EntityTypeBuilder<Transfer> builder)
    {
        ConfigureTransferTable(builder);
    }

    private static void ConfigureTransferTable(EntityTypeBuilder<Transfer> builder)
    {
        builder.ToTable("Transfers");

        builder.HasKey(t => t.Id);

        builder.Property(t => t.Id)
            .ValueGeneratedOnAdd();

        builder.Property(t => t.TargetAmount)
        .HasColumnType("decimal(18,2)")
        .IsRequired();

        builder.Property(t => t.BaseAmount)
        .HasColumnType("decimal(18,2)")
        .IsRequired();

        builder.Property(t => t.TotalTaken)
        .HasColumnType("decimal(18,2)")
        .IsRequired();

        builder.Property(t => t.BaseCurrency)
            .HasMaxLength(3)
            .IsRequired();

        builder.Property(t => t.TargetCurrency)
            .HasMaxLength(3)
            .IsRequired();

        builder.Property(t => t.SenderId)
            .IsRequired();

        builder.Property(t => t.RecipientId)
            .IsRequired();

        builder.Property(t => t.TransferDate)
            .IsRequired();

        builder.Property(t => t.Fees)
            .HasColumnType("decimal(18,2)");
    }
}