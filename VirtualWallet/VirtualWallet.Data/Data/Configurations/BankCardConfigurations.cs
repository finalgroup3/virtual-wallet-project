﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VirtualWallet.Domain.CardEntity;

namespace VirtualWallet.Data.Data.Configurations;

public class BankCardConfigurations : IEntityTypeConfiguration<BankCard>
{
    public void Configure(EntityTypeBuilder<BankCard> builder)
    {
        ConfigureBankCardTable(builder);
    }

    private static void ConfigureBankCardTable(EntityTypeBuilder<BankCard> builder)
    {
        builder.ToTable("BankCards");

        builder.HasKey(c => c.Id);

        builder.Property(c => c.Id)
            .ValueGeneratedOnAdd();

        builder.Property(c => c.Number)
            .IsRequired()
            .HasMaxLength(16);

        builder.HasIndex(c => c.Number)
            .IsUnique();

        builder.Property(c => c.SecurityCode)
            .IsRequired()
            .HasMaxLength(3);

        builder.Property(c => c.ExpirationDate)
            .IsRequired();

        builder.Property(c => c.UserId);

        builder.HasOne(c => c.User)
            .WithMany(u => u.BankCards)
            .HasForeignKey(c => c.UserId);
    }
}
