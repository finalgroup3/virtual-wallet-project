﻿using Microsoft.EntityFrameworkCore;
using VirtualWallet.Domain.TransferEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;

namespace VirtualWallet.Data.Data.Persistence.Repositories;

public class TransferRepository : ITransferRepository
{
    private readonly VirtualWalletDbContext _dbContext;
    public TransferRepository(VirtualWalletDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    public async Task CreateTransfer(Transfer transfer)
    {
        await _dbContext.AddAsync(transfer);
        await _dbContext.SaveChangesAsync();
    }

	public async Task<IQueryable<Transfer>> GetTransfersQuery()
	{
        await Task.CompletedTask;

        return  _dbContext.Transfers;
    }

    public async Task SaveChanges()
    {
        await _dbContext.SaveChangesAsync();
    }

    public async Task<Transfer> GetTransferById(int id)
    {
        return await _dbContext.Transfers
            .Include(transfer => transfer.Recipient)
            .Include(transfer => transfer.Sender)
            .FirstOrDefaultAsync(transfer => transfer.Id == id);
    }
}
