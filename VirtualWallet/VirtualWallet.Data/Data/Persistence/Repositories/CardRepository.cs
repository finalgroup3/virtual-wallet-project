﻿using Microsoft.EntityFrameworkCore;
using VirtualWallet.Domain.CardEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;

namespace VirtualWallet.Data.Data.Persistence.Repositories;

public class CardRepository : ICardRepository
{
    private readonly VirtualWalletDbContext _dbContext;

    public CardRepository(VirtualWalletDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    public async Task CreateCard(BankCard card)
    {
		
		await _dbContext.AddAsync(card);
		await _dbContext.SaveChangesAsync();
	}

	public async Task<List<BankCard>> GetAllCardsOfUser(int userId)
	{
        return await _dbContext.BankCards.Include(card => card.User).Where(card => card.UserId == userId && !card.IsDeleted).ToListAsync()!;
	}

	public async Task<BankCard> GetCardById(int id)
	{
		return await _dbContext.BankCards.Include(card => card.User).FirstOrDefaultAsync(card => card.Id == id && !card.IsDeleted)!;
	}

	public async Task<BankCard> GetCardByNumber(string number)
    {
		return await _dbContext.BankCards.Include(card => card.User).FirstOrDefaultAsync(card => card.Number == number && !card.IsDeleted)!;
	}

    public async Task<bool> IsNumberUnique(string number)
    {
        return !await _dbContext.BankCards.AnyAsync(card => card.Number == number);
    }

    public async Task SaveChanges()
    {
       await _dbContext.SaveChangesAsync();
    }

}