using Microsoft.EntityFrameworkCore;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;

namespace VirtualWallet.Data.Data.Persistence.Repositories;

public class WalletRepository : IWalletRepository
{
    private readonly VirtualWalletDbContext _dbContext;
    public WalletRepository(VirtualWalletDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task AddAsync(Wallet wallet)
    {
        _dbContext.Add(wallet);
        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateAsync(Wallet wallet)
    {
        _dbContext.Update(wallet);
        await _dbContext.SaveChangesAsync();
    }
    public async Task<Wallet?> GetWalletById(int walletId)
    {
        return await _dbContext.Wallets.FirstOrDefaultAsync(w => w.Id == walletId);
    }

	public async Task<Wallet?> GetWalletByUserId(int userId)
	{
		return await _dbContext.Wallets.FirstOrDefaultAsync(w => w.UserId == userId);
	}

	public async Task SaveChangesAsync()
    {
        await _dbContext.SaveChangesAsync();
    }
}