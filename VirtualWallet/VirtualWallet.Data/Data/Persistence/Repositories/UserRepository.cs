﻿using Microsoft.EntityFrameworkCore;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;

namespace VirtualWallet.Data.Data.Persistence.Repositories;

public class UserRepository : IUserRepository
{
    private readonly VirtualWalletDbContext _dbContext;

    public UserRepository(VirtualWalletDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task AddAsync(User user)
    {
        await _dbContext.AddAsync(user);
        await _dbContext.SaveChangesAsync();
    }
#pragma warning disable CS8603 // Possible null reference return. 
    public async Task<User> GetUserById(int id)
    {
        return await _dbContext.Users
            .Include(u => u.TransfersSent)
            .ThenInclude(t => t.Recipient)
            .Include(u => u.TransfersReceived)
            .ThenInclude(t => t.Sender)
            .Include(u => u.Wallet)
            .Include(user => user.BankCards)
            .FirstOrDefaultAsync(user => user.Id == id && !user.IsDeleted);
    }
    public async Task<User> GetUserByUsername(string username)
    {
        return await _dbContext.Users
            .Include(u => u.TransfersSent)
            .ThenInclude(t => t.Recipient)
            .Include(u => u.TransfersReceived)
            .ThenInclude(t => t.Sender)
            .Include(u => u.Wallet)
            .Include(user => user.BankCards)
            .FirstOrDefaultAsync(user => user.Username == username && !user.IsDeleted);
    }

    public async Task<IEnumerable<User>> GetAllUsers(CancellationToken token)
    {
        return await _dbContext.Users
            .Include(u => u.TransfersSent)
            .ThenInclude(t => t.Recipient)
            .Include(u => u.TransfersReceived)
            .ThenInclude(t => t.Sender)
            .Include(u => u.Wallet)
            .Include(user => user.BankCards)
            .Where(u => u.IsVerified && !u.IsDeleted)
            .ToListAsync(token);
    }

    public async Task<User> GetUserByToken(Guid token, CancellationToken cancellationToken)
    {
        return await _dbContext.Users.FirstOrDefaultAsync(user => user.VerificationToken == token && !user.IsDeleted, cancellationToken)!;
    }

    public async Task<User> GetUserByEmail(string email)
    {
        return await _dbContext.Users.FirstOrDefaultAsync(user => user.Email == email && !user.IsDeleted)!;
    }

    public async Task Update(User user)
    {
        _dbContext.Update(user);
        await _dbContext.SaveChangesAsync();
    }

    public async Task<IQueryable<User>> GetUsersQuery()
    {
        await Task.CompletedTask;

        return _dbContext.Users.Where(
            user => user.IsVerified 
            && !user.IsDeleted
            && !user.IsBlocked);
    }

    public async Task<bool> EmailExistsAsync(string email)
    {
        return await _dbContext.Users.AnyAsync(user => user.Email == email && !user.IsDeleted);
    }

    public async Task<bool> UsernameExistsAsync(string username)
    {
        return await _dbContext.Users.AnyAsync(user => user.Username == username && !user.IsDeleted);
    }

    public async Task<bool> PhoneNumberExistsAsync(string phoneNumber)
    {
        return await _dbContext.Users.AnyAsync(user => user.PhoneNumber == phoneNumber && !user.IsDeleted);
    }

    public async Task<User> GetUserByResetToken(Guid token, CancellationToken cancellationToken)
    {
        return await _dbContext.Users.FirstOrDefaultAsync(user => user.ResetPasswordToken == token && !user.IsDeleted, cancellationToken)!;
    }

    public async Task<User> GetUserByEmailToken(Guid token, CancellationToken cancellationToken)
    {
        return await _dbContext.Users.FirstOrDefaultAsync(user => user.ChangeEmailToken == token && !user.IsDeleted, cancellationToken)!;
    }

    public async Task SaveChanges()
    {
        await _dbContext.SaveChangesAsync();
    }
}

