﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VirtualWallet.Data.Migrations
{
    /// <inheritdoc />
    public partial class Initial1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 100m, "SGD", 0.500m, 80, 47, 100m, "SGD", 100.500m, new DateTime(2023, 7, 27, 18, 19, 37, 462, DateTimeKind.Utc).AddTicks(6306) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6300m, "CHF", 31.500m, 28, 90, 6300m, "CHF", 6331.500m, new DateTime(2023, 6, 1, 12, 39, 37, 462, DateTimeKind.Utc).AddTicks(6315) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3800m, "USD", 19.000m, 89, 49, 3800m, "USD", 3819.000m, new DateTime(2023, 8, 16, 15, 12, 37, 462, DateTimeKind.Utc).AddTicks(6318) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1900m, "BRL", 9.500m, 4, 38, 1900m, "BRL", 1909.500m, new DateTime(2023, 5, 31, 10, 29, 37, 462, DateTimeKind.Utc).AddTicks(6321) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "INR", 30.500m, 81, 28, 6100m, "INR", 6130.500m, new DateTime(2023, 7, 2, 9, 42, 37, 462, DateTimeKind.Utc).AddTicks(6324) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "NZD", 22.000m, 63, 86, 4400m, "NZD", 4422.000m, new DateTime(2023, 6, 21, 16, 1, 37, 462, DateTimeKind.Utc).AddTicks(6328) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2200m, "NZD", 11.000m, 86, 52, 2200m, "NZD", 2211.000m, new DateTime(2023, 6, 25, 5, 44, 37, 462, DateTimeKind.Utc).AddTicks(6332) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "AUD", 15.000m, 32, 59, 3000m, "AUD", 3015.000m, new DateTime(2023, 5, 29, 18, 58, 37, 462, DateTimeKind.Utc).AddTicks(6335) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4900m, "CNY", 24.500m, 124, 86, 4900m, "CNY", 4924.500m, new DateTime(2023, 6, 29, 3, 59, 37, 462, DateTimeKind.Utc).AddTicks(6338) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3700m, "NZD", 18.500m, 48, 49, 3700m, "NZD", 3718.500m, new DateTime(2023, 7, 25, 16, 41, 37, 462, DateTimeKind.Utc).AddTicks(6342) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 700m, 3.500m, 14, 32, 700m, 703.500m, new DateTime(2023, 8, 11, 22, 33, 37, 462, DateTimeKind.Utc).AddTicks(6345) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "CHF", 41.000m, 9, 99, 8200m, "CHF", 8241.000m, new DateTime(2023, 7, 1, 16, 5, 37, 462, DateTimeKind.Utc).AddTicks(6348) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "NZD", 15.000m, 82, 3, 3000m, "NZD", 3015.000m, new DateTime(2023, 7, 14, 1, 33, 37, 462, DateTimeKind.Utc).AddTicks(6351) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1900m, "CAD", 9.500m, 36, 99, 1900m, "CAD", 1909.500m, new DateTime(2023, 7, 3, 0, 25, 37, 462, DateTimeKind.Utc).AddTicks(6354) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "GBP", 30.500m, 41, 118, 6100m, "GBP", 6130.500m, new DateTime(2023, 7, 30, 9, 57, 37, 462, DateTimeKind.Utc).AddTicks(6357) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4500m, "JPY", 22.500m, 88, 107, 4500m, "JPY", 4522.500m, new DateTime(2023, 6, 16, 5, 5, 37, 462, DateTimeKind.Utc).AddTicks(6361) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5500m, "CNY", 27.500m, 47, 102, 5500m, "CNY", 5527.500m, new DateTime(2023, 6, 15, 10, 45, 37, 462, DateTimeKind.Utc).AddTicks(6364) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6500m, "BRL", 32.500m, 72, 29, 6500m, "BRL", 6532.500m, new DateTime(2023, 6, 9, 6, 38, 37, 462, DateTimeKind.Utc).AddTicks(6369) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3700m, "USD", 18.500m, 77, 104, 3700m, "USD", 3718.500m, new DateTime(2023, 7, 6, 21, 58, 37, 462, DateTimeKind.Utc).AddTicks(6372) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "EUR", 8.500m, 104, 88, 1700m, "EUR", 1708.500m, new DateTime(2023, 6, 6, 10, 20, 37, 462, DateTimeKind.Utc).AddTicks(6375) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4800m, "INR", 24.000m, 6, 73, 4800m, "INR", 4824.000m, new DateTime(2023, 6, 23, 15, 46, 37, 462, DateTimeKind.Utc).AddTicks(6379) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "USD", 38.000m, 9, 111, 7600m, "USD", 7638.000m, new DateTime(2023, 7, 2, 2, 15, 37, 462, DateTimeKind.Utc).AddTicks(6415) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, "GBP", 21.500m, 31, 56, 4300m, "GBP", 4321.500m, new DateTime(2023, 7, 22, 3, 9, 37, 462, DateTimeKind.Utc).AddTicks(6418) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "JPY", 15.000m, 93, 45, 3000m, "JPY", 3015.000m, new DateTime(2023, 5, 23, 18, 49, 37, 462, DateTimeKind.Utc).AddTicks(6421) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "GBP", 22.000m, 91, 10, 4400m, "GBP", 4422.000m, new DateTime(2023, 8, 1, 0, 35, 37, 462, DateTimeKind.Utc).AddTicks(6424) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5900m, "CAD", 29.500m, 65, 39, 5900m, "CAD", 5929.500m, new DateTime(2023, 6, 19, 14, 51, 37, 462, DateTimeKind.Utc).AddTicks(6427) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "ZAR", 25.000m, 75, 67, 5000m, "ZAR", 5025.000m, new DateTime(2023, 7, 15, 21, 9, 37, 462, DateTimeKind.Utc).AddTicks(6462) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "SGD", 17.500m, 48, 12, 3500m, "SGD", 3517.500m, new DateTime(2023, 7, 20, 0, 52, 37, 462, DateTimeKind.Utc).AddTicks(6466) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7900m, "BGN", 39.500m, 47, 27, 7900m, "BGN", 7939.500m, new DateTime(2023, 6, 5, 16, 27, 37, 462, DateTimeKind.Utc).AddTicks(6469) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4700m, "BGN", 23.500m, 124, 30, 4700m, "BGN", 4723.500m, new DateTime(2023, 7, 27, 7, 8, 37, 462, DateTimeKind.Utc).AddTicks(6472) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7900m, "CHF", 39.500m, 112, 129, 7900m, "CHF", 7939.500m, new DateTime(2023, 7, 18, 6, 55, 37, 462, DateTimeKind.Utc).AddTicks(6476) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "SGD", 34.000m, 29, 64, 6800m, "SGD", 6834.000m, new DateTime(2023, 7, 20, 16, 21, 37, 462, DateTimeKind.Utc).AddTicks(6479) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7000m, "JPY", 35.000m, 86, 61, 7000m, "JPY", 7035.000m, new DateTime(2023, 7, 27, 1, 15, 37, 462, DateTimeKind.Utc).AddTicks(6482) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3300m, "AUD", 16.500m, 116, 95, 3300m, "AUD", 3316.500m, new DateTime(2023, 7, 3, 0, 58, 37, 462, DateTimeKind.Utc).AddTicks(6486) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6400m, "ZAR", 32.000m, 38, 6, 6400m, "ZAR", 6432.000m, new DateTime(2023, 6, 10, 12, 5, 37, 462, DateTimeKind.Utc).AddTicks(6489) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3800m, "BRL", 19.000m, 27, 48, 3800m, "BRL", 3819.000m, new DateTime(2023, 6, 29, 19, 57, 37, 462, DateTimeKind.Utc).AddTicks(6493) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2200m, "CAD", 11.000m, 97, 68, 2200m, "CAD", 2211.000m, new DateTime(2023, 8, 5, 2, 46, 37, 462, DateTimeKind.Utc).AddTicks(6497) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6000m, "CAD", 30.000m, 54, 110, 6000m, "CAD", 6030.000m, new DateTime(2023, 6, 2, 16, 59, 37, 462, DateTimeKind.Utc).AddTicks(6500) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5400m, "AUD", 27.000m, 40, 19, 5400m, "AUD", 5427.000m, new DateTime(2023, 8, 1, 3, 6, 37, 462, DateTimeKind.Utc).AddTicks(6503) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9700m, "CHF", 48.500m, 20, 129, 9700m, "CHF", 9748.500m, new DateTime(2023, 8, 3, 6, 57, 37, 462, DateTimeKind.Utc).AddTicks(6506) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1900m, "GBP", 9.500m, 18, 119, 1900m, "GBP", 1909.500m, new DateTime(2023, 7, 21, 11, 11, 37, 462, DateTimeKind.Utc).AddTicks(6509) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 500m, "BRL", 2.500m, 118, 42, 500m, "BRL", 502.500m, new DateTime(2023, 6, 8, 21, 9, 37, 462, DateTimeKind.Utc).AddTicks(6513) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "JPY", 11.500m, 46, 40, 2300m, "JPY", 2311.500m, new DateTime(2023, 5, 18, 17, 5, 37, 462, DateTimeKind.Utc).AddTicks(6516) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "SGD", 41.000m, 112, 123, 8200m, "SGD", 8241.000m, new DateTime(2023, 5, 27, 12, 44, 37, 462, DateTimeKind.Utc).AddTicks(6519) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7700m, "JPY", 38.500m, 49, 81, 7700m, "JPY", 7738.500m, new DateTime(2023, 5, 21, 3, 5, 37, 462, DateTimeKind.Utc).AddTicks(6522) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "ZAR", 17.500m, 128, 46, 3500m, "ZAR", 3517.500m, new DateTime(2023, 5, 20, 20, 43, 37, 462, DateTimeKind.Utc).AddTicks(6525) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "CHF", 17.500m, 13, 29, 3500m, "CHF", 3517.500m, new DateTime(2023, 6, 5, 6, 21, 37, 462, DateTimeKind.Utc).AddTicks(6528) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 700m, "CHF", 3.500m, 20, 40, 700m, "CHF", 703.500m, new DateTime(2023, 5, 21, 14, 9, 37, 462, DateTimeKind.Utc).AddTicks(6531) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8600m, "AUD", 43.000m, 10, 103, 8600m, "AUD", 8643.000m, new DateTime(2023, 5, 26, 21, 0, 37, 462, DateTimeKind.Utc).AddTicks(6534) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8000m, "EUR", 40.000m, 110, 122, 8000m, "EUR", 8040.000m, new DateTime(2023, 7, 8, 22, 31, 37, 462, DateTimeKind.Utc).AddTicks(6537) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9600m, "EUR", 48.000m, 73, 113, 9600m, "EUR", 9648.000m, new DateTime(2023, 7, 5, 7, 3, 37, 462, DateTimeKind.Utc).AddTicks(6540) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5900m, "NZD", 29.500m, 99, 122, 5900m, "NZD", 5929.500m, new DateTime(2023, 5, 27, 3, 57, 37, 462, DateTimeKind.Utc).AddTicks(6544) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3200m, "ZAR", 16.000m, 77, 61, 3200m, "ZAR", 3216.000m, new DateTime(2023, 7, 15, 19, 59, 37, 462, DateTimeKind.Utc).AddTicks(6547) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8600m, "CNY", 43.000m, 26, 95, 8600m, "CNY", 8643.000m, new DateTime(2023, 7, 6, 0, 52, 37, 462, DateTimeKind.Utc).AddTicks(6550) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "AUD", 44.000m, 2, 124, 8800m, "AUD", 8844.000m, new DateTime(2023, 7, 22, 16, 17, 37, 462, DateTimeKind.Utc).AddTicks(6554) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3100m, "GBP", 15.500m, 126, 25, 3100m, "GBP", 3115.500m, new DateTime(2023, 7, 4, 4, 35, 37, 462, DateTimeKind.Utc).AddTicks(6557) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2400m, "CHF", 12.000m, 37, 98, 2400m, "CHF", 2412.000m, new DateTime(2023, 6, 13, 22, 59, 37, 462, DateTimeKind.Utc).AddTicks(6560) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8900m, "INR", 44.500m, 50, 26, 8900m, "INR", 8944.500m, new DateTime(2023, 5, 20, 4, 20, 37, 462, DateTimeKind.Utc).AddTicks(6563) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8900m, "ZAR", 44.500m, 4, 119, 8900m, "ZAR", 8944.500m, new DateTime(2023, 7, 6, 7, 5, 37, 462, DateTimeKind.Utc).AddTicks(6567) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4200m, "ZAR", 21.000m, 4, 99, 4200m, "ZAR", 4221.000m, new DateTime(2023, 6, 1, 8, 17, 37, 462, DateTimeKind.Utc).AddTicks(6570) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7500m, "ZAR", 37.500m, 108, 106, 7500m, "ZAR", 7537.500m, new DateTime(2023, 8, 9, 15, 1, 37, 462, DateTimeKind.Utc).AddTicks(6573) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8600m, "BRL", 43.000m, 76, 52, 8600m, "BRL", 8643.000m, new DateTime(2023, 6, 17, 4, 21, 37, 462, DateTimeKind.Utc).AddTicks(6576) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7500m, "BGN", 37.500m, 65, 60, 7500m, "BGN", 7537.500m, new DateTime(2023, 8, 10, 22, 7, 37, 462, DateTimeKind.Utc).AddTicks(6580) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 800m, "AUD", 4.000m, 98, 76, 800m, "AUD", 804.000m, new DateTime(2023, 6, 6, 17, 40, 37, 462, DateTimeKind.Utc).AddTicks(6583) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2800m, "USD", 14.000m, 60, 32, 2800m, "USD", 2814.000m, new DateTime(2023, 6, 28, 8, 16, 37, 462, DateTimeKind.Utc).AddTicks(6586) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4600m, "SGD", 23.000m, 94, 93, 4600m, "SGD", 4623.000m, new DateTime(2023, 7, 30, 17, 48, 37, 462, DateTimeKind.Utc).AddTicks(6591) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 600m, "EUR", 3.000m, 86, 121, 600m, "EUR", 603.000m, new DateTime(2023, 7, 25, 18, 25, 37, 462, DateTimeKind.Utc).AddTicks(6594) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "CHF", 34.000m, 55, 66, 6800m, "CHF", 6834.000m, new DateTime(2023, 7, 20, 0, 40, 37, 462, DateTimeKind.Utc).AddTicks(6660) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 900m, "ZAR", 4.500m, 4, 125, 900m, "ZAR", 904.500m, new DateTime(2023, 8, 6, 9, 20, 37, 462, DateTimeKind.Utc).AddTicks(6665) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9400m, "BRL", 47.000m, 11, 63, 9400m, "BRL", 9447.000m, new DateTime(2023, 7, 28, 0, 10, 37, 462, DateTimeKind.Utc).AddTicks(6668) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 300m, "INR", 1.500m, 104, 81, 300m, "INR", 301.500m, new DateTime(2023, 5, 28, 5, 34, 37, 462, DateTimeKind.Utc).AddTicks(6672) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8000m, "RUB", 40.000m, 6, 114, 8000m, "RUB", 8040.000m, new DateTime(2023, 5, 29, 17, 18, 37, 462, DateTimeKind.Utc).AddTicks(6676) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "RUB", 69, 96, "RUB", new DateTime(2023, 6, 26, 11, 42, 37, 462, DateTimeKind.Utc).AddTicks(6679) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5600m, "CAD", 28.000m, 7, 42, 5600m, "CAD", 5628.000m, new DateTime(2023, 7, 6, 22, 23, 37, 462, DateTimeKind.Utc).AddTicks(6681) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5900m, "JPY", 29.500m, 1, 112, 5900m, "JPY", 5929.500m, new DateTime(2023, 7, 12, 10, 58, 37, 462, DateTimeKind.Utc).AddTicks(6685) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3700m, "CHF", 18.500m, 127, 95, 3700m, "CHF", 3718.500m, new DateTime(2023, 7, 8, 20, 21, 37, 462, DateTimeKind.Utc).AddTicks(6688) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8100m, "ZAR", 40.500m, 29, 8100m, "ZAR", 8140.500m, new DateTime(2023, 7, 19, 17, 15, 37, 462, DateTimeKind.Utc).AddTicks(6691) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3100m, "BGN", 15.500m, 13, 7, 3100m, "BGN", 3115.500m, new DateTime(2023, 7, 2, 14, 48, 37, 462, DateTimeKind.Utc).AddTicks(6694) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6900m, "ZAR", 34.500m, 30, 79, 6900m, "ZAR", 6934.500m, new DateTime(2023, 5, 28, 16, 29, 37, 462, DateTimeKind.Utc).AddTicks(6697) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2100m, "USD", 10.500m, 106, 2, 2100m, "USD", 2110.500m, new DateTime(2023, 6, 2, 5, 57, 37, 462, DateTimeKind.Utc).AddTicks(6700) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6200m, "BRL", 31.000m, 72, 93, 6200m, "BRL", 6231.000m, new DateTime(2023, 7, 24, 21, 37, 37, 462, DateTimeKind.Utc).AddTicks(6703) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4800m, "USD", 24.000m, 106, 69, 4800m, "USD", 4824.000m, new DateTime(2023, 6, 29, 1, 53, 37, 462, DateTimeKind.Utc).AddTicks(6706) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2200m, "CNY", 11.000m, 100, 68, 2200m, "CNY", 2211.000m, new DateTime(2023, 5, 25, 0, 17, 37, 462, DateTimeKind.Utc).AddTicks(6709) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "AUD", 6.000m, 59, 70, 1200m, "AUD", 1206.000m, new DateTime(2023, 5, 27, 15, 8, 37, 462, DateTimeKind.Utc).AddTicks(6712) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6400m, "BRL", 32.000m, 118, 24, 6400m, "BRL", 6432.000m, new DateTime(2023, 5, 25, 5, 48, 37, 462, DateTimeKind.Utc).AddTicks(6715) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 2700m, 13.500m, 15, 84, 2700m, 2713.500m, new DateTime(2023, 7, 18, 20, 9, 37, 462, DateTimeKind.Utc).AddTicks(6718) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5700m, "BRL", 28.500m, 29, 61, 5700m, "BRL", 5728.500m, new DateTime(2023, 6, 15, 11, 6, 37, 462, DateTimeKind.Utc).AddTicks(6721) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9300m, "AUD", 46.500m, 104, 50, 9300m, "AUD", 9346.500m, new DateTime(2023, 5, 23, 15, 46, 37, 462, DateTimeKind.Utc).AddTicks(6724) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "NZD", 20.500m, 80, 67, 4100m, "NZD", 4120.500m, new DateTime(2023, 8, 6, 17, 31, 37, 462, DateTimeKind.Utc).AddTicks(6727) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5500m, "BGN", 27.500m, 78, 29, 5500m, "BGN", 5527.500m, new DateTime(2023, 6, 7, 18, 31, 37, 462, DateTimeKind.Utc).AddTicks(6730) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4000m, "AUD", 20.000m, 75, 78, 4000m, "AUD", 4020.000m, new DateTime(2023, 5, 27, 17, 57, 37, 462, DateTimeKind.Utc).AddTicks(6733) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "RUB", 45.500m, 67, 48, 9100m, "RUB", 9145.500m, new DateTime(2023, 5, 21, 18, 37, 37, 462, DateTimeKind.Utc).AddTicks(6736) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6200m, "CAD", 31.000m, 75, 13, 6200m, "CAD", 6231.000m, new DateTime(2023, 8, 14, 15, 22, 37, 462, DateTimeKind.Utc).AddTicks(6739) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "INR", 41.000m, 96, 25, 8200m, "INR", 8241.000m, new DateTime(2023, 5, 19, 10, 56, 37, 462, DateTimeKind.Utc).AddTicks(6742) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2400m, "ZAR", 12.000m, 55, 91, 2400m, "ZAR", 2412.000m, new DateTime(2023, 8, 14, 22, 8, 37, 462, DateTimeKind.Utc).AddTicks(6745) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6000m, "CAD", 30.000m, 4, 39, 6000m, "CAD", 6030.000m, new DateTime(2023, 8, 6, 12, 8, 37, 462, DateTimeKind.Utc).AddTicks(6748) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8900m, "AUD", 44.500m, 43, 46, 8900m, "AUD", 8944.500m, new DateTime(2023, 6, 28, 6, 45, 37, 462, DateTimeKind.Utc).AddTicks(6751) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "NZD", 38.000m, 63, 24, 7600m, "NZD", 7638.000m, new DateTime(2023, 5, 20, 20, 15, 37, 462, DateTimeKind.Utc).AddTicks(6754) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5400m, "NZD", 27.000m, 125, 48, 5400m, "NZD", 5427.000m, new DateTime(2023, 7, 30, 8, 30, 37, 462, DateTimeKind.Utc).AddTicks(6757) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "BRL", 11.500m, 29, 32, 2300m, "BRL", 2311.500m, new DateTime(2023, 6, 2, 19, 6, 37, 462, DateTimeKind.Utc).AddTicks(6760) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6900m, "BRL", 34.500m, 2, 9, 6900m, "BRL", 6934.500m, new DateTime(2023, 6, 1, 17, 43, 37, 462, DateTimeKind.Utc).AddTicks(6764) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2100m, "CAD", 10.500m, 51, 95, 2100m, "CAD", 2110.500m, new DateTime(2023, 7, 29, 11, 40, 37, 462, DateTimeKind.Utc).AddTicks(6767) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "BaseAmount", "Fees", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, 45.500m, 45, 9100m, 9145.500m, new DateTime(2023, 6, 22, 22, 46, 37, 462, DateTimeKind.Utc).AddTicks(6770) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "CHF", 38.000m, 61, 102, 7600m, "CHF", 7638.000m, new DateTime(2023, 8, 9, 1, 9, 37, 462, DateTimeKind.Utc).AddTicks(6773) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8400m, "NZD", 42.000m, 72, 64, 8400m, "NZD", 8442.000m, new DateTime(2023, 6, 28, 4, 55, 37, 462, DateTimeKind.Utc).AddTicks(6776) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 9500m, 47.500m, 6, 97, 9500m, 9547.500m, new DateTime(2023, 7, 14, 2, 52, 37, 462, DateTimeKind.Utc).AddTicks(6779) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "NZD", 17.500m, 116, 48, 3500m, "NZD", 3517.500m, new DateTime(2023, 5, 18, 22, 13, 37, 462, DateTimeKind.Utc).AddTicks(6783) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9800m, "SGD", 49.000m, 85, 111, 9800m, "SGD", 9849.000m, new DateTime(2023, 7, 31, 22, 34, 37, 462, DateTimeKind.Utc).AddTicks(6786) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "CNY", 5.500m, 25, 23, 1100m, "CNY", 1105.500m, new DateTime(2023, 8, 5, 8, 22, 37, 462, DateTimeKind.Utc).AddTicks(6789) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 5400m, 27.000m, 89, 109, 5400m, 5427.000m, new DateTime(2023, 6, 10, 13, 15, 37, 462, DateTimeKind.Utc).AddTicks(6792) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "CHF", 17.500m, 50, 90, 3500m, "CHF", 3517.500m, new DateTime(2023, 6, 3, 6, 1, 37, 462, DateTimeKind.Utc).AddTicks(6795) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6400m, "SGD", 32.000m, 44, 56, 6400m, "SGD", 6432.000m, new DateTime(2023, 6, 13, 7, 32, 37, 462, DateTimeKind.Utc).AddTicks(6798) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4200m, "CAD", 21.000m, 52, 102, 4200m, "CAD", 4221.000m, new DateTime(2023, 6, 8, 19, 9, 37, 462, DateTimeKind.Utc).AddTicks(6801) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, "ZAR", 21.500m, 54, 68, 4300m, "ZAR", 4321.500m, new DateTime(2023, 6, 12, 15, 26, 37, 462, DateTimeKind.Utc).AddTicks(6804) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "USD", 5.500m, 48, 62, 1100m, "USD", 1105.500m, new DateTime(2023, 6, 21, 15, 3, 37, 462, DateTimeKind.Utc).AddTicks(6807) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2400m, "JPY", 12.000m, 86, 2400m, "JPY", 2412.000m, new DateTime(2023, 7, 23, 16, 2, 37, 462, DateTimeKind.Utc).AddTicks(6810) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1500m, "USD", 7.500m, 57, 66, 1500m, "USD", 1507.500m, new DateTime(2023, 8, 3, 8, 56, 37, 462, DateTimeKind.Utc).AddTicks(6813) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9000m, "RUB", 45.000m, 2, 110, 9000m, "RUB", 9045.000m, new DateTime(2023, 7, 27, 7, 29, 37, 462, DateTimeKind.Utc).AddTicks(6816) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8100m, "BGN", 40.500m, 54, 35, 8100m, "BGN", 8140.500m, new DateTime(2023, 5, 20, 12, 32, 37, 462, DateTimeKind.Utc).AddTicks(6851) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "GBP", 8.500m, 104, 124, 1700m, "GBP", 1708.500m, new DateTime(2023, 8, 10, 10, 51, 37, 462, DateTimeKind.Utc).AddTicks(6856) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "NZD", 44.000m, 80, 112, 8800m, "NZD", 8844.000m, new DateTime(2023, 8, 7, 8, 46, 37, 462, DateTimeKind.Utc).AddTicks(6859) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5900m, "BRL", 29.500m, 52, 53, 5900m, "BRL", 5929.500m, new DateTime(2023, 6, 25, 9, 39, 37, 462, DateTimeKind.Utc).AddTicks(6863) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "SGD", 34.000m, 4, 35, 6800m, "SGD", 6834.000m, new DateTime(2023, 7, 21, 10, 54, 37, 462, DateTimeKind.Utc).AddTicks(6867) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "EUR", 8.500m, 128, 103, 1700m, "EUR", 1708.500m, new DateTime(2023, 7, 11, 10, 45, 37, 462, DateTimeKind.Utc).AddTicks(6869) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3300m, "NZD", 16.500m, 86, 25, 3300m, "NZD", 3316.500m, new DateTime(2023, 6, 3, 17, 40, 37, 462, DateTimeKind.Utc).AddTicks(6872) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1500m, "EUR", 7.500m, 121, 52, 1500m, "EUR", 1507.500m, new DateTime(2023, 6, 6, 23, 45, 37, 462, DateTimeKind.Utc).AddTicks(6876) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, "CAD", 21.500m, 22, 2, 4300m, "CAD", 4321.500m, new DateTime(2023, 7, 25, 13, 55, 37, 462, DateTimeKind.Utc).AddTicks(6879) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1900m, "CAD", 9.500m, 101, 7, 1900m, "CAD", 1909.500m, new DateTime(2023, 7, 25, 22, 46, 37, 462, DateTimeKind.Utc).AddTicks(6883) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5600m, "CHF", 28.000m, 6, 95, 5600m, "CHF", 5628.000m, new DateTime(2023, 5, 20, 22, 59, 37, 462, DateTimeKind.Utc).AddTicks(6887) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2800m, "CNY", 14.000m, 32, 110, 2800m, "CNY", 2814.000m, new DateTime(2023, 8, 4, 20, 37, 37, 462, DateTimeKind.Utc).AddTicks(6896) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "CAD", 11.500m, 90, 49, 2300m, "CAD", 2311.500m, new DateTime(2023, 7, 7, 10, 12, 37, 462, DateTimeKind.Utc).AddTicks(6899) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "BGN", 25.000m, 100, 119, 5000m, "BGN", 5025.000m, new DateTime(2023, 6, 13, 17, 7, 37, 462, DateTimeKind.Utc).AddTicks(6902) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "CHF", 17.500m, 12, 83, 3500m, "CHF", 3517.500m, new DateTime(2023, 6, 18, 20, 40, 37, 462, DateTimeKind.Utc).AddTicks(6906) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2800m, "CHF", 14.000m, 83, 61, 2800m, "CHF", 2814.000m, new DateTime(2023, 8, 16, 2, 32, 37, 462, DateTimeKind.Utc).AddTicks(6910) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2700m, "CAD", 13.500m, 33, 12, 2700m, "CAD", 2713.500m, new DateTime(2023, 6, 3, 4, 50, 37, 462, DateTimeKind.Utc).AddTicks(6913) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8900m, "BGN", 44.500m, 40, 119, 8900m, "BGN", 8944.500m, new DateTime(2023, 7, 14, 7, 6, 37, 462, DateTimeKind.Utc).AddTicks(6917) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4900m, "USD", 24.500m, 22, 24, 4900m, "USD", 4924.500m, new DateTime(2023, 6, 12, 2, 51, 37, 462, DateTimeKind.Utc).AddTicks(6920) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "CAD", 68, 67, "CAD", new DateTime(2023, 7, 14, 6, 52, 37, 462, DateTimeKind.Utc).AddTicks(6923) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 5200m, 26.000m, 46, 11, 5200m, 5226.000m, new DateTime(2023, 8, 11, 16, 35, 37, 462, DateTimeKind.Utc).AddTicks(6926) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1600m, "BRL", 8.000m, 26, 20, 1600m, "BRL", 1608.000m, new DateTime(2023, 6, 9, 16, 4, 37, 462, DateTimeKind.Utc).AddTicks(6930) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "EUR", 34.000m, 129, 53, 6800m, "EUR", 6834.000m, new DateTime(2023, 7, 6, 4, 48, 37, 462, DateTimeKind.Utc).AddTicks(6933) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "USD", 45.500m, 8, 119, 9100m, "USD", 9145.500m, new DateTime(2023, 5, 23, 3, 18, 37, 462, DateTimeKind.Utc).AddTicks(6936) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7200m, "CNY", 36.000m, 101, 11, 7200m, "CNY", 7236.000m, new DateTime(2023, 6, 9, 8, 35, 37, 462, DateTimeKind.Utc).AddTicks(6941) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1600m, "USD", 8.000m, 61, 11, 1600m, "USD", 1608.000m, new DateTime(2023, 6, 3, 3, 58, 37, 462, DateTimeKind.Utc).AddTicks(6945) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "CNY", 2.000m, 1, 111, 400m, "CNY", 402.000m, new DateTime(2023, 7, 5, 17, 31, 37, 462, DateTimeKind.Utc).AddTicks(6948) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "ZAR", 41.000m, 32, 13, 8200m, "ZAR", 8241.000m, new DateTime(2023, 6, 4, 16, 58, 37, 462, DateTimeKind.Utc).AddTicks(6952) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3100m, "BRL", 15.500m, 41, 112, 3100m, "BRL", 3115.500m, new DateTime(2023, 5, 28, 14, 24, 37, 462, DateTimeKind.Utc).AddTicks(6956) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "INR", 25.000m, 6, 89, 5000m, "INR", 5025.000m, new DateTime(2023, 8, 1, 18, 0, 37, 462, DateTimeKind.Utc).AddTicks(6959) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1500m, "CHF", 7.500m, 123, 115, 1500m, "CHF", 1507.500m, new DateTime(2023, 6, 7, 9, 4, 37, 462, DateTimeKind.Utc).AddTicks(6964) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, "GBP", 21.500m, 28, 102, 4300m, "GBP", 4321.500m, new DateTime(2023, 5, 24, 10, 51, 37, 462, DateTimeKind.Utc).AddTicks(6968) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 151,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "RUB", 30.500m, 8, 108, 6100m, "RUB", 6130.500m, new DateTime(2023, 8, 7, 4, 37, 37, 462, DateTimeKind.Utc).AddTicks(6972) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 152,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6500m, "SGD", 32.500m, 116, 2, 6500m, "SGD", 6532.500m, new DateTime(2023, 6, 11, 8, 24, 37, 462, DateTimeKind.Utc).AddTicks(6978) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 153,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9000m, "CHF", 45.000m, 127, 31, 9000m, "CHF", 9045.000m, new DateTime(2023, 8, 11, 17, 44, 37, 462, DateTimeKind.Utc).AddTicks(6981) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 154,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3200m, "GBP", 16.000m, 26, 49, 3200m, "GBP", 3216.000m, new DateTime(2023, 6, 27, 19, 6, 37, 462, DateTimeKind.Utc).AddTicks(6985) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 155,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4700m, "CNY", 23.500m, 49, 59, 4700m, "CNY", 4723.500m, new DateTime(2023, 5, 24, 7, 26, 37, 462, DateTimeKind.Utc).AddTicks(6989) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 156,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4200m, "BRL", 21.000m, 114, 109, 4200m, "BRL", 4221.000m, new DateTime(2023, 5, 27, 0, 18, 37, 462, DateTimeKind.Utc).AddTicks(6993) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 157,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "BGN", 34.000m, 110, 82, 6800m, "BGN", 6834.000m, new DateTime(2023, 8, 12, 20, 42, 37, 462, DateTimeKind.Utc).AddTicks(7059) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 158,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 1800m, 9.000m, 5, 3, 1800m, 1809.000m, new DateTime(2023, 6, 15, 8, 36, 37, 462, DateTimeKind.Utc).AddTicks(7063) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 159,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, 41.000m, 119, 38, 8200m, 8241.000m, new DateTime(2023, 6, 21, 21, 22, 37, 462, DateTimeKind.Utc).AddTicks(7066) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 160,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 800m, "BGN", 4.000m, 74, 122, 800m, "BGN", 804.000m, new DateTime(2023, 7, 30, 19, 56, 37, 462, DateTimeKind.Utc).AddTicks(7180) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 161,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "CHF", 42.500m, 127, 44, 8500m, "CHF", 8542.500m, new DateTime(2023, 6, 4, 15, 22, 37, 462, DateTimeKind.Utc).AddTicks(7183) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 162,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9500m, "CHF", 47.500m, 57, 79, 9500m, "CHF", 9547.500m, new DateTime(2023, 7, 3, 7, 10, 37, 462, DateTimeKind.Utc).AddTicks(7187) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 163,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7900m, "USD", 39.500m, 60, 22, 7900m, "USD", 7939.500m, new DateTime(2023, 8, 13, 15, 5, 37, 462, DateTimeKind.Utc).AddTicks(7190) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 164,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "CAD", 2.000m, 107, 88, 400m, "CAD", 402.000m, new DateTime(2023, 7, 24, 6, 59, 37, 462, DateTimeKind.Utc).AddTicks(7193) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 165,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 800m, "GBP", 4.000m, 48, 1, 800m, "GBP", 804.000m, new DateTime(2023, 8, 8, 8, 37, 37, 462, DateTimeKind.Utc).AddTicks(7196) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 166,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "ZAR", 6.000m, 88, 125, 1200m, "ZAR", 1206.000m, new DateTime(2023, 7, 30, 0, 57, 37, 462, DateTimeKind.Utc).AddTicks(7199) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 167,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3800m, "GBP", 19.000m, 18, 126, 3800m, "GBP", 3819.000m, new DateTime(2023, 6, 29, 21, 42, 37, 462, DateTimeKind.Utc).AddTicks(7202) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 168,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7200m, "CAD", 36.000m, 91, 75, 7200m, "CAD", 7236.000m, new DateTime(2023, 6, 24, 14, 20, 37, 462, DateTimeKind.Utc).AddTicks(7205) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 169,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "BRL", 2.000m, 117, 56, 400m, "BRL", 402.000m, new DateTime(2023, 5, 23, 23, 13, 37, 462, DateTimeKind.Utc).AddTicks(7209) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 170,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5700m, "USD", 28.500m, 94, 13, 5700m, "USD", 5728.500m, new DateTime(2023, 8, 12, 14, 26, 37, 462, DateTimeKind.Utc).AddTicks(7212) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 171,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 800m, "RUB", 4.000m, 77, 14, 800m, "RUB", 804.000m, new DateTime(2023, 7, 7, 20, 8, 37, 462, DateTimeKind.Utc).AddTicks(7215) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 172,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4800m, "AUD", 24.000m, 35, 89, 4800m, "AUD", 4824.000m, new DateTime(2023, 7, 16, 7, 34, 37, 462, DateTimeKind.Utc).AddTicks(7218) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 173,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3300m, "GBP", 16.500m, 88, 59, 3300m, "GBP", 3316.500m, new DateTime(2023, 8, 12, 8, 41, 37, 462, DateTimeKind.Utc).AddTicks(7221) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 174,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "RUB", 42.500m, 90, 43, 8500m, "RUB", 8542.500m, new DateTime(2023, 6, 23, 2, 8, 37, 462, DateTimeKind.Utc).AddTicks(7224) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 175,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, 2.000m, 48, 13, 400m, 402.000m, new DateTime(2023, 7, 18, 2, 2, 37, 462, DateTimeKind.Utc).AddTicks(7227) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 176,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5300m, "SGD", 26.500m, 29, 104, 5300m, "SGD", 5326.500m, new DateTime(2023, 7, 27, 13, 17, 37, 462, DateTimeKind.Utc).AddTicks(7230) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 177,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8300m, "INR", 41.500m, 8, 81, 8300m, "INR", 8341.500m, new DateTime(2023, 5, 28, 4, 9, 37, 462, DateTimeKind.Utc).AddTicks(7233) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 178,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6300m, "SGD", 31.500m, 68, 26, 6300m, "SGD", 6331.500m, new DateTime(2023, 5, 27, 23, 20, 37, 462, DateTimeKind.Utc).AddTicks(7237) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 179,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4200m, "BRL", 21.000m, 28, 109, 4200m, "BRL", 4221.000m, new DateTime(2023, 7, 25, 14, 2, 37, 462, DateTimeKind.Utc).AddTicks(7240) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 180,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9800m, "CHF", 49.000m, 102, 72, 9800m, "CHF", 9849.000m, new DateTime(2023, 7, 10, 5, 16, 37, 462, DateTimeKind.Utc).AddTicks(7243) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 181,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7700m, "INR", 38.500m, 114, 51, 7700m, "INR", 7738.500m, new DateTime(2023, 8, 1, 9, 32, 37, 462, DateTimeKind.Utc).AddTicks(7246) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 182,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, 6.000m, 49, 4, 1200m, 1206.000m, new DateTime(2023, 5, 30, 11, 47, 37, 462, DateTimeKind.Utc).AddTicks(7249) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 183,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8700m, "BRL", 43.500m, 115, 91, 8700m, "BRL", 8743.500m, new DateTime(2023, 6, 25, 14, 43, 37, 462, DateTimeKind.Utc).AddTicks(7252) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 184,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7900m, "AUD", 39.500m, 69, 126, 7900m, "AUD", 7939.500m, new DateTime(2023, 7, 21, 23, 34, 37, 462, DateTimeKind.Utc).AddTicks(7255) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 185,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1500m, "ZAR", 7.500m, 20, 32, 1500m, "ZAR", 1507.500m, new DateTime(2023, 8, 12, 17, 4, 37, 462, DateTimeKind.Utc).AddTicks(7258) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 186,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 900m, "ZAR", 4.500m, 116, 61, 900m, "ZAR", 904.500m, new DateTime(2023, 8, 16, 9, 55, 37, 462, DateTimeKind.Utc).AddTicks(7261) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 187,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2500m, "EUR", 12.500m, 63, 104, 2500m, "EUR", 2512.500m, new DateTime(2023, 5, 28, 1, 3, 37, 462, DateTimeKind.Utc).AddTicks(7264) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 188,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "RUB", 17.500m, 68, 27, 3500m, "RUB", 3517.500m, new DateTime(2023, 6, 7, 22, 51, 37, 462, DateTimeKind.Utc).AddTicks(7267) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 189,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 6600m, 33.000m, 129, 54, 6600m, 6633.000m, new DateTime(2023, 6, 3, 20, 32, 37, 462, DateTimeKind.Utc).AddTicks(7270) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 190,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "SGD", 2.000m, 29, 87, 400m, "SGD", 402.000m, new DateTime(2023, 6, 20, 22, 27, 37, 462, DateTimeKind.Utc).AddTicks(7273) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 191,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9800m, "CAD", 49.000m, 7, 17, 9800m, "CAD", 9849.000m, new DateTime(2023, 6, 2, 3, 50, 37, 462, DateTimeKind.Utc).AddTicks(7276) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 192,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6000m, "INR", 30.000m, 20, 53, 6000m, "INR", 6030.000m, new DateTime(2023, 7, 8, 21, 3, 37, 462, DateTimeKind.Utc).AddTicks(7279) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 193,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7000m, "GBP", 35.000m, 59, 16, 7000m, "GBP", 7035.000m, new DateTime(2023, 5, 22, 20, 48, 37, 462, DateTimeKind.Utc).AddTicks(7282) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 194,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4800m, "BGN", 24.000m, 74, 112, 4800m, "BGN", 4824.000m, new DateTime(2023, 5, 23, 18, 39, 37, 462, DateTimeKind.Utc).AddTicks(7285) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 195,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4200m, "EUR", 21.000m, 12, 56, 4200m, "EUR", 4221.000m, new DateTime(2023, 5, 28, 17, 52, 37, 462, DateTimeKind.Utc).AddTicks(7288) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 196,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7400m, "AUD", 37.000m, 29, 26, 7400m, "AUD", 7437.000m, new DateTime(2023, 6, 4, 23, 42, 37, 462, DateTimeKind.Utc).AddTicks(7292) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 197,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2600m, "AUD", 13.000m, 129, 83, 2600m, "AUD", 2613.000m, new DateTime(2023, 6, 6, 21, 25, 37, 462, DateTimeKind.Utc).AddTicks(7295) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 198,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 600m, "CNY", 3.000m, 65, 104, 600m, "CNY", 603.000m, new DateTime(2023, 6, 5, 7, 16, 37, 462, DateTimeKind.Utc).AddTicks(7298) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 199,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 700m, 3.500m, 69, 44, 700m, 703.500m, new DateTime(2023, 8, 6, 12, 24, 37, 462, DateTimeKind.Utc).AddTicks(7301) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 200,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6500m, "CAD", 32.500m, 11, 97, 6500m, "CAD", 6532.500m, new DateTime(2023, 6, 28, 5, 2, 37, 462, DateTimeKind.Utc).AddTicks(7304) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 201,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2500m, "INR", 12.500m, 126, 90, 2500m, "INR", 2512.500m, new DateTime(2023, 7, 18, 14, 17, 37, 462, DateTimeKind.Utc).AddTicks(7307) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 202,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2200m, "ZAR", 11.000m, 4, 122, 2200m, "ZAR", 2211.000m, new DateTime(2023, 7, 17, 12, 44, 37, 462, DateTimeKind.Utc).AddTicks(7310) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 203,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "JPY", 5.500m, 100, 2, 1100m, "JPY", 1105.500m, new DateTime(2023, 8, 8, 19, 51, 37, 462, DateTimeKind.Utc).AddTicks(7314) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 204,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8300m, "JPY", 41.500m, 88, 107, 8300m, "JPY", 8341.500m, new DateTime(2023, 6, 10, 3, 53, 37, 462, DateTimeKind.Utc).AddTicks(7318) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 205,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "JPY", 22.000m, 57, 27, 4400m, "JPY", 4422.000m, new DateTime(2023, 7, 25, 18, 16, 37, 462, DateTimeKind.Utc).AddTicks(7322) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 206,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 600m, "CAD", 3.000m, 27, 4, 600m, "CAD", 603.000m, new DateTime(2023, 7, 22, 8, 18, 37, 462, DateTimeKind.Utc).AddTicks(7327) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 207,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "GBP", 15.000m, 34, 90, 3000m, "GBP", 3015.000m, new DateTime(2023, 8, 8, 6, 11, 37, 462, DateTimeKind.Utc).AddTicks(7331) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 208,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "BRL", 2.000m, 58, 69, 400m, "BRL", 402.000m, new DateTime(2023, 7, 22, 20, 30, 37, 462, DateTimeKind.Utc).AddTicks(7367) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 209,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 800m, "CHF", 4.000m, 83, 45, 800m, "CHF", 804.000m, new DateTime(2023, 5, 20, 16, 34, 37, 462, DateTimeKind.Utc).AddTicks(7371) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 210,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 2500m, 12.500m, 102, 17, 2500m, 2512.500m, new DateTime(2023, 7, 19, 0, 2, 37, 462, DateTimeKind.Utc).AddTicks(7376) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 211,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1400m, "NZD", 7.000m, 97, 76, 1400m, "NZD", 1407.000m, new DateTime(2023, 8, 17, 3, 2, 37, 462, DateTimeKind.Utc).AddTicks(7380) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 212,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "BRL", 44.000m, 4, 50, 8800m, "BRL", 8844.000m, new DateTime(2023, 5, 29, 23, 47, 37, 462, DateTimeKind.Utc).AddTicks(7384) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 213,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3900m, "RUB", 19.500m, 103, 125, 3900m, "RUB", 3919.500m, new DateTime(2023, 7, 10, 17, 48, 37, 462, DateTimeKind.Utc).AddTicks(7389) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 214,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9500m, "BRL", 47.500m, 27, 48, 9500m, "BRL", 9547.500m, new DateTime(2023, 7, 12, 22, 19, 37, 462, DateTimeKind.Utc).AddTicks(7393) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 215,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4200m, "INR", 21.000m, 87, 16, 4200m, "INR", 4221.000m, new DateTime(2023, 6, 16, 12, 1, 37, 462, DateTimeKind.Utc).AddTicks(7396) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 216,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "SGD", 29.000m, 84, 39, 5800m, "SGD", 5829.000m, new DateTime(2023, 7, 23, 23, 32, 37, 462, DateTimeKind.Utc).AddTicks(7400) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 217,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "CNY", 34.000m, 75, 66, 6800m, "CNY", 6834.000m, new DateTime(2023, 8, 7, 20, 27, 37, 462, DateTimeKind.Utc).AddTicks(7403) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 218,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "CHF", 34.000m, 68, 125, 6800m, "CHF", 6834.000m, new DateTime(2023, 7, 2, 18, 32, 37, 462, DateTimeKind.Utc).AddTicks(7408) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 219,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7900m, "BRL", 39.500m, 95, 79, 7900m, "BRL", 7939.500m, new DateTime(2023, 5, 19, 6, 21, 37, 462, DateTimeKind.Utc).AddTicks(7411) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 220,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "AUD", 41.000m, 82, 60, 8200m, "AUD", 8241.000m, new DateTime(2023, 8, 11, 18, 44, 37, 462, DateTimeKind.Utc).AddTicks(7415) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 221,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3800m, "SGD", 19.000m, 32, 19, 3800m, "SGD", 3819.000m, new DateTime(2023, 7, 4, 11, 42, 37, 462, DateTimeKind.Utc).AddTicks(7419) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 222,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4700m, "BRL", 23.500m, 112, 6, 4700m, "BRL", 4723.500m, new DateTime(2023, 8, 1, 6, 34, 37, 462, DateTimeKind.Utc).AddTicks(7423) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 223,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "NZD", 34.000m, 48, 107, 6800m, "NZD", 6834.000m, new DateTime(2023, 7, 16, 6, 36, 37, 462, DateTimeKind.Utc).AddTicks(7427) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 224,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "BGN", 17.500m, 60, 54, 3500m, "BGN", 3517.500m, new DateTime(2023, 8, 1, 4, 56, 37, 462, DateTimeKind.Utc).AddTicks(7431) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 225,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "ZAR", 15.000m, 61, 80, 3000m, "ZAR", 3015.000m, new DateTime(2023, 6, 14, 11, 21, 37, 462, DateTimeKind.Utc).AddTicks(7434) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 226,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8900m, "GBP", 44.500m, 15, 85, 8900m, "GBP", 8944.500m, new DateTime(2023, 8, 2, 19, 17, 37, 462, DateTimeKind.Utc).AddTicks(7438) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 227,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "GBP", 2.000m, 66, 39, 400m, "GBP", 402.000m, new DateTime(2023, 7, 11, 18, 19, 37, 462, DateTimeKind.Utc).AddTicks(7441) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 228,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "GBP", 17, 36, "GBP", new DateTime(2023, 5, 27, 19, 13, 37, 462, DateTimeKind.Utc).AddTicks(7445) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 229,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5900m, "USD", 29.500m, 81, 107, 5900m, "USD", 5929.500m, new DateTime(2023, 7, 1, 10, 53, 37, 462, DateTimeKind.Utc).AddTicks(7447) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 230,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3700m, "BGN", 18.500m, 71, 35, 3700m, "BGN", 3718.500m, new DateTime(2023, 5, 19, 0, 38, 37, 462, DateTimeKind.Utc).AddTicks(7450) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 231,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7700m, "JPY", 38.500m, 78, 64, 7700m, "JPY", 7738.500m, new DateTime(2023, 7, 22, 0, 46, 37, 462, DateTimeKind.Utc).AddTicks(7454) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 232,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8900m, "CNY", 44.500m, 102, 128, 8900m, "CNY", 8944.500m, new DateTime(2023, 7, 28, 11, 24, 37, 462, DateTimeKind.Utc).AddTicks(7457) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 233,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "INR", 44.000m, 12, 115, 8800m, "INR", 8844.000m, new DateTime(2023, 7, 27, 16, 44, 37, 462, DateTimeKind.Utc).AddTicks(7459) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 234,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1500m, "GBP", 7.500m, 73, 111, 1500m, "GBP", 1507.500m, new DateTime(2023, 6, 3, 9, 13, 37, 462, DateTimeKind.Utc).AddTicks(7462) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 235,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9600m, "BRL", 48.000m, 43, 78, 9600m, "BRL", 9648.000m, new DateTime(2023, 6, 19, 2, 18, 37, 462, DateTimeKind.Utc).AddTicks(7466) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 236,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "USD", 44.000m, 114, 9, 8800m, "USD", 8844.000m, new DateTime(2023, 8, 5, 10, 32, 37, 462, DateTimeKind.Utc).AddTicks(7469) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 237,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 8700m, 43.500m, 111, 45, 8700m, 8743.500m, new DateTime(2023, 6, 14, 16, 39, 37, 462, DateTimeKind.Utc).AddTicks(7472) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 238,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 300m, "NZD", 1.500m, 113, 111, 300m, "NZD", 301.500m, new DateTime(2023, 8, 6, 1, 12, 37, 462, DateTimeKind.Utc).AddTicks(7475) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 239,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8300m, "CAD", 41.500m, 39, 123, 8300m, "CAD", 8341.500m, new DateTime(2023, 5, 19, 12, 44, 37, 462, DateTimeKind.Utc).AddTicks(7478) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 240,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 500m, "EUR", 2.500m, 107, 99, 500m, "EUR", 502.500m, new DateTime(2023, 7, 16, 6, 3, 37, 462, DateTimeKind.Utc).AddTicks(7481) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 241,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8600m, "GBP", 43.000m, 8, 9, 8600m, "GBP", 8643.000m, new DateTime(2023, 5, 20, 13, 30, 37, 462, DateTimeKind.Utc).AddTicks(7484) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 242,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 9000m, 45.000m, 74, 26, 9000m, 9045.000m, new DateTime(2023, 5, 29, 16, 24, 37, 462, DateTimeKind.Utc).AddTicks(7487) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 243,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 600m, "BRL", 3.000m, 103, 86, 600m, "BRL", 603.000m, new DateTime(2023, 6, 6, 20, 28, 37, 462, DateTimeKind.Utc).AddTicks(7490) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 244,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8700m, "JPY", 43.500m, 16, 38, 8700m, "JPY", 8743.500m, new DateTime(2023, 5, 30, 22, 47, 37, 462, DateTimeKind.Utc).AddTicks(7493) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 245,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5400m, "EUR", 27.000m, 61, 35, 5400m, "EUR", 5427.000m, new DateTime(2023, 7, 22, 18, 48, 37, 462, DateTimeKind.Utc).AddTicks(7496) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 246,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "AUD", 41.000m, 29, 41, 8200m, "AUD", 8241.000m, new DateTime(2023, 6, 5, 18, 21, 37, 462, DateTimeKind.Utc).AddTicks(7499) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 247,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2200m, "ZAR", 11.000m, 101, 3, 2200m, "ZAR", 2211.000m, new DateTime(2023, 7, 22, 22, 27, 37, 462, DateTimeKind.Utc).AddTicks(7502) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 248,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3900m, "BRL", 19.500m, 85, 74, 3900m, "BRL", 3919.500m, new DateTime(2023, 8, 1, 5, 2, 37, 462, DateTimeKind.Utc).AddTicks(7505) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 249,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5900m, "NZD", 29.500m, 34, 115, 5900m, "NZD", 5929.500m, new DateTime(2023, 8, 8, 15, 16, 37, 462, DateTimeKind.Utc).AddTicks(7508) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 250,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8900m, "JPY", 44.500m, 91, 86, 8900m, "JPY", 8944.500m, new DateTime(2023, 7, 21, 6, 56, 37, 462, DateTimeKind.Utc).AddTicks(7511) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 251,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2700m, "CHF", 13.500m, 71, 73, 2700m, "CHF", 2713.500m, new DateTime(2023, 5, 31, 9, 47, 37, 462, DateTimeKind.Utc).AddTicks(7514) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 252,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2000m, "JPY", 10.000m, 67, 83, 2000m, "JPY", 2010.000m, new DateTime(2023, 6, 29, 13, 30, 37, 462, DateTimeKind.Utc).AddTicks(7517) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 253,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "INR", 29.000m, 55, 71, 5800m, "INR", 5829.000m, new DateTime(2023, 8, 9, 2, 4, 37, 462, DateTimeKind.Utc).AddTicks(7520) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 254,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7800m, "ZAR", 39.000m, 80, 83, 7800m, "ZAR", 7839.000m, new DateTime(2023, 6, 1, 13, 48, 37, 462, DateTimeKind.Utc).AddTicks(7523) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 255,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6400m, "BRL", 32.000m, 69, 79, 6400m, "BRL", 6432.000m, new DateTime(2023, 6, 13, 18, 4, 37, 462, DateTimeKind.Utc).AddTicks(7526) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 256,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9200m, "ZAR", 46.000m, 92, 111, 9200m, "ZAR", 9246.000m, new DateTime(2023, 7, 15, 1, 22, 37, 462, DateTimeKind.Utc).AddTicks(7530) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 257,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4600m, "GBP", 23.000m, 29, 64, 4600m, "GBP", 4623.000m, new DateTime(2023, 5, 20, 0, 44, 37, 462, DateTimeKind.Utc).AddTicks(7533) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 258,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "NZD", 38.000m, 102, 30, 7600m, "NZD", 7638.000m, new DateTime(2023, 7, 4, 11, 54, 37, 462, DateTimeKind.Utc).AddTicks(7568) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 259,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1400m, "CHF", 7.000m, 82, 106, 1400m, "CHF", 1407.000m, new DateTime(2023, 7, 30, 10, 38, 37, 462, DateTimeKind.Utc).AddTicks(7571) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "VerificationToken",
                value: new Guid("8778f18e-dc47-445d-a683-3a5e5544e71a"));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0889521134", new Guid("2642ea57-1ffb-4908-9c1e-3e4630de5e87") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Widow@google.ru", "0884532891", new Guid("758b61ed-c509-4d97-bceb-73a0844de6ee") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Turbo@apple.com", "0889965286", new Guid("e87f5b46-5674-4074-8a73-0ac020f5721b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cuddlebug@yahoo.bg", "0888363682", new Guid("5a45c20c-4bea-4198-acef-565f2f69c90a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Thrill@abv.bg", "0884922414", new Guid("da44a830-818f-44b4-8a16-14ba530563ea") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "AsteroidZ@abv.com", "0886815816", new Guid("a461696b-48b9-4754-9b78-06e985fd1cc1") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "An1a@yahoo.net", "0883613380", new Guid("e3086918-84c4-415a-8f6d-290b1f391967") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Bluelight@yahoo.eu", "0885768618", new Guid("ac1d3467-9aed-4817-acb9-1a7ce4b507de") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Loaded@apple.net", "0886018988", new Guid("b0fa217d-a4e1-4e80-b5af-ff02e4bcdacb") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Rant@yahoo.net", "0883928861", new Guid("919fcb05-a7df-4356-8fa6-680cf339fd94") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Diva@google.eu", "0887885879", new Guid("d942b040-56a6-45dc-87ca-bc2ab9f1fa3e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Yukki@yahoo.eu", "0888067894", new Guid("9bb8f6fe-ac9f-4808-a4b7-57ed59d95f55") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Werice@yahoo.ru", "0887611515", new Guid("776aa4b9-42ad-4c69-9655-a495a2decd25") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Perfect@abv.com", "0888248259", new Guid("c48a883e-65aa-44de-9482-a904bc58e5aa") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "MissK@apple.com", "0887533392", new Guid("a3e57ed9-185f-4eba-ae98-81aef1c8a12b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Bubbles@abv.ru", "0885048500", new Guid("079232de-b042-4cd3-a7a0-d715544c51cf") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "ChEk@apple.net", "0886868204", new Guid("855ab553-b9f6-46bc-9c34-9545ce0030e6") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Done@google.com", "0889773231", new Guid("4fc5c2f1-ec43-4ce6-9194-9b9c05dc2f9a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "HugsForDrubs@google.ru", "0886594210", new Guid("240dfa90-a6d8-4020-bb87-8efe4c19049b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pippen@apple.net", "0889451939", new Guid("c1e893b4-acda-4196-975e-c5ae5b89a7ee") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Aisha@abv.net", "0887201196", new Guid("6d7d6a5f-e57c-44cd-83e5-ef6656783dbb") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Protectshell@abv.net", "0883926104", new Guid("e507bea3-e104-4d2c-bcbc-a9f15708daca") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Geez@yahoo.ru", "0889605617", new Guid("e072aac4-d2a7-49b3-a908-efc9f3a3bc1f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Monkey_see@google.com", "0884355068", new Guid("cdfd34e5-b6ff-457e-b231-7db2ea35054f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Tight@apple.net", "0889509126", new Guid("5ff0e276-2149-452a-93c7-6b044591fd3a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cutefest@google.ru", "0889981207", new Guid("09587f4d-5cef-4a20-8057-c0a322e33437") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Prong@yahoo.ru", "0887556911", new Guid("f761158c-41ea-4978-b70b-8784be188ff5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Strongarmed@yahoo.com", "0885389385", new Guid("795f1aa2-a2ce-48ef-8eb6-3a5216903e3c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Sovenance@yahoo.net", "0885263507", new Guid("364c81f2-22e5-42a7-8ebb-7fc50e982c42") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Angel@abv.ru", "0887049126", new Guid("5270831e-8237-425a-b79f-5c55e8aefd51") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Mad@yahoo.net", "0889350598", new Guid("eda7d3a6-b531-4320-89f2-4694cdbca202") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dimple@abv.ru", "0884875380", new Guid("4ce14098-91bf-4081-a6ad-95d2cd4039f8") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dollspell@abv.bg", "0888119869", new Guid("1dbdfd31-395b-4acb-8e74-4fb275a0fd8e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "State@abv.com", "0888975650", new Guid("3f02d755-8dba-4f1c-8106-b5abb5e22cc0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Vellojoy@google.net", "0884630966", new Guid("64eb4062-c727-4508-b17a-ad40c7d88ec2") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Eagle@yahoo.bg", "0883726865", new Guid("bb96c1df-73b1-4024-a1b9-c68745207a6e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Brutal@yahoo.com", "0886622997", new Guid("96dbba21-d722-40bb-b5ce-bbae581e2b2b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0887879810", new Guid("d9bd3107-451c-4502-9051-0a725c8411f9") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Intella@apple.com", "0886614840", new Guid("fe315175-27f7-4a23-ba7e-7742ae1f97a8") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Rainblow@yahoo.com", "0889954860", new Guid("50af102f-7b63-454b-97c1-d0b49cd1f3ed") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Venereology@yahoo.ru", "0885917450", new Guid("f5aad078-e05a-47b6-b708-d921666ebf23") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dogbone@abv.bg", "0886615586", new Guid("77459d0d-ebff-4b04-8494-b60f6ad9bbea") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Grep@abv.eu", "0883855463", new Guid("46e4d0c4-5c08-4a73-8ffa-946ba5727572") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hawkeye@yahoo.eu", "0888832602", new Guid("5735bc63-9e67-4e65-8ab0-258b8ea3a6ff") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Glue@abv.com", "0883159014", new Guid("6bb4fa41-26a5-43dd-bac1-ed99503a1943") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Princess@abv.eu", "0885175857", new Guid("6a726a57-530a-4c6b-b2c2-7f8aef30d423") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "L4Legit@yahoo.eu", "0883734925", new Guid("ce8bf084-ed58-4846-a4a5-01f1143c9f53") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Peapod@google.eu", "0888103560", new Guid("d83d395d-dfc7-428b-9908-3559dc922a6e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Networked@apple.com", "0889310207", new Guid("51914a09-267f-4362-b5b7-c9ebdaa0bb12") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Prep@yahoo.net", "0888848880", new Guid("5992f345-0c6e-4356-b3ba-872857736ab5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Sharken@apple.net", "0883551961", new Guid("84ff8b0a-c8cd-4f97-8c93-b0f8ce8143de") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Curio@yahoo.bg", "0889661106", new Guid("93e2a3d8-83df-44fa-9bc5-3c9eac8e4750") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Ender@google.com", "0884928084", new Guid("3501e1f4-5c03-4147-ac3a-58ddad5beeb2") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Icelolly@abv.ru", "0883254876", new Guid("1a925e78-c03f-42a4-808e-b2920c2e4e4e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0886050845", new Guid("38f99f9c-91b3-47a1-95ff-e0a3598ce908") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Zyber@abv.com", "0883790760", new Guid("c32331fb-b343-4584-b4e2-99b5ced909bd") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Moonchild@google.eu", "0887129135", new Guid("226b1597-9a2b-4e20-86bb-bedba0d9c2f4") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Precision@google.ru", "0889031384", new Guid("dea8ab1e-27c7-45d9-97d6-49fb38ad430b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hydrospace@apple.net", "0889873544", new Guid("08480d86-e2dd-477a-9c8f-8b2519b2b88f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Talon@apple.bg", "0884191747", new Guid("7d8df60b-6e82-4db8-ac6b-88558cc70915") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "BattlerBeauty@google.ru", "0883937541", new Guid("86c338f1-82b0-4032-95ab-97e8844973f8") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Delta@yahoo.bg", "0886047059", new Guid("ee2d68c8-f949-47e1-a980-782e66f2148b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Lens@google.net", "0884854354", new Guid("f1a39197-992b-4e7b-a24f-8cd72b956a6d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Spit@google.net", "0885856482", new Guid("16928d75-8fb7-4b27-8643-edfe02569f73") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Zoom@google.ru", "0883125590", new Guid("f1559b51-4715-4371-b555-7392e6d41069") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Gothichic@google.net", "0886098290", new Guid("b9ea4f41-19d6-4f9c-8051-099551f2afb2") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Purple@google.eu", "0889839390", new Guid("ece9c9d4-b886-4adb-bd44-d03f5c450ea0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Ahem@apple.ru", "0888904924", new Guid("bb3c7768-87af-418e-94ae-0a3c26ccde65") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Swag@apple.bg", "0883053304", new Guid("2e4c64de-61c1-4b82-9d0c-a1b861cbc3f6") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cuteness@abv.eu", "0887905228", new Guid("d3c282ec-075b-455d-9648-ecd00e7f5659") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "SkillDevil@apple.eu", "0883687462", new Guid("15e94227-5dd9-460b-a9ff-1eb1f8d1bdfc") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Island@abv.com", "0886803883", new Guid("d7c64b5e-9920-4195-8847-a6d176ab8a42") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0883897646", new Guid("da9b9951-0511-491e-a0a7-2d728c0cd05e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cosmosaegis@apple.bg", "0889221573", new Guid("7224add7-e108-4b51-8c39-dc8e9c065c23") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dove@apple.com", "0886785800", new Guid("79d28387-281b-4047-a9fa-953f654a3e11") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Crybaby@abv.bg", "0886516943", new Guid("4eca8b3a-1e56-457f-9ecd-2113ca0f67ac") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cyberproof@apple.com", "0889475228", new Guid("fdfa7c62-66c1-4919-a14a-1f1d1581633f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dedicated@yahoo.bg", "0889157830", new Guid("c0854e13-d42f-4553-a432-ff0aa660dca6") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Epic@abv.net", "0886911882", new Guid("3f87d566-d731-4e3d-9097-f45c5ae8bb9b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Secure@yahoo.com", "0889137122", new Guid("5803b82f-a2cf-4bd5-bdaa-6f0dc1401871") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Doppler@yahoo.bg", "0885062942", new Guid("f073e8d2-f7dc-4701-8705-7102284e59e7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Buddy@apple.bg", "0886942033", new Guid("f76ba9ac-a0a6-435a-a17c-343f03ee1cb1") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "FibBage@google.com", "0883256101", new Guid("7d5d7b23-9146-4a2f-9db7-29f27f394880") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Strawboy@google.com", "0887974591", new Guid("ca91843a-94a6-4c00-b73e-db180974039a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Piggy@apple.eu", "0885256874", new Guid("4ab65b5f-7fbd-41b0-98eb-70fe91b3dc53") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Homes@yahoo.eu", "0885195570", new Guid("d32cf6f7-f370-4a74-83bf-90ee4f778d6a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "JellyBott_om@abv.ru", "0886204643", new Guid("70b0a3c7-4dd8-4ef0-a640-6e3449c71be7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Marill@apple.eu", "0884701280", new Guid("b191d62d-7a3a-4300-8518-8ab872d01463") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Guaranteed@apple.bg", "0883530080", new Guid("319f9b28-e5f2-48e9-8896-889b1bbb237c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hardcover@google.ru", "0884814070", new Guid("35803232-db9d-411a-8604-392af6b67a4f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Warmblush@yahoo.net", "0889684972", new Guid("c847b2a3-3aa9-4132-9b86-15479a736d7b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Croncity@google.eu", "0885164323", new Guid("30688b4f-ff37-4575-aa27-9e5fbd8fedd5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Chub@yahoo.com", "0889302885", new Guid("d3e6a3ad-b61d-489a-af3c-7074c93e35a3") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Paradise@abv.com", "0883834060", new Guid("acf1328e-574d-413f-b906-28cefc7d5d8d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Complete@abv.bg", "0889060810", new Guid("d0a77464-2f45-4d38-8937-57a39f860875") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Moonlight@abv.net", "0889179171", new Guid("aa7d976b-2abe-40a6-8a4c-366fc30f041f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Unicorn@google.com", "0884402741", new Guid("a68072d4-0608-46b3-a216-a27b65a24974") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Limonad@apple.bg", "0885043942", new Guid("5061b737-4924-4819-bfc9-665d21296c4b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Peak@google.bg", "0883529291", new Guid("fae4a858-8e06-4d58-af47-a32928a9fca9") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Sly@abv.eu", "0885588298", new Guid("6ef6584c-afa0-41fa-9e2c-abfdbf2c6af5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Mercy@google.eu", "0883022857", new Guid("6952b09c-2ac3-4023-9d45-ba9a44c3a3a8") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cutie@yahoo.ru", "0889662769", new Guid("6aef0623-1a2d-4aa0-8775-897b7cdaf97d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0884385814", new Guid("bea29e98-4eeb-4003-8e60-9b1a114fb76e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Safenet@google.net", "0884976257", new Guid("79d529d2-75e4-4f0c-b425-cf2ce2dc840b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Shine@apple.ru", "0884098551", new Guid("b12d877f-eae1-4ad2-af80-a28902b0b153") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Night@apple.bg", "0885385871", new Guid("6b7447dd-3166-422b-9206-fef790265cbe") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Task@yahoo.eu", "0885519902", new Guid("6e1a4f66-af85-412b-b69e-435c6f587a79") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Septenary@google.com", "0889111210", new Guid("8afbcc50-a69a-48ac-9fc6-ffb0c774ab3a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Queenberry@yahoo.bg", "0886377754", new Guid("555dd431-65e8-4e8e-ac7e-199aa121da7e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "DrawFul@abv.eu", "0888424244", new Guid("68c99c3d-8cb1-4019-8fcb-3693affff3e9") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Proeyeroller@apple.eu", "0884576362", new Guid("cb24dd09-3250-43d3-bf52-09e1ee146e20") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Bad@apple.eu", "0888480605", new Guid("21e0306e-995e-444c-8557-fe3e1fd95028") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Grifen@yahoo.eu", "0889264718", new Guid("7f2a2abb-7aa5-43e8-b6ae-093cae581f7b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Secret@yahoo.bg", "0886197770", new Guid("66f64592-3e1f-4a83-903f-bd63c4f0f672") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Elite@apple.ru", "0885225875", new Guid("f8391863-a487-4cfb-9c6c-45cb7e744642") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Guardsmark@google.bg", "0888533494", new Guid("49de218f-7d41-4fcf-b530-ae41f5edf78d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pinkness@apple.net", "0885027293", new Guid("0bb49d68-e750-4b3f-87ff-4d7949dc67ba") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dark@apple.com", "0883637887", new Guid("e206f6fa-9ddb-4f85-a9f8-8881425ee7b0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Blu@abv.ru", "0888433064", new Guid("87f0dfb3-9f6b-4adb-8f26-3f589dcedc46") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Crashion@abv.com", "0889442739", new Guid("a9404571-c9d0-4d84-bd86-c653ebcf35a9") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Star@apple.ru", "0886024492", new Guid("0997cc56-3488-4bad-82fa-4dae5c11b47e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pink@yahoo.eu", "0886001483", new Guid("88dc5db4-e30f-4d86-9e30-a47171f04202") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Kitten@apple.eu", "0889686408", new Guid("478ce316-4c95-450d-a8f7-17b2694d48fe") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Xentrix@google.bg", "0889053056", new Guid("379b6cbd-1b3c-443d-b35a-46bae1da7e81") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hug@apple.ru", "0889021821", new Guid("e8b9a9c5-5f38-4fa4-b22a-63ac83a7da6a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pioneer@yahoo.eu", "0888303518", new Guid("5921de44-a49a-47d6-bd5f-dad7740c9450") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pro@abv.com", "0888247038", new Guid("59ebf264-d01e-4e96-acc5-6f32d9d52e07") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Alert@yahoo.com", "0884247266", new Guid("e4dce5c2-81d0-4941-aa39-c38002000ea4") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Fittofuel@google.ru", "0887610430", new Guid("e219e678-a760-4da8-bedb-e2e905e9c4e9") });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 24900m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 87100m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 31600m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 89100m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 5,
                column: "Balance",
                value: 68500m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 32400m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 85900m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 34000m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 1200m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 51300m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 36900m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 4900m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 7600m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 73200m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 21900m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 83000m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 20000m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 99000m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 69100m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 68400m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 45800m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 54800m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 26600m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 18100m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 6900m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 51400m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 57400m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 87300m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 68400m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 88400m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 76500m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 39500m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 49200m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 55400m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 55200m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 90500m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 37,
                column: "Balance",
                value: 54500m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 4600m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 40800m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 1000m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 58900m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 59000m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 43,
                column: "Balance",
                value: 47700m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 15600m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 57300m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 89600m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 99600m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 38000m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 5200m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 85800m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 51,
                column: "Balance",
                value: 30000m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 2200m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 27700m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 99400m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 12100m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 67300m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 4800m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 36600m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 58100m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 95500m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 45200m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28500m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 53900m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 69000m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 23300m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 45400m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 30100m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 68,
                column: "Balance",
                value: 66700m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 29000m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 14500m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 87000m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 86500m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 13700m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 57500m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 20100m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 88300m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 78100m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 55500m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 83800m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 55700m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 57500m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 52500m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 9300m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 14400m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 85800m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 23800m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 17600m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 78700m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 85400m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 36000m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 30400m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 24200m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 77500m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 89400m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 59500m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 50600m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 90200m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 3100m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 53300m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 56500m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 66600m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 37100m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 69400m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 19900m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 47000m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 76200m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 11800m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 3200m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 13900m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 39200m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 49300m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 34900m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 45700m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 10200m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 11700m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 65600m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 12800m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 61400m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 94500m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 10400m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 94600m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 97900m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 17700m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 7000m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 70200m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 64800m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 78600m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 78300m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 71900m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 74300m, "EUR" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "BGN", 41.000m, 58, 62, 8200m, "BGN", 8241.000m, new DateTime(2023, 7, 2, 17, 38, 44, 633, DateTimeKind.Utc).AddTicks(4937) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3100m, "SGD", 15.500m, 20, 4, 3100m, "SGD", 3115.500m, new DateTime(2023, 6, 10, 13, 24, 44, 633, DateTimeKind.Utc).AddTicks(4948) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3900m, "BGN", 19.500m, 41, 76, 3900m, "BGN", 3919.500m, new DateTime(2023, 5, 31, 17, 38, 44, 633, DateTimeKind.Utc).AddTicks(4951) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7100m, "ZAR", 35.500m, 47, 8, 7100m, "ZAR", 7135.500m, new DateTime(2023, 7, 29, 20, 25, 44, 633, DateTimeKind.Utc).AddTicks(4954) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4500m, "ZAR", 22.500m, 66, 90, 4500m, "ZAR", 4522.500m, new DateTime(2023, 6, 3, 0, 49, 44, 633, DateTimeKind.Utc).AddTicks(4964) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "USD", 45.500m, 55, 5, 9100m, "USD", 9145.500m, new DateTime(2023, 7, 1, 11, 7, 44, 633, DateTimeKind.Utc).AddTicks(4967) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "INR", 44.000m, 46, 123, 8800m, "INR", 8844.000m, new DateTime(2023, 7, 19, 23, 23, 44, 633, DateTimeKind.Utc).AddTicks(4970) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8300m, "CHF", 41.500m, 22, 18, 8300m, "CHF", 8341.500m, new DateTime(2023, 7, 10, 7, 12, 44, 633, DateTimeKind.Utc).AddTicks(4973) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, "BRL", 21.500m, 82, 122, 4300m, "BRL", 4321.500m, new DateTime(2023, 7, 30, 21, 9, 44, 633, DateTimeKind.Utc).AddTicks(4975) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "BRL", 41.000m, 74, 123, 8200m, "BRL", 8241.000m, new DateTime(2023, 7, 3, 22, 41, 44, 633, DateTimeKind.Utc).AddTicks(4979) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 7000m, 35.000m, 27, 31, 7000m, 7035.000m, new DateTime(2023, 6, 17, 15, 54, 44, 633, DateTimeKind.Utc).AddTicks(4981) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7900m, "BRL", 39.500m, 10, 22, 7900m, "BRL", 7939.500m, new DateTime(2023, 6, 14, 23, 31, 44, 633, DateTimeKind.Utc).AddTicks(4984) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5400m, "SGD", 27.000m, 120, 78, 5400m, "SGD", 5427.000m, new DateTime(2023, 5, 25, 6, 7, 44, 633, DateTimeKind.Utc).AddTicks(4986) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "INR", 45.500m, 39, 70, 9100m, "INR", 9145.500m, new DateTime(2023, 7, 18, 21, 43, 44, 633, DateTimeKind.Utc).AddTicks(4989) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7500m, "EUR", 37.500m, 61, 96, 7500m, "EUR", 7537.500m, new DateTime(2023, 7, 29, 1, 22, 44, 633, DateTimeKind.Utc).AddTicks(4991) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7300m, "EUR", 36.500m, 118, 113, 7300m, "EUR", 7336.500m, new DateTime(2023, 7, 2, 23, 53, 44, 633, DateTimeKind.Utc).AddTicks(4994) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7300m, "EUR", 36.500m, 54, 19, 7300m, "EUR", 7336.500m, new DateTime(2023, 6, 26, 21, 36, 44, 633, DateTimeKind.Utc).AddTicks(4997) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 800m, "JPY", 4.000m, 62, 86, 800m, "JPY", 804.000m, new DateTime(2023, 6, 12, 22, 8, 44, 633, DateTimeKind.Utc).AddTicks(5000) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "INR", 22.000m, 9, 31, 4400m, "INR", 4422.000m, new DateTime(2023, 6, 12, 4, 43, 44, 633, DateTimeKind.Utc).AddTicks(5002) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8700m, "RUB", 43.500m, 8, 18, 8700m, "RUB", 8743.500m, new DateTime(2023, 7, 8, 7, 44, 44, 633, DateTimeKind.Utc).AddTicks(5005) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9600m, "ZAR", 48.000m, 27, 70, 9600m, "ZAR", 9648.000m, new DateTime(2023, 7, 2, 3, 41, 44, 633, DateTimeKind.Utc).AddTicks(5008) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "INR", 29.000m, 5, 30, 5800m, "INR", 5829.000m, new DateTime(2023, 6, 16, 7, 3, 44, 633, DateTimeKind.Utc).AddTicks(5010) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3600m, "AUD", 18.000m, 34, 54, 3600m, "AUD", 3618.000m, new DateTime(2023, 8, 15, 12, 31, 44, 633, DateTimeKind.Utc).AddTicks(5012) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6700m, "RUB", 33.500m, 15, 30, 6700m, "RUB", 6733.500m, new DateTime(2023, 8, 8, 1, 26, 44, 633, DateTimeKind.Utc).AddTicks(5015) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "JPY", 2.000m, 62, 43, 400m, "JPY", 402.000m, new DateTime(2023, 6, 27, 10, 41, 44, 633, DateTimeKind.Utc).AddTicks(5018) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "INR", 38.000m, 21, 126, 7600m, "INR", 7638.000m, new DateTime(2023, 7, 10, 3, 56, 44, 633, DateTimeKind.Utc).AddTicks(5020) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2400m, "EUR", 12.000m, 85, 64, 2400m, "EUR", 2412.000m, new DateTime(2023, 7, 21, 18, 18, 44, 633, DateTimeKind.Utc).AddTicks(5022) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4000m, "CHF", 20.000m, 101, 4, 4000m, "CHF", 4020.000m, new DateTime(2023, 6, 18, 16, 26, 44, 633, DateTimeKind.Utc).AddTicks(5025) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "CNY", 45.500m, 88, 100, 9100m, "CNY", 9145.500m, new DateTime(2023, 6, 20, 8, 56, 44, 633, DateTimeKind.Utc).AddTicks(5028) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7200m, "EUR", 36.000m, 32, 72, 7200m, "EUR", 7236.000m, new DateTime(2023, 6, 29, 5, 59, 44, 633, DateTimeKind.Utc).AddTicks(5030) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7000m, "CNY", 35.000m, 127, 38, 7000m, "CNY", 7035.000m, new DateTime(2023, 5, 21, 23, 49, 44, 633, DateTimeKind.Utc).AddTicks(5032) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 600m, "CNY", 3.000m, 88, 39, 600m, "CNY", 603.000m, new DateTime(2023, 6, 17, 19, 31, 44, 633, DateTimeKind.Utc).AddTicks(5034) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6500m, "USD", 32.500m, 79, 52, 6500m, "USD", 6532.500m, new DateTime(2023, 5, 30, 18, 49, 44, 633, DateTimeKind.Utc).AddTicks(5038) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3200m, "BGN", 16.000m, 126, 33, 3200m, "BGN", 3216.000m, new DateTime(2023, 7, 30, 10, 49, 44, 633, DateTimeKind.Utc).AddTicks(5041) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "USD", 41.000m, 19, 4, 8200m, "USD", 8241.000m, new DateTime(2023, 6, 21, 10, 21, 44, 633, DateTimeKind.Utc).AddTicks(5043) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5500m, "JPY", 27.500m, 82, 94, 5500m, "JPY", 5527.500m, new DateTime(2023, 7, 7, 21, 3, 44, 633, DateTimeKind.Utc).AddTicks(5046) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9500m, "NZD", 47.500m, 114, 46, 9500m, "NZD", 9547.500m, new DateTime(2023, 6, 25, 10, 49, 44, 633, DateTimeKind.Utc).AddTicks(5048) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "INR", 8.500m, 64, 9, 1700m, "INR", 1708.500m, new DateTime(2023, 6, 3, 16, 2, 44, 633, DateTimeKind.Utc).AddTicks(5050) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5600m, "JPY", 28.000m, 52, 1, 5600m, "JPY", 5628.000m, new DateTime(2023, 7, 29, 4, 20, 44, 633, DateTimeKind.Utc).AddTicks(5053) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "NZD", 6.000m, 6, 23, 1200m, "NZD", 1206.000m, new DateTime(2023, 7, 17, 11, 2, 44, 633, DateTimeKind.Utc).AddTicks(5056) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "RUB", 34.000m, 48, 87, 6800m, "RUB", 6834.000m, new DateTime(2023, 6, 7, 16, 19, 44, 633, DateTimeKind.Utc).AddTicks(5058) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9600m, "AUD", 48.000m, 62, 79, 9600m, "AUD", 9648.000m, new DateTime(2023, 7, 12, 14, 47, 44, 633, DateTimeKind.Utc).AddTicks(5061) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2900m, "BGN", 14.500m, 86, 29, 2900m, "BGN", 2914.500m, new DateTime(2023, 7, 7, 5, 52, 44, 633, DateTimeKind.Utc).AddTicks(5064) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "EUR", 29.000m, 27, 113, 5800m, "EUR", 5829.000m, new DateTime(2023, 8, 8, 20, 6, 44, 633, DateTimeKind.Utc).AddTicks(5066) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8900m, "USD", 44.500m, 6, 68, 8900m, "USD", 8944.500m, new DateTime(2023, 7, 12, 20, 25, 44, 633, DateTimeKind.Utc).AddTicks(5068) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "BRL", 11.500m, 19, 5, 2300m, "BRL", 2311.500m, new DateTime(2023, 6, 30, 1, 57, 44, 633, DateTimeKind.Utc).AddTicks(5071) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4600m, "BGN", 23.000m, 42, 57, 4600m, "BGN", 4623.000m, new DateTime(2023, 7, 29, 0, 11, 44, 633, DateTimeKind.Utc).AddTicks(5073) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7500m, "INR", 37.500m, 8, 69, 7500m, "INR", 7537.500m, new DateTime(2023, 5, 28, 5, 8, 44, 633, DateTimeKind.Utc).AddTicks(5076) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5600m, "JPY", 28.000m, 109, 94, 5600m, "JPY", 5628.000m, new DateTime(2023, 8, 12, 22, 8, 44, 633, DateTimeKind.Utc).AddTicks(5083) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8100m, "AUD", 40.500m, 63, 36, 8100m, "AUD", 8140.500m, new DateTime(2023, 5, 29, 12, 24, 44, 633, DateTimeKind.Utc).AddTicks(5086) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "GBP", 25.000m, 78, 96, 5000m, "GBP", 5025.000m, new DateTime(2023, 7, 22, 19, 2, 44, 633, DateTimeKind.Utc).AddTicks(5089) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "GBP", 30.500m, 28, 49, 6100m, "GBP", 6130.500m, new DateTime(2023, 7, 8, 8, 28, 44, 633, DateTimeKind.Utc).AddTicks(5091) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "EUR", 20.500m, 70, 76, 4100m, "EUR", 4120.500m, new DateTime(2023, 7, 14, 23, 13, 44, 633, DateTimeKind.Utc).AddTicks(5093) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "JPY", 45.500m, 4, 81, 9100m, "JPY", 9145.500m, new DateTime(2023, 8, 9, 21, 11, 44, 633, DateTimeKind.Utc).AddTicks(5096) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3200m, "CNY", 16.000m, 104, 52, 3200m, "CNY", 3216.000m, new DateTime(2023, 7, 4, 7, 14, 44, 633, DateTimeKind.Utc).AddTicks(5098) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7700m, "CNY", 38.500m, 6, 32, 7700m, "CNY", 7738.500m, new DateTime(2023, 8, 4, 10, 43, 44, 633, DateTimeKind.Utc).AddTicks(5100) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9300m, "BGN", 46.500m, 31, 91, 9300m, "BGN", 9346.500m, new DateTime(2023, 6, 3, 7, 41, 44, 633, DateTimeKind.Utc).AddTicks(5103) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8000m, "JPY", 40.000m, 96, 22, 8000m, "JPY", 8040.000m, new DateTime(2023, 6, 15, 7, 13, 44, 633, DateTimeKind.Utc).AddTicks(5105) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8000m, "CAD", 40.000m, 116, 111, 8000m, "CAD", 8040.000m, new DateTime(2023, 7, 21, 5, 25, 44, 633, DateTimeKind.Utc).AddTicks(5108) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2800m, "RUB", 14.000m, 83, 73, 2800m, "RUB", 2814.000m, new DateTime(2023, 6, 11, 6, 10, 44, 633, DateTimeKind.Utc).AddTicks(5110) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6500m, "NZD", 32.500m, 80, 43, 6500m, "NZD", 6532.500m, new DateTime(2023, 7, 6, 3, 21, 44, 633, DateTimeKind.Utc).AddTicks(5113) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3700m, "ZAR", 18.500m, 96, 57, 3700m, "ZAR", 3718.500m, new DateTime(2023, 6, 10, 11, 1, 44, 633, DateTimeKind.Utc).AddTicks(5116) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9300m, "ZAR", 46.500m, 107, 101, 9300m, "ZAR", 9346.500m, new DateTime(2023, 6, 14, 11, 24, 44, 633, DateTimeKind.Utc).AddTicks(5118) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 500m, "CHF", 2.500m, 27, 70, 500m, "CHF", 502.500m, new DateTime(2023, 5, 25, 9, 2, 44, 633, DateTimeKind.Utc).AddTicks(5121) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1300m, "EUR", 6.500m, 42, 5, 1300m, "EUR", 1306.500m, new DateTime(2023, 7, 12, 23, 43, 44, 633, DateTimeKind.Utc).AddTicks(5124) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3100m, "CNY", 15.500m, 107, 10, 3100m, "CNY", 3115.500m, new DateTime(2023, 6, 15, 21, 17, 44, 633, DateTimeKind.Utc).AddTicks(5128) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "BRL", 25.000m, 35, 26, 5000m, "BRL", 5025.000m, new DateTime(2023, 6, 8, 20, 40, 44, 633, DateTimeKind.Utc).AddTicks(5130) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "INR", 30.500m, 76, 37, 6100m, "INR", 6130.500m, new DateTime(2023, 7, 25, 19, 13, 44, 633, DateTimeKind.Utc).AddTicks(5133) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "AUD", 30.500m, 48, 64, 6100m, "AUD", 6130.500m, new DateTime(2023, 6, 14, 14, 2, 44, 633, DateTimeKind.Utc).AddTicks(5135) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5400m, "CNY", 27.000m, 16, 48, 5400m, "CNY", 5427.000m, new DateTime(2023, 8, 9, 15, 33, 44, 633, DateTimeKind.Utc).AddTicks(5138) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2000m, "CHF", 10.000m, 64, 104, 2000m, "CHF", 2010.000m, new DateTime(2023, 7, 17, 5, 33, 44, 633, DateTimeKind.Utc).AddTicks(5140) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4800m, "SGD", 24.000m, 31, 11, 4800m, "SGD", 4824.000m, new DateTime(2023, 7, 15, 7, 58, 44, 633, DateTimeKind.Utc).AddTicks(5143) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "BGN", 92, 33, "BGN", new DateTime(2023, 7, 20, 9, 16, 44, 633, DateTimeKind.Utc).AddTicks(5145) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4500m, "CHF", 22.500m, 95, 114, 4500m, "CHF", 4522.500m, new DateTime(2023, 8, 10, 14, 25, 44, 633, DateTimeKind.Utc).AddTicks(5147) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "SGD", 38.000m, 95, 80, 7600m, "SGD", 7638.000m, new DateTime(2023, 7, 21, 7, 27, 44, 633, DateTimeKind.Utc).AddTicks(5150) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "BGN", 20.500m, 109, 74, 4100m, "BGN", 4120.500m, new DateTime(2023, 5, 31, 6, 19, 44, 633, DateTimeKind.Utc).AddTicks(5152) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6400m, "RUB", 32.000m, 20, 6400m, "RUB", 6432.000m, new DateTime(2023, 6, 4, 0, 19, 44, 633, DateTimeKind.Utc).AddTicks(5155) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5100m, "EUR", 25.500m, 23, 129, 5100m, "EUR", 5125.500m, new DateTime(2023, 6, 2, 11, 15, 44, 633, DateTimeKind.Utc).AddTicks(5157) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1300m, "BGN", 6.500m, 120, 25, 1300m, "BGN", 1306.500m, new DateTime(2023, 7, 17, 3, 55, 44, 633, DateTimeKind.Utc).AddTicks(5159) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "BGN", 15.000m, 70, 101, 3000m, "BGN", 3015.000m, new DateTime(2023, 6, 24, 2, 36, 44, 633, DateTimeKind.Utc).AddTicks(5162) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "BGN", 8.500m, 92, 37, 1700m, "BGN", 1708.500m, new DateTime(2023, 8, 16, 8, 43, 44, 633, DateTimeKind.Utc).AddTicks(5164) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6700m, "BRL", 33.500m, 25, 53, 6700m, "BRL", 6733.500m, new DateTime(2023, 7, 6, 6, 20, 44, 633, DateTimeKind.Utc).AddTicks(5166) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 900m, "RUB", 4.500m, 15, 122, 900m, "RUB", 904.500m, new DateTime(2023, 7, 10, 19, 42, 44, 633, DateTimeKind.Utc).AddTicks(5169) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1900m, "SGD", 9.500m, 85, 50, 1900m, "SGD", 1909.500m, new DateTime(2023, 6, 28, 14, 14, 44, 633, DateTimeKind.Utc).AddTicks(5171) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3700m, "EUR", 18.500m, 28, 16, 3700m, "EUR", 3718.500m, new DateTime(2023, 7, 12, 4, 52, 44, 633, DateTimeKind.Utc).AddTicks(5174) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 4500m, 22.500m, 108, 19, 4500m, 4522.500m, new DateTime(2023, 7, 31, 21, 45, 44, 633, DateTimeKind.Utc).AddTicks(5176) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8100m, "JPY", 40.500m, 82, 108, 8100m, "JPY", 8140.500m, new DateTime(2023, 6, 28, 5, 3, 44, 633, DateTimeKind.Utc).AddTicks(5179) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "GBP", 5.500m, 15, 12, 1100m, "GBP", 1105.500m, new DateTime(2023, 7, 27, 10, 58, 44, 633, DateTimeKind.Utc).AddTicks(5181) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9600m, "BRL", 48.000m, 9, 4, 9600m, "BRL", 9648.000m, new DateTime(2023, 8, 13, 1, 44, 44, 633, DateTimeKind.Utc).AddTicks(5184) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "GBP", 42.500m, 19, 88, 8500m, "GBP", 8542.500m, new DateTime(2023, 6, 18, 11, 46, 44, 633, DateTimeKind.Utc).AddTicks(5186) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8600m, "CHF", 43.000m, 124, 20, 8600m, "CHF", 8643.000m, new DateTime(2023, 7, 8, 13, 0, 44, 633, DateTimeKind.Utc).AddTicks(5188) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 900m, "BGN", 4.500m, 89, 36, 900m, "BGN", 904.500m, new DateTime(2023, 8, 15, 4, 20, 44, 633, DateTimeKind.Utc).AddTicks(5191) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9000m, "BRL", 45.000m, 120, 91, 9000m, "BRL", 9045.000m, new DateTime(2023, 8, 13, 9, 44, 44, 633, DateTimeKind.Utc).AddTicks(5202) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4000m, "CHF", 20.000m, 7, 128, 4000m, "CHF", 4020.000m, new DateTime(2023, 6, 7, 4, 6, 44, 633, DateTimeKind.Utc).AddTicks(5204) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8600m, "CNY", 43.000m, 53, 21, 8600m, "CNY", 8643.000m, new DateTime(2023, 6, 1, 0, 50, 44, 633, DateTimeKind.Utc).AddTicks(5207) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "RUB", 25.000m, 106, 5, 5000m, "RUB", 5025.000m, new DateTime(2023, 6, 12, 15, 16, 44, 633, DateTimeKind.Utc).AddTicks(5209) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "BGN", 2.000m, 109, 95, 400m, "BGN", 402.000m, new DateTime(2023, 6, 26, 10, 49, 44, 633, DateTimeKind.Utc).AddTicks(5211) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3800m, "INR", 19.000m, 5, 70, 3800m, "INR", 3819.000m, new DateTime(2023, 7, 10, 19, 10, 44, 633, DateTimeKind.Utc).AddTicks(5214) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1900m, "SGD", 9.500m, 26, 35, 1900m, "SGD", 1909.500m, new DateTime(2023, 5, 24, 15, 4, 44, 633, DateTimeKind.Utc).AddTicks(5217) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9500m, "ZAR", 47.500m, 46, 50, 9500m, "ZAR", 9547.500m, new DateTime(2023, 8, 16, 23, 16, 44, 633, DateTimeKind.Utc).AddTicks(5219) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8900m, "SGD", 44.500m, 65, 18, 8900m, "SGD", 8944.500m, new DateTime(2023, 6, 13, 9, 41, 44, 633, DateTimeKind.Utc).AddTicks(5222) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "BRL", 42.500m, 121, 76, 8500m, "BRL", 8542.500m, new DateTime(2023, 5, 22, 7, 30, 44, 633, DateTimeKind.Utc).AddTicks(5224) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "BaseAmount", "Fees", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 7100m, 35.500m, 107, 7100m, 7135.500m, new DateTime(2023, 7, 14, 2, 11, 44, 633, DateTimeKind.Utc).AddTicks(5227) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3400m, "CNY", 17.000m, 91, 100, 3400m, "CNY", 3417.000m, new DateTime(2023, 8, 6, 5, 19, 44, 633, DateTimeKind.Utc).AddTicks(5229) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 500m, "CHF", 2.500m, 73, 70, 500m, "CHF", 502.500m, new DateTime(2023, 5, 20, 22, 36, 44, 633, DateTimeKind.Utc).AddTicks(5232) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 6700m, 33.500m, 63, 35, 6700m, 6733.500m, new DateTime(2023, 6, 26, 16, 6, 44, 633, DateTimeKind.Utc).AddTicks(5234) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 700m, "CHF", 3.500m, 8, 103, 700m, "CHF", 703.500m, new DateTime(2023, 7, 26, 22, 31, 44, 633, DateTimeKind.Utc).AddTicks(5237) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "BRL", 2.000m, 106, 51, 400m, "BRL", 402.000m, new DateTime(2023, 5, 27, 12, 51, 44, 633, DateTimeKind.Utc).AddTicks(5239) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "BGN", 42.500m, 61, 62, 8500m, "BGN", 8542.500m, new DateTime(2023, 7, 4, 23, 54, 44, 633, DateTimeKind.Utc).AddTicks(5242) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 2600m, 13.000m, 83, 100, 2600m, 2613.000m, new DateTime(2023, 5, 18, 5, 49, 44, 633, DateTimeKind.Utc).AddTicks(5244) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "CNY", 25.000m, 53, 32, 5000m, "CNY", 5025.000m, new DateTime(2023, 5, 22, 3, 6, 44, 633, DateTimeKind.Utc).AddTicks(5246) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5300m, "AUD", 26.500m, 27, 73, 5300m, "AUD", 5326.500m, new DateTime(2023, 6, 1, 2, 49, 44, 633, DateTimeKind.Utc).AddTicks(5252) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2800m, "INR", 14.000m, 47, 99, 2800m, "INR", 2814.000m, new DateTime(2023, 6, 15, 11, 13, 44, 633, DateTimeKind.Utc).AddTicks(5255) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "BGN", 20.500m, 61, 129, 4100m, "BGN", 4120.500m, new DateTime(2023, 6, 20, 10, 17, 44, 633, DateTimeKind.Utc).AddTicks(5257) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2500m, "AUD", 12.500m, 4, 16, 2500m, "AUD", 2512.500m, new DateTime(2023, 6, 24, 23, 37, 44, 633, DateTimeKind.Utc).AddTicks(5260) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "NZD", 6.000m, 111, 1200m, "NZD", 1206.000m, new DateTime(2023, 6, 18, 5, 0, 44, 633, DateTimeKind.Utc).AddTicks(5262) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6200m, "SGD", 31.000m, 11, 81, 6200m, "SGD", 6231.000m, new DateTime(2023, 8, 10, 21, 3, 44, 633, DateTimeKind.Utc).AddTicks(5265) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "CNY", 34.000m, 97, 90, 6800m, "CNY", 6834.000m, new DateTime(2023, 7, 25, 23, 55, 44, 633, DateTimeKind.Utc).AddTicks(5268) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4500m, "EUR", 22.500m, 70, 73, 4500m, "EUR", 4522.500m, new DateTime(2023, 6, 28, 7, 41, 44, 633, DateTimeKind.Utc).AddTicks(5271) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6600m, "JPY", 33.000m, 109, 32, 6600m, "JPY", 6633.000m, new DateTime(2023, 7, 7, 11, 13, 44, 633, DateTimeKind.Utc).AddTicks(5273) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "AUD", 22.000m, 72, 87, 4400m, "AUD", 4422.000m, new DateTime(2023, 6, 4, 15, 1, 44, 633, DateTimeKind.Utc).AddTicks(5275) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2000m, "NZD", 10.000m, 116, 21, 2000m, "NZD", 2010.000m, new DateTime(2023, 7, 22, 8, 20, 44, 633, DateTimeKind.Utc).AddTicks(5278) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "INR", 6.000m, 55, 125, 1200m, "INR", 1206.000m, new DateTime(2023, 7, 16, 4, 50, 44, 633, DateTimeKind.Utc).AddTicks(5280) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5900m, "BRL", 29.500m, 28, 98, 5900m, "BRL", 5929.500m, new DateTime(2023, 7, 7, 9, 20, 44, 633, DateTimeKind.Utc).AddTicks(5283) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "CHF", 5.500m, 65, 109, 1100m, "CHF", 1105.500m, new DateTime(2023, 6, 22, 19, 3, 44, 633, DateTimeKind.Utc).AddTicks(5285) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8000m, "CHF", 40.000m, 118, 91, 8000m, "CHF", 8040.000m, new DateTime(2023, 7, 13, 17, 35, 44, 633, DateTimeKind.Utc).AddTicks(5288) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "NZD", 29.000m, 96, 127, 5800m, "NZD", 5829.000m, new DateTime(2023, 7, 20, 22, 49, 44, 633, DateTimeKind.Utc).AddTicks(5290) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8400m, "GBP", 42.000m, 91, 17, 8400m, "GBP", 8442.000m, new DateTime(2023, 8, 10, 0, 58, 44, 633, DateTimeKind.Utc).AddTicks(5293) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7200m, "CNY", 36.000m, 102, 111, 7200m, "CNY", 7236.000m, new DateTime(2023, 6, 15, 2, 13, 44, 633, DateTimeKind.Utc).AddTicks(5295) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1800m, "RUB", 9.000m, 26, 88, 1800m, "RUB", 1809.000m, new DateTime(2023, 6, 4, 3, 55, 44, 633, DateTimeKind.Utc).AddTicks(5299) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6600m, "BRL", 33.000m, 113, 11, 6600m, "BRL", 6633.000m, new DateTime(2023, 8, 1, 22, 12, 44, 633, DateTimeKind.Utc).AddTicks(5301) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5500m, "EUR", 27.500m, 99, 118, 5500m, "EUR", 5527.500m, new DateTime(2023, 7, 20, 2, 31, 44, 633, DateTimeKind.Utc).AddTicks(5311) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8600m, "JPY", 43.000m, 111, 86, 8600m, "JPY", 8643.000m, new DateTime(2023, 6, 20, 0, 26, 44, 633, DateTimeKind.Utc).AddTicks(5314) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "ZAR", 41.000m, 120, 89, 8200m, "ZAR", 8241.000m, new DateTime(2023, 5, 28, 8, 42, 44, 633, DateTimeKind.Utc).AddTicks(5316) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2000m, "CNY", 10.000m, 125, 43, 2000m, "CNY", 2010.000m, new DateTime(2023, 6, 20, 12, 3, 44, 633, DateTimeKind.Utc).AddTicks(5318) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4900m, "BRL", 24.500m, 20, 23, 4900m, "BRL", 4924.500m, new DateTime(2023, 8, 10, 15, 41, 44, 633, DateTimeKind.Utc).AddTicks(5321) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3200m, "BGN", 16.000m, 34, 21, 3200m, "BGN", 3216.000m, new DateTime(2023, 6, 15, 7, 13, 44, 633, DateTimeKind.Utc).AddTicks(5323) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "ZAR", 9, 37, "ZAR", new DateTime(2023, 7, 10, 19, 11, 44, 633, DateTimeKind.Utc).AddTicks(5326) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 4700m, 23.500m, 35, 34, 4700m, 4723.500m, new DateTime(2023, 5, 31, 6, 54, 44, 633, DateTimeKind.Utc).AddTicks(5328) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "JPY", 15.000m, 4, 123, 3000m, "JPY", 3015.000m, new DateTime(2023, 7, 20, 2, 47, 44, 633, DateTimeKind.Utc).AddTicks(5331) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7800m, "JPY", 39.000m, 99, 121, 7800m, "JPY", 7839.000m, new DateTime(2023, 7, 2, 0, 16, 44, 633, DateTimeKind.Utc).AddTicks(5333) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6300m, "AUD", 31.500m, 3, 120, 6300m, "AUD", 6331.500m, new DateTime(2023, 7, 19, 1, 8, 44, 633, DateTimeKind.Utc).AddTicks(5336) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "EUR", 11.500m, 119, 20, 2300m, "EUR", 2311.500m, new DateTime(2023, 6, 16, 8, 33, 44, 633, DateTimeKind.Utc).AddTicks(5339) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2500m, "ZAR", 12.500m, 65, 128, 2500m, "ZAR", 2512.500m, new DateTime(2023, 7, 20, 12, 17, 44, 633, DateTimeKind.Utc).AddTicks(5341) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5300m, "BGN", 26.500m, 121, 115, 5300m, "BGN", 5326.500m, new DateTime(2023, 7, 13, 1, 25, 44, 633, DateTimeKind.Utc).AddTicks(5344) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "NZD", 29.000m, 56, 27, 5800m, "NZD", 5829.000m, new DateTime(2023, 6, 10, 10, 39, 44, 633, DateTimeKind.Utc).AddTicks(5346) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4200m, "BGN", 21.000m, 90, 111, 4200m, "BGN", 4221.000m, new DateTime(2023, 6, 22, 9, 28, 44, 633, DateTimeKind.Utc).AddTicks(5349) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6400m, "RUB", 32.000m, 64, 72, 6400m, "RUB", 6432.000m, new DateTime(2023, 7, 27, 7, 12, 44, 633, DateTimeKind.Utc).AddTicks(5351) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6400m, "ZAR", 32.000m, 115, 66, 6400m, "ZAR", 6432.000m, new DateTime(2023, 6, 3, 19, 48, 44, 633, DateTimeKind.Utc).AddTicks(5354) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7200m, "ZAR", 36.000m, 85, 51, 7200m, "ZAR", 7236.000m, new DateTime(2023, 5, 24, 23, 47, 44, 633, DateTimeKind.Utc).AddTicks(5356) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 151,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 900m, "JPY", 4.500m, 121, 19, 900m, "JPY", 904.500m, new DateTime(2023, 8, 10, 23, 44, 44, 633, DateTimeKind.Utc).AddTicks(5358) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 152,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4800m, "GBP", 24.000m, 47, 69, 4800m, "GBP", 4824.000m, new DateTime(2023, 7, 3, 18, 11, 44, 633, DateTimeKind.Utc).AddTicks(5361) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 153,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, "USD", 21.500m, 106, 62, 4300m, "USD", 4321.500m, new DateTime(2023, 5, 22, 13, 30, 44, 633, DateTimeKind.Utc).AddTicks(5364) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 154,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "EUR", 34.000m, 79, 111, 6800m, "EUR", 6834.000m, new DateTime(2023, 5, 27, 21, 48, 44, 633, DateTimeKind.Utc).AddTicks(5366) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 155,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "USD", 25.000m, 2, 38, 5000m, "USD", 5025.000m, new DateTime(2023, 7, 6, 10, 19, 44, 633, DateTimeKind.Utc).AddTicks(5369) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 156,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5100m, "BGN", 25.500m, 53, 21, 5100m, "BGN", 5125.500m, new DateTime(2023, 7, 17, 21, 36, 44, 633, DateTimeKind.Utc).AddTicks(5371) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 157,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3600m, "USD", 18.000m, 18, 128, 3600m, "USD", 3618.000m, new DateTime(2023, 8, 4, 22, 8, 44, 633, DateTimeKind.Utc).AddTicks(5373) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 158,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, 21.500m, 47, 22, 4300m, 4321.500m, new DateTime(2023, 6, 21, 15, 39, 44, 633, DateTimeKind.Utc).AddTicks(5376) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 159,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, 29.000m, 50, 56, 5800m, 5829.000m, new DateTime(2023, 8, 12, 3, 20, 44, 633, DateTimeKind.Utc).AddTicks(5378) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 160,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1600m, "GBP", 8.000m, 119, 77, 1600m, "GBP", 1608.000m, new DateTime(2023, 5, 29, 11, 0, 44, 633, DateTimeKind.Utc).AddTicks(5380) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 161,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8400m, "AUD", 42.000m, 60, 33, 8400m, "AUD", 8442.000m, new DateTime(2023, 5, 19, 18, 10, 44, 633, DateTimeKind.Utc).AddTicks(5383) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 162,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3800m, "SGD", 19.000m, 103, 36, 3800m, "SGD", 3819.000m, new DateTime(2023, 8, 12, 0, 46, 44, 633, DateTimeKind.Utc).AddTicks(5385) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 163,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4900m, "AUD", 24.500m, 35, 71, 4900m, "AUD", 4924.500m, new DateTime(2023, 7, 25, 4, 29, 44, 633, DateTimeKind.Utc).AddTicks(5388) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 164,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4900m, "BGN", 24.500m, 55, 26, 4900m, "BGN", 4924.500m, new DateTime(2023, 7, 30, 18, 3, 44, 633, DateTimeKind.Utc).AddTicks(5391) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 165,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "SGD", 38.000m, 16, 124, 7600m, "SGD", 7638.000m, new DateTime(2023, 6, 17, 23, 44, 44, 633, DateTimeKind.Utc).AddTicks(5394) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 166,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7000m, "CNY", 35.000m, 18, 35, 7000m, "CNY", 7035.000m, new DateTime(2023, 6, 9, 20, 8, 44, 633, DateTimeKind.Utc).AddTicks(5396) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 167,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "BRL", 22.000m, 65, 117, 4400m, "BRL", 4422.000m, new DateTime(2023, 5, 26, 5, 36, 44, 633, DateTimeKind.Utc).AddTicks(5399) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 168,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, "SGD", 21.500m, 69, 57, 4300m, "SGD", 4321.500m, new DateTime(2023, 8, 3, 7, 5, 44, 633, DateTimeKind.Utc).AddTicks(5401) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 169,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8300m, "SGD", 41.500m, 73, 85, 8300m, "SGD", 8341.500m, new DateTime(2023, 6, 24, 23, 27, 44, 633, DateTimeKind.Utc).AddTicks(5404) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 170,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1900m, "AUD", 9.500m, 78, 1, 1900m, "AUD", 1909.500m, new DateTime(2023, 7, 5, 18, 27, 44, 633, DateTimeKind.Utc).AddTicks(5406) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 171,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "EUR", 17.500m, 63, 34, 3500m, "EUR", 3517.500m, new DateTime(2023, 5, 30, 0, 41, 44, 633, DateTimeKind.Utc).AddTicks(5409) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 172,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1000m, "INR", 5.000m, 53, 22, 1000m, "INR", 1005.000m, new DateTime(2023, 7, 14, 3, 34, 44, 633, DateTimeKind.Utc).AddTicks(5412) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 173,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3600m, "JPY", 18.000m, 112, 37, 3600m, "JPY", 3618.000m, new DateTime(2023, 7, 5, 3, 48, 44, 633, DateTimeKind.Utc).AddTicks(5414) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 174,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "USD", 45.500m, 94, 25, 9100m, "USD", 9145.500m, new DateTime(2023, 7, 30, 7, 20, 44, 633, DateTimeKind.Utc).AddTicks(5417) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 175,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, 6.000m, 75, 85, 1200m, 1206.000m, new DateTime(2023, 5, 29, 20, 45, 44, 633, DateTimeKind.Utc).AddTicks(5419) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 176,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3400m, "CNY", 17.000m, 28, 15, 3400m, "CNY", 3417.000m, new DateTime(2023, 6, 13, 1, 34, 44, 633, DateTimeKind.Utc).AddTicks(5421) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 177,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "ZAR", 34.000m, 94, 4, 6800m, "ZAR", 6834.000m, new DateTime(2023, 7, 29, 22, 37, 44, 633, DateTimeKind.Utc).AddTicks(5424) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 178,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "ZAR", 42.500m, 42, 18, 8500m, "ZAR", 8542.500m, new DateTime(2023, 8, 5, 17, 9, 44, 633, DateTimeKind.Utc).AddTicks(5426) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 179,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1000m, "INR", 5.000m, 56, 14, 1000m, "INR", 1005.000m, new DateTime(2023, 6, 12, 7, 34, 44, 633, DateTimeKind.Utc).AddTicks(5428) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 180,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "NZD", 30.500m, 119, 112, 6100m, "NZD", 6130.500m, new DateTime(2023, 6, 3, 14, 0, 44, 633, DateTimeKind.Utc).AddTicks(5431) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 181,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "BRL", 5.500m, 44, 114, 1100m, "BRL", 1105.500m, new DateTime(2023, 5, 29, 22, 7, 44, 633, DateTimeKind.Utc).AddTicks(5433) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 182,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 3300m, 16.500m, 78, 118, 3300m, 3316.500m, new DateTime(2023, 6, 18, 19, 13, 44, 633, DateTimeKind.Utc).AddTicks(5436) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 183,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4000m, "BGN", 20.000m, 103, 107, 4000m, "BGN", 4020.000m, new DateTime(2023, 7, 1, 12, 11, 44, 633, DateTimeKind.Utc).AddTicks(5446) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 184,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9200m, "BRL", 46.000m, 45, 48, 9200m, "BRL", 9246.000m, new DateTime(2023, 5, 21, 0, 47, 44, 633, DateTimeKind.Utc).AddTicks(5448) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 185,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5200m, "BGN", 26.000m, 24, 86, 5200m, "BGN", 5226.000m, new DateTime(2023, 6, 1, 11, 13, 44, 633, DateTimeKind.Utc).AddTicks(5450) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 186,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "CAD", 2.000m, 5, 127, 400m, "CAD", 402.000m, new DateTime(2023, 5, 29, 8, 54, 44, 633, DateTimeKind.Utc).AddTicks(5453) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 187,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9300m, "BRL", 46.500m, 53, 101, 9300m, "BRL", 9346.500m, new DateTime(2023, 7, 13, 20, 35, 44, 633, DateTimeKind.Utc).AddTicks(5455) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 188,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5600m, "EUR", 28.000m, 82, 4, 5600m, "EUR", 5628.000m, new DateTime(2023, 5, 30, 12, 28, 44, 633, DateTimeKind.Utc).AddTicks(5458) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 189,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 8300m, 41.500m, 101, 109, 8300m, 8341.500m, new DateTime(2023, 7, 12, 23, 50, 44, 633, DateTimeKind.Utc).AddTicks(5461) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 190,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "CHF", 11.500m, 78, 4, 2300m, "CHF", 2311.500m, new DateTime(2023, 5, 28, 17, 28, 44, 633, DateTimeKind.Utc).AddTicks(5463) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 191,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "RUB", 44.000m, 68, 90, 8800m, "RUB", 8844.000m, new DateTime(2023, 6, 5, 21, 9, 44, 633, DateTimeKind.Utc).AddTicks(5466) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 192,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "USD", 45.500m, 24, 75, 9100m, "USD", 9145.500m, new DateTime(2023, 5, 30, 16, 38, 44, 633, DateTimeKind.Utc).AddTicks(5468) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 193,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "ZAR", 15.000m, 95, 83, 3000m, "ZAR", 3015.000m, new DateTime(2023, 7, 2, 14, 9, 44, 633, DateTimeKind.Utc).AddTicks(5471) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 194,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "INR", 42.500m, 17, 67, 8500m, "INR", 8542.500m, new DateTime(2023, 6, 4, 8, 23, 44, 633, DateTimeKind.Utc).AddTicks(5473) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 195,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "CNY", 15.000m, 121, 44, 3000m, "CNY", 3015.000m, new DateTime(2023, 8, 17, 4, 11, 44, 633, DateTimeKind.Utc).AddTicks(5476) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 196,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "JPY", 22.000m, 35, 126, 4400m, "JPY", 4422.000m, new DateTime(2023, 6, 25, 7, 9, 44, 633, DateTimeKind.Utc).AddTicks(5478) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 197,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3400m, "CAD", 17.000m, 53, 90, 3400m, "CAD", 3417.000m, new DateTime(2023, 7, 25, 20, 25, 44, 633, DateTimeKind.Utc).AddTicks(5480) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 198,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2100m, "CHF", 10.500m, 19, 114, 2100m, "CHF", 2110.500m, new DateTime(2023, 6, 1, 14, 21, 44, 633, DateTimeKind.Utc).AddTicks(5483) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 199,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 9600m, 48.000m, 63, 20, 9600m, 9648.000m, new DateTime(2023, 8, 8, 13, 7, 44, 633, DateTimeKind.Utc).AddTicks(5485) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 200,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "EUR", 34.000m, 128, 2, 6800m, "EUR", 6834.000m, new DateTime(2023, 8, 4, 10, 16, 44, 633, DateTimeKind.Utc).AddTicks(5487) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 201,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "CAD", 5.500m, 109, 30, 1100m, "CAD", 1105.500m, new DateTime(2023, 5, 25, 9, 2, 44, 633, DateTimeKind.Utc).AddTicks(5490) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 202,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "RUB", 5.500m, 71, 4, 1100m, "RUB", 1105.500m, new DateTime(2023, 7, 5, 7, 16, 44, 633, DateTimeKind.Utc).AddTicks(5492) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 203,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4700m, "GBP", 23.500m, 96, 64, 4700m, "GBP", 4723.500m, new DateTime(2023, 6, 19, 9, 16, 44, 633, DateTimeKind.Utc).AddTicks(5495) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 204,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2700m, "CAD", 13.500m, 40, 88, 2700m, "CAD", 2713.500m, new DateTime(2023, 8, 1, 21, 41, 44, 633, DateTimeKind.Utc).AddTicks(5498) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 205,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8100m, "INR", 40.500m, 106, 61, 8100m, "INR", 8140.500m, new DateTime(2023, 8, 7, 17, 0, 44, 633, DateTimeKind.Utc).AddTicks(5500) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 206,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "BGN", 6.000m, 17, 42, 1200m, "BGN", 1206.000m, new DateTime(2023, 8, 3, 12, 14, 44, 633, DateTimeKind.Utc).AddTicks(5503) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 207,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5600m, "BRL", 28.000m, 79, 12, 5600m, "BRL", 5628.000m, new DateTime(2023, 6, 23, 16, 21, 44, 633, DateTimeKind.Utc).AddTicks(5505) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 208,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "NZD", 20.500m, 91, 105, 4100m, "NZD", 4120.500m, new DateTime(2023, 6, 16, 12, 58, 44, 633, DateTimeKind.Utc).AddTicks(5508) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 209,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1300m, "BGN", 6.500m, 4, 128, 1300m, "BGN", 1306.500m, new DateTime(2023, 7, 9, 5, 40, 44, 633, DateTimeKind.Utc).AddTicks(5510) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 210,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 1300m, 6.500m, 22, 90, 1300m, 1306.500m, new DateTime(2023, 7, 17, 5, 25, 44, 633, DateTimeKind.Utc).AddTicks(5513) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 211,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4700m, "AUD", 23.500m, 7, 34, 4700m, "AUD", 4723.500m, new DateTime(2023, 6, 13, 23, 17, 44, 633, DateTimeKind.Utc).AddTicks(5515) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 212,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9000m, "JPY", 45.000m, 65, 51, 9000m, "JPY", 9045.000m, new DateTime(2023, 8, 14, 1, 58, 44, 633, DateTimeKind.Utc).AddTicks(5518) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 213,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 200m, "GBP", 1.000m, 71, 80, 200m, "GBP", 201.000m, new DateTime(2023, 7, 13, 23, 52, 44, 633, DateTimeKind.Utc).AddTicks(5520) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 214,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8000m, "NZD", 40.000m, 75, 8, 8000m, "NZD", 8040.000m, new DateTime(2023, 6, 10, 8, 34, 44, 633, DateTimeKind.Utc).AddTicks(5523) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 215,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "SGD", 38.000m, 31, 67, 7600m, "SGD", 7638.000m, new DateTime(2023, 6, 16, 4, 50, 44, 633, DateTimeKind.Utc).AddTicks(5525) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 216,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5600m, "NZD", 28.000m, 51, 37, 5600m, "NZD", 5628.000m, new DateTime(2023, 6, 9, 10, 14, 44, 633, DateTimeKind.Utc).AddTicks(5528) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 217,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 900m, "JPY", 4.500m, 30, 65, 900m, "JPY", 904.500m, new DateTime(2023, 5, 29, 11, 27, 44, 633, DateTimeKind.Utc).AddTicks(5530) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 218,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4700m, "USD", 23.500m, 26, 110, 4700m, "USD", 4723.500m, new DateTime(2023, 7, 15, 18, 53, 44, 633, DateTimeKind.Utc).AddTicks(5532) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 219,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 700m, "RUB", 3.500m, 27, 80, 700m, "RUB", 703.500m, new DateTime(2023, 7, 4, 21, 9, 44, 633, DateTimeKind.Utc).AddTicks(5535) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 220,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 700m, "ZAR", 3.500m, 12, 84, 700m, "ZAR", 703.500m, new DateTime(2023, 7, 10, 1, 18, 44, 633, DateTimeKind.Utc).AddTicks(5537) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 221,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7300m, "AUD", 36.500m, 90, 86, 7300m, "AUD", 7336.500m, new DateTime(2023, 7, 9, 14, 45, 44, 633, DateTimeKind.Utc).AddTicks(5540) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 222,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "CHF", 42.500m, 45, 44, 8500m, "CHF", 8542.500m, new DateTime(2023, 7, 28, 2, 22, 44, 633, DateTimeKind.Utc).AddTicks(5542) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 223,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6300m, "CNY", 31.500m, 120, 88, 6300m, "CNY", 6331.500m, new DateTime(2023, 8, 11, 6, 5, 44, 633, DateTimeKind.Utc).AddTicks(5545) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 224,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "USD", 11.500m, 10, 75, 2300m, "USD", 2311.500m, new DateTime(2023, 7, 23, 2, 11, 44, 633, DateTimeKind.Utc).AddTicks(5547) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 225,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2200m, "BRL", 11.000m, 93, 43, 2200m, "BRL", 2211.000m, new DateTime(2023, 8, 15, 20, 56, 44, 633, DateTimeKind.Utc).AddTicks(5550) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 226,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8100m, "CNY", 40.500m, 19, 79, 8100m, "CNY", 8140.500m, new DateTime(2023, 8, 1, 9, 27, 44, 633, DateTimeKind.Utc).AddTicks(5552) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 227,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9000m, "ZAR", 45.000m, 39, 21, 9000m, "ZAR", 9045.000m, new DateTime(2023, 6, 20, 1, 18, 44, 633, DateTimeKind.Utc).AddTicks(5555) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 228,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "BRL", 84, 14, "BRL", new DateTime(2023, 7, 14, 12, 58, 44, 633, DateTimeKind.Utc).AddTicks(5557) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 229,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "INR", 5.500m, 64, 70, 1100m, "INR", 1105.500m, new DateTime(2023, 8, 7, 0, 17, 44, 633, DateTimeKind.Utc).AddTicks(5560) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 230,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 600m, "RUB", 3.000m, 89, 122, 600m, "RUB", 603.000m, new DateTime(2023, 8, 11, 23, 34, 44, 633, DateTimeKind.Utc).AddTicks(5562) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 231,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "BGN", 38.000m, 19, 100, 7600m, "BGN", 7638.000m, new DateTime(2023, 7, 28, 19, 42, 44, 633, DateTimeKind.Utc).AddTicks(5565) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 232,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "JPY", 17.500m, 69, 45, 3500m, "JPY", 3517.500m, new DateTime(2023, 7, 24, 17, 34, 44, 633, DateTimeKind.Utc).AddTicks(5567) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 233,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 500m, "GBP", 2.500m, 90, 129, 500m, "GBP", 502.500m, new DateTime(2023, 6, 19, 23, 37, 44, 633, DateTimeKind.Utc).AddTicks(5570) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 234,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "CAD", 8.500m, 65, 4, 1700m, "CAD", 1708.500m, new DateTime(2023, 8, 2, 23, 54, 44, 633, DateTimeKind.Utc).AddTicks(5579) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 235,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 900m, "CHF", 4.500m, 100, 64, 900m, "CHF", 904.500m, new DateTime(2023, 5, 19, 4, 46, 44, 633, DateTimeKind.Utc).AddTicks(5582) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 236,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1300m, "ZAR", 6.500m, 127, 31, 1300m, "ZAR", 1306.500m, new DateTime(2023, 8, 2, 0, 58, 44, 633, DateTimeKind.Utc).AddTicks(5584) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 237,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, 11.500m, 97, 17, 2300m, 2311.500m, new DateTime(2023, 6, 18, 15, 3, 44, 633, DateTimeKind.Utc).AddTicks(5587) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 238,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5500m, "EUR", 27.500m, 63, 118, 5500m, "EUR", 5527.500m, new DateTime(2023, 7, 17, 10, 8, 44, 633, DateTimeKind.Utc).AddTicks(5589) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 239,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5700m, "RUB", 28.500m, 27, 92, 5700m, "RUB", 5728.500m, new DateTime(2023, 6, 26, 19, 49, 44, 633, DateTimeKind.Utc).AddTicks(5591) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 240,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7800m, "AUD", 39.000m, 51, 93, 7800m, "AUD", 7839.000m, new DateTime(2023, 6, 6, 14, 19, 44, 633, DateTimeKind.Utc).AddTicks(5594) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 241,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "ZAR", 42.500m, 29, 31, 8500m, "ZAR", 8542.500m, new DateTime(2023, 5, 29, 23, 34, 44, 633, DateTimeKind.Utc).AddTicks(5597) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 242,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 5500m, 27.500m, 32, 124, 5500m, 5527.500m, new DateTime(2023, 6, 23, 17, 15, 44, 633, DateTimeKind.Utc).AddTicks(5600) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 243,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5100m, "SGD", 25.500m, 18, 13, 5100m, "SGD", 5125.500m, new DateTime(2023, 6, 27, 19, 0, 44, 633, DateTimeKind.Utc).AddTicks(5602) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 244,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "AUD", 42.500m, 58, 97, 8500m, "AUD", 8542.500m, new DateTime(2023, 7, 23, 19, 59, 44, 633, DateTimeKind.Utc).AddTicks(5605) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 245,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "CAD", 11.500m, 22, 45, 2300m, "CAD", 2311.500m, new DateTime(2023, 8, 4, 22, 10, 44, 633, DateTimeKind.Utc).AddTicks(5608) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 246,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8700m, "JPY", 43.500m, 66, 17, 8700m, "JPY", 8743.500m, new DateTime(2023, 8, 10, 21, 32, 44, 633, DateTimeKind.Utc).AddTicks(5610) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 247,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2500m, "EUR", 12.500m, 36, 10, 2500m, "EUR", 2512.500m, new DateTime(2023, 6, 25, 9, 54, 44, 633, DateTimeKind.Utc).AddTicks(5612) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 248,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "CNY", 30.500m, 62, 94, 6100m, "CNY", 6130.500m, new DateTime(2023, 8, 6, 18, 29, 44, 633, DateTimeKind.Utc).AddTicks(5615) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 249,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "BRL", 42.500m, 24, 90, 8500m, "BRL", 8542.500m, new DateTime(2023, 7, 24, 21, 9, 44, 633, DateTimeKind.Utc).AddTicks(5617) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 250,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6500m, "CAD", 32.500m, 31, 42, 6500m, "CAD", 6532.500m, new DateTime(2023, 8, 12, 20, 52, 44, 633, DateTimeKind.Utc).AddTicks(5620) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 251,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5500m, "BRL", 27.500m, 63, 99, 5500m, "BRL", 5527.500m, new DateTime(2023, 6, 17, 10, 16, 44, 633, DateTimeKind.Utc).AddTicks(5622) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 252,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "NZD", 44.000m, 100, 38, 8800m, "NZD", 8844.000m, new DateTime(2023, 6, 5, 14, 56, 44, 633, DateTimeKind.Utc).AddTicks(5625) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 253,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "ZAR", 15.000m, 10, 86, 3000m, "ZAR", 3015.000m, new DateTime(2023, 8, 6, 5, 19, 44, 633, DateTimeKind.Utc).AddTicks(5627) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 254,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "BGN", 29.000m, 18, 119, 5800m, "BGN", 5829.000m, new DateTime(2023, 6, 21, 12, 22, 44, 633, DateTimeKind.Utc).AddTicks(5630) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 255,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7000m, "AUD", 35.000m, 46, 50, 7000m, "AUD", 7035.000m, new DateTime(2023, 8, 3, 0, 36, 44, 633, DateTimeKind.Utc).AddTicks(5633) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 256,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7100m, "AUD", 35.500m, 60, 107, 7100m, "AUD", 7135.500m, new DateTime(2023, 6, 17, 1, 14, 44, 633, DateTimeKind.Utc).AddTicks(5635) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 257,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "BRL", 8.500m, 125, 28, 1700m, "BRL", 1708.500m, new DateTime(2023, 8, 9, 12, 11, 44, 633, DateTimeKind.Utc).AddTicks(5638) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 258,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "BGN", 17.500m, 58, 71, 3500m, "BGN", 3517.500m, new DateTime(2023, 7, 21, 1, 57, 44, 633, DateTimeKind.Utc).AddTicks(5642) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 259,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7400m, "GBP", 37.000m, 62, 61, 7400m, "GBP", 7437.000m, new DateTime(2023, 7, 25, 10, 0, 44, 633, DateTimeKind.Utc).AddTicks(5650) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "VerificationToken",
                value: new Guid("15ade644-97db-4af4-93e1-ae414a0d25b0"));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0887743909", new Guid("6cf480b4-569e-4ae6-9053-15265ab249a1") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Widow@abv.com", "0885276988", new Guid("a7d4128a-ee4b-4f92-80a9-6afefaaff5a6") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Turbo@google.ru", "0884796759", new Guid("844ab745-734c-463b-ba17-29270391d8a4") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cuddlebug@yahoo.com", "0886071820", new Guid("b6e4e29d-79ac-4e7c-a08b-bae303c4aa13") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Thrill@google.net", "0889581483", new Guid("7b78665b-4c96-4580-966a-736c50945651") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "AsteroidZ@apple.ru", "0886169535", new Guid("8d9d6dde-88b4-4ae2-93b6-54fb54d7ced1") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "An1a@apple.bg", "0886081147", new Guid("a53f679d-6381-406b-bb86-60f2f06ecac0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Bluelight@google.eu", "0884254198", new Guid("ffe64756-6e93-41e3-89c7-01199cffdc25") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Loaded@abv.bg", "0887949511", new Guid("e0cc0714-5d27-4ff4-9e54-e4cd7e70c505") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Rant@yahoo.com", "0885673734", new Guid("ebdbd126-4921-41d0-a4b3-d27b2f9f10ba") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Diva@yahoo.bg", "0884154162", new Guid("5bfe34ef-d48f-49a0-924c-30267f5dae4f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Yukki@yahoo.net", "0883686085", new Guid("b019ca12-d320-43c6-ba5f-c79f590909cd") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Werice@abv.com", "0888196204", new Guid("e70ed9f0-210c-4bde-a942-c2ebeb8ad154") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Perfect@yahoo.bg", "0887924574", new Guid("25332981-9568-4a33-acc6-be94aa96069b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "MissK@abv.eu", "0888141280", new Guid("dfef1378-3f85-48ca-bff3-2dc90a2ffd63") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Bubbles@yahoo.net", "0883270573", new Guid("c3f05fc3-1987-4eef-8efb-59815d1a0802") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "ChEk@abv.bg", "0884113929", new Guid("e6f3ef95-bf59-42c2-b683-1a836d77b39b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Done@abv.net", "0884753325", new Guid("77ac9b4a-45f3-4c02-a055-58a462f7d0a7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "HugsForDrubs@yahoo.bg", "0883347589", new Guid("71cd27d5-5b3a-4a2f-a782-09c74bfe2f65") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pippen@abv.bg", "0883946159", new Guid("9602c548-ca7e-4e46-a318-0200dcfe1365") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Aisha@yahoo.ru", "0888151171", new Guid("701e339b-4391-4834-bed4-c8353adbab64") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Protectshell@yahoo.eu", "0884125263", new Guid("62516eb0-eac1-4f2f-b799-e87fc6760894") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Geez@yahoo.bg", "0887274301", new Guid("bed24b08-354f-43b3-9e2e-8781ffd048a2") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Monkey_see@apple.bg", "0886855312", new Guid("3a16a4ab-3c46-4761-9c4c-58569e5faad6") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Tight@abv.ru", "0887011560", new Guid("b5033ea8-c4d1-42b2-913e-301914bd94fd") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cutefest@google.bg", "0889690937", new Guid("1cd2385a-fa30-41a8-8001-8dbd128dd67a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Prong@abv.eu", "0884162166", new Guid("97c5f53f-d990-4dd0-ab70-bfa5c9ff20a5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Strongarmed@abv.com", "0888599828", new Guid("6380b436-b29b-4a9c-bb0c-1e83f119a8cd") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Sovenance@apple.eu", "0888421606", new Guid("a413c874-a48a-4cfe-a952-83b7b3f056a5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Angel@abv.net", "0886090779", new Guid("24a90555-b528-4bbf-8228-53e8a306209f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Mad@apple.eu", "0885814385", new Guid("c2350f26-56e6-4fbb-bd73-8513284ad47d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dimple@yahoo.ru", "0886218926", new Guid("7d520366-02f8-401b-b5d2-42cecf276ffe") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dollspell@google.ru", "0889961515", new Guid("e453e14b-38ed-4ecb-b912-0976f5bc6eef") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "State@abv.eu", "0886088613", new Guid("c011c906-7731-4c0f-8c45-4d83e968054b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Vellojoy@abv.com", "0885314616", new Guid("67a2e4a5-7029-4bd1-9233-ca4848954b87") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Eagle@abv.eu", "0885500829", new Guid("a268c320-7a6d-43ce-894c-622a52b6d65e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Brutal@abv.bg", "0886855891", new Guid("6cccbd24-7741-4716-b15e-af7ab9135436") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0889395489", new Guid("f4049612-30b6-4582-bded-3e7d8ba186b0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Intella@yahoo.eu", "0889769152", new Guid("d3782cdd-6146-47e9-bf27-ca5f1bca8e69") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Rainblow@apple.com", "0884143167", new Guid("cead993b-9276-4f18-a376-58bae1e89995") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Venereology@google.eu", "0883285831", new Guid("125246c7-c4f4-4ae3-ab3f-049f5f12d994") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dogbone@yahoo.com", "0886281002", new Guid("35274df3-cf08-416c-9199-8a90bbe64729") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Grep@abv.com", "0883309082", new Guid("8845e7af-17f1-4767-aa67-d4e35ee85f53") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hawkeye@google.ru", "0885321921", new Guid("62ab192c-f5f1-4211-8e0b-29788b958344") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Glue@apple.ru", "0884124845", new Guid("51d32a96-c563-47ec-8bc0-661b16257de5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Princess@abv.com", "0886464249", new Guid("ce2bb25b-4caf-4788-bb0c-4ca0546a6213") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "L4Legit@abv.ru", "0884645215", new Guid("c96013b9-c35f-41de-b0b6-b1b6e1312b44") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Peapod@google.ru", "0889613328", new Guid("dbb3ebb8-c95d-47c2-bee7-994141900d6a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Networked@google.net", "0885551399", new Guid("582e0613-52ef-4134-b249-f8bb7e1f6687") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Prep@abv.com", "0883951008", new Guid("b9838c41-e930-43b5-a686-4132d5b15739") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Sharken@apple.bg", "0883183960", new Guid("fe7af945-f9ad-44bd-9250-fcf81fba01fb") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Curio@abv.bg", "0885840710", new Guid("d5138480-2469-4e09-a1d0-b9547b50db66") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Ender@google.net", "0885030035", new Guid("2a16621b-d525-4276-a1d4-d45118a7290a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Icelolly@abv.net", "0884574846", new Guid("a92b61be-0dfd-468d-be25-2999ffacd526") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0883655272", new Guid("6813299a-3d09-4e67-95e1-2b4905d8dd84") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Zyber@abv.net", "0884012544", new Guid("141171b8-fa71-4403-861c-ee13418f2c6c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Moonchild@apple.eu", "0883502016", new Guid("0169a818-9bd0-4f48-a177-51cf71f0a74e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Precision@google.bg", "0884460259", new Guid("32d7cee1-668b-41df-8f36-449f6a47d946") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hydrospace@apple.bg", "0884983077", new Guid("1b54ec5c-983e-42b6-a172-0605ab148d7f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Talon@abv.bg", "0886533775", new Guid("1fc52b67-3c00-4c2a-85ae-92ca6b5e4158") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "BattlerBeauty@abv.com", "0889263584", new Guid("43e91bd6-d282-4bf5-8b51-fcaaf82ab7f6") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Delta@google.eu", "0885467651", new Guid("2960e9d7-e8de-41f8-85fd-11ac5ba377d9") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Lens@google.ru", "0886417690", new Guid("46d16f6a-8403-45ac-9c70-2a422707541c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Spit@google.eu", "0888106747", new Guid("2fd14b70-c074-43ac-b890-aae402c6a18a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Zoom@abv.bg", "0884563655", new Guid("027ee604-7eff-4740-b21f-c6cca6510ce0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Gothichic@google.com", "0887401835", new Guid("a81446ae-fc22-4baf-a820-f0523481f80d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Purple@apple.net", "0887817509", new Guid("1c9d58e0-bdb4-4086-a837-ccaa9616d9ca") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Ahem@google.net", "0888542982", new Guid("da294da6-65fd-4e13-856d-b6486abe6638") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Swag@yahoo.bg", "0884671918", new Guid("a01253ea-0886-43a5-9487-20f774e13766") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cuteness@yahoo.bg", "0888545839", new Guid("5a9d6744-eedf-4e86-949f-d1b0c04f3938") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "SkillDevil@yahoo.com", "0883103003", new Guid("5880da0e-828d-4984-bb75-d64c2be149b5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Island@apple.com", "0888909538", new Guid("022180db-7c60-49e0-ad45-e09b7115bddd") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0885445831", new Guid("df343d85-5ace-443b-ba12-324839e25f6d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cosmosaegis@google.eu", "0889045355", new Guid("3f6849b4-a763-4a73-a0d6-9b1a8fac440c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dove@google.bg", "0886232456", new Guid("2ea218d1-0ade-46b1-95d4-ba4e156ef5d8") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Crybaby@abv.net", "0888917694", new Guid("078b53fc-3b19-4427-8258-8d8bd2c26272") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cyberproof@abv.ru", "0885445109", new Guid("b1852ca9-c404-4dec-9c36-25afbabdaf7d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dedicated@abv.net", "0889621388", new Guid("965b0ddb-e630-4281-97a8-f3c8b919f228") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Epic@apple.net", "0884919251", new Guid("4426fd8a-8be9-475e-986c-59e0739f1d7b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Secure@abv.ru", "0886171384", new Guid("91550cf9-4099-4461-9eb4-cc596b2808a7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Doppler@apple.ru", "0888074736", new Guid("87066183-dc0d-416f-9058-19f8e2a2e042") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Buddy@google.eu", "0884610717", new Guid("718ed21b-1610-42c4-a57a-60aae290f4e3") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "FibBage@apple.com", "0883660759", new Guid("e148d7c2-37ce-4a29-9162-a87e160a8baf") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Strawboy@yahoo.eu", "0886631971", new Guid("5a1c56ab-dc4e-4a81-8638-5b7a14cd5ce0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Piggy@yahoo.ru", "0885967514", new Guid("bc3793d6-0f67-47ec-8d7f-c58e0f725efd") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Homes@google.net", "0883058194", new Guid("2e12315b-7bce-44b5-b769-5bbbb34978bf") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "JellyBott_om@apple.eu", "0885943733", new Guid("74555638-0d2a-45d9-8286-2ce4262f63ed") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Marill@google.eu", "0888371562", new Guid("b5bf8ceb-a7a4-4c13-be4b-94c92df04488") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Guaranteed@abv.ru", "0888299071", new Guid("ef092368-e271-4cf0-aebb-b3df07c4c52c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hardcover@apple.eu", "0883820552", new Guid("8106cf61-69b0-4f57-a94d-1249ab094e59") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Warmblush@google.com", "0886684763", new Guid("bf8015a8-2be9-4c3d-8426-cdeaeed20f12") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Croncity@yahoo.ru", "0888069015", new Guid("a12d8618-8962-4fe8-a279-dadb008db872") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Chub@google.bg", "0889902144", new Guid("35d14650-b085-495c-9c23-c4cd71a8aa14") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Paradise@yahoo.eu", "0886902785", new Guid("6d60ff19-2ecc-4d21-b4cf-3919e6c28c98") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Complete@yahoo.ru", "0884253000", new Guid("9c0c5968-ebdf-47cf-999e-5b7cbc601d03") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Moonlight@abv.com", "0887436556", new Guid("fb3f0117-af90-467c-86f7-dac12d20746f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Unicorn@yahoo.bg", "0883293203", new Guid("24dd2dbf-2520-4c55-a0f7-3254c9d411f1") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Limonad@google.com", "0885364255", new Guid("612ead63-cada-4ab9-a347-9e355454588a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Peak@yahoo.bg", "0885669418", new Guid("8cb98f1d-282f-404a-92c0-58070107cfb7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Sly@yahoo.eu", "0885652967", new Guid("2ba10aea-24c6-49d6-a9a9-1dffb8b46030") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Mercy@yahoo.com", "0886741735", new Guid("f54cfb88-769b-4601-8b0c-0003656f155c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cutie@abv.com", "0883951589", new Guid("7b7dda26-0198-4f65-adfc-41a3d7741be4") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0884227806", new Guid("2ca09bc5-d204-4cd1-a7cf-1653a97053c7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Safenet@apple.eu", "0887915509", new Guid("2630083b-aa4c-465d-a047-eabdbf8566ae") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Shine@apple.bg", "0889871006", new Guid("06ddee07-79fa-4414-b4da-f6be8bd728e5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Night@google.com", "0883493750", new Guid("06f8a111-8c11-4dc4-9a88-b04f3065ce3e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Task@apple.ru", "0883523336", new Guid("e63c8983-77c2-4d08-93a6-5329114e4e6b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Septenary@yahoo.bg", "0889794960", new Guid("320cd31c-8c7c-4d9b-9375-24cf73858d99") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Queenberry@yahoo.eu", "0888535705", new Guid("acbc776e-2fc4-4b62-b708-5b8221959106") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "DrawFul@abv.ru", "0883896946", new Guid("d24b3993-703d-436c-b2b2-d79b47da7e3b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Proeyeroller@apple.ru", "0888645284", new Guid("bac345ab-ebb7-476d-bce4-a580a8dfb62d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Bad@google.com", "0887700807", new Guid("393daa98-a571-4795-83ac-99ad75a379eb") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Grifen@google.net", "0886214960", new Guid("489821b8-a782-483a-b6d1-e7da4e9a879c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Secret@abv.eu", "0886088170", new Guid("4456bb42-df72-47c6-9b1f-92267948281b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Elite@google.net", "0885693959", new Guid("92fbbb36-cff1-46e1-a9bd-0371f5c8608a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Guardsmark@google.ru", "0887120664", new Guid("ae5b2b57-ea87-42dc-9b25-f0eca7079380") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pinkness@abv.ru", "0888478200", new Guid("39f93409-642d-425c-a069-8ae91de4bf8e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dark@yahoo.com", "0883004298", new Guid("e76a31ef-caf3-4ace-b461-00a462a06e53") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Blu@yahoo.ru", "0884816819", new Guid("c4b034e4-f9a5-4a99-830c-8449c7664364") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Crashion@apple.net", "0883644382", new Guid("222ad6b6-efb9-4c24-b25c-f8417480121e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Star@google.net", "0883409442", new Guid("e7e1cb56-6c4f-4b18-8e4e-f7e5412b669b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pink@apple.net", "0884378934", new Guid("cd47b5fd-8e38-4e45-aa6d-00a0cb796e5a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Kitten@yahoo.bg", "0889876433", new Guid("290a5f20-6434-4a2c-8f5f-b1c03a2909e5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Xentrix@yahoo.net", "0888504034", new Guid("2903bdc6-8fd0-4800-85bc-a1ee0640960d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hug@abv.net", "0885683252", new Guid("a9249c1b-e239-4cdc-bf61-dbd5fc4b9fe4") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pioneer@abv.eu", "0888718664", new Guid("02664329-1a22-4e71-8cc0-a6659f85990d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pro@yahoo.net", "0886312681", new Guid("b1dd474b-641c-4930-87e0-2a98cca363b7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Alert@apple.eu", "0884887875", new Guid("9dc8d7e2-fc68-4754-b05a-403ca9b8a57a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Fittofuel@yahoo.net", "0886895216", new Guid("8c4693e9-0404-45a6-b385-862e66482fa6") });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 42200m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 99100m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 44300m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 69600m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 5,
                column: "Balance",
                value: 40500m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 60600m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 2600m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 23300m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 63000m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 43200m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 85600m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 95700m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 30500m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28200m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 1100m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 44600m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 2800m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 20300m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 68500m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 35200m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 20700m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 82500m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 77700m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 39300m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 3100m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 66300m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 25200m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 17700m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 90000m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 82800m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 6100m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 35300m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 83300m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 44900m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 96000m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 31800m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 37,
                column: "Balance",
                value: 76300m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 5700m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 42100m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 60800m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 23900m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 73000m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 43,
                column: "Balance",
                value: 9700m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 54400m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 30000m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28100m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 50200m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 46500m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 1800m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 40200m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 51,
                column: "Balance",
                value: 80600m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 46000m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 64500m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 12800m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 10100m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 17900m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 84100m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 4000m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 98200m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28000m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 32800m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 12800m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 24600m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 90700m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 39100m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 30500m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 77400m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 68,
                column: "Balance",
                value: 90200m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 68800m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 63000m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 38000m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 77100m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 52400m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 81500m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 52100m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 30100m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 81100m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 51200m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 37900m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 39200m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28500m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 25800m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 27500m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 21300m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 38500m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 35000m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 85300m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 66800m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 33800m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 83200m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 86900m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 5900m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 91000m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 8400m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 86400m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 47800m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 48600m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 37900m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 15400m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 16600m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 74600m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 9400m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 72200m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 98000m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 21000m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 51800m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 97800m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 29000m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 1300m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 37100m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 59300m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28800m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 23600m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 51000m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 39000m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 35500m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 68000m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 73000m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 87300m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 8500m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 29300m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 25400m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 94600m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 85700m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 84100m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 69100m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 53300m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 45400m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 81600m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 22400m, "INR" });
        }
    }
}
