﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VirtualWallet.Data.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "BGN", 41.000m, 58, 62, 8200m, "BGN", 8241.000m, new DateTime(2023, 7, 2, 17, 38, 44, 633, DateTimeKind.Utc).AddTicks(4937) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3100m, "SGD", 15.500m, 20, 4, 3100m, "SGD", 3115.500m, new DateTime(2023, 6, 10, 13, 24, 44, 633, DateTimeKind.Utc).AddTicks(4948) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3900m, "BGN", 19.500m, 41, 76, 3900m, "BGN", 3919.500m, new DateTime(2023, 5, 31, 17, 38, 44, 633, DateTimeKind.Utc).AddTicks(4951) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7100m, "ZAR", 35.500m, 47, 8, 7100m, "ZAR", 7135.500m, new DateTime(2023, 7, 29, 20, 25, 44, 633, DateTimeKind.Utc).AddTicks(4954) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4500m, "ZAR", 22.500m, 66, 90, 4500m, "ZAR", 4522.500m, new DateTime(2023, 6, 3, 0, 49, 44, 633, DateTimeKind.Utc).AddTicks(4964) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "USD", 45.500m, 55, 5, 9100m, "USD", 9145.500m, new DateTime(2023, 7, 1, 11, 7, 44, 633, DateTimeKind.Utc).AddTicks(4967) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "INR", 44.000m, 46, 123, 8800m, "INR", 8844.000m, new DateTime(2023, 7, 19, 23, 23, 44, 633, DateTimeKind.Utc).AddTicks(4970) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8300m, "CHF", 41.500m, 22, 18, 8300m, "CHF", 8341.500m, new DateTime(2023, 7, 10, 7, 12, 44, 633, DateTimeKind.Utc).AddTicks(4973) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, "BRL", 21.500m, 82, 122, 4300m, "BRL", 4321.500m, new DateTime(2023, 7, 30, 21, 9, 44, 633, DateTimeKind.Utc).AddTicks(4975) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "BRL", 41.000m, 74, 123, 8200m, "BRL", 8241.000m, new DateTime(2023, 7, 3, 22, 41, 44, 633, DateTimeKind.Utc).AddTicks(4979) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7000m, "USD", 35.000m, 27, 31, 7000m, "USD", 7035.000m, new DateTime(2023, 6, 17, 15, 54, 44, 633, DateTimeKind.Utc).AddTicks(4981) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7900m, "BRL", 39.500m, 10, 22, 7900m, "BRL", 7939.500m, new DateTime(2023, 6, 14, 23, 31, 44, 633, DateTimeKind.Utc).AddTicks(4984) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5400m, "SGD", 27.000m, 120, 78, 5400m, "SGD", 5427.000m, new DateTime(2023, 5, 25, 6, 7, 44, 633, DateTimeKind.Utc).AddTicks(4986) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "INR", 45.500m, 39, 70, 9100m, "INR", 9145.500m, new DateTime(2023, 7, 18, 21, 43, 44, 633, DateTimeKind.Utc).AddTicks(4989) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7500m, "EUR", 37.500m, 61, 96, 7500m, "EUR", 7537.500m, new DateTime(2023, 7, 29, 1, 22, 44, 633, DateTimeKind.Utc).AddTicks(4991) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7300m, "EUR", 36.500m, 118, 113, 7300m, "EUR", 7336.500m, new DateTime(2023, 7, 2, 23, 53, 44, 633, DateTimeKind.Utc).AddTicks(4994) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7300m, "EUR", 36.500m, 54, 19, 7300m, "EUR", 7336.500m, new DateTime(2023, 6, 26, 21, 36, 44, 633, DateTimeKind.Utc).AddTicks(4997) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 800m, "JPY", 4.000m, 62, 86, 800m, "JPY", 804.000m, new DateTime(2023, 6, 12, 22, 8, 44, 633, DateTimeKind.Utc).AddTicks(5000) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "INR", 22.000m, 9, 31, 4400m, "INR", 4422.000m, new DateTime(2023, 6, 12, 4, 43, 44, 633, DateTimeKind.Utc).AddTicks(5002) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8700m, "RUB", 43.500m, 8, 18, 8700m, "RUB", 8743.500m, new DateTime(2023, 7, 8, 7, 44, 44, 633, DateTimeKind.Utc).AddTicks(5005) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9600m, "ZAR", 48.000m, 27, 70, 9600m, "ZAR", 9648.000m, new DateTime(2023, 7, 2, 3, 41, 44, 633, DateTimeKind.Utc).AddTicks(5008) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "INR", 29.000m, 5, 30, 5800m, "INR", 5829.000m, new DateTime(2023, 6, 16, 7, 3, 44, 633, DateTimeKind.Utc).AddTicks(5010) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3600m, "AUD", 18.000m, 34, 54, 3600m, "AUD", 3618.000m, new DateTime(2023, 8, 15, 12, 31, 44, 633, DateTimeKind.Utc).AddTicks(5012) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6700m, "RUB", 33.500m, 15, 30, 6700m, "RUB", 6733.500m, new DateTime(2023, 8, 8, 1, 26, 44, 633, DateTimeKind.Utc).AddTicks(5015) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "JPY", 2.000m, 62, 43, 400m, "JPY", 402.000m, new DateTime(2023, 6, 27, 10, 41, 44, 633, DateTimeKind.Utc).AddTicks(5018) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "INR", 38.000m, 21, 126, 7600m, "INR", 7638.000m, new DateTime(2023, 7, 10, 3, 56, 44, 633, DateTimeKind.Utc).AddTicks(5020) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2400m, "EUR", 12.000m, 85, 64, 2400m, "EUR", 2412.000m, new DateTime(2023, 7, 21, 18, 18, 44, 633, DateTimeKind.Utc).AddTicks(5022) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4000m, "CHF", 20.000m, 101, 4, 4000m, "CHF", 4020.000m, new DateTime(2023, 6, 18, 16, 26, 44, 633, DateTimeKind.Utc).AddTicks(5025) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "CNY", 45.500m, 88, 100, 9100m, "CNY", 9145.500m, new DateTime(2023, 6, 20, 8, 56, 44, 633, DateTimeKind.Utc).AddTicks(5028) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7200m, "EUR", 36.000m, 32, 72, 7200m, "EUR", 7236.000m, new DateTime(2023, 6, 29, 5, 59, 44, 633, DateTimeKind.Utc).AddTicks(5030) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7000m, "CNY", 35.000m, 127, 38, 7000m, "CNY", 7035.000m, new DateTime(2023, 5, 21, 23, 49, 44, 633, DateTimeKind.Utc).AddTicks(5032) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 600m, "CNY", 3.000m, 88, 39, 600m, "CNY", 603.000m, new DateTime(2023, 6, 17, 19, 31, 44, 633, DateTimeKind.Utc).AddTicks(5034) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6500m, "USD", 32.500m, 79, 52, 6500m, "USD", 6532.500m, new DateTime(2023, 5, 30, 18, 49, 44, 633, DateTimeKind.Utc).AddTicks(5038) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3200m, "BGN", 16.000m, 126, 33, 3200m, "BGN", 3216.000m, new DateTime(2023, 7, 30, 10, 49, 44, 633, DateTimeKind.Utc).AddTicks(5041) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "USD", 41.000m, 19, 4, 8200m, "USD", 8241.000m, new DateTime(2023, 6, 21, 10, 21, 44, 633, DateTimeKind.Utc).AddTicks(5043) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5500m, "JPY", 27.500m, 82, 94, 5500m, "JPY", 5527.500m, new DateTime(2023, 7, 7, 21, 3, 44, 633, DateTimeKind.Utc).AddTicks(5046) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "NZD", 114, 46, "NZD", new DateTime(2023, 6, 25, 10, 49, 44, 633, DateTimeKind.Utc).AddTicks(5048) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "INR", 8.500m, 64, 9, 1700m, "INR", 1708.500m, new DateTime(2023, 6, 3, 16, 2, 44, 633, DateTimeKind.Utc).AddTicks(5050) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5600m, "JPY", 28.000m, 52, 1, 5600m, "JPY", 5628.000m, new DateTime(2023, 7, 29, 4, 20, 44, 633, DateTimeKind.Utc).AddTicks(5053) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "NZD", 6.000m, 6, 23, 1200m, "NZD", 1206.000m, new DateTime(2023, 7, 17, 11, 2, 44, 633, DateTimeKind.Utc).AddTicks(5056) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "RUB", 34.000m, 48, 87, 6800m, "RUB", 6834.000m, new DateTime(2023, 6, 7, 16, 19, 44, 633, DateTimeKind.Utc).AddTicks(5058) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9600m, "AUD", 48.000m, 62, 79, 9600m, "AUD", 9648.000m, new DateTime(2023, 7, 12, 14, 47, 44, 633, DateTimeKind.Utc).AddTicks(5061) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2900m, "BGN", 14.500m, 86, 29, 2900m, "BGN", 2914.500m, new DateTime(2023, 7, 7, 5, 52, 44, 633, DateTimeKind.Utc).AddTicks(5064) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "EUR", 29.000m, 27, 113, 5800m, "EUR", 5829.000m, new DateTime(2023, 8, 8, 20, 6, 44, 633, DateTimeKind.Utc).AddTicks(5066) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8900m, "USD", 44.500m, 6, 68, 8900m, "USD", 8944.500m, new DateTime(2023, 7, 12, 20, 25, 44, 633, DateTimeKind.Utc).AddTicks(5068) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "BRL", 11.500m, 19, 5, 2300m, "BRL", 2311.500m, new DateTime(2023, 6, 30, 1, 57, 44, 633, DateTimeKind.Utc).AddTicks(5071) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4600m, "BGN", 23.000m, 42, 57, 4600m, "BGN", 4623.000m, new DateTime(2023, 7, 29, 0, 11, 44, 633, DateTimeKind.Utc).AddTicks(5073) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7500m, "INR", 37.500m, 8, 69, 7500m, "INR", 7537.500m, new DateTime(2023, 5, 28, 5, 8, 44, 633, DateTimeKind.Utc).AddTicks(5076) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5600m, "JPY", 28.000m, 109, 94, 5600m, "JPY", 5628.000m, new DateTime(2023, 8, 12, 22, 8, 44, 633, DateTimeKind.Utc).AddTicks(5083) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8100m, "AUD", 40.500m, 63, 36, 8100m, "AUD", 8140.500m, new DateTime(2023, 5, 29, 12, 24, 44, 633, DateTimeKind.Utc).AddTicks(5086) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "GBP", 25.000m, 78, 96, 5000m, "GBP", 5025.000m, new DateTime(2023, 7, 22, 19, 2, 44, 633, DateTimeKind.Utc).AddTicks(5089) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, 30.500m, 28, 49, 6100m, 6130.500m, new DateTime(2023, 7, 8, 8, 28, 44, 633, DateTimeKind.Utc).AddTicks(5091) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "EUR", 20.500m, 70, 76, 4100m, "EUR", 4120.500m, new DateTime(2023, 7, 14, 23, 13, 44, 633, DateTimeKind.Utc).AddTicks(5093) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "JPY", 45.500m, 4, 81, 9100m, "JPY", 9145.500m, new DateTime(2023, 8, 9, 21, 11, 44, 633, DateTimeKind.Utc).AddTicks(5096) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3200m, "CNY", 16.000m, 104, 52, 3200m, "CNY", 3216.000m, new DateTime(2023, 7, 4, 7, 14, 44, 633, DateTimeKind.Utc).AddTicks(5098) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7700m, "CNY", 38.500m, 6, 32, 7700m, "CNY", 7738.500m, new DateTime(2023, 8, 4, 10, 43, 44, 633, DateTimeKind.Utc).AddTicks(5100) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9300m, "BGN", 46.500m, 31, 91, 9300m, "BGN", 9346.500m, new DateTime(2023, 6, 3, 7, 41, 44, 633, DateTimeKind.Utc).AddTicks(5103) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8000m, "JPY", 40.000m, 96, 22, 8000m, "JPY", 8040.000m, new DateTime(2023, 6, 15, 7, 13, 44, 633, DateTimeKind.Utc).AddTicks(5105) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "CAD", 116, 111, "CAD", new DateTime(2023, 7, 21, 5, 25, 44, 633, DateTimeKind.Utc).AddTicks(5108) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2800m, "RUB", 14.000m, 83, 73, 2800m, "RUB", 2814.000m, new DateTime(2023, 6, 11, 6, 10, 44, 633, DateTimeKind.Utc).AddTicks(5110) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6500m, "NZD", 32.500m, 80, 43, 6500m, "NZD", 6532.500m, new DateTime(2023, 7, 6, 3, 21, 44, 633, DateTimeKind.Utc).AddTicks(5113) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3700m, "ZAR", 18.500m, 96, 57, 3700m, "ZAR", 3718.500m, new DateTime(2023, 6, 10, 11, 1, 44, 633, DateTimeKind.Utc).AddTicks(5116) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9300m, "ZAR", 46.500m, 107, 101, 9300m, "ZAR", 9346.500m, new DateTime(2023, 6, 14, 11, 24, 44, 633, DateTimeKind.Utc).AddTicks(5118) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 500m, "CHF", 2.500m, 27, 70, 500m, "CHF", 502.500m, new DateTime(2023, 5, 25, 9, 2, 44, 633, DateTimeKind.Utc).AddTicks(5121) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1300m, "EUR", 6.500m, 42, 5, 1300m, "EUR", 1306.500m, new DateTime(2023, 7, 12, 23, 43, 44, 633, DateTimeKind.Utc).AddTicks(5124) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3100m, "CNY", 15.500m, 107, 10, 3100m, "CNY", 3115.500m, new DateTime(2023, 6, 15, 21, 17, 44, 633, DateTimeKind.Utc).AddTicks(5128) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "BRL", 25.000m, 35, 26, 5000m, "BRL", 5025.000m, new DateTime(2023, 6, 8, 20, 40, 44, 633, DateTimeKind.Utc).AddTicks(5130) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "INR", 30.500m, 76, 37, 6100m, "INR", 6130.500m, new DateTime(2023, 7, 25, 19, 13, 44, 633, DateTimeKind.Utc).AddTicks(5133) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "AUD", 30.500m, 48, 64, 6100m, "AUD", 6130.500m, new DateTime(2023, 6, 14, 14, 2, 44, 633, DateTimeKind.Utc).AddTicks(5135) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5400m, "CNY", 27.000m, 16, 48, 5400m, "CNY", 5427.000m, new DateTime(2023, 8, 9, 15, 33, 44, 633, DateTimeKind.Utc).AddTicks(5138) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2000m, "CHF", 10.000m, 64, 104, 2000m, "CHF", 2010.000m, new DateTime(2023, 7, 17, 5, 33, 44, 633, DateTimeKind.Utc).AddTicks(5140) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4800m, "SGD", 24.000m, 31, 11, 4800m, "SGD", 4824.000m, new DateTime(2023, 7, 15, 7, 58, 44, 633, DateTimeKind.Utc).AddTicks(5143) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 700m, 3.500m, 92, 33, 700m, 703.500m, new DateTime(2023, 7, 20, 9, 16, 44, 633, DateTimeKind.Utc).AddTicks(5145) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4500m, "CHF", 22.500m, 95, 114, 4500m, "CHF", 4522.500m, new DateTime(2023, 8, 10, 14, 25, 44, 633, DateTimeKind.Utc).AddTicks(5147) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "SGD", 38.000m, 95, 80, 7600m, "SGD", 7638.000m, new DateTime(2023, 7, 21, 7, 27, 44, 633, DateTimeKind.Utc).AddTicks(5150) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "BGN", 20.500m, 109, 74, 4100m, "BGN", 4120.500m, new DateTime(2023, 5, 31, 6, 19, 44, 633, DateTimeKind.Utc).AddTicks(5152) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6400m, "RUB", 32.000m, 40, 20, 6400m, "RUB", 6432.000m, new DateTime(2023, 6, 4, 0, 19, 44, 633, DateTimeKind.Utc).AddTicks(5155) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5100m, "EUR", 25.500m, 23, 129, 5100m, "EUR", 5125.500m, new DateTime(2023, 6, 2, 11, 15, 44, 633, DateTimeKind.Utc).AddTicks(5157) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1300m, "BGN", 6.500m, 120, 25, 1300m, "BGN", 1306.500m, new DateTime(2023, 7, 17, 3, 55, 44, 633, DateTimeKind.Utc).AddTicks(5159) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "BGN", 15.000m, 70, 101, 3000m, "BGN", 3015.000m, new DateTime(2023, 6, 24, 2, 36, 44, 633, DateTimeKind.Utc).AddTicks(5162) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "BGN", 8.500m, 92, 37, 1700m, "BGN", 1708.500m, new DateTime(2023, 8, 16, 8, 43, 44, 633, DateTimeKind.Utc).AddTicks(5164) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 6700m, 33.500m, 25, 53, 6700m, 6733.500m, new DateTime(2023, 7, 6, 6, 20, 44, 633, DateTimeKind.Utc).AddTicks(5166) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 900m, "RUB", 4.500m, 15, 122, 900m, "RUB", 904.500m, new DateTime(2023, 7, 10, 19, 42, 44, 633, DateTimeKind.Utc).AddTicks(5169) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1900m, "SGD", 9.500m, 85, 50, 1900m, "SGD", 1909.500m, new DateTime(2023, 6, 28, 14, 14, 44, 633, DateTimeKind.Utc).AddTicks(5171) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3700m, "EUR", 18.500m, 28, 16, 3700m, "EUR", 3718.500m, new DateTime(2023, 7, 12, 4, 52, 44, 633, DateTimeKind.Utc).AddTicks(5174) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4500m, "USD", 22.500m, 108, 19, 4500m, "USD", 4522.500m, new DateTime(2023, 7, 31, 21, 45, 44, 633, DateTimeKind.Utc).AddTicks(5176) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8100m, "JPY", 40.500m, 82, 108, 8100m, "JPY", 8140.500m, new DateTime(2023, 6, 28, 5, 3, 44, 633, DateTimeKind.Utc).AddTicks(5179) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, 5.500m, 15, 12, 1100m, 1105.500m, new DateTime(2023, 7, 27, 10, 58, 44, 633, DateTimeKind.Utc).AddTicks(5181) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9600m, "BRL", 48.000m, 9, 4, 9600m, "BRL", 9648.000m, new DateTime(2023, 8, 13, 1, 44, 44, 633, DateTimeKind.Utc).AddTicks(5184) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "GBP", 42.500m, 19, 88, 8500m, "GBP", 8542.500m, new DateTime(2023, 6, 18, 11, 46, 44, 633, DateTimeKind.Utc).AddTicks(5186) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8600m, "CHF", 43.000m, 124, 20, 8600m, "CHF", 8643.000m, new DateTime(2023, 7, 8, 13, 0, 44, 633, DateTimeKind.Utc).AddTicks(5188) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 900m, "BGN", 4.500m, 89, 36, 900m, "BGN", 904.500m, new DateTime(2023, 8, 15, 4, 20, 44, 633, DateTimeKind.Utc).AddTicks(5191) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9000m, "BRL", 45.000m, 120, 91, 9000m, "BRL", 9045.000m, new DateTime(2023, 8, 13, 9, 44, 44, 633, DateTimeKind.Utc).AddTicks(5202) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4000m, "CHF", 20.000m, 7, 128, 4000m, "CHF", 4020.000m, new DateTime(2023, 6, 7, 4, 6, 44, 633, DateTimeKind.Utc).AddTicks(5204) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8600m, "CNY", 43.000m, 53, 21, 8600m, "CNY", 8643.000m, new DateTime(2023, 6, 1, 0, 50, 44, 633, DateTimeKind.Utc).AddTicks(5207) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "RUB", 25.000m, 106, 5, 5000m, "RUB", 5025.000m, new DateTime(2023, 6, 12, 15, 16, 44, 633, DateTimeKind.Utc).AddTicks(5209) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "BGN", 2.000m, 109, 95, 400m, "BGN", 402.000m, new DateTime(2023, 6, 26, 10, 49, 44, 633, DateTimeKind.Utc).AddTicks(5211) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3800m, "INR", 19.000m, 5, 70, 3800m, "INR", 3819.000m, new DateTime(2023, 7, 10, 19, 10, 44, 633, DateTimeKind.Utc).AddTicks(5214) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1900m, "SGD", 9.500m, 26, 35, 1900m, "SGD", 1909.500m, new DateTime(2023, 5, 24, 15, 4, 44, 633, DateTimeKind.Utc).AddTicks(5217) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9500m, "ZAR", 47.500m, 46, 50, 9500m, "ZAR", 9547.500m, new DateTime(2023, 8, 16, 23, 16, 44, 633, DateTimeKind.Utc).AddTicks(5219) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8900m, "SGD", 44.500m, 65, 18, 8900m, "SGD", 8944.500m, new DateTime(2023, 6, 13, 9, 41, 44, 633, DateTimeKind.Utc).AddTicks(5222) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, 42.500m, 121, 76, 8500m, 8542.500m, new DateTime(2023, 5, 22, 7, 30, 44, 633, DateTimeKind.Utc).AddTicks(5224) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7100m, "RUB", 35.500m, 55, 107, 7100m, "RUB", 7135.500m, new DateTime(2023, 7, 14, 2, 11, 44, 633, DateTimeKind.Utc).AddTicks(5227) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3400m, "CNY", 17.000m, 91, 100, 3400m, "CNY", 3417.000m, new DateTime(2023, 8, 6, 5, 19, 44, 633, DateTimeKind.Utc).AddTicks(5229) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 500m, "CHF", 2.500m, 73, 70, 500m, "CHF", 502.500m, new DateTime(2023, 5, 20, 22, 36, 44, 633, DateTimeKind.Utc).AddTicks(5232) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6700m, "ZAR", 33.500m, 63, 35, 6700m, "ZAR", 6733.500m, new DateTime(2023, 6, 26, 16, 6, 44, 633, DateTimeKind.Utc).AddTicks(5234) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 700m, 3.500m, 8, 103, 700m, 703.500m, new DateTime(2023, 7, 26, 22, 31, 44, 633, DateTimeKind.Utc).AddTicks(5237) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "BRL", 2.000m, 106, 51, 400m, "BRL", 402.000m, new DateTime(2023, 5, 27, 12, 51, 44, 633, DateTimeKind.Utc).AddTicks(5239) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "BGN", 42.500m, 61, 62, 8500m, "BGN", 8542.500m, new DateTime(2023, 7, 4, 23, 54, 44, 633, DateTimeKind.Utc).AddTicks(5242) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2600m, "SGD", 13.000m, 83, 100, 2600m, "SGD", 2613.000m, new DateTime(2023, 5, 18, 5, 49, 44, 633, DateTimeKind.Utc).AddTicks(5244) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "CNY", 25.000m, 53, 32, 5000m, "CNY", 5025.000m, new DateTime(2023, 5, 22, 3, 6, 44, 633, DateTimeKind.Utc).AddTicks(5246) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5300m, "AUD", 26.500m, 27, 73, 5300m, "AUD", 5326.500m, new DateTime(2023, 6, 1, 2, 49, 44, 633, DateTimeKind.Utc).AddTicks(5252) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 2800m, 14.000m, 47, 99, 2800m, 2814.000m, new DateTime(2023, 6, 15, 11, 13, 44, 633, DateTimeKind.Utc).AddTicks(5255) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, 20.500m, 61, 129, 4100m, 4120.500m, new DateTime(2023, 6, 20, 10, 17, 44, 633, DateTimeKind.Utc).AddTicks(5257) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 2500m, 12.500m, 4, 16, 2500m, 2512.500m, new DateTime(2023, 6, 24, 23, 37, 44, 633, DateTimeKind.Utc).AddTicks(5260) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "NZD", 6.000m, 111, 32, 1200m, "NZD", 1206.000m, new DateTime(2023, 6, 18, 5, 0, 44, 633, DateTimeKind.Utc).AddTicks(5262) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6200m, "SGD", 31.000m, 11, 81, 6200m, "SGD", 6231.000m, new DateTime(2023, 8, 10, 21, 3, 44, 633, DateTimeKind.Utc).AddTicks(5265) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "CNY", 34.000m, 97, 90, 6800m, "CNY", 6834.000m, new DateTime(2023, 7, 25, 23, 55, 44, 633, DateTimeKind.Utc).AddTicks(5268) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4500m, "EUR", 22.500m, 70, 73, 4500m, "EUR", 4522.500m, new DateTime(2023, 6, 28, 7, 41, 44, 633, DateTimeKind.Utc).AddTicks(5271) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6600m, "JPY", 33.000m, 109, 32, 6600m, "JPY", 6633.000m, new DateTime(2023, 7, 7, 11, 13, 44, 633, DateTimeKind.Utc).AddTicks(5273) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "AUD", 22.000m, 72, 87, 4400m, "AUD", 4422.000m, new DateTime(2023, 6, 4, 15, 1, 44, 633, DateTimeKind.Utc).AddTicks(5275) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2000m, "NZD", 10.000m, 116, 21, 2000m, "NZD", 2010.000m, new DateTime(2023, 7, 22, 8, 20, 44, 633, DateTimeKind.Utc).AddTicks(5278) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "INR", 6.000m, 55, 125, 1200m, "INR", 1206.000m, new DateTime(2023, 7, 16, 4, 50, 44, 633, DateTimeKind.Utc).AddTicks(5280) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5900m, "BRL", 29.500m, 28, 98, 5900m, "BRL", 5929.500m, new DateTime(2023, 7, 7, 9, 20, 44, 633, DateTimeKind.Utc).AddTicks(5283) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "CHF", 5.500m, 65, 109, 1100m, "CHF", 1105.500m, new DateTime(2023, 6, 22, 19, 3, 44, 633, DateTimeKind.Utc).AddTicks(5285) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8000m, "CHF", 40.000m, 118, 91, 8000m, "CHF", 8040.000m, new DateTime(2023, 7, 13, 17, 35, 44, 633, DateTimeKind.Utc).AddTicks(5288) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "NZD", 29.000m, 96, 127, 5800m, "NZD", 5829.000m, new DateTime(2023, 7, 20, 22, 49, 44, 633, DateTimeKind.Utc).AddTicks(5290) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8400m, "GBP", 42.000m, 91, 17, 8400m, "GBP", 8442.000m, new DateTime(2023, 8, 10, 0, 58, 44, 633, DateTimeKind.Utc).AddTicks(5293) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7200m, "CNY", 36.000m, 102, 111, 7200m, "CNY", 7236.000m, new DateTime(2023, 6, 15, 2, 13, 44, 633, DateTimeKind.Utc).AddTicks(5295) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1800m, "RUB", 9.000m, 26, 88, 1800m, "RUB", 1809.000m, new DateTime(2023, 6, 4, 3, 55, 44, 633, DateTimeKind.Utc).AddTicks(5299) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6600m, "BRL", 33.000m, 113, 11, 6600m, "BRL", 6633.000m, new DateTime(2023, 8, 1, 22, 12, 44, 633, DateTimeKind.Utc).AddTicks(5301) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5500m, "EUR", 27.500m, 99, 118, 5500m, "EUR", 5527.500m, new DateTime(2023, 7, 20, 2, 31, 44, 633, DateTimeKind.Utc).AddTicks(5311) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8600m, "JPY", 43.000m, 111, 86, 8600m, "JPY", 8643.000m, new DateTime(2023, 6, 20, 0, 26, 44, 633, DateTimeKind.Utc).AddTicks(5314) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "ZAR", 41.000m, 120, 89, 8200m, "ZAR", 8241.000m, new DateTime(2023, 5, 28, 8, 42, 44, 633, DateTimeKind.Utc).AddTicks(5316) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2000m, "CNY", 10.000m, 125, 43, 2000m, "CNY", 2010.000m, new DateTime(2023, 6, 20, 12, 3, 44, 633, DateTimeKind.Utc).AddTicks(5318) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4900m, "BRL", 24.500m, 20, 23, 4900m, "BRL", 4924.500m, new DateTime(2023, 8, 10, 15, 41, 44, 633, DateTimeKind.Utc).AddTicks(5321) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3200m, "BGN", 16.000m, 34, 21, 3200m, "BGN", 3216.000m, new DateTime(2023, 6, 15, 7, 13, 44, 633, DateTimeKind.Utc).AddTicks(5323) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 100m, "ZAR", 0.500m, 9, 37, 100m, "ZAR", 100.500m, new DateTime(2023, 7, 10, 19, 11, 44, 633, DateTimeKind.Utc).AddTicks(5326) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4700m, "CAD", 23.500m, 35, 34, 4700m, "CAD", 4723.500m, new DateTime(2023, 5, 31, 6, 54, 44, 633, DateTimeKind.Utc).AddTicks(5328) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "JPY", 15.000m, 4, 123, 3000m, "JPY", 3015.000m, new DateTime(2023, 7, 20, 2, 47, 44, 633, DateTimeKind.Utc).AddTicks(5331) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 7800m, 39.000m, 99, 121, 7800m, 7839.000m, new DateTime(2023, 7, 2, 0, 16, 44, 633, DateTimeKind.Utc).AddTicks(5333) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6300m, "AUD", 31.500m, 3, 120, 6300m, "AUD", 6331.500m, new DateTime(2023, 7, 19, 1, 8, 44, 633, DateTimeKind.Utc).AddTicks(5336) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "EUR", 11.500m, 119, 20, 2300m, "EUR", 2311.500m, new DateTime(2023, 6, 16, 8, 33, 44, 633, DateTimeKind.Utc).AddTicks(5339) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2500m, "ZAR", 12.500m, 65, 128, 2500m, "ZAR", 2512.500m, new DateTime(2023, 7, 20, 12, 17, 44, 633, DateTimeKind.Utc).AddTicks(5341) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5300m, "BGN", 26.500m, 121, 115, 5300m, "BGN", 5326.500m, new DateTime(2023, 7, 13, 1, 25, 44, 633, DateTimeKind.Utc).AddTicks(5344) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "NZD", 29.000m, 56, 27, 5800m, "NZD", 5829.000m, new DateTime(2023, 6, 10, 10, 39, 44, 633, DateTimeKind.Utc).AddTicks(5346) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4200m, "BGN", 21.000m, 90, 111, 4200m, "BGN", 4221.000m, new DateTime(2023, 6, 22, 9, 28, 44, 633, DateTimeKind.Utc).AddTicks(5349) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6400m, "RUB", 32.000m, 64, 72, 6400m, "RUB", 6432.000m, new DateTime(2023, 7, 27, 7, 12, 44, 633, DateTimeKind.Utc).AddTicks(5351) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6400m, "ZAR", 32.000m, 115, 66, 6400m, "ZAR", 6432.000m, new DateTime(2023, 6, 3, 19, 48, 44, 633, DateTimeKind.Utc).AddTicks(5354) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7200m, "ZAR", 36.000m, 85, 51, 7200m, "ZAR", 7236.000m, new DateTime(2023, 5, 24, 23, 47, 44, 633, DateTimeKind.Utc).AddTicks(5356) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 151,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 900m, "JPY", 4.500m, 121, 19, 900m, "JPY", 904.500m, new DateTime(2023, 8, 10, 23, 44, 44, 633, DateTimeKind.Utc).AddTicks(5358) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 152,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4800m, "GBP", 24.000m, 47, 69, 4800m, "GBP", 4824.000m, new DateTime(2023, 7, 3, 18, 11, 44, 633, DateTimeKind.Utc).AddTicks(5361) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 153,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, "USD", 21.500m, 106, 62, 4300m, "USD", 4321.500m, new DateTime(2023, 5, 22, 13, 30, 44, 633, DateTimeKind.Utc).AddTicks(5364) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 154,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "EUR", 34.000m, 79, 111, 6800m, "EUR", 6834.000m, new DateTime(2023, 5, 27, 21, 48, 44, 633, DateTimeKind.Utc).AddTicks(5366) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 155,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "USD", 25.000m, 2, 38, 5000m, "USD", 5025.000m, new DateTime(2023, 7, 6, 10, 19, 44, 633, DateTimeKind.Utc).AddTicks(5369) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 156,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5100m, "BGN", 25.500m, 53, 21, 5100m, "BGN", 5125.500m, new DateTime(2023, 7, 17, 21, 36, 44, 633, DateTimeKind.Utc).AddTicks(5371) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 157,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3600m, "USD", 18.000m, 18, 128, 3600m, "USD", 3618.000m, new DateTime(2023, 8, 4, 22, 8, 44, 633, DateTimeKind.Utc).AddTicks(5373) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 158,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, "GBP", 21.500m, 47, 22, 4300m, "GBP", 4321.500m, new DateTime(2023, 6, 21, 15, 39, 44, 633, DateTimeKind.Utc).AddTicks(5376) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 159,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "GBP", 29.000m, 50, 56, 5800m, "GBP", 5829.000m, new DateTime(2023, 8, 12, 3, 20, 44, 633, DateTimeKind.Utc).AddTicks(5378) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 160,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1600m, "GBP", 8.000m, 119, 77, 1600m, "GBP", 1608.000m, new DateTime(2023, 5, 29, 11, 0, 44, 633, DateTimeKind.Utc).AddTicks(5380) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 161,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8400m, "AUD", 42.000m, 60, 33, 8400m, "AUD", 8442.000m, new DateTime(2023, 5, 19, 18, 10, 44, 633, DateTimeKind.Utc).AddTicks(5383) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 162,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "SGD", 103, 36, "SGD", new DateTime(2023, 8, 12, 0, 46, 44, 633, DateTimeKind.Utc).AddTicks(5385) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 163,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4900m, "AUD", 24.500m, 35, 71, 4900m, "AUD", 4924.500m, new DateTime(2023, 7, 25, 4, 29, 44, 633, DateTimeKind.Utc).AddTicks(5388) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 164,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4900m, "BGN", 24.500m, 55, 26, 4900m, "BGN", 4924.500m, new DateTime(2023, 7, 30, 18, 3, 44, 633, DateTimeKind.Utc).AddTicks(5391) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 165,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "SGD", 38.000m, 16, 124, 7600m, "SGD", 7638.000m, new DateTime(2023, 6, 17, 23, 44, 44, 633, DateTimeKind.Utc).AddTicks(5394) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 166,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7000m, "CNY", 35.000m, 18, 35, 7000m, "CNY", 7035.000m, new DateTime(2023, 6, 9, 20, 8, 44, 633, DateTimeKind.Utc).AddTicks(5396) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 167,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "BRL", 22.000m, 65, 117, 4400m, "BRL", 4422.000m, new DateTime(2023, 5, 26, 5, 36, 44, 633, DateTimeKind.Utc).AddTicks(5399) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 168,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, "SGD", 21.500m, 69, 57, 4300m, "SGD", 4321.500m, new DateTime(2023, 8, 3, 7, 5, 44, 633, DateTimeKind.Utc).AddTicks(5401) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 169,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 8300m, 41.500m, 73, 85, 8300m, 8341.500m, new DateTime(2023, 6, 24, 23, 27, 44, 633, DateTimeKind.Utc).AddTicks(5404) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 170,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 1900m, 9.500m, 78, 1, 1900m, 1909.500m, new DateTime(2023, 7, 5, 18, 27, 44, 633, DateTimeKind.Utc).AddTicks(5406) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 171,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "EUR", 17.500m, 63, 34, 3500m, "EUR", 3517.500m, new DateTime(2023, 5, 30, 0, 41, 44, 633, DateTimeKind.Utc).AddTicks(5409) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 172,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1000m, "INR", 5.000m, 53, 22, 1000m, "INR", 1005.000m, new DateTime(2023, 7, 14, 3, 34, 44, 633, DateTimeKind.Utc).AddTicks(5412) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 173,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3600m, "JPY", 18.000m, 112, 37, 3600m, "JPY", 3618.000m, new DateTime(2023, 7, 5, 3, 48, 44, 633, DateTimeKind.Utc).AddTicks(5414) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 174,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "USD", 45.500m, 94, 25, 9100m, "USD", 9145.500m, new DateTime(2023, 7, 30, 7, 20, 44, 633, DateTimeKind.Utc).AddTicks(5417) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 175,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "RUB", 6.000m, 75, 85, 1200m, "RUB", 1206.000m, new DateTime(2023, 5, 29, 20, 45, 44, 633, DateTimeKind.Utc).AddTicks(5419) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 176,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3400m, "CNY", 17.000m, 28, 15, 3400m, "CNY", 3417.000m, new DateTime(2023, 6, 13, 1, 34, 44, 633, DateTimeKind.Utc).AddTicks(5421) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 177,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "ZAR", 34.000m, 94, 4, 6800m, "ZAR", 6834.000m, new DateTime(2023, 7, 29, 22, 37, 44, 633, DateTimeKind.Utc).AddTicks(5424) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 178,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "ZAR", 42.500m, 42, 18, 8500m, "ZAR", 8542.500m, new DateTime(2023, 8, 5, 17, 9, 44, 633, DateTimeKind.Utc).AddTicks(5426) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 179,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1000m, "INR", 5.000m, 56, 14, 1000m, "INR", 1005.000m, new DateTime(2023, 6, 12, 7, 34, 44, 633, DateTimeKind.Utc).AddTicks(5428) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 180,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "NZD", 30.500m, 119, 112, 6100m, "NZD", 6130.500m, new DateTime(2023, 6, 3, 14, 0, 44, 633, DateTimeKind.Utc).AddTicks(5431) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 181,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "BRL", 5.500m, 44, 114, 1100m, "BRL", 1105.500m, new DateTime(2023, 5, 29, 22, 7, 44, 633, DateTimeKind.Utc).AddTicks(5433) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 182,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3300m, "INR", 16.500m, 78, 118, 3300m, "INR", 3316.500m, new DateTime(2023, 6, 18, 19, 13, 44, 633, DateTimeKind.Utc).AddTicks(5436) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 183,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4000m, "BGN", 20.000m, 103, 107, 4000m, "BGN", 4020.000m, new DateTime(2023, 7, 1, 12, 11, 44, 633, DateTimeKind.Utc).AddTicks(5446) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 184,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9200m, "BRL", 46.000m, 45, 48, 9200m, "BRL", 9246.000m, new DateTime(2023, 5, 21, 0, 47, 44, 633, DateTimeKind.Utc).AddTicks(5448) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 185,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5200m, "BGN", 26.000m, 24, 86, 5200m, "BGN", 5226.000m, new DateTime(2023, 6, 1, 11, 13, 44, 633, DateTimeKind.Utc).AddTicks(5450) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 186,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "CAD", 2.000m, 5, 127, 400m, "CAD", 402.000m, new DateTime(2023, 5, 29, 8, 54, 44, 633, DateTimeKind.Utc).AddTicks(5453) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 187,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9300m, "BRL", 46.500m, 53, 101, 9300m, "BRL", 9346.500m, new DateTime(2023, 7, 13, 20, 35, 44, 633, DateTimeKind.Utc).AddTicks(5455) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 188,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5600m, "EUR", 28.000m, 82, 4, 5600m, "EUR", 5628.000m, new DateTime(2023, 5, 30, 12, 28, 44, 633, DateTimeKind.Utc).AddTicks(5458) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 189,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8300m, "CAD", 41.500m, 101, 109, 8300m, "CAD", 8341.500m, new DateTime(2023, 7, 12, 23, 50, 44, 633, DateTimeKind.Utc).AddTicks(5461) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 190,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "CHF", 11.500m, 78, 4, 2300m, "CHF", 2311.500m, new DateTime(2023, 5, 28, 17, 28, 44, 633, DateTimeKind.Utc).AddTicks(5463) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 191,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "RUB", 44.000m, 68, 90, 8800m, "RUB", 8844.000m, new DateTime(2023, 6, 5, 21, 9, 44, 633, DateTimeKind.Utc).AddTicks(5466) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 192,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "USD", 45.500m, 24, 75, 9100m, "USD", 9145.500m, new DateTime(2023, 5, 30, 16, 38, 44, 633, DateTimeKind.Utc).AddTicks(5468) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 193,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "ZAR", 15.000m, 95, 83, 3000m, "ZAR", 3015.000m, new DateTime(2023, 7, 2, 14, 9, 44, 633, DateTimeKind.Utc).AddTicks(5471) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 194,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "INR", 42.500m, 17, 67, 8500m, "INR", 8542.500m, new DateTime(2023, 6, 4, 8, 23, 44, 633, DateTimeKind.Utc).AddTicks(5473) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 195,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "CNY", 15.000m, 121, 44, 3000m, "CNY", 3015.000m, new DateTime(2023, 8, 17, 4, 11, 44, 633, DateTimeKind.Utc).AddTicks(5476) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 196,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "JPY", 22.000m, 35, 126, 4400m, "JPY", 4422.000m, new DateTime(2023, 6, 25, 7, 9, 44, 633, DateTimeKind.Utc).AddTicks(5478) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 197,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3400m, "CAD", 17.000m, 53, 90, 3400m, "CAD", 3417.000m, new DateTime(2023, 7, 25, 20, 25, 44, 633, DateTimeKind.Utc).AddTicks(5480) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 198,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2100m, "CHF", 10.500m, 19, 114, 2100m, "CHF", 2110.500m, new DateTime(2023, 6, 1, 14, 21, 44, 633, DateTimeKind.Utc).AddTicks(5483) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 199,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9600m, "GBP", 48.000m, 63, 20, 9600m, "GBP", 9648.000m, new DateTime(2023, 8, 8, 13, 7, 44, 633, DateTimeKind.Utc).AddTicks(5485) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 200,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "EUR", 34.000m, 128, 2, 6800m, "EUR", 6834.000m, new DateTime(2023, 8, 4, 10, 16, 44, 633, DateTimeKind.Utc).AddTicks(5487) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 201,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "CAD", 5.500m, 109, 30, 1100m, "CAD", 1105.500m, new DateTime(2023, 5, 25, 9, 2, 44, 633, DateTimeKind.Utc).AddTicks(5490) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 202,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "RUB", 5.500m, 71, 4, 1100m, "RUB", 1105.500m, new DateTime(2023, 7, 5, 7, 16, 44, 633, DateTimeKind.Utc).AddTicks(5492) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 203,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4700m, "GBP", 23.500m, 96, 64, 4700m, "GBP", 4723.500m, new DateTime(2023, 6, 19, 9, 16, 44, 633, DateTimeKind.Utc).AddTicks(5495) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 204,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2700m, "CAD", 13.500m, 40, 88, 2700m, "CAD", 2713.500m, new DateTime(2023, 8, 1, 21, 41, 44, 633, DateTimeKind.Utc).AddTicks(5498) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 205,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8100m, "INR", 40.500m, 106, 61, 8100m, "INR", 8140.500m, new DateTime(2023, 8, 7, 17, 0, 44, 633, DateTimeKind.Utc).AddTicks(5500) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 206,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "BGN", 6.000m, 17, 42, 1200m, "BGN", 1206.000m, new DateTime(2023, 8, 3, 12, 14, 44, 633, DateTimeKind.Utc).AddTicks(5503) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 207,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5600m, "BRL", 28.000m, 79, 12, 5600m, "BRL", 5628.000m, new DateTime(2023, 6, 23, 16, 21, 44, 633, DateTimeKind.Utc).AddTicks(5505) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 208,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "NZD", 20.500m, 91, 105, 4100m, "NZD", 4120.500m, new DateTime(2023, 6, 16, 12, 58, 44, 633, DateTimeKind.Utc).AddTicks(5508) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 209,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1300m, "BGN", 6.500m, 4, 128, 1300m, "BGN", 1306.500m, new DateTime(2023, 7, 9, 5, 40, 44, 633, DateTimeKind.Utc).AddTicks(5510) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 210,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1300m, "USD", 6.500m, 22, 90, 1300m, "USD", 1306.500m, new DateTime(2023, 7, 17, 5, 25, 44, 633, DateTimeKind.Utc).AddTicks(5513) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 211,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4700m, "AUD", 23.500m, 7, 34, 4700m, "AUD", 4723.500m, new DateTime(2023, 6, 13, 23, 17, 44, 633, DateTimeKind.Utc).AddTicks(5515) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 212,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9000m, "JPY", 45.000m, 65, 51, 9000m, "JPY", 9045.000m, new DateTime(2023, 8, 14, 1, 58, 44, 633, DateTimeKind.Utc).AddTicks(5518) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 213,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "GBP", 71, 80, "GBP", new DateTime(2023, 7, 13, 23, 52, 44, 633, DateTimeKind.Utc).AddTicks(5520) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 214,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8000m, "NZD", 40.000m, 75, 8, 8000m, "NZD", 8040.000m, new DateTime(2023, 6, 10, 8, 34, 44, 633, DateTimeKind.Utc).AddTicks(5523) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 215,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "SGD", 38.000m, 31, 67, 7600m, "SGD", 7638.000m, new DateTime(2023, 6, 16, 4, 50, 44, 633, DateTimeKind.Utc).AddTicks(5525) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 216,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5600m, "NZD", 28.000m, 51, 37, 5600m, "NZD", 5628.000m, new DateTime(2023, 6, 9, 10, 14, 44, 633, DateTimeKind.Utc).AddTicks(5528) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 217,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 900m, "JPY", 4.500m, 30, 65, 900m, "JPY", 904.500m, new DateTime(2023, 5, 29, 11, 27, 44, 633, DateTimeKind.Utc).AddTicks(5530) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 218,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4700m, "USD", 23.500m, 26, 110, 4700m, "USD", 4723.500m, new DateTime(2023, 7, 15, 18, 53, 44, 633, DateTimeKind.Utc).AddTicks(5532) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 219,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 700m, "RUB", 3.500m, 27, 80, 700m, "RUB", 703.500m, new DateTime(2023, 7, 4, 21, 9, 44, 633, DateTimeKind.Utc).AddTicks(5535) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 220,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 700m, "ZAR", 3.500m, 12, 84, 700m, "ZAR", 703.500m, new DateTime(2023, 7, 10, 1, 18, 44, 633, DateTimeKind.Utc).AddTicks(5537) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 221,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 7300m, 36.500m, 90, 86, 7300m, 7336.500m, new DateTime(2023, 7, 9, 14, 45, 44, 633, DateTimeKind.Utc).AddTicks(5540) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 222,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "CHF", 42.500m, 45, 44, 8500m, "CHF", 8542.500m, new DateTime(2023, 7, 28, 2, 22, 44, 633, DateTimeKind.Utc).AddTicks(5542) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 223,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6300m, "CNY", 31.500m, 120, 88, 6300m, "CNY", 6331.500m, new DateTime(2023, 8, 11, 6, 5, 44, 633, DateTimeKind.Utc).AddTicks(5545) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 224,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "USD", 11.500m, 10, 75, 2300m, "USD", 2311.500m, new DateTime(2023, 7, 23, 2, 11, 44, 633, DateTimeKind.Utc).AddTicks(5547) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 225,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2200m, "BRL", 11.000m, 93, 43, 2200m, "BRL", 2211.000m, new DateTime(2023, 8, 15, 20, 56, 44, 633, DateTimeKind.Utc).AddTicks(5550) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 226,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8100m, "CNY", 40.500m, 19, 79, 8100m, "CNY", 8140.500m, new DateTime(2023, 8, 1, 9, 27, 44, 633, DateTimeKind.Utc).AddTicks(5552) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 227,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9000m, "ZAR", 45.000m, 39, 21, 9000m, "ZAR", 9045.000m, new DateTime(2023, 6, 20, 1, 18, 44, 633, DateTimeKind.Utc).AddTicks(5555) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 228,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1500m, "BRL", 7.500m, 84, 14, 1500m, "BRL", 1507.500m, new DateTime(2023, 7, 14, 12, 58, 44, 633, DateTimeKind.Utc).AddTicks(5557) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 229,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "INR", 5.500m, 64, 70, 1100m, "INR", 1105.500m, new DateTime(2023, 8, 7, 0, 17, 44, 633, DateTimeKind.Utc).AddTicks(5560) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 230,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 600m, "RUB", 3.000m, 89, 122, 600m, "RUB", 603.000m, new DateTime(2023, 8, 11, 23, 34, 44, 633, DateTimeKind.Utc).AddTicks(5562) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 231,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "BGN", 38.000m, 19, 100, 7600m, "BGN", 7638.000m, new DateTime(2023, 7, 28, 19, 42, 44, 633, DateTimeKind.Utc).AddTicks(5565) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 232,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "JPY", 17.500m, 69, 45, 3500m, "JPY", 3517.500m, new DateTime(2023, 7, 24, 17, 34, 44, 633, DateTimeKind.Utc).AddTicks(5567) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 233,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 500m, "GBP", 2.500m, 90, 129, 500m, "GBP", 502.500m, new DateTime(2023, 6, 19, 23, 37, 44, 633, DateTimeKind.Utc).AddTicks(5570) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 234,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "CAD", 8.500m, 65, 4, 1700m, "CAD", 1708.500m, new DateTime(2023, 8, 2, 23, 54, 44, 633, DateTimeKind.Utc).AddTicks(5579) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 235,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 900m, "CHF", 4.500m, 100, 64, 900m, "CHF", 904.500m, new DateTime(2023, 5, 19, 4, 46, 44, 633, DateTimeKind.Utc).AddTicks(5582) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 236,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1300m, "ZAR", 6.500m, 127, 31, 1300m, "ZAR", 1306.500m, new DateTime(2023, 8, 2, 0, 58, 44, 633, DateTimeKind.Utc).AddTicks(5584) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 237,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "JPY", 11.500m, 97, 17, 2300m, "JPY", 2311.500m, new DateTime(2023, 6, 18, 15, 3, 44, 633, DateTimeKind.Utc).AddTicks(5587) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 238,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5500m, "EUR", 27.500m, 118, 5500m, "EUR", 5527.500m, new DateTime(2023, 7, 17, 10, 8, 44, 633, DateTimeKind.Utc).AddTicks(5589) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 239,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5700m, "RUB", 28.500m, 27, 92, 5700m, "RUB", 5728.500m, new DateTime(2023, 6, 26, 19, 49, 44, 633, DateTimeKind.Utc).AddTicks(5591) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 240,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7800m, "AUD", 39.000m, 51, 93, 7800m, "AUD", 7839.000m, new DateTime(2023, 6, 6, 14, 19, 44, 633, DateTimeKind.Utc).AddTicks(5594) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 241,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "ZAR", 42.500m, 29, 31, 8500m, "ZAR", 8542.500m, new DateTime(2023, 5, 29, 23, 34, 44, 633, DateTimeKind.Utc).AddTicks(5597) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 242,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5500m, "INR", 27.500m, 32, 124, 5500m, "INR", 5527.500m, new DateTime(2023, 6, 23, 17, 15, 44, 633, DateTimeKind.Utc).AddTicks(5600) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 243,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5100m, "SGD", 25.500m, 18, 13, 5100m, "SGD", 5125.500m, new DateTime(2023, 6, 27, 19, 0, 44, 633, DateTimeKind.Utc).AddTicks(5602) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 244,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, "AUD", 42.500m, 58, 97, 8500m, "AUD", 8542.500m, new DateTime(2023, 7, 23, 19, 59, 44, 633, DateTimeKind.Utc).AddTicks(5605) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 245,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "CAD", 11.500m, 22, 45, 2300m, "CAD", 2311.500m, new DateTime(2023, 8, 4, 22, 10, 44, 633, DateTimeKind.Utc).AddTicks(5608) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 246,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8700m, "JPY", 43.500m, 66, 17, 8700m, "JPY", 8743.500m, new DateTime(2023, 8, 10, 21, 32, 44, 633, DateTimeKind.Utc).AddTicks(5610) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 247,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2500m, "EUR", 12.500m, 36, 10, 2500m, "EUR", 2512.500m, new DateTime(2023, 6, 25, 9, 54, 44, 633, DateTimeKind.Utc).AddTicks(5612) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 248,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "CNY", 30.500m, 62, 94, 6100m, "CNY", 6130.500m, new DateTime(2023, 8, 6, 18, 29, 44, 633, DateTimeKind.Utc).AddTicks(5615) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 249,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, 42.500m, 24, 90, 8500m, 8542.500m, new DateTime(2023, 7, 24, 21, 9, 44, 633, DateTimeKind.Utc).AddTicks(5617) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 250,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6500m, "CAD", 32.500m, 31, 42, 6500m, "CAD", 6532.500m, new DateTime(2023, 8, 12, 20, 52, 44, 633, DateTimeKind.Utc).AddTicks(5620) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 251,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5500m, "BRL", 27.500m, 63, 99, 5500m, "BRL", 5527.500m, new DateTime(2023, 6, 17, 10, 16, 44, 633, DateTimeKind.Utc).AddTicks(5622) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 252,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "NZD", 44.000m, 100, 38, 8800m, "NZD", 8844.000m, new DateTime(2023, 6, 5, 14, 56, 44, 633, DateTimeKind.Utc).AddTicks(5625) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 253,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "ZAR", 15.000m, 10, 3000m, "ZAR", 3015.000m, new DateTime(2023, 8, 6, 5, 19, 44, 633, DateTimeKind.Utc).AddTicks(5627) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 254,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "BGN", 29.000m, 18, 119, 5800m, "BGN", 5829.000m, new DateTime(2023, 6, 21, 12, 22, 44, 633, DateTimeKind.Utc).AddTicks(5630) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 255,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7000m, "AUD", 35.000m, 46, 50, 7000m, "AUD", 7035.000m, new DateTime(2023, 8, 3, 0, 36, 44, 633, DateTimeKind.Utc).AddTicks(5633) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 256,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7100m, "AUD", 35.500m, 60, 107, 7100m, "AUD", 7135.500m, new DateTime(2023, 6, 17, 1, 14, 44, 633, DateTimeKind.Utc).AddTicks(5635) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 257,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "BRL", 8.500m, 125, 28, 1700m, "BRL", 1708.500m, new DateTime(2023, 8, 9, 12, 11, 44, 633, DateTimeKind.Utc).AddTicks(5638) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 258,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "BGN", 17.500m, 71, 3500m, "BGN", 3517.500m, new DateTime(2023, 7, 21, 1, 57, 44, 633, DateTimeKind.Utc).AddTicks(5642) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 259,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7400m, "GBP", 37.000m, 62, 61, 7400m, "GBP", 7437.000m, new DateTime(2023, 7, 25, 10, 0, 44, 633, DateTimeKind.Utc).AddTicks(5650) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "VerificationToken",
                value: new Guid("15ade644-97db-4af4-93e1-ae414a0d25b0"));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Red@abv.com", "0887743909", new Guid("6cf480b4-569e-4ae6-9053-15265ab249a1") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0885276988", new Guid("a7d4128a-ee4b-4f92-80a9-6afefaaff5a6") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Turbo@google.ru", "0884796759", new Guid("844ab745-734c-463b-ba17-29270391d8a4") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cuddlebug@yahoo.com", "0886071820", new Guid("b6e4e29d-79ac-4e7c-a08b-bae303c4aa13") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Thrill@google.net", "0889581483", new Guid("7b78665b-4c96-4580-966a-736c50945651") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "AsteroidZ@apple.ru", "0886169535", new Guid("8d9d6dde-88b4-4ae2-93b6-54fb54d7ced1") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "An1a@apple.bg", "0886081147", new Guid("a53f679d-6381-406b-bb86-60f2f06ecac0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Bluelight@google.eu", "0884254198", new Guid("ffe64756-6e93-41e3-89c7-01199cffdc25") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Loaded@abv.bg", "0887949511", new Guid("e0cc0714-5d27-4ff4-9e54-e4cd7e70c505") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Rant@yahoo.com", "0885673734", new Guid("ebdbd126-4921-41d0-a4b3-d27b2f9f10ba") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Diva@yahoo.bg", "0884154162", new Guid("5bfe34ef-d48f-49a0-924c-30267f5dae4f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Yukki@yahoo.net", "0883686085", new Guid("b019ca12-d320-43c6-ba5f-c79f590909cd") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Werice@abv.com", "0888196204", new Guid("e70ed9f0-210c-4bde-a942-c2ebeb8ad154") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Perfect@yahoo.bg", "0887924574", new Guid("25332981-9568-4a33-acc6-be94aa96069b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "MissK@abv.eu", "0888141280", new Guid("dfef1378-3f85-48ca-bff3-2dc90a2ffd63") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Bubbles@yahoo.net", "0883270573", new Guid("c3f05fc3-1987-4eef-8efb-59815d1a0802") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "ChEk@abv.bg", "0884113929", new Guid("e6f3ef95-bf59-42c2-b683-1a836d77b39b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0884753325", new Guid("77ac9b4a-45f3-4c02-a055-58a462f7d0a7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "HugsForDrubs@yahoo.bg", "0883347589", new Guid("71cd27d5-5b3a-4a2f-a782-09c74bfe2f65") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pippen@abv.bg", "0883946159", new Guid("9602c548-ca7e-4e46-a318-0200dcfe1365") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Aisha@yahoo.ru", "0888151171", new Guid("701e339b-4391-4834-bed4-c8353adbab64") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Protectshell@yahoo.eu", "0884125263", new Guid("62516eb0-eac1-4f2f-b799-e87fc6760894") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Geez@yahoo.bg", "0887274301", new Guid("bed24b08-354f-43b3-9e2e-8781ffd048a2") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Monkey_see@apple.bg", "0886855312", new Guid("3a16a4ab-3c46-4761-9c4c-58569e5faad6") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Tight@abv.ru", "0887011560", new Guid("b5033ea8-c4d1-42b2-913e-301914bd94fd") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0889690937", new Guid("1cd2385a-fa30-41a8-8001-8dbd128dd67a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Prong@abv.eu", "0884162166", new Guid("97c5f53f-d990-4dd0-ab70-bfa5c9ff20a5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Strongarmed@abv.com", "0888599828", new Guid("6380b436-b29b-4a9c-bb0c-1e83f119a8cd") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Sovenance@apple.eu", "0888421606", new Guid("a413c874-a48a-4cfe-a952-83b7b3f056a5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Angel@abv.net", "0886090779", new Guid("24a90555-b528-4bbf-8228-53e8a306209f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Mad@apple.eu", "0885814385", new Guid("c2350f26-56e6-4fbb-bd73-8513284ad47d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dimple@yahoo.ru", "0886218926", new Guid("7d520366-02f8-401b-b5d2-42cecf276ffe") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dollspell@google.ru", "0889961515", new Guid("e453e14b-38ed-4ecb-b912-0976f5bc6eef") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "State@abv.eu", "0886088613", new Guid("c011c906-7731-4c0f-8c45-4d83e968054b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Vellojoy@abv.com", "0885314616", new Guid("67a2e4a5-7029-4bd1-9233-ca4848954b87") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Eagle@abv.eu", "0885500829", new Guid("a268c320-7a6d-43ce-894c-622a52b6d65e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Brutal@abv.bg", "0886855891", new Guid("6cccbd24-7741-4716-b15e-af7ab9135436") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Solar@apple.com", "0889395489", new Guid("f4049612-30b6-4582-bded-3e7d8ba186b0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Intella@yahoo.eu", "0889769152", new Guid("d3782cdd-6146-47e9-bf27-ca5f1bca8e69") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Rainblow@apple.com", "0884143167", new Guid("cead993b-9276-4f18-a376-58bae1e89995") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Venereology@google.eu", "0883285831", new Guid("125246c7-c4f4-4ae3-ab3f-049f5f12d994") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dogbone@yahoo.com", "0886281002", new Guid("35274df3-cf08-416c-9199-8a90bbe64729") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Grep@abv.com", "0883309082", new Guid("8845e7af-17f1-4767-aa67-d4e35ee85f53") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hawkeye@google.ru", "0885321921", new Guid("62ab192c-f5f1-4211-8e0b-29788b958344") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Glue@apple.ru", "0884124845", new Guid("51d32a96-c563-47ec-8bc0-661b16257de5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Princess@abv.com", "0886464249", new Guid("ce2bb25b-4caf-4788-bb0c-4ca0546a6213") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "L4Legit@abv.ru", "0884645215", new Guid("c96013b9-c35f-41de-b0b6-b1b6e1312b44") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Peapod@google.ru", "0889613328", new Guid("dbb3ebb8-c95d-47c2-bee7-994141900d6a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Networked@google.net", "0885551399", new Guid("582e0613-52ef-4134-b249-f8bb7e1f6687") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Prep@abv.com", "0883951008", new Guid("b9838c41-e930-43b5-a686-4132d5b15739") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Sharken@apple.bg", "0883183960", new Guid("fe7af945-f9ad-44bd-9250-fcf81fba01fb") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Curio@abv.bg", "0885840710", new Guid("d5138480-2469-4e09-a1d0-b9547b50db66") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0885030035", new Guid("2a16621b-d525-4276-a1d4-d45118a7290a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0884574846", new Guid("a92b61be-0dfd-468d-be25-2999ffacd526") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Muffinhead@apple.eu", "0883655272", new Guid("6813299a-3d09-4e67-95e1-2b4905d8dd84") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Zyber@abv.net", "0884012544", new Guid("141171b8-fa71-4403-861c-ee13418f2c6c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Moonchild@apple.eu", "0883502016", new Guid("0169a818-9bd0-4f48-a177-51cf71f0a74e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Precision@google.bg", "0884460259", new Guid("32d7cee1-668b-41df-8f36-449f6a47d946") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hydrospace@apple.bg", "0884983077", new Guid("1b54ec5c-983e-42b6-a172-0605ab148d7f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Talon@abv.bg", "0886533775", new Guid("1fc52b67-3c00-4c2a-85ae-92ca6b5e4158") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "BattlerBeauty@abv.com", "0889263584", new Guid("43e91bd6-d282-4bf5-8b51-fcaaf82ab7f6") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Delta@google.eu", "0885467651", new Guid("2960e9d7-e8de-41f8-85fd-11ac5ba377d9") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Lens@google.ru", "0886417690", new Guid("46d16f6a-8403-45ac-9c70-2a422707541c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Spit@google.eu", "0888106747", new Guid("2fd14b70-c074-43ac-b890-aae402c6a18a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Zoom@abv.bg", "0884563655", new Guid("027ee604-7eff-4740-b21f-c6cca6510ce0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Gothichic@google.com", "0887401835", new Guid("a81446ae-fc22-4baf-a820-f0523481f80d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Purple@apple.net", "0887817509", new Guid("1c9d58e0-bdb4-4086-a837-ccaa9616d9ca") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Ahem@google.net", "0888542982", new Guid("da294da6-65fd-4e13-856d-b6486abe6638") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Swag@yahoo.bg", "0884671918", new Guid("a01253ea-0886-43a5-9487-20f774e13766") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cuteness@yahoo.bg", "0888545839", new Guid("5a9d6744-eedf-4e86-949f-d1b0c04f3938") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "SkillDevil@yahoo.com", "0883103003", new Guid("5880da0e-828d-4984-bb75-d64c2be149b5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Island@apple.com", "0888909538", new Guid("022180db-7c60-49e0-ad45-e09b7115bddd") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Gran@apple.bg", "0885445831", new Guid("df343d85-5ace-443b-ba12-324839e25f6d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cosmosaegis@google.eu", "0889045355", new Guid("3f6849b4-a763-4a73-a0d6-9b1a8fac440c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dove@google.bg", "0886232456", new Guid("2ea218d1-0ade-46b1-95d4-ba4e156ef5d8") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Crybaby@abv.net", "0888917694", new Guid("078b53fc-3b19-4427-8258-8d8bd2c26272") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cyberproof@abv.ru", "0885445109", new Guid("b1852ca9-c404-4dec-9c36-25afbabdaf7d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dedicated@abv.net", "0889621388", new Guid("965b0ddb-e630-4281-97a8-f3c8b919f228") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Epic@apple.net", "0884919251", new Guid("4426fd8a-8be9-475e-986c-59e0739f1d7b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Secure@abv.ru", "0886171384", new Guid("91550cf9-4099-4461-9eb4-cc596b2808a7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Doppler@apple.ru", "0888074736", new Guid("87066183-dc0d-416f-9058-19f8e2a2e042") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Buddy@google.eu", "0884610717", new Guid("718ed21b-1610-42c4-a57a-60aae290f4e3") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "FibBage@apple.com", "0883660759", new Guid("e148d7c2-37ce-4a29-9162-a87e160a8baf") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Strawboy@yahoo.eu", "0886631971", new Guid("5a1c56ab-dc4e-4a81-8638-5b7a14cd5ce0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Piggy@yahoo.ru", "0885967514", new Guid("bc3793d6-0f67-47ec-8d7f-c58e0f725efd") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Homes@google.net", "0883058194", new Guid("2e12315b-7bce-44b5-b769-5bbbb34978bf") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "JellyBott_om@apple.eu", "0885943733", new Guid("74555638-0d2a-45d9-8286-2ce4262f63ed") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Marill@google.eu", "0888371562", new Guid("b5bf8ceb-a7a4-4c13-be4b-94c92df04488") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Guaranteed@abv.ru", "0888299071", new Guid("ef092368-e271-4cf0-aebb-b3df07c4c52c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hardcover@apple.eu", "0883820552", new Guid("8106cf61-69b0-4f57-a94d-1249ab094e59") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Warmblush@google.com", "0886684763", new Guid("bf8015a8-2be9-4c3d-8426-cdeaeed20f12") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0888069015", new Guid("a12d8618-8962-4fe8-a279-dadb008db872") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Chub@google.bg", "0889902144", new Guid("35d14650-b085-495c-9c23-c4cd71a8aa14") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Paradise@yahoo.eu", "0886902785", new Guid("6d60ff19-2ecc-4d21-b4cf-3919e6c28c98") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Complete@yahoo.ru", "0884253000", new Guid("9c0c5968-ebdf-47cf-999e-5b7cbc601d03") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Moonlight@abv.com", "0887436556", new Guid("fb3f0117-af90-467c-86f7-dac12d20746f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Unicorn@yahoo.bg", "0883293203", new Guid("24dd2dbf-2520-4c55-a0f7-3254c9d411f1") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Limonad@google.com", "0885364255", new Guid("612ead63-cada-4ab9-a347-9e355454588a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0885669418", new Guid("8cb98f1d-282f-404a-92c0-58070107cfb7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Sly@yahoo.eu", "0885652967", new Guid("2ba10aea-24c6-49d6-a9a9-1dffb8b46030") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Mercy@yahoo.com", "0886741735", new Guid("f54cfb88-769b-4601-8b0c-0003656f155c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cutie@abv.com", "0883951589", new Guid("7b7dda26-0198-4f65-adfc-41a3d7741be4") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cyberrock@yahoo.net", "0884227806", new Guid("2ca09bc5-d204-4cd1-a7cf-1653a97053c7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Safenet@apple.eu", "0887915509", new Guid("2630083b-aa4c-465d-a047-eabdbf8566ae") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Shine@apple.bg", "0889871006", new Guid("06ddee07-79fa-4414-b4da-f6be8bd728e5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Night@google.com", "0883493750", new Guid("06f8a111-8c11-4dc4-9a88-b04f3065ce3e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Task@apple.ru", "0883523336", new Guid("e63c8983-77c2-4d08-93a6-5329114e4e6b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Septenary@yahoo.bg", "0889794960", new Guid("320cd31c-8c7c-4d9b-9375-24cf73858d99") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Queenberry@yahoo.eu", "0888535705", new Guid("acbc776e-2fc4-4b62-b708-5b8221959106") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "DrawFul@abv.ru", "0883896946", new Guid("d24b3993-703d-436c-b2b2-d79b47da7e3b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Proeyeroller@apple.ru", "0888645284", new Guid("bac345ab-ebb7-476d-bce4-a580a8dfb62d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Bad@google.com", "0887700807", new Guid("393daa98-a571-4795-83ac-99ad75a379eb") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0886214960", new Guid("489821b8-a782-483a-b6d1-e7da4e9a879c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Secret@abv.eu", "0886088170", new Guid("4456bb42-df72-47c6-9b1f-92267948281b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Elite@google.net", "0885693959", new Guid("92fbbb36-cff1-46e1-a9bd-0371f5c8608a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0887120664", new Guid("ae5b2b57-ea87-42dc-9b25-f0eca7079380") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0888478200", new Guid("39f93409-642d-425c-a069-8ae91de4bf8e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dark@yahoo.com", "0883004298", new Guid("e76a31ef-caf3-4ace-b461-00a462a06e53") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Blu@yahoo.ru", "0884816819", new Guid("c4b034e4-f9a5-4a99-830c-8449c7664364") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Crashion@apple.net", "0883644382", new Guid("222ad6b6-efb9-4c24-b25c-f8417480121e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Star@google.net", "0883409442", new Guid("e7e1cb56-6c4f-4b18-8e4e-f7e5412b669b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pink@apple.net", "0884378934", new Guid("cd47b5fd-8e38-4e45-aa6d-00a0cb796e5a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Kitten@yahoo.bg", "0889876433", new Guid("290a5f20-6434-4a2c-8f5f-b1c03a2909e5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Xentrix@yahoo.net", "0888504034", new Guid("2903bdc6-8fd0-4800-85bc-a1ee0640960d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hug@abv.net", "0885683252", new Guid("a9249c1b-e239-4cdc-bf61-dbd5fc4b9fe4") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pioneer@abv.eu", "0888718664", new Guid("02664329-1a22-4e71-8cc0-a6659f85990d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pro@yahoo.net", "0886312681", new Guid("b1dd474b-641c-4930-87e0-2a98cca363b7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Alert@apple.eu", "0884887875", new Guid("9dc8d7e2-fc68-4754-b05a-403ca9b8a57a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Fittofuel@yahoo.net", "0886895216", new Guid("8c4693e9-0404-45a6-b385-862e66482fa6") });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 42200m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 99100m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 44300m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 69600m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 40500m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 60600m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 2600m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 23300m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 63000m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 43200m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 85600m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 95700m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 30500m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28200m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 1100m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 44600m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 2800m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 20300m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 68500m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 35200m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 20700m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 82500m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 77700m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 39300m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 3100m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 66300m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 25200m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 17700m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 90000m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 82800m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 6100m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 35300m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 83300m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 44900m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 96000m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 31800m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 76300m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 5700m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 42100m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 60800m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 23900m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 73000m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 9700m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 54400m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 30000m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28100m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 50200m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 46500m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 1800m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 40200m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 80600m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 46000m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 64500m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 54,
                column: "Balance",
                value: 12800m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 10100m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 17900m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 84100m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 4000m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 59,
                column: "Balance",
                value: 98200m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28000m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 32800m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 12800m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 24600m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 90700m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 39100m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 30500m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 77400m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 90200m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 68800m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 63000m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 38000m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 77100m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 52400m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 81500m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 52100m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 30100m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 81100m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 78,
                column: "Balance",
                value: 51200m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 37900m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 80,
                column: "Balance",
                value: 39200m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28500m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 25800m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 27500m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 21300m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 38500m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 35000m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 85300m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 66800m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 33800m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 83200m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 86900m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 5900m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 91000m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 8400m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 95,
                column: "Balance",
                value: 86400m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 47800m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 48600m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 37900m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 15400m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 16600m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 74600m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 9400m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 72200m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 98000m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 21000m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 51800m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 97800m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 29000m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 1300m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 37100m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 59300m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28800m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 23600m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 51000m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 39000m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 35500m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 68000m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 73000m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 87300m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 8500m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 29300m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 25400m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 94600m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 85700m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 84100m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 69100m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 53300m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 45400m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 81600m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 22400m, "INR" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8400m, "BRL", 42.000m, 80, 127, 8400m, "BRL", 8442.000m, new DateTime(2023, 6, 16, 13, 32, 51, 541, DateTimeKind.Utc).AddTicks(1893) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2900m, "JPY", 14.500m, 44, 31, 2900m, "JPY", 2914.500m, new DateTime(2023, 7, 21, 10, 42, 51, 541, DateTimeKind.Utc).AddTicks(1900) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1000m, "ZAR", 5.000m, 77, 126, 1000m, "ZAR", 1005.000m, new DateTime(2023, 6, 21, 2, 24, 51, 541, DateTimeKind.Utc).AddTicks(1903) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4600m, "BGN", 23.000m, 63, 45, 4600m, "BGN", 4623.000m, new DateTime(2023, 8, 11, 16, 49, 51, 541, DateTimeKind.Utc).AddTicks(1905) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1600m, "BRL", 8.000m, 81, 106, 1600m, "BRL", 1608.000m, new DateTime(2023, 8, 8, 0, 24, 51, 541, DateTimeKind.Utc).AddTicks(1908) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8400m, "ZAR", 42.000m, 98, 13, 8400m, "ZAR", 8442.000m, new DateTime(2023, 6, 16, 22, 37, 51, 541, DateTimeKind.Utc).AddTicks(1911) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8700m, "EUR", 43.500m, 13, 3, 8700m, "EUR", 8743.500m, new DateTime(2023, 8, 2, 0, 23, 51, 541, DateTimeKind.Utc).AddTicks(1913) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "RUB", 25.000m, 45, 24, 5000m, "RUB", 5025.000m, new DateTime(2023, 8, 6, 13, 12, 51, 541, DateTimeKind.Utc).AddTicks(1915) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "CNY", 6.000m, 62, 93, 1200m, "CNY", 1206.000m, new DateTime(2023, 8, 2, 20, 3, 51, 541, DateTimeKind.Utc).AddTicks(1918) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4000m, "AUD", 20.000m, 108, 54, 4000m, "AUD", 4020.000m, new DateTime(2023, 6, 7, 2, 23, 51, 541, DateTimeKind.Utc).AddTicks(1920) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6900m, "BGN", 34.500m, 5, 11, 6900m, "BGN", 6934.500m, new DateTime(2023, 7, 5, 9, 4, 51, 541, DateTimeKind.Utc).AddTicks(1922) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9300m, "AUD", 46.500m, 95, 91, 9300m, "AUD", 9346.500m, new DateTime(2023, 5, 31, 22, 35, 51, 541, DateTimeKind.Utc).AddTicks(1925) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "GBP", 45.500m, 24, 57, 9100m, "GBP", 9145.500m, new DateTime(2023, 6, 10, 5, 8, 51, 541, DateTimeKind.Utc).AddTicks(1927) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1800m, "NZD", 9.000m, 24, 79, 1800m, "NZD", 1809.000m, new DateTime(2023, 7, 20, 6, 2, 51, 541, DateTimeKind.Utc).AddTicks(1929) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5700m, "ZAR", 28.500m, 25, 18, 5700m, "ZAR", 5728.500m, new DateTime(2023, 6, 19, 3, 48, 51, 541, DateTimeKind.Utc).AddTicks(1931) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8300m, "AUD", 41.500m, 86, 122, 8300m, "AUD", 8341.500m, new DateTime(2023, 6, 25, 20, 10, 51, 541, DateTimeKind.Utc).AddTicks(1977) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9200m, "JPY", 46.000m, 56, 108, 9200m, "JPY", 9246.000m, new DateTime(2023, 7, 26, 12, 2, 51, 541, DateTimeKind.Utc).AddTicks(1979) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3800m, "RUB", 19.000m, 39, 54, 3800m, "RUB", 3819.000m, new DateTime(2023, 7, 23, 13, 54, 51, 541, DateTimeKind.Utc).AddTicks(1982) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 100m, "BRL", 0.500m, 6, 19, 100m, "BRL", 100.500m, new DateTime(2023, 6, 23, 17, 8, 51, 541, DateTimeKind.Utc).AddTicks(1985) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6900m, "CAD", 34.500m, 48, 89, 6900m, "CAD", 6934.500m, new DateTime(2023, 5, 22, 16, 31, 51, 541, DateTimeKind.Utc).AddTicks(1987) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "CHF", 22.000m, 79, 20, 4400m, "CHF", 4422.000m, new DateTime(2023, 6, 25, 8, 24, 51, 541, DateTimeKind.Utc).AddTicks(1989) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "CAD", 17.500m, 77, 98, 3500m, "CAD", 3517.500m, new DateTime(2023, 8, 7, 8, 55, 51, 541, DateTimeKind.Utc).AddTicks(1991) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "USD", 44.000m, 86, 77, 8800m, "USD", 8844.000m, new DateTime(2023, 8, 13, 21, 51, 51, 541, DateTimeKind.Utc).AddTicks(1993) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1400m, "JPY", 7.000m, 84, 85, 1400m, "JPY", 1407.000m, new DateTime(2023, 8, 2, 23, 8, 51, 541, DateTimeKind.Utc).AddTicks(1995) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8700m, "SGD", 43.500m, 51, 79, 8700m, "SGD", 8743.500m, new DateTime(2023, 7, 31, 18, 55, 51, 541, DateTimeKind.Utc).AddTicks(1998) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "BGN", 8.500m, 11, 100, 1700m, "BGN", 1708.500m, new DateTime(2023, 7, 18, 5, 26, 51, 541, DateTimeKind.Utc).AddTicks(2000) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "BGN", 44.000m, 84, 82, 8800m, "BGN", 8844.000m, new DateTime(2023, 5, 19, 15, 49, 51, 541, DateTimeKind.Utc).AddTicks(2002) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 800m, "INR", 4.000m, 91, 52, 800m, "INR", 804.000m, new DateTime(2023, 8, 10, 2, 34, 51, 541, DateTimeKind.Utc).AddTicks(2004) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3800m, "EUR", 19.000m, 4, 42, 3800m, "EUR", 3819.000m, new DateTime(2023, 5, 24, 19, 0, 51, 541, DateTimeKind.Utc).AddTicks(2006) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8100m, "NZD", 40.500m, 29, 96, 8100m, "NZD", 8140.500m, new DateTime(2023, 5, 18, 21, 23, 51, 541, DateTimeKind.Utc).AddTicks(2036) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2100m, "BGN", 10.500m, 60, 41, 2100m, "BGN", 2110.500m, new DateTime(2023, 7, 31, 4, 34, 51, 541, DateTimeKind.Utc).AddTicks(2038) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "NZD", 38.000m, 45, 60, 7600m, "NZD", 7638.000m, new DateTime(2023, 7, 5, 7, 8, 51, 541, DateTimeKind.Utc).AddTicks(2041) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3400m, "RUB", 17.000m, 58, 18, 3400m, "RUB", 3417.000m, new DateTime(2023, 5, 31, 18, 3, 51, 541, DateTimeKind.Utc).AddTicks(2043) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5900m, "CAD", 29.500m, 120, 80, 5900m, "CAD", 5929.500m, new DateTime(2023, 6, 1, 21, 37, 51, 541, DateTimeKind.Utc).AddTicks(2046) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "GBP", 20.500m, 120, 128, 4100m, "GBP", 4120.500m, new DateTime(2023, 8, 9, 1, 24, 51, 541, DateTimeKind.Utc).AddTicks(2048) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3400m, "RUB", 17.000m, 64, 33, 3400m, "RUB", 3417.000m, new DateTime(2023, 6, 28, 22, 47, 51, 541, DateTimeKind.Utc).AddTicks(2050) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "EUR", 30, 112, "EUR", new DateTime(2023, 7, 15, 6, 44, 51, 541, DateTimeKind.Utc).AddTicks(2052) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5700m, "USD", 28.500m, 45, 106, 5700m, "USD", 5728.500m, new DateTime(2023, 5, 24, 11, 45, 51, 541, DateTimeKind.Utc).AddTicks(2054) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "SGD", 34.000m, 82, 46, 6800m, "SGD", 6834.000m, new DateTime(2023, 8, 6, 3, 53, 51, 541, DateTimeKind.Utc).AddTicks(2056) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6300m, "CNY", 31.500m, 14, 79, 6300m, "CNY", 6331.500m, new DateTime(2023, 8, 2, 14, 16, 51, 541, DateTimeKind.Utc).AddTicks(2058) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 600m, "CAD", 3.000m, 80, 109, 600m, "CAD", 603.000m, new DateTime(2023, 7, 22, 16, 4, 51, 541, DateTimeKind.Utc).AddTicks(2061) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "SGD", 6.000m, 14, 86, 1200m, "SGD", 1206.000m, new DateTime(2023, 7, 26, 21, 14, 51, 541, DateTimeKind.Utc).AddTicks(2063) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9600m, "INR", 48.000m, 120, 95, 9600m, "INR", 9648.000m, new DateTime(2023, 6, 3, 18, 54, 51, 541, DateTimeKind.Utc).AddTicks(2066) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 100m, "SGD", 0.500m, 79, 29, 100m, "SGD", 100.500m, new DateTime(2023, 7, 28, 17, 59, 51, 541, DateTimeKind.Utc).AddTicks(2068) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4200m, "NZD", 21.000m, 19, 8, 4200m, "NZD", 4221.000m, new DateTime(2023, 7, 5, 22, 16, 51, 541, DateTimeKind.Utc).AddTicks(2070) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5300m, "USD", 26.500m, 34, 112, 5300m, "USD", 5326.500m, new DateTime(2023, 7, 16, 10, 38, 51, 541, DateTimeKind.Utc).AddTicks(2072) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "GBP", 22.000m, 88, 64, 4400m, "GBP", 4422.000m, new DateTime(2023, 7, 7, 8, 25, 51, 541, DateTimeKind.Utc).AddTicks(2074) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2900m, "CAD", 14.500m, 11, 14, 2900m, "CAD", 2914.500m, new DateTime(2023, 8, 15, 1, 51, 51, 541, DateTimeKind.Utc).AddTicks(2076) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2200m, "NZD", 11.000m, 92, 85, 2200m, "NZD", 2211.000m, new DateTime(2023, 6, 19, 20, 50, 51, 541, DateTimeKind.Utc).AddTicks(2078) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6200m, "BGN", 31.000m, 109, 49, 6200m, "BGN", 6231.000m, new DateTime(2023, 7, 19, 15, 18, 51, 541, DateTimeKind.Utc).AddTicks(2080) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "CHF", 30.500m, 92, 121, 6100m, "CHF", 6130.500m, new DateTime(2023, 7, 25, 21, 45, 51, 541, DateTimeKind.Utc).AddTicks(2082) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 8400m, 42.000m, 65, 102, 8400m, 8442.000m, new DateTime(2023, 7, 19, 14, 12, 51, 541, DateTimeKind.Utc).AddTicks(2084) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7700m, "RUB", 38.500m, 105, 58, 7700m, "RUB", 7738.500m, new DateTime(2023, 5, 19, 1, 2, 51, 541, DateTimeKind.Utc).AddTicks(2087) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9600m, "CHF", 48.000m, 19, 71, 9600m, "CHF", 9648.000m, new DateTime(2023, 8, 8, 0, 59, 51, 541, DateTimeKind.Utc).AddTicks(2089) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "BRL", 20.500m, 127, 26, 4100m, "BRL", 4120.500m, new DateTime(2023, 7, 24, 22, 20, 51, 541, DateTimeKind.Utc).AddTicks(2091) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4900m, "SGD", 24.500m, 94, 68, 4900m, "SGD", 4924.500m, new DateTime(2023, 7, 24, 5, 57, 51, 541, DateTimeKind.Utc).AddTicks(2093) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 600m, "NZD", 3.000m, 114, 83, 600m, "NZD", 603.000m, new DateTime(2023, 8, 13, 22, 30, 51, 541, DateTimeKind.Utc).AddTicks(2095) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1100m, "SGD", 5.500m, 60, 15, 1100m, "SGD", 1105.500m, new DateTime(2023, 6, 25, 17, 30, 51, 541, DateTimeKind.Utc).AddTicks(2097) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "INR", 50, 30, "INR", new DateTime(2023, 6, 13, 10, 20, 51, 541, DateTimeKind.Utc).AddTicks(2099) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4500m, "ZAR", 22.500m, 100, 92, 4500m, "ZAR", 4522.500m, new DateTime(2023, 7, 20, 1, 59, 51, 541, DateTimeKind.Utc).AddTicks(2101) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3200m, "BRL", 16.000m, 64, 70, 3200m, "BRL", 3216.000m, new DateTime(2023, 6, 13, 16, 11, 51, 541, DateTimeKind.Utc).AddTicks(2103) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5700m, "CHF", 28.500m, 3, 106, 5700m, "CHF", 5728.500m, new DateTime(2023, 8, 2, 13, 7, 51, 541, DateTimeKind.Utc).AddTicks(2105) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4000m, "BGN", 20.000m, 105, 38, 4000m, "BGN", 4020.000m, new DateTime(2023, 6, 23, 19, 22, 51, 541, DateTimeKind.Utc).AddTicks(2107) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, "USD", 21.500m, 110, 65, 4300m, "USD", 4321.500m, new DateTime(2023, 6, 15, 16, 5, 51, 541, DateTimeKind.Utc).AddTicks(2109) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3800m, "SGD", 19.000m, 57, 18, 3800m, "SGD", 3819.000m, new DateTime(2023, 8, 14, 7, 6, 51, 541, DateTimeKind.Utc).AddTicks(2111) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4800m, "USD", 24.000m, 72, 36, 4800m, "USD", 4824.000m, new DateTime(2023, 8, 13, 21, 17, 51, 541, DateTimeKind.Utc).AddTicks(2114) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6000m, "NZD", 30.000m, 73, 76, 6000m, "NZD", 6030.000m, new DateTime(2023, 5, 23, 10, 44, 51, 541, DateTimeKind.Utc).AddTicks(2117) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8300m, "CNY", 41.500m, 74, 116, 8300m, "CNY", 8341.500m, new DateTime(2023, 6, 15, 18, 13, 51, 541, DateTimeKind.Utc).AddTicks(2119) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6200m, "CHF", 31.000m, 104, 14, 6200m, "CHF", 6231.000m, new DateTime(2023, 7, 31, 5, 6, 51, 541, DateTimeKind.Utc).AddTicks(2121) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8100m, "BRL", 40.500m, 11, 10, 8100m, "BRL", 8140.500m, new DateTime(2023, 8, 6, 16, 40, 51, 541, DateTimeKind.Utc).AddTicks(2123) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "RUB", 30.500m, 111, 88, 6100m, "RUB", 6130.500m, new DateTime(2023, 7, 18, 16, 33, 51, 541, DateTimeKind.Utc).AddTicks(2125) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6400m, "NZD", 32.000m, 85, 126, 6400m, "NZD", 6432.000m, new DateTime(2023, 7, 5, 17, 27, 51, 541, DateTimeKind.Utc).AddTicks(2145) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 8500m, 42.500m, 66, 110, 8500m, 8542.500m, new DateTime(2023, 5, 28, 18, 16, 51, 541, DateTimeKind.Utc).AddTicks(2147) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6700m, "SGD", 33.500m, 13, 68, 6700m, "SGD", 6733.500m, new DateTime(2023, 7, 14, 11, 5, 51, 541, DateTimeKind.Utc).AddTicks(2149) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "CAD", 22.000m, 20, 91, 4400m, "CAD", 4422.000m, new DateTime(2023, 6, 29, 10, 8, 51, 541, DateTimeKind.Utc).AddTicks(2151) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8600m, "BRL", 43.000m, 58, 120, 8600m, "BRL", 8643.000m, new DateTime(2023, 5, 19, 2, 55, 51, 541, DateTimeKind.Utc).AddTicks(2153) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "CNY", 25.000m, 7, 10, 5000m, "CNY", 5025.000m, new DateTime(2023, 6, 24, 12, 35, 51, 541, DateTimeKind.Utc).AddTicks(2155) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2800m, "ZAR", 14.000m, 95, 2, 2800m, "ZAR", 2814.000m, new DateTime(2023, 8, 2, 5, 5, 51, 541, DateTimeKind.Utc).AddTicks(2157) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4500m, "SGD", 22.500m, 61, 89, 4500m, "SGD", 4522.500m, new DateTime(2023, 7, 29, 4, 16, 51, 541, DateTimeKind.Utc).AddTicks(2159) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6300m, "NZD", 31.500m, 114, 66, 6300m, "NZD", 6331.500m, new DateTime(2023, 7, 30, 22, 56, 51, 541, DateTimeKind.Utc).AddTicks(2161) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "USD", 25.000m, 42, 36, 5000m, "USD", 5025.000m, new DateTime(2023, 6, 28, 1, 25, 51, 541, DateTimeKind.Utc).AddTicks(2163) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 1600m, 8.000m, 93, 106, 1600m, 1608.000m, new DateTime(2023, 6, 27, 14, 26, 51, 541, DateTimeKind.Utc).AddTicks(2165) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2500m, "USD", 12.500m, 48, 102, 2500m, "USD", 2512.500m, new DateTime(2023, 7, 3, 17, 59, 51, 541, DateTimeKind.Utc).AddTicks(2167) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3700m, "ZAR", 18.500m, 15, 88, 3700m, "ZAR", 3718.500m, new DateTime(2023, 8, 16, 3, 37, 51, 541, DateTimeKind.Utc).AddTicks(2169) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "INR", 29.000m, 72, 80, 5800m, "INR", 5829.000m, new DateTime(2023, 8, 4, 15, 50, 51, 541, DateTimeKind.Utc).AddTicks(2171) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3300m, "CNY", 16.500m, 125, 71, 3300m, "CNY", 3316.500m, new DateTime(2023, 8, 15, 1, 58, 51, 541, DateTimeKind.Utc).AddTicks(2173) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2200m, "EUR", 11.000m, 47, 14, 2200m, "EUR", 2211.000m, new DateTime(2023, 8, 2, 0, 45, 51, 541, DateTimeKind.Utc).AddTicks(2175) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 7900m, 39.500m, 111, 40, 7900m, 7939.500m, new DateTime(2023, 6, 28, 8, 57, 51, 541, DateTimeKind.Utc).AddTicks(2177) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7300m, "SGD", 36.500m, 65, 11, 7300m, "SGD", 7336.500m, new DateTime(2023, 6, 4, 2, 18, 51, 541, DateTimeKind.Utc).AddTicks(2180) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5900m, "SGD", 29.500m, 97, 20, 5900m, "SGD", 5929.500m, new DateTime(2023, 5, 21, 16, 56, 51, 541, DateTimeKind.Utc).AddTicks(2182) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3200m, "ZAR", 16.000m, 81, 94, 3200m, "ZAR", 3216.000m, new DateTime(2023, 7, 27, 0, 8, 51, 541, DateTimeKind.Utc).AddTicks(2184) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "INR", 20.500m, 25, 106, 4100m, "INR", 4120.500m, new DateTime(2023, 5, 23, 17, 5, 51, 541, DateTimeKind.Utc).AddTicks(2186) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "ZAR", 22.000m, 9, 22, 4400m, "ZAR", 4422.000m, new DateTime(2023, 6, 10, 22, 6, 51, 541, DateTimeKind.Utc).AddTicks(2188) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "EUR", 8.500m, 66, 60, 1700m, "EUR", 1708.500m, new DateTime(2023, 8, 6, 23, 28, 51, 541, DateTimeKind.Utc).AddTicks(2190) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5200m, "RUB", 26.000m, 121, 79, 5200m, "RUB", 5226.000m, new DateTime(2023, 6, 26, 20, 3, 51, 541, DateTimeKind.Utc).AddTicks(2192) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7700m, "BGN", 38.500m, 19, 22, 7700m, "BGN", 7738.500m, new DateTime(2023, 6, 28, 6, 1, 51, 541, DateTimeKind.Utc).AddTicks(2194) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7700m, "CAD", 38.500m, 111, 21, 7700m, "CAD", 7738.500m, new DateTime(2023, 6, 6, 6, 27, 51, 541, DateTimeKind.Utc).AddTicks(2196) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8900m, "CNY", 44.500m, 93, 122, 8900m, "CNY", 8944.500m, new DateTime(2023, 6, 15, 10, 41, 51, 541, DateTimeKind.Utc).AddTicks(2198) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6600m, "RUB", 33.000m, 28, 53, 6600m, "RUB", 6633.000m, new DateTime(2023, 7, 2, 4, 32, 51, 541, DateTimeKind.Utc).AddTicks(2200) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "GBP", 41.000m, 30, 93, 8200m, "GBP", 8241.000m, new DateTime(2023, 6, 9, 12, 33, 51, 541, DateTimeKind.Utc).AddTicks(2202) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2600m, "CNY", 13.000m, 81, 87, 2600m, "CNY", 2613.000m, new DateTime(2023, 6, 8, 15, 21, 51, 541, DateTimeKind.Utc).AddTicks(2204) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 6600m, 33.000m, 41, 116, 6600m, 6633.000m, new DateTime(2023, 5, 30, 9, 17, 51, 541, DateTimeKind.Utc).AddTicks(2206) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3200m, "AUD", 16.000m, 128, 74, 3200m, "AUD", 3216.000m, new DateTime(2023, 8, 10, 18, 47, 51, 541, DateTimeKind.Utc).AddTicks(2208) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1600m, "NZD", 8.000m, 103, 18, 1600m, "NZD", 1608.000m, new DateTime(2023, 5, 27, 16, 56, 51, 541, DateTimeKind.Utc).AddTicks(2210) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4800m, "AUD", 24.000m, 97, 31, 4800m, "AUD", 4824.000m, new DateTime(2023, 6, 12, 15, 53, 51, 541, DateTimeKind.Utc).AddTicks(2212) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4500m, "CHF", 22.500m, 90, 67, 4500m, "CHF", 4522.500m, new DateTime(2023, 6, 16, 14, 41, 51, 541, DateTimeKind.Utc).AddTicks(2214) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, 30.500m, 123, 85, 6100m, 6130.500m, new DateTime(2023, 5, 17, 18, 51, 51, 541, DateTimeKind.Utc).AddTicks(2217) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6300m, "CHF", 31.500m, 37, 76, 6300m, "CHF", 6331.500m, new DateTime(2023, 5, 20, 21, 14, 51, 541, DateTimeKind.Utc).AddTicks(2218) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1600m, "JPY", 8.000m, 38, 105, 1600m, "JPY", 1608.000m, new DateTime(2023, 6, 6, 14, 46, 51, 541, DateTimeKind.Utc).AddTicks(2221) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "USD", 29.000m, 31, 48, 5800m, "USD", 5829.000m, new DateTime(2023, 6, 13, 19, 29, 51, 541, DateTimeKind.Utc).AddTicks(2222) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 700m, "BRL", 3.500m, 100, 46, 700m, "BRL", 703.500m, new DateTime(2023, 6, 15, 20, 14, 51, 541, DateTimeKind.Utc).AddTicks(2224) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9500m, "INR", 47.500m, 11, 45, 9500m, "INR", 9547.500m, new DateTime(2023, 6, 22, 8, 40, 51, 541, DateTimeKind.Utc).AddTicks(2226) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 3400m, 17.000m, 65, 94, 3400m, 3417.000m, new DateTime(2023, 5, 22, 11, 4, 51, 541, DateTimeKind.Utc).AddTicks(2228) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 1800m, 9.000m, 11, 68, 1800m, 1809.000m, new DateTime(2023, 6, 4, 4, 45, 51, 541, DateTimeKind.Utc).AddTicks(2230) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, 44.000m, 81, 80, 8800m, 8844.000m, new DateTime(2023, 6, 18, 17, 3, 51, 541, DateTimeKind.Utc).AddTicks(2233) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3600m, "GBP", 18.000m, 4, 124, 3600m, "GBP", 3618.000m, new DateTime(2023, 7, 6, 14, 8, 51, 541, DateTimeKind.Utc).AddTicks(2234) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3100m, "CNY", 15.500m, 93, 28, 3100m, "CNY", 3115.500m, new DateTime(2023, 6, 6, 21, 46, 51, 541, DateTimeKind.Utc).AddTicks(2237) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7900m, "SGD", 39.500m, 80, 31, 7900m, "SGD", 7939.500m, new DateTime(2023, 6, 12, 5, 36, 51, 541, DateTimeKind.Utc).AddTicks(2239) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5100m, "CHF", 25.500m, 19, 52, 5100m, "CHF", 5125.500m, new DateTime(2023, 6, 19, 8, 7, 51, 541, DateTimeKind.Utc).AddTicks(2241) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "NZD", 22.000m, 116, 108, 4400m, "NZD", 4422.000m, new DateTime(2023, 7, 3, 19, 31, 51, 541, DateTimeKind.Utc).AddTicks(2243) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9500m, "JPY", 47.500m, 1, 6, 9500m, "JPY", 9547.500m, new DateTime(2023, 7, 15, 18, 9, 51, 541, DateTimeKind.Utc).AddTicks(2245) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4000m, "JPY", 20.000m, 57, 30, 4000m, "JPY", 4020.000m, new DateTime(2023, 5, 21, 19, 0, 51, 541, DateTimeKind.Utc).AddTicks(2247) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9800m, "RUB", 49.000m, 71, 23, 9800m, "RUB", 9849.000m, new DateTime(2023, 7, 8, 16, 6, 51, 541, DateTimeKind.Utc).AddTicks(2267) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 100m, "BGN", 0.500m, 20, 128, 100m, "BGN", 100.500m, new DateTime(2023, 6, 26, 11, 32, 51, 541, DateTimeKind.Utc).AddTicks(2269) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9300m, "JPY", 46.500m, 56, 53, 9300m, "JPY", 9346.500m, new DateTime(2023, 7, 22, 19, 16, 51, 541, DateTimeKind.Utc).AddTicks(2271) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7400m, "ZAR", 37.000m, 124, 107, 7400m, "ZAR", 7437.000m, new DateTime(2023, 7, 25, 23, 8, 51, 541, DateTimeKind.Utc).AddTicks(2297) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6500m, "INR", 32.500m, 39, 64, 6500m, "INR", 6532.500m, new DateTime(2023, 7, 2, 13, 37, 51, 541, DateTimeKind.Utc).AddTicks(2300) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 500m, "INR", 2.500m, 73, 3, 500m, "INR", 502.500m, new DateTime(2023, 6, 26, 9, 45, 51, 541, DateTimeKind.Utc).AddTicks(2302) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7100m, "SGD", 35.500m, 36, 58, 7100m, "SGD", 7135.500m, new DateTime(2023, 6, 8, 3, 41, 51, 541, DateTimeKind.Utc).AddTicks(2304) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7500m, "GBP", 37.500m, 85, 87, 7500m, "GBP", 7537.500m, new DateTime(2023, 5, 21, 11, 26, 51, 541, DateTimeKind.Utc).AddTicks(2308) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5500m, "EUR", 27.500m, 69, 1, 5500m, "EUR", 5527.500m, new DateTime(2023, 8, 6, 15, 30, 51, 541, DateTimeKind.Utc).AddTicks(2310) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3200m, "BRL", 16.000m, 42, 8, 3200m, "BRL", 3216.000m, new DateTime(2023, 8, 14, 6, 32, 51, 541, DateTimeKind.Utc).AddTicks(2312) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6300m, "AUD", 31.500m, 124, 58, 6300m, "AUD", 6331.500m, new DateTime(2023, 5, 28, 18, 52, 51, 541, DateTimeKind.Utc).AddTicks(2314) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6600m, "EUR", 33.000m, 90, 124, 6600m, "EUR", 6633.000m, new DateTime(2023, 6, 30, 5, 14, 51, 541, DateTimeKind.Utc).AddTicks(2316) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "BGN", 2.000m, 60, 111, 400m, "BGN", 402.000m, new DateTime(2023, 7, 19, 23, 40, 51, 541, DateTimeKind.Utc).AddTicks(2318) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2900m, "JPY", 14.500m, 50, 24, 2900m, "JPY", 2914.500m, new DateTime(2023, 5, 18, 11, 10, 51, 541, DateTimeKind.Utc).AddTicks(2320) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2900m, "USD", 14.500m, 89, 116, 2900m, "USD", 2914.500m, new DateTime(2023, 6, 28, 18, 3, 51, 541, DateTimeKind.Utc).AddTicks(2322) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1300m, "JPY", 6.500m, 76, 80, 1300m, "JPY", 1306.500m, new DateTime(2023, 7, 8, 18, 13, 51, 541, DateTimeKind.Utc).AddTicks(2324) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5600m, "JPY", 28.000m, 92, 71, 5600m, "JPY", 5628.000m, new DateTime(2023, 7, 28, 8, 2, 51, 541, DateTimeKind.Utc).AddTicks(2326) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7600m, "NZD", 38.000m, 33, 43, 7600m, "NZD", 7638.000m, new DateTime(2023, 7, 7, 20, 48, 51, 541, DateTimeKind.Utc).AddTicks(2328) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, 2.000m, 117, 113, 400m, 402.000m, new DateTime(2023, 6, 7, 12, 49, 51, 541, DateTimeKind.Utc).AddTicks(2330) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4000m, "CHF", 20.000m, 22, 114, 4000m, "CHF", 4020.000m, new DateTime(2023, 6, 24, 21, 38, 51, 541, DateTimeKind.Utc).AddTicks(2332) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5700m, "CNY", 28.500m, 90, 43, 5700m, "CNY", 5728.500m, new DateTime(2023, 6, 15, 14, 45, 51, 541, DateTimeKind.Utc).AddTicks(2335) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5400m, "AUD", 27.000m, 98, 12, 5400m, "AUD", 5427.000m, new DateTime(2023, 8, 8, 15, 3, 51, 541, DateTimeKind.Utc).AddTicks(2337) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5800m, "USD", 29.000m, 76, 43, 5800m, "USD", 5829.000m, new DateTime(2023, 7, 1, 7, 23, 51, 541, DateTimeKind.Utc).AddTicks(2340) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "SGD", 41.000m, 112, 100, 8200m, "SGD", 8241.000m, new DateTime(2023, 6, 27, 18, 35, 51, 541, DateTimeKind.Utc).AddTicks(2342) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6700m, "NZD", 33.500m, 54, 45, 6700m, "NZD", 6733.500m, new DateTime(2023, 6, 18, 19, 38, 51, 541, DateTimeKind.Utc).AddTicks(2344) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "SGD", 44.000m, 90, 22, 8800m, "SGD", 8844.000m, new DateTime(2023, 7, 27, 4, 21, 51, 541, DateTimeKind.Utc).AddTicks(2346) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "RUB", 2.000m, 65, 69, 400m, "RUB", 402.000m, new DateTime(2023, 7, 5, 23, 18, 51, 541, DateTimeKind.Utc).AddTicks(2348) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1300m, "AUD", 6.500m, 88, 55, 1300m, "AUD", 1306.500m, new DateTime(2023, 6, 26, 17, 38, 51, 541, DateTimeKind.Utc).AddTicks(2350) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 151,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9000m, "AUD", 45.000m, 70, 105, 9000m, "AUD", 9045.000m, new DateTime(2023, 8, 6, 16, 29, 51, 541, DateTimeKind.Utc).AddTicks(2352) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 152,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9200m, "RUB", 46.000m, 71, 6, 9200m, "RUB", 9246.000m, new DateTime(2023, 6, 15, 12, 15, 51, 541, DateTimeKind.Utc).AddTicks(2354) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 153,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7800m, "CHF", 39.000m, 129, 84, 7800m, "CHF", 7839.000m, new DateTime(2023, 6, 27, 11, 20, 51, 541, DateTimeKind.Utc).AddTicks(2356) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 154,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 800m, "NZD", 4.000m, 73, 59, 800m, "NZD", 804.000m, new DateTime(2023, 8, 2, 23, 9, 51, 541, DateTimeKind.Utc).AddTicks(2358) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 155,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2000m, "INR", 10.000m, 117, 21, 2000m, "INR", 2010.000m, new DateTime(2023, 5, 27, 9, 17, 51, 541, DateTimeKind.Utc).AddTicks(2360) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 156,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6300m, "RUB", 31.500m, 9, 11, 6300m, "RUB", 6331.500m, new DateTime(2023, 8, 5, 20, 38, 51, 541, DateTimeKind.Utc).AddTicks(2361) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 157,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 300m, "CNY", 1.500m, 34, 56, 300m, "CNY", 301.500m, new DateTime(2023, 7, 27, 18, 49, 51, 541, DateTimeKind.Utc).AddTicks(2363) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 158,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6700m, "INR", 33.500m, 61, 129, 6700m, "INR", 6733.500m, new DateTime(2023, 5, 26, 5, 49, 51, 541, DateTimeKind.Utc).AddTicks(2365) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 159,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "JPY", 11.500m, 85, 100, 2300m, "JPY", 2311.500m, new DateTime(2023, 5, 31, 9, 46, 51, 541, DateTimeKind.Utc).AddTicks(2367) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 160,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3300m, "NZD", 16.500m, 40, 111, 3300m, "NZD", 3316.500m, new DateTime(2023, 6, 2, 18, 53, 51, 541, DateTimeKind.Utc).AddTicks(2369) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 161,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8300m, "BRL", 41.500m, 113, 82, 8300m, "BRL", 8341.500m, new DateTime(2023, 7, 25, 2, 12, 51, 541, DateTimeKind.Utc).AddTicks(2389) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 162,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "CHF", 117, 9, "CHF", new DateTime(2023, 7, 18, 21, 49, 51, 541, DateTimeKind.Utc).AddTicks(2391) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 163,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1900m, "BGN", 9.500m, 29, 56, 1900m, "BGN", 1909.500m, new DateTime(2023, 7, 31, 0, 46, 51, 541, DateTimeKind.Utc).AddTicks(2393) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 164,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 400m, "GBP", 2.000m, 96, 39, 400m, "GBP", 402.000m, new DateTime(2023, 6, 4, 16, 57, 51, 541, DateTimeKind.Utc).AddTicks(2395) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 165,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5000m, "INR", 25.000m, 105, 60, 5000m, "INR", 5025.000m, new DateTime(2023, 6, 23, 4, 48, 51, 541, DateTimeKind.Utc).AddTicks(2397) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 166,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6600m, "RUB", 33.000m, 72, 120, 6600m, "RUB", 6633.000m, new DateTime(2023, 6, 30, 6, 9, 51, 541, DateTimeKind.Utc).AddTicks(2399) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 167,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1400m, "EUR", 7.000m, 127, 61, 1400m, "EUR", 1407.000m, new DateTime(2023, 6, 25, 7, 51, 51, 541, DateTimeKind.Utc).AddTicks(2401) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 168,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6400m, "NZD", 32.000m, 112, 42, 6400m, "NZD", 6432.000m, new DateTime(2023, 6, 16, 4, 47, 51, 541, DateTimeKind.Utc).AddTicks(2403) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 169,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 7300m, 36.500m, 21, 112, 7300m, 7336.500m, new DateTime(2023, 7, 15, 7, 46, 51, 541, DateTimeKind.Utc).AddTicks(2405) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 170,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, 6.000m, 13, 8, 1200m, 1206.000m, new DateTime(2023, 6, 15, 6, 51, 51, 541, DateTimeKind.Utc).AddTicks(2407) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 171,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3700m, "ZAR", 18.500m, 67, 121, 3700m, "ZAR", 3718.500m, new DateTime(2023, 5, 29, 11, 4, 51, 541, DateTimeKind.Utc).AddTicks(2409) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 172,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9600m, "BRL", 48.000m, 121, 15, 9600m, "BRL", 9648.000m, new DateTime(2023, 5, 24, 21, 27, 51, 541, DateTimeKind.Utc).AddTicks(2411) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 173,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8800m, "INR", 44.000m, 126, 24, 8800m, "INR", 8844.000m, new DateTime(2023, 6, 10, 21, 14, 51, 541, DateTimeKind.Utc).AddTicks(2413) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 174,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "RUB", 20.500m, 107, 55, 4100m, "RUB", 4120.500m, new DateTime(2023, 8, 5, 16, 20, 51, 541, DateTimeKind.Utc).AddTicks(2414) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 175,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6500m, "BGN", 32.500m, 104, 39, 6500m, "BGN", 6532.500m, new DateTime(2023, 7, 11, 9, 2, 51, 541, DateTimeKind.Utc).AddTicks(2416) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 176,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6200m, "CHF", 31.000m, 125, 109, 6200m, "CHF", 6231.000m, new DateTime(2023, 5, 18, 3, 38, 51, 541, DateTimeKind.Utc).AddTicks(2418) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 177,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2800m, "GBP", 14.000m, 119, 35, 2800m, "GBP", 2814.000m, new DateTime(2023, 6, 5, 19, 31, 51, 541, DateTimeKind.Utc).AddTicks(2420) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 178,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1500m, "CNY", 7.500m, 40, 52, 1500m, "CNY", 1507.500m, new DateTime(2023, 7, 5, 4, 14, 51, 541, DateTimeKind.Utc).AddTicks(2422) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 179,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6500m, "BRL", 32.500m, 117, 94, 6500m, "BRL", 6532.500m, new DateTime(2023, 8, 7, 14, 42, 51, 541, DateTimeKind.Utc).AddTicks(2424) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 180,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7100m, "JPY", 35.500m, 65, 88, 7100m, "JPY", 7135.500m, new DateTime(2023, 7, 27, 19, 58, 51, 541, DateTimeKind.Utc).AddTicks(2426) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 181,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6200m, "SGD", 31.000m, 79, 42, 6200m, "SGD", 6231.000m, new DateTime(2023, 8, 10, 11, 27, 51, 541, DateTimeKind.Utc).AddTicks(2428) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 182,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5400m, "USD", 27.000m, 43, 106, 5400m, "USD", 5427.000m, new DateTime(2023, 6, 6, 21, 39, 51, 541, DateTimeKind.Utc).AddTicks(2431) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 183,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5400m, "EUR", 27.000m, 51, 55, 5400m, "EUR", 5427.000m, new DateTime(2023, 7, 31, 20, 18, 51, 541, DateTimeKind.Utc).AddTicks(2432) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 184,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6200m, "EUR", 31.000m, 23, 124, 6200m, "EUR", 6231.000m, new DateTime(2023, 7, 16, 12, 9, 51, 541, DateTimeKind.Utc).AddTicks(2434) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 185,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9700m, "NZD", 48.500m, 104, 108, 9700m, "NZD", 9748.500m, new DateTime(2023, 7, 28, 8, 46, 51, 541, DateTimeKind.Utc).AddTicks(2436) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 186,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2400m, "EUR", 12.000m, 40, 125, 2400m, "EUR", 2412.000m, new DateTime(2023, 8, 10, 18, 2, 51, 541, DateTimeKind.Utc).AddTicks(2438) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 187,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6600m, "SGD", 33.000m, 21, 87, 6600m, "SGD", 6633.000m, new DateTime(2023, 6, 15, 21, 16, 51, 541, DateTimeKind.Utc).AddTicks(2441) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 188,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "BGN", 6.000m, 116, 65, 1200m, "BGN", 1206.000m, new DateTime(2023, 7, 7, 20, 28, 51, 541, DateTimeKind.Utc).AddTicks(2443) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 189,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "ZAR", 11.500m, 76, 55, 2300m, "ZAR", 2311.500m, new DateTime(2023, 7, 1, 15, 4, 51, 541, DateTimeKind.Utc).AddTicks(2445) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 190,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7000m, "NZD", 35.000m, 110, 109, 7000m, "NZD", 7035.000m, new DateTime(2023, 8, 13, 0, 49, 51, 541, DateTimeKind.Utc).AddTicks(2447) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 191,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1700m, "EUR", 8.500m, 45, 25, 1700m, "EUR", 1708.500m, new DateTime(2023, 7, 3, 22, 30, 51, 541, DateTimeKind.Utc).AddTicks(2449) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 192,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3800m, "RUB", 19.000m, 41, 17, 3800m, "RUB", 3819.000m, new DateTime(2023, 6, 9, 12, 39, 51, 541, DateTimeKind.Utc).AddTicks(2451) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 193,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "USD", 41.000m, 61, 33, 8200m, "USD", 8241.000m, new DateTime(2023, 6, 10, 10, 27, 51, 541, DateTimeKind.Utc).AddTicks(2453) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 194,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1800m, "USD", 9.000m, 92, 77, 1800m, "USD", 1809.000m, new DateTime(2023, 5, 22, 8, 22, 51, 541, DateTimeKind.Utc).AddTicks(2455) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 195,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 100m, "BGN", 0.500m, 5, 13, 100m, "BGN", 100.500m, new DateTime(2023, 6, 14, 22, 36, 51, 541, DateTimeKind.Utc).AddTicks(2457) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 196,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8300m, "SGD", 41.500m, 30, 112, 8300m, "SGD", 8341.500m, new DateTime(2023, 7, 27, 1, 58, 51, 541, DateTimeKind.Utc).AddTicks(2459) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 197,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 800m, "JPY", 4.000m, 67, 2, 800m, "JPY", 804.000m, new DateTime(2023, 6, 26, 8, 20, 51, 541, DateTimeKind.Utc).AddTicks(2461) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 198,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1300m, "BGN", 6.500m, 23, 127, 1300m, "BGN", 1306.500m, new DateTime(2023, 6, 19, 10, 0, 51, 541, DateTimeKind.Utc).AddTicks(2463) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 199,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2200m, "NZD", 11.000m, 80, 35, 2200m, "NZD", 2211.000m, new DateTime(2023, 7, 7, 23, 12, 51, 541, DateTimeKind.Utc).AddTicks(2465) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 200,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 200m, "RUB", 1.000m, 104, 126, 200m, "RUB", 201.000m, new DateTime(2023, 7, 10, 2, 14, 51, 541, DateTimeKind.Utc).AddTicks(2467) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 201,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "CHF", 20.500m, 19, 73, 4100m, "CHF", 4120.500m, new DateTime(2023, 6, 18, 5, 19, 51, 541, DateTimeKind.Utc).AddTicks(2470) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 202,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2400m, "USD", 12.000m, 6, 108, 2400m, "USD", 2412.000m, new DateTime(2023, 7, 24, 6, 7, 51, 541, DateTimeKind.Utc).AddTicks(2472) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 203,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "ZAR", 15.000m, 27, 32, 3000m, "ZAR", 3015.000m, new DateTime(2023, 6, 23, 8, 32, 51, 541, DateTimeKind.Utc).AddTicks(2474) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 204,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4200m, "BRL", 21.000m, 89, 46, 4200m, "BRL", 4221.000m, new DateTime(2023, 7, 14, 7, 55, 51, 541, DateTimeKind.Utc).AddTicks(2476) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 205,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3100m, "USD", 15.500m, 10, 103, 3100m, "USD", 3115.500m, new DateTime(2023, 5, 17, 18, 47, 51, 541, DateTimeKind.Utc).AddTicks(2478) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 206,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2300m, "CAD", 11.500m, 105, 89, 2300m, "CAD", 2311.500m, new DateTime(2023, 7, 1, 19, 22, 51, 541, DateTimeKind.Utc).AddTicks(2480) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 207,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2500m, "SGD", 12.500m, 118, 126, 2500m, "SGD", 2512.500m, new DateTime(2023, 7, 29, 21, 25, 51, 541, DateTimeKind.Utc).AddTicks(2482) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 208,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "CHF", 15.000m, 97, 15, 3000m, "CHF", 3015.000m, new DateTime(2023, 8, 5, 10, 15, 51, 541, DateTimeKind.Utc).AddTicks(2484) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 209,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 800m, "USD", 4.000m, 29, 3, 800m, "USD", 804.000m, new DateTime(2023, 7, 22, 22, 52, 51, 541, DateTimeKind.Utc).AddTicks(2486) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 210,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 200m, "ZAR", 1.000m, 29, 104, 200m, "ZAR", 201.000m, new DateTime(2023, 6, 1, 5, 23, 51, 541, DateTimeKind.Utc).AddTicks(2488) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 211,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "CHF", 15.000m, 75, 21, 3000m, "CHF", 3015.000m, new DateTime(2023, 6, 29, 1, 32, 51, 541, DateTimeKind.Utc).AddTicks(2490) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 212,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 300m, "BRL", 1.500m, 50, 25, 300m, "BRL", 301.500m, new DateTime(2023, 6, 19, 14, 10, 51, 541, DateTimeKind.Utc).AddTicks(2510) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 213,
                columns: new[] { "BaseCurrency", "RecipientId", "SenderId", "TargetCurrency", "TransferDate" },
                values: new object[] { "BRL", 117, 20, "BRL", new DateTime(2023, 7, 29, 7, 25, 51, 541, DateTimeKind.Utc).AddTicks(2512) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 214,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2400m, "CHF", 12.000m, 87, 39, 2400m, "CHF", 2412.000m, new DateTime(2023, 7, 19, 4, 54, 51, 541, DateTimeKind.Utc).AddTicks(2514) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 215,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1400m, "USD", 7.000m, 119, 113, 1400m, "USD", 1407.000m, new DateTime(2023, 7, 28, 11, 23, 51, 541, DateTimeKind.Utc).AddTicks(2516) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 216,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4300m, "INR", 21.500m, 113, 115, 4300m, "INR", 4321.500m, new DateTime(2023, 7, 30, 11, 36, 51, 541, DateTimeKind.Utc).AddTicks(2518) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 217,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4100m, "GBP", 20.500m, 79, 74, 4100m, "GBP", 4120.500m, new DateTime(2023, 7, 2, 11, 4, 51, 541, DateTimeKind.Utc).AddTicks(2520) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 218,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9000m, "RUB", 45.000m, 73, 35, 9000m, "RUB", 9045.000m, new DateTime(2023, 7, 12, 22, 33, 51, 541, DateTimeKind.Utc).AddTicks(2522) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 219,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3600m, "CAD", 18.000m, 8, 35, 3600m, "CAD", 3618.000m, new DateTime(2023, 6, 14, 18, 21, 51, 541, DateTimeKind.Utc).AddTicks(2524) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 220,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6200m, "INR", 31.000m, 123, 31, 6200m, "INR", 6231.000m, new DateTime(2023, 7, 21, 6, 16, 51, 541, DateTimeKind.Utc).AddTicks(2526) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 221,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 2800m, 14.000m, 119, 71, 2800m, 2814.000m, new DateTime(2023, 7, 9, 1, 4, 51, 541, DateTimeKind.Utc).AddTicks(2528) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 222,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2900m, "CAD", 14.500m, 21, 110, 2900m, "CAD", 2914.500m, new DateTime(2023, 7, 10, 14, 4, 51, 541, DateTimeKind.Utc).AddTicks(2530) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 223,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4900m, "ZAR", 24.500m, 48, 121, 4900m, "ZAR", 4924.500m, new DateTime(2023, 6, 7, 20, 32, 51, 541, DateTimeKind.Utc).AddTicks(2532) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 224,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 500m, "BGN", 2.500m, 23, 117, 500m, "BGN", 502.500m, new DateTime(2023, 6, 17, 6, 39, 51, 541, DateTimeKind.Utc).AddTicks(2534) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 225,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "JPY", 15.000m, 17, 77, 3000m, "JPY", 3015.000m, new DateTime(2023, 7, 8, 6, 11, 51, 541, DateTimeKind.Utc).AddTicks(2536) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 226,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8200m, "NZD", 41.000m, 21, 114, 8200m, "NZD", 8241.000m, new DateTime(2023, 8, 1, 16, 53, 51, 541, DateTimeKind.Utc).AddTicks(2538) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 227,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9800m, "CAD", 49.000m, 69, 59, 9800m, "CAD", 9849.000m, new DateTime(2023, 5, 28, 19, 3, 51, 541, DateTimeKind.Utc).AddTicks(2540) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 228,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4200m, "CAD", 21.000m, 88, 52, 4200m, "CAD", 4221.000m, new DateTime(2023, 7, 8, 13, 52, 51, 541, DateTimeKind.Utc).AddTicks(2542) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 229,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1200m, "AUD", 6.000m, 129, 127, 1200m, "AUD", 1206.000m, new DateTime(2023, 6, 16, 19, 14, 51, 541, DateTimeKind.Utc).AddTicks(2544) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 230,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2600m, "CAD", 13.000m, 96, 100, 2600m, "CAD", 2613.000m, new DateTime(2023, 5, 26, 4, 48, 51, 541, DateTimeKind.Utc).AddTicks(2546) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 231,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1600m, "CAD", 8.000m, 65, 75, 1600m, "CAD", 1608.000m, new DateTime(2023, 6, 26, 23, 59, 51, 541, DateTimeKind.Utc).AddTicks(2547) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 232,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6100m, "AUD", 30.500m, 49, 78, 6100m, "AUD", 6130.500m, new DateTime(2023, 6, 19, 18, 6, 51, 541, DateTimeKind.Utc).AddTicks(2550) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 233,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3000m, "NZD", 15.000m, 97, 103, 3000m, "NZD", 3015.000m, new DateTime(2023, 8, 11, 6, 55, 51, 541, DateTimeKind.Utc).AddTicks(2552) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 234,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9500m, "INR", 47.500m, 26, 84, 9500m, "INR", 9547.500m, new DateTime(2023, 7, 7, 18, 17, 51, 541, DateTimeKind.Utc).AddTicks(2553) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 235,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9100m, "ZAR", 45.500m, 49, 71, 9100m, "ZAR", 9145.500m, new DateTime(2023, 6, 5, 23, 32, 51, 541, DateTimeKind.Utc).AddTicks(2556) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 236,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1500m, "CAD", 7.500m, 93, 2, 1500m, "CAD", 1507.500m, new DateTime(2023, 5, 30, 7, 56, 51, 541, DateTimeKind.Utc).AddTicks(2558) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 237,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 600m, "GBP", 3.000m, 118, 125, 600m, "GBP", 603.000m, new DateTime(2023, 7, 29, 2, 48, 51, 541, DateTimeKind.Utc).AddTicks(2560) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 238,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9500m, "INR", 47.500m, 43, 9500m, "INR", 9547.500m, new DateTime(2023, 6, 16, 17, 41, 51, 541, DateTimeKind.Utc).AddTicks(2562) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 239,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6500m, "CHF", 32.500m, 10, 59, 6500m, "CHF", 6532.500m, new DateTime(2023, 7, 11, 12, 29, 51, 541, DateTimeKind.Utc).AddTicks(2564) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 240,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 7700m, "BGN", 38.500m, 122, 58, 7700m, "BGN", 7738.500m, new DateTime(2023, 8, 12, 20, 25, 51, 541, DateTimeKind.Utc).AddTicks(2566) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 241,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "AUD", 22.000m, 20, 121, 4400m, "AUD", 4422.000m, new DateTime(2023, 5, 29, 23, 2, 51, 541, DateTimeKind.Utc).AddTicks(2568) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 242,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "EUR", 34.000m, 89, 47, 6800m, "EUR", 6834.000m, new DateTime(2023, 8, 11, 21, 33, 51, 541, DateTimeKind.Utc).AddTicks(2570) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 243,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3500m, "EUR", 17.500m, 3, 17, 3500m, "EUR", 3517.500m, new DateTime(2023, 7, 8, 9, 24, 51, 541, DateTimeKind.Utc).AddTicks(2572) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 244,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 9200m, "CHF", 46.000m, 129, 81, 9200m, "CHF", 9246.000m, new DateTime(2023, 6, 30, 22, 55, 51, 541, DateTimeKind.Utc).AddTicks(2574) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 245,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 5200m, "BRL", 26.000m, 29, 100, 5200m, "BRL", 5226.000m, new DateTime(2023, 5, 21, 23, 34, 51, 541, DateTimeKind.Utc).AddTicks(2576) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 246,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 8900m, "GBP", 44.500m, 107, 100, 8900m, "GBP", 8944.500m, new DateTime(2023, 6, 5, 23, 40, 51, 541, DateTimeKind.Utc).AddTicks(2578) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 247,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1800m, "BRL", 9.000m, 110, 8, 1800m, "BRL", 1809.000m, new DateTime(2023, 8, 14, 5, 34, 51, 541, DateTimeKind.Utc).AddTicks(2580) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 248,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6200m, "CHF", 31.000m, 21, 8, 6200m, "CHF", 6231.000m, new DateTime(2023, 6, 27, 5, 18, 51, 541, DateTimeKind.Utc).AddTicks(2582) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 249,
                columns: new[] { "BaseAmount", "Fees", "RecipientId", "SenderId", "TargetAmount", "TotalTaken", "TransferDate" },
                values: new object[] { 3900m, 19.500m, 33, 81, 3900m, 3919.500m, new DateTime(2023, 8, 16, 11, 13, 51, 541, DateTimeKind.Utc).AddTicks(2584) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 250,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 600m, "CHF", 3.000m, 30, 20, 600m, "CHF", 603.000m, new DateTime(2023, 8, 14, 16, 8, 51, 541, DateTimeKind.Utc).AddTicks(2586) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 251,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2100m, "ZAR", 10.500m, 71, 90, 2100m, "ZAR", 2110.500m, new DateTime(2023, 5, 24, 2, 0, 51, 541, DateTimeKind.Utc).AddTicks(2588) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 252,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4400m, "RUB", 22.000m, 93, 41, 4400m, "RUB", 4422.000m, new DateTime(2023, 7, 22, 18, 8, 51, 541, DateTimeKind.Utc).AddTicks(2590) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 253,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 1400m, "RUB", 7.000m, 23, 1400m, "RUB", 1407.000m, new DateTime(2023, 7, 30, 7, 36, 51, 541, DateTimeKind.Utc).AddTicks(2592) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 254,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6900m, "INR", 34.500m, 28, 113, 6900m, "INR", 6934.500m, new DateTime(2023, 5, 27, 7, 42, 51, 541, DateTimeKind.Utc).AddTicks(2594) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 255,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 2400m, "NZD", 12.000m, 129, 119, 2400m, "NZD", 2412.000m, new DateTime(2023, 6, 17, 16, 34, 51, 541, DateTimeKind.Utc).AddTicks(2596) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 256,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 3600m, "GBP", 18.000m, 10, 38, 3600m, "GBP", 3618.000m, new DateTime(2023, 8, 9, 20, 57, 51, 541, DateTimeKind.Utc).AddTicks(2598) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 257,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 4200m, "BGN", 21.000m, 81, 99, 4200m, "BGN", 4221.000m, new DateTime(2023, 6, 8, 5, 57, 51, 541, DateTimeKind.Utc).AddTicks(2600) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 258,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6800m, "ZAR", 34.000m, 101, 6800m, "ZAR", 6834.000m, new DateTime(2023, 7, 26, 23, 25, 51, 541, DateTimeKind.Utc).AddTicks(2621) });

            migrationBuilder.UpdateData(
                table: "Transfers",
                keyColumn: "Id",
                keyValue: 259,
                columns: new[] { "BaseAmount", "BaseCurrency", "Fees", "RecipientId", "SenderId", "TargetAmount", "TargetCurrency", "TotalTaken", "TransferDate" },
                values: new object[] { 6600m, "CAD", 33.000m, 107, 20, 6600m, "CAD", 6633.000m, new DateTime(2023, 8, 2, 20, 58, 51, 541, DateTimeKind.Utc).AddTicks(2623) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "VerificationToken",
                value: new Guid("294fb200-5457-4deb-88e7-be41a197290f"));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Red@abv.eu", "0889902596", new Guid("d7e703db-0d3f-4800-b337-d1fcb814cad1") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0888338684", new Guid("50fddaef-6863-4ed4-91ef-aca81de10223") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Turbo@abv.net", "0885435041", new Guid("459ae2a5-83ab-415a-91ea-8d90290b40c5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cuddlebug@yahoo.eu", "0886610675", new Guid("f78e566f-63c9-4fd1-a949-bbca981e8277") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Thrill@google.eu", "0884126408", new Guid("5fe71daa-234b-44d1-b746-808d0850a800") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "AsteroidZ@abv.net", "0884869383", new Guid("d6362fd5-c59a-448f-a50e-01d4b51ca6f0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "An1a@apple.eu", "0888632462", new Guid("ecaf00aa-c6bd-4d92-a9c5-ca2c331391cb") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Bluelight@abv.ru", "0889875626", new Guid("d5079649-fb5f-47fe-86fa-07cb78d06ea3") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Loaded@yahoo.eu", "0883650741", new Guid("6bc38fcb-64d3-4030-a86a-9d663ff40a95") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Rant@google.net", "0887901336", new Guid("a2fa3557-2d4a-43dd-80ce-b491662900a2") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Diva@apple.eu", "0888584126", new Guid("26e8f21b-9c03-4e43-b954-57de3d9deefc") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Yukki@yahoo.eu", "0884172411", new Guid("885cb984-d516-43dc-9905-d5005c86d307") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Werice@google.ru", "0884870506", new Guid("a304ca47-c057-4c18-aa88-4591ff5d3b64") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Perfect@abv.eu", "0887411703", new Guid("c9f40577-a99c-4b18-8a76-2374c066c8ef") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "MissK@apple.ru", "0884990878", new Guid("ad9b6b9a-759a-4d19-8893-3b5fce141be7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Bubbles@apple.ru", "0883360013", new Guid("29aea4a7-65a4-422a-993f-7aa6b31c22aa") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "ChEk@google.eu", "0883895522", new Guid("d2cad7fc-e387-4635-95fb-0fe32178354a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0887400939", new Guid("159ce50b-6fa3-40ff-b627-8c482c457114") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "HugsForDrubs@google.com", "0883482292", new Guid("61e87daf-c9b3-450b-812d-05e0a9003a8f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pippen@google.com", "0887513402", new Guid("1fd4887b-d203-4ffc-b4a4-ee0c3750793b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Aisha@google.bg", "0887168975", new Guid("0717c999-bb0b-446e-9663-704f284a4b92") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Protectshell@abv.bg", "0888865325", new Guid("5ea7d24b-e7f0-4b38-b96e-01223d13d268") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Geez@abv.com", "0885600104", new Guid("ac5121e3-ee6a-4997-be55-5c5715bcf4c7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Monkey_see@abv.com", "0885459642", new Guid("36d96695-f68a-4ba9-9543-98b9a8c546b3") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Tight@yahoo.com", "0885512261", new Guid("29031697-a18b-43ad-8a86-7ee11b35ce1a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0887710194", new Guid("125ff66e-864d-4f16-9026-964d5c8e6b81") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Prong@yahoo.ru", "0883540484", new Guid("53fe6e61-58ca-489b-a744-878bfdbfc980") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Strongarmed@apple.com", "0884139345", new Guid("60809921-cbd3-4ea5-8b7e-3500575f73e1") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Sovenance@yahoo.com", "0888993137", new Guid("4eb42220-2931-4bc6-9ad1-3ca945110f01") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Angel@google.eu", "0884966310", new Guid("a527eda0-0cdd-461b-9305-a08533df8480") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Mad@abv.ru", "0886213699", new Guid("b03916aa-bfed-4935-b596-ae9c75341f34") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dimple@abv.com", "0884897846", new Guid("40a9a697-a70a-488e-ac5f-a876d3ddf482") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dollspell@yahoo.eu", "0884001157", new Guid("831a493b-31bc-41bd-9f69-b38d126e14ed") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "State@yahoo.ru", "0888058817", new Guid("b0d2d47d-5af0-4c5f-8fcb-f918c60447a9") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Vellojoy@abv.eu", "0885877947", new Guid("05dea7ea-0fc3-468b-9664-86ad064f40f7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Eagle@abv.net", "0888987446", new Guid("af148700-c9bb-4b23-8e8f-a1a273f0fa4a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Brutal@google.bg", "0888056358", new Guid("079b615e-eff0-4c41-88de-dd3b532f50c6") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Solar@google.com", "0887248714", new Guid("21bffbab-b326-4cd0-bbaa-51982ec1abd4") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Intella@google.bg", "0885439525", new Guid("3dba4978-6569-47e9-ab94-3e6d51154710") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Rainblow@abv.com", "0885382874", new Guid("5997a117-dafd-4e24-ac7a-c958f2d3b5d0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Venereology@apple.bg", "0887255526", new Guid("9a2f4957-5bf3-454e-92b7-a97a28662361") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dogbone@google.ru", "0889251856", new Guid("26a86efc-4763-4164-b70c-3957d1a7bbe2") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Grep@google.net", "0886189165", new Guid("0403c9a0-8a07-4b20-8d2f-163b755a82ad") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hawkeye@google.com", "0887489751", new Guid("fe42eea0-ea44-4e76-8bcd-93ca22ed0c91") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Glue@google.ru", "0888669209", new Guid("ff005813-fc88-4629-9078-9879aa252047") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Princess@apple.bg", "0886449177", new Guid("51e3a694-e2b2-4ef9-87fb-8ee3b298ba40") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "L4Legit@apple.bg", "0883215988", new Guid("a2a759ef-0eb7-4523-bf1b-60a4e50f5c39") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Peapod@google.net", "0885814141", new Guid("bcfcaa66-4a81-41a1-ba46-c890acbc495e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Networked@google.com", "0885600588", new Guid("e1ac5d62-c398-471a-8ae6-b55d5c5652a6") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Prep@yahoo.ru", "0888187368", new Guid("8b7a656c-0e1d-4ad2-bc47-8f83d56ae7b8") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Sharken@apple.com", "0883277547", new Guid("8765e973-b65a-43b3-8b4b-d0fcf738ce58") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Curio@yahoo.ru", "0889158424", new Guid("a3ca23f3-5ef6-4561-af00-f9cc41256dbe") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0888774386", new Guid("6f685b51-9170-4afb-8315-c2ba3332fb10") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0888055653", new Guid("fe494dc1-c414-4774-92b4-b84911428e65") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Muffinhead@abv.ru", "0885175858", new Guid("66cd8738-1aea-4ab1-bc2b-276a276d47a9") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Zyber@google.net", "0886345068", new Guid("caa1b63a-9d48-4bf0-9aaa-9aa11c09a1ed") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Moonchild@apple.net", "0888044694", new Guid("2b123139-2b7f-4ca2-a7ea-882f35171a94") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Precision@yahoo.ru", "0888590249", new Guid("08020a47-962f-4ee5-8528-e03069055c3b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hydrospace@apple.eu", "0887311965", new Guid("21857ea9-2734-4d4f-9bcb-0e542515ae27") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Talon@abv.eu", "0886323008", new Guid("c18d22e6-0b3a-4a51-b4de-a18428fd9ce9") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "BattlerBeauty@yahoo.eu", "0884965320", new Guid("07beb401-80ff-4484-9e9d-e4b4d59e7d34") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Delta@yahoo.eu", "0885638677", new Guid("505c76ca-d67a-48a0-bad7-66fd4c35473a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Lens@abv.net", "0888557041", new Guid("27ca03ce-49c3-4101-b75f-b344ec40b6cc") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Spit@abv.bg", "0887701505", new Guid("134b4f12-0869-446e-8c97-88ed685cffae") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Zoom@google.eu", "0888746680", new Guid("d2ed30d0-f2ad-49ad-baf8-87f8709c6e47") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Gothichic@abv.ru", "0884551683", new Guid("d6c23650-2968-4852-b23f-b469385102ba") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Purple@yahoo.com", "0886748496", new Guid("4c6f82c9-566a-44c1-8864-04d85ffc3554") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Ahem@yahoo.com", "0888798905", new Guid("18891fcf-b37e-466f-903a-754b03e47479") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Swag@yahoo.ru", "0884919259", new Guid("25d72745-2876-4c31-a3af-f033d73432cb") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cuteness@abv.bg", "0887193567", new Guid("b5445eb1-1b6e-4afd-9955-ff62c44d560c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "SkillDevil@abv.ru", "0887490902", new Guid("828a55b7-d2b9-40e1-8ed8-d276c24606b5") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Island@yahoo.eu", "0887049591", new Guid("adde6bbf-1631-4c0f-a3da-56c78567dded") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Gran@apple.eu", "0887738080", new Guid("81650232-b27e-4548-aff0-cc0e4e3b995d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cosmosaegis@abv.com", "0883357715", new Guid("7d8be7d4-c7a3-4108-9a66-52cd49220e13") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dove@apple.bg", "0883513668", new Guid("800a8da1-2806-4f74-810b-43106a6576e4") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Crybaby@apple.bg", "0883978798", new Guid("36932b65-3bbf-43b2-9a30-cb52bee81815") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cyberproof@yahoo.bg", "0884089163", new Guid("3a78b527-5cd8-4604-b1ab-04c495ee77c4") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dedicated@google.eu", "0885268226", new Guid("efaeefc3-0b84-4631-9ecd-455b17187dfe") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Epic@google.bg", "0887160993", new Guid("c4fb35ba-e018-4cf7-8482-e7e3a0cf8de4") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Secure@apple.ru", "0886185311", new Guid("3418497c-2ff2-4c5f-877c-222d1ad1842f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Doppler@yahoo.com", "0887693015", new Guid("7a1296c5-0c8b-454f-ba29-2d030efd7dce") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Buddy@google.com", "0886381975", new Guid("4b85e869-4b73-4081-bf81-f347e149a3d0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "FibBage@apple.bg", "0885990813", new Guid("489a9676-3fab-4181-a217-212c26b8491d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Strawboy@yahoo.com", "0888421061", new Guid("5b60ad7c-97ad-49b3-9745-aa72d14d035e") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Piggy@apple.net", "0884906194", new Guid("ed8564c8-60c3-4d67-b241-63d29b32e2ba") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Homes@google.eu", "0888667573", new Guid("ae491e8a-0d50-496c-9205-860295c11844") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "JellyBott_om@google.com", "0886120361", new Guid("3dfb4046-ec81-4fbd-ae2b-4ba4231f39d0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Marill@apple.com", "0888866225", new Guid("fb0b01f6-57a9-463d-8728-2e8c7a17ca57") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Guaranteed@apple.com", "0886138896", new Guid("38339b62-0aa8-425b-882a-ab5d07248c5b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hardcover@google.net", "0883750637", new Guid("e52948e2-b397-4b3d-bb4d-c5172738dc53") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Warmblush@abv.eu", "0889223111", new Guid("61a5cd37-a298-4957-8072-3b22e01fc18c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0888686918", new Guid("f2c0d49b-5fb3-4934-95d8-6fbc3843234a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Chub@abv.net", "0885753347", new Guid("d5fa2643-1c13-40b9-af4f-8a121d8b7fce") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Paradise@apple.net", "0888535396", new Guid("70927c63-e907-48c9-afc5-3cfda57cc7d7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Complete@apple.eu", "0887432456", new Guid("eb860526-81a5-490f-9b85-10a531b48753") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Moonlight@google.bg", "0887517515", new Guid("330ea07b-97c5-49bb-8945-1ec5b1c95e4a") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Unicorn@google.ru", "0883742350", new Guid("46060484-a900-411f-95f5-c2ecc17949e8") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Limonad@yahoo.com", "0886042626", new Guid("c6d3ce79-3fff-48cf-94f6-da91fb3a7443") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0884832934", new Guid("c611ba8c-11d6-469d-8827-6f0072a9e2b1") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Sly@abv.net", "0888825397", new Guid("dc715966-0218-4c95-859a-5094760965c7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Mercy@google.net", "0885731796", new Guid("d9dfabb5-eddc-4c85-9374-f9560987887c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cutie@abv.net", "0886628309", new Guid("2a86097e-5c10-4a26-8013-d67366267a49") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Cyberrock@abv.com", "0887738324", new Guid("f083dd0d-aced-4455-8e3c-8f66fb4665a7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Safenet@google.eu", "0883746585", new Guid("fa44ac02-25a9-412b-beb1-f13bdf6529be") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Shine@google.ru", "0884897687", new Guid("81c6de4c-8d32-4dca-b499-bf8a904ed4b3") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Night@abv.com", "0887450929", new Guid("bcb42237-21fc-4d2a-b3d5-9e49e60d4fc6") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Task@yahoo.bg", "0886611269", new Guid("b1dd91dd-b9bf-4f18-ad26-b10c630e4a1d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Septenary@abv.bg", "0883829329", new Guid("8f584c80-6100-4b84-af1c-1fc157ed2a3c") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Queenberry@yahoo.ru", "0885854276", new Guid("9e7746c3-19a8-43fe-ab76-ce26fd5e277b") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "DrawFul@google.ru", "0889176130", new Guid("fb5eb69f-2b3a-4114-b700-f1877dc012fa") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Proeyeroller@google.eu", "0888204569", new Guid("8f5cf2a3-8fcd-4166-9ba5-5f1a943f3846") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Bad@apple.bg", "0889644082", new Guid("f2218838-63b8-46d3-b6bc-f4e5a52b2bb6") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0886951397", new Guid("92aa1d61-272f-47f4-b331-9f8668c3507d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Secret@abv.ru", "0886093260", new Guid("13fb7ce5-b797-4c77-ade1-f66afd3852d9") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Elite@abv.ru", "0883411641", new Guid("c917f05f-496b-4245-b3fd-bba321bead50") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0887285507", new Guid("b4d2ef35-1cfb-44de-b476-c9ad63a9c141") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "PhoneNumber", "VerificationToken" },
                values: new object[] { "0886079227", new Guid("7e055079-fa78-4405-92db-ea8837778e36") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Dark@google.ru", "0889325117", new Guid("a0138f2a-313e-43da-a854-cdb9ac66455d") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Blu@apple.net", "0889414506", new Guid("4be2ec03-fa92-4b3a-874e-3e28c6349737") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Crashion@yahoo.eu", "0889063323", new Guid("e69f1088-0f3d-487e-87f5-55e97c25f64f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Star@apple.com", "0887958891", new Guid("1e20934b-b976-4371-8c12-c518c3ddcc80") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pink@yahoo.eu", "0889553914", new Guid("3423fa60-3df7-40d4-a6f5-5addbe59f8e8") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Kitten@abv.net", "0885058625", new Guid("b986d4c8-f5c6-41e3-a552-ef54edf50769") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Xentrix@yahoo.com", "0886001963", new Guid("bec7b22c-f501-4173-8224-f1b7080a3173") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Hug@google.bg", "0884044426", new Guid("488bb533-82e1-429c-a945-1861df9511f0") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pioneer@yahoo.ru", "0884793649", new Guid("604d34d8-eb3e-4ece-ac17-dd3b1ae83d1f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Pro@abv.bg", "0884849891", new Guid("5b55bfae-9c54-42cd-9ead-b9db589ccaa7") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Alert@apple.net", "0889912673", new Guid("2ba80b0d-77a4-4c50-97a4-663424e3af3f") });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "Email", "PhoneNumber", "VerificationToken" },
                values: new object[] { "Fittofuel@abv.ru", "0884840063", new Guid("1bb35f55-929c-4db5-82e0-e5340dd1b5b5") });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 55800m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 24700m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 45500m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 3800m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 30900m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 38300m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 85400m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 70700m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 80400m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 27800m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 40700m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 10000m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 33100m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 84500m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 7900m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 16000m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 97600m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 24300m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 58900m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 66400m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 3700m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 55200m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 91500m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 73000m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 74400m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 26400m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 59100m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 4400m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 16900m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 66600m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 27600m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 82200m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 90100m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 14900m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 20400m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 89300m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 47700m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 69000m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 64200m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 87900m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 60100m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 47900m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 11700m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 23600m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 93400m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 79400m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 34300m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 91600m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 14700m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 39500m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 14600m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 49600m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 62400m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 54,
                column: "Balance",
                value: 61000m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 53500m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 49900m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28800m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 93400m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 59,
                column: "Balance",
                value: 32800m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 26200m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 9700m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 73200m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 51600m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 50200m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 43400m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 43600m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 27500m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 93300m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 44800m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 66700m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 78000m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 89400m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 35300m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 77100m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 52600m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 99800m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 90900m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 78,
                column: "Balance",
                value: 54700m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 95200m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 80,
                column: "Balance",
                value: 59500m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 40100m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 44500m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 53400m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 22000m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 32500m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 20400m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 78200m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 41100m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 99800m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 8400m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 81800m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 73600m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 81900m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 46300m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 95,
                column: "Balance",
                value: 48800m);

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 44900m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 24100m, "CNY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 84400m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28900m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 25100m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 82800m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 41600m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 53700m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 18800m, "USD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 31200m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 80800m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 23300m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 89900m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 21300m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 21200m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 2400m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 3200m, "JPY" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 68400m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 22700m, "EUR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 35700m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 12200m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 36900m, "RUB" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 91900m, "CAD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 34400m, "INR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 59700m, "GBP" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 62700m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 19600m, "ZAR" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 6400m, "NZD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 86200m, "CHF" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 85100m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 28200m, "AUD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 87900m, "BRL" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 94200m, "BGN" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 8100m, "SGD" });

            migrationBuilder.UpdateData(
                table: "Wallets",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "Balance", "Currency" },
                values: new object[] { 47900m, "RUB" });
        }
    }
}
