﻿using Moq;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.TransferEntity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Transfer.Requests;
using static VirtualWallet.Features.Transfers.FilterTransfers.FilterTransfersFeature;

namespace VirtualWallet.Features.UnitTests.Transfers;

public class FilterTransfersFeatureTests
{
    private readonly Mock<ITransferRepository> _transferRepositoryMock;

    public FilterTransfersFeatureTests()
    {
        _transferRepositoryMock = new Mock<ITransferRepository>();
    }

    [Fact]
    public async Task Feature_ShouldFilter_ByTransferedAfterWhenRequested()
    {
        // Arrange	 
        var byTransferedAfter = futureDate;
        var request = new FilterTransfersRequest(byTransferedAfter, minDate, null, null, 0, 0, null, null, intParam, intParam);
        var command = new FilterTransfersCommand(request);

        var transfer = GetTestTransfer();
        transfer.SetTransferDate(byTransferedAfter.AddDays(1));       
        var query = GetTestTransfersQuery(transfer);
        _transferRepositoryMock.Setup(repo => repo.GetTransfersQuery()).ReturnsAsync(query);

        var handler = new FilterTransfersHandler(_transferRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotEqual(query.Count(), result.Transfers.TotalItems);
        Assert.Equal(1, result.Transfers.TotalItems);
        Assert.NotNull(result);
    }

    [Fact]
    public async Task Feature_ShouldFilter_ByTransferedBeforeWhenRequested()
    {
        // Arrange	 
        var byTransferedBefore = pastDate;
        var request = new FilterTransfersRequest(minDate, byTransferedBefore, null, null, 0, 0, null, null, intParam, intParam);
        var command = new FilterTransfersCommand(request);

        var transfer = GetTestTransfer();
        transfer.SetTransferDate(byTransferedBefore.AddDays(-1));
        var query = GetTestTransfersQuery(transfer);
        _transferRepositoryMock.Setup(repo => repo.GetTransfersQuery()).ReturnsAsync(query);

        var handler = new FilterTransfersHandler(_transferRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotEqual(query.Count(), result.Transfers.TotalItems);
        Assert.Equal(1, result.Transfers.TotalItems);
        Assert.NotNull(result);
    }

    [Fact]
    public async Task Feature_ShouldFilter_BySenderWhenRequested()
    {
        // Arrange	 
        var bySender = GetTestString(5);
        var request = new FilterTransfersRequest(minDate, minDate, bySender, null, 0, 0, null, null, intParam, intParam);
        var command = new FilterTransfersCommand(request);

        var transfer = GetTestTransfer();
        var sender = User.CreateUser(bySender, stringParam, stringParam, stringParam);
        transfer.SetSender(sender);

        var query = GetTestTransfersQuery(transfer);
        _transferRepositoryMock.Setup(repo => repo.GetTransfersQuery()).ReturnsAsync(query);

        var handler = new FilterTransfersHandler(_transferRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotEqual(query.Count(), result.Transfers.TotalItems);
        Assert.Equal(1, result.Transfers.TotalItems);
        Assert.NotNull(result);
    }

    [Fact]
    public async Task Feature_ShouldFilter_ByRecipientWhenRequested()
    {
        // Arrange	 
        var byRecipient = GetTestString(5);
        var request = new FilterTransfersRequest(minDate, minDate, null, byRecipient, 0, 0, null, null, intParam, intParam);
        var command = new FilterTransfersCommand(request);

        var transfer = GetTestTransfer();
        var sender = User.CreateUser(byRecipient, stringParam, stringParam, stringParam);
        transfer.SetRecipient(sender);

        var query = GetTestTransfersQuery(transfer);
        _transferRepositoryMock.Setup(repo => repo.GetTransfersQuery()).ReturnsAsync(query);

        var handler = new FilterTransfersHandler(_transferRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotEqual(query.Count(), result.Transfers.TotalItems);
        Assert.Equal(1, result.Transfers.TotalItems);
        Assert.NotNull(result);
    }

    [Fact]
    public async Task Feature_ShouldFilter_ByMinimumAmountWhenRequested()
    {
        // Arrange	 
        decimal minimumAmount = 5;
        var request = new FilterTransfersRequest(minDate, minDate, null, null, minimumAmount, 0, null, null, intParam, intParam);
        var command = new FilterTransfersCommand(request);

        var transfer = Transfer.CreateTransfer(
            intParam, intParam, decimalParam, stringParam, minimumAmount + 5, stringParam, decimalParam, decimalParam);
        var user = GetTestUser();
        transfer.SetSender(user);
        transfer.SetRecipient(user);

        var query = GetTestTransfersQuery(transfer);
        _transferRepositoryMock.Setup(repo => repo.GetTransfersQuery()).ReturnsAsync(query);

        var handler = new FilterTransfersHandler(_transferRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotEqual(query.Count(), result.Transfers.TotalItems);
        Assert.Equal(1, result.Transfers.TotalItems);
        Assert.NotNull(result);
    }

    [Fact]
    public async Task Feature_ShouldFilter_ByMaximumAmountWhenRequested()
    {
        // Arrange	 
        decimal maximumAmount = 5;
        var request = new FilterTransfersRequest(minDate, minDate, null, null, 0, maximumAmount, null, null, intParam, intParam);
        var command = new FilterTransfersCommand(request);

        var transfer = Transfer.CreateTransfer(
            intParam, intParam, decimalParam, stringParam, maximumAmount + 5, stringParam, decimalParam, decimalParam);
        var user = GetTestUser();
        transfer.SetSender(user);
        transfer.SetRecipient(user);

        var query = GetTestTransfersQuery(transfer);
        _transferRepositoryMock.Setup(repo => repo.GetTransfersQuery()).ReturnsAsync(query);

        var handler = new FilterTransfersHandler(_transferRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotEqual(query.Count(), result.Transfers.TotalItems);
        Assert.Equal(5, result.Transfers.TotalItems);
        Assert.NotNull(result);
    }

    [Fact]
    public async Task Feature_ShouldReturnEmptyList_WhenNoTeransfersFound()
    {
        // Arrange	 
        decimal minimumAmount = 5;
        var request = new FilterTransfersRequest(minDate, minDate, null, null, minimumAmount, 0, null, null, intParam, intParam);
        var command = new FilterTransfersCommand(request);

        var transfer = GetTestTransfer();
        var query = GetTestTransfersQuery(transfer);
        _transferRepositoryMock.Setup(repo => repo.GetTransfersQuery()).ReturnsAsync(query);

        var handler = new FilterTransfersHandler(_transferRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotEqual(query.Count(), result.Transfers.TotalItems);
        Assert.Equal(0, result.Transfers.TotalItems);
        Assert.NotNull(result);
    }

    [Theory]
    [InlineData("ascending")]
    [InlineData("descending")]
    public async Task Feature_Should_OrderTransfersWhenRequested(string orderBy)
    {
        // Arrange	         
        var request = new FilterTransfersRequest(minDate, minDate, null, null, 0, 0, null, orderBy, intParam, intParam);
        var command = new FilterTransfersCommand(request);
        decimal baseAmount = 10;
        var transfer = Transfer.CreateTransfer(
            intParam, intParam, baseAmount, stringParam, decimalParam, stringParam, decimalParam, decimalParam);
        var user = GetTestUser();
        transfer.SetSender(user);
        transfer.SetRecipient(user);

        var query = GetTestTransfersQuery(transfer);
        _transferRepositoryMock.Setup(repo => repo.GetTransfersQuery()).ReturnsAsync(query);

        var handler = new FilterTransfersHandler(_transferRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        var lastQueryUsername = query.Last().BaseAmount;
        var lastResultUsername = result.Transfers.Items.Last().BaseAmount;
        Assert.False(lastQueryUsername == lastResultUsername);
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenOrderByIsInvalid()
    {
        // Arrange	         
        var orderBy = GetTestString(5);
        var request = new FilterTransfersRequest(minDate, minDate, null, null, 0, 0, null, orderBy, intParam, intParam);
        var command = new FilterTransfersCommand(request);

        var transfer = GetTestTransfer();
        var query = GetTestTransfersQuery(transfer);
        _transferRepositoryMock.Setup(repo => repo.GetTransfersQuery()).ReturnsAsync(query);

        var handler = new FilterTransfersHandler(_transferRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<InvalidRequestException>(() => handler.Handle(command, default));
    }

    [Theory]
    [InlineData("amount")]
    [InlineData("sender")]
    [InlineData("recipient")]
    [InlineData("transferdate")]
    public async Task Feature_Should_SortUsersWhenRequested(string sortBy)
    {
        // Arrange	         
        var request = new FilterTransfersRequest(minDate, minDate, null, null, 0, 0, sortBy, null, intParam, intParam);
        var command = new FilterTransfersCommand(request);
        decimal baseAmount = 10;
        var transfer = Transfer.CreateTransfer(
            intParam, intParam, baseAmount, stringParam, decimalParam, stringParam, decimalParam, decimalParam);
        var user = GetTestUser();
        transfer.SetSender(user);
        transfer.SetRecipient(user);

        var query = GetTestTransfersQuery(transfer);
        _transferRepositoryMock.Setup(repo => repo.GetTransfersQuery()).ReturnsAsync(query);

        var handler = new FilterTransfersHandler(_transferRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        var lastQueryUsername = query.Last().BaseAmount;
        var lastResultUsername = result.Transfers.Items.Last().BaseAmount;
        Assert.False(lastQueryUsername == lastResultUsername);
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenSortByIsInvalid()
    {
        // Arrange	
        var sortBy = GetTestString(5);
        var request = new FilterTransfersRequest(minDate, minDate, null, null, 0, 0, sortBy, null, intParam, intParam);
        var command = new FilterTransfersCommand(request);

        var transfer = GetTestTransfer();
        var query = GetTestTransfersQuery(transfer);
        _transferRepositoryMock.Setup(repo => repo.GetTransfersQuery()).ReturnsAsync(query);

        var handler = new FilterTransfersHandler(_transferRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<InvalidRequestException>(() => handler.Handle(command, default));
    }
}
