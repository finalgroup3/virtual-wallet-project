﻿using Moq;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.TransferEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Transfer.Requests;
using static VirtualWallet.Features.Transfers.GetTransfer.GetTransferFeature;

namespace VirtualWallet.Features.UnitTests.Transfers;

public class GetTransfersFeatureTests
{
    private readonly Mock<ITransferRepository> _transferRepositoryMock;

    public GetTransfersFeatureTests()
    {
        _transferRepositoryMock = new Mock<ITransferRepository>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenTransferNotFound()
    {
        // Arrange			
        var request = new TransferRequest(intParam);
        var command = new GetTransferCommand(request);

        _transferRepositoryMock.Setup(repo => repo.GetTransferById(intParam)).ReturnsAsync((Transfer)null);

        var handler = new GetTransferHandler(_transferRepositoryMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<TransferNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldReturnTransfer_WhenRequestIsValid()
    {
        // Arrange			
        var request = new TransferRequest(intParam);
        var command = new GetTransferCommand(request);

        var transfer = GetTestTransfer();
        _transferRepositoryMock.Setup(repo => repo.GetTransferById(intParam)).ReturnsAsync(transfer);

        var handler = new GetTransferHandler(_transferRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(transfer.Recipient.Username, result.RecipientUsername);
        Assert.Equal(transfer.Sender.Username, result.SenderUsername);
        Assert.Equal(transfer.TargetAmount, result.TargetAmount);
    }
}
