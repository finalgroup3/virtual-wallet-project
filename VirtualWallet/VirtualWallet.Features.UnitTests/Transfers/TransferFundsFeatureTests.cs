﻿using Moq;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.TransferEntity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Transfer.Requests;
using static VirtualWallet.Features.Transfers.TransferFunds.TransferFundsFeature;

namespace VirtualWallet.Features.UnitTests.Transfers;

public class TransferFundsFeatureTests
{
    private readonly Mock<ITransferRepository> _transferRepositoryMock;
    private readonly Mock<IUserRepository> _userRepositoryMock;
    private readonly Mock<IExchangeRateService> _exchangeRateServiceMock;
   
    public TransferFundsFeatureTests()
    {        
        _transferRepositoryMock = new Mock<ITransferRepository>();
        _userRepositoryMock = new Mock<IUserRepository>();
        _exchangeRateServiceMock = new Mock<IExchangeRateService>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenSendingToYourself()
    {
        // Arrange			
        var request = new TransferFundsRequest(intParam, intParam, decimalParam);
        var command = new TransferFundsCommand(request);

        var handler = new TransferFundsHandler(_userRepositoryMock.Object, _transferRepositoryMock.Object, _exchangeRateServiceMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<InvalidRequestException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenRecipientNotFound()
    {
        // Arrange			
        var request = new TransferFundsRequest(intParam + 1, intParam, decimalParam);
        var command = new TransferFundsCommand(request);

        _userRepositoryMock.Setup(repo => repo.GetUserById(It.IsAny<int>())).ReturnsAsync((User)null);

        var handler = new TransferFundsHandler(_userRepositoryMock.Object, _transferRepositoryMock.Object, _exchangeRateServiceMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenRecipientNotVerified()
    {
        // Arrange			
        var request = new TransferFundsRequest(intParam +1, intParam, decimalParam);
        var command = new TransferFundsCommand(request);

        var user = GetTestUser();
        _userRepositoryMock.Setup(repo => repo.GetUserById(It.IsAny<int>())).ReturnsAsync(user);

        var handler = new TransferFundsHandler(_userRepositoryMock.Object, _transferRepositoryMock.Object, _exchangeRateServiceMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<UnverifiedRecipinetException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenSenderNotVerified()
    {
        // Arrange			
        var request = new TransferFundsRequest(intParam + 1, intParam, decimalParam);
        var command = new TransferFundsCommand(request);

        var sender = GetTestUser();
        var recipient = GetTestUser();
        recipient.Verify();

        _userRepositoryMock.SetupSequence(repo => repo.GetUserById(It.IsAny<int>()))
            .ReturnsAsync(recipient)
            .ReturnsAsync(sender);

        var handler = new TransferFundsHandler(_userRepositoryMock.Object, _transferRepositoryMock.Object, _exchangeRateServiceMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<UnverifiedAccountException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenSenderHasNotEnoughFunds()
    {
        // Arrange			
        var request = new TransferFundsRequest(intParam + 1, intParam, decimalParam);
        var command = new TransferFundsCommand(request);

        var sender = GetTestUser();
        sender.Verify();
        sender.CreateWallet(currencyParam);

        var recipient = GetTestUser();
        recipient.Verify();

        _userRepositoryMock.SetupSequence(repo => repo.GetUserById(It.IsAny<int>()))
            .ReturnsAsync(recipient)
            .ReturnsAsync(sender);

        var handler = new TransferFundsHandler(_userRepositoryMock.Object, _transferRepositoryMock.Object, _exchangeRateServiceMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<InvalidRequestException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldTransferFunds_WhenRequestIsValid()
    {
        // Arrange		
        decimal amount = 10;	
        var request = new TransferFundsRequest(intParam + 1, intParam, amount);
        var command = new TransferFundsCommand(request);

        var sender = GetTestUser();
        sender.Verify();
        sender.CreateWallet(currencyParam);
        sender.AddFunds(amount);

        var recipient = GetTestUser();
        recipient.Verify();
        recipient.CreateWallet(currencyParam);

        _userRepositoryMock.SetupSequence(repo => repo.GetUserById(It.IsAny<int>()))
            .ReturnsAsync(recipient)
            .ReturnsAsync(sender);

        var exchangeRates = GetExchangeRateResponse();
        _exchangeRateServiceMock.Setup(repo => repo.GetExchangeRateAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal>()))
           .ReturnsAsync(exchangeRates);

        var handler = new TransferFundsHandler(_userRepositoryMock.Object, _transferRepositoryMock.Object, _exchangeRateServiceMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(request.Amount, result.BaseAmount);
        _transferRepositoryMock.Verify(repo => repo.CreateTransfer(It.IsAny<Transfer>()), Times.Once);
        _userRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Once);
    }
}
