﻿using Moq;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using static VirtualWallet.Features.Users.UnblockUser.UnblockUserFeature;

namespace VirtualWallet.Features.UnitTests.Users;

public class UnblockUserFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;

    public UnblockUserFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenLoggedUserNotFound()
    {
        // Arrange			
        var request = new BlockStatusRequest(intParam, intParam);
        var command = new UnblockUserCommand(request);

        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync((User)null);

        var handler = new UnblockUserHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenUserNotAdmin()
    {
        // Arrange			
        var request = new BlockStatusRequest(intParam, intParam);
        var command = new UnblockUserCommand(request);
        var user = GetTestUser();
        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync(user);

        var handler = new UnblockUserHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<UnauthorizedOperationException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenUserToBlockNotFound()
    {
        // Arrange			
        var request = new BlockStatusRequest(intParam, intParam);
        var command = new UnblockUserCommand(request);
        var loggedUser = GetTestUser();
        loggedUser.PromoteToAdmin();
        
        _userRepositoryMock.SetupSequence(repo => repo.GetUserById(intParam))
            .ReturnsAsync(loggedUser)
            .ReturnsAsync((User)null);

        var handler = new UnblockUserHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenUserIsNotBlocked()
    {
        // Arrange			
        var request = new BlockStatusRequest(intParam, intParam);
        var command = new UnblockUserCommand(request);
        var user = GetTestUser();
        user.PromoteToAdmin();
        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync(user);

        var handler = new UnblockUserHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<InvalidRequestException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldUnblock_WhenRequestIsValid()
    {
        // Arrange			
        var request = new BlockStatusRequest(intParam, intParam);
        var command = new UnblockUserCommand(request);
        var loggedUser = GetTestUser();
        loggedUser.PromoteToAdmin();
        var user = GetTestUser();
        user.Block();          
        _userRepositoryMock.SetupSequence(repo => repo.GetUserById(intParam))
            .ReturnsAsync(loggedUser)
            .ReturnsAsync(user);

        var handler = new UnblockUserHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.False(result.BlockedStatus);        
    }
}
