﻿using Moq;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Authentication.Requests;
using static VirtualWallet.Features.Users.ForgotPassword.ForgotPasswordFeature;

namespace VirtualWallet.Features.UnitTests.Users;

public class ForgotPasswordFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;
    public ForgotPasswordFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenUserNotFound()
    {
        // Arrange			
        var request = new ForgotPasswordRequest(stringParam);
        var command = new ForgotPasswordCommand(request);

        _userRepositoryMock.Setup(repo => repo.GetUserByEmail(It.IsAny<string>()))
            .ReturnsAsync((User)null);

        var handler = new ForgotPasswordHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldSetResetToken_WhenRequestIsValid()
    {
        // Arrange			
        var request = new ForgotPasswordRequest(stringParam);
        var command = new ForgotPasswordCommand(request);

        var user = GetTestUser();
        _userRepositoryMock.Setup(repo => repo.GetUserByEmail(It.IsAny<string>()))
            .ReturnsAsync(user);

        var handler = new ForgotPasswordHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.True(user.ResetPasswordToken != Guid.Empty);
        Assert.False(string.IsNullOrEmpty(result.ResetPasswordLink));
        Assert.NotNull(result);
    }
}
