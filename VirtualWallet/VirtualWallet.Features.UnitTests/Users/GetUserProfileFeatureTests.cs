﻿using Moq;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Abstractions;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using static VirtualWallet.Features.Users.GetUserProfile.GetUserProfileFeature;

namespace VirtualWallet.Features.UnitTests.Users;

public class GetUserProfileFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;
    private readonly Mock<ILinkService> _linkServiceMock;

    public GetUserProfileFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
        _linkServiceMock = new Mock<ILinkService>();
    }

    [Fact]
    public async Task Feature_ShouldTrown_WhenUserNotFound()
    {
        // Arrange			
        var request = new GetUserRequest(intParam);
        var command = new GetUserProfileCommand(request);

        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync((User)null);

        var handler = new GetUserProfileHandler(_userRepositoryMock.Object, _linkServiceMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldReturnUser_WhenRequestIsValid()
    {
        // Arrange			
        var request = new GetUserRequest(intParam);
        var command = new GetUserProfileCommand(request);

        var username = GetTestString(5);
        var user = User.CreateUser(username, stringParam, stringParam, stringParam);
        user.CreateWallet(currencyParam);
        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync(user);

        var handler = new GetUserProfileHandler(_userRepositoryMock.Object, _linkServiceMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.NotNull(result.BankCards);
        Assert.NotNull(result.ReceivedTransfers);
        Assert.NotNull(result.SentTransfers);
        Assert.Equal(username, result.Username);
    }
}
