﻿using Moq;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using static VirtualWallet.Features.Users.DeleteUser.DeleteUserFeature;

namespace VirtualWallet.Features.UnitTests.Users;

public class DeleteUserFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;
    public DeleteUserFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenUserNotFound()
    {
        // Arrange			
        var request = new DeleteUserRequest(intParam);
        var command = new DeleteUserCommand(request);

        _userRepositoryMock.Setup(repo => repo.GetUserById(It.IsAny<int>()))
            .ReturnsAsync((User)null);

        var handler = new DeleteUserHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    

    [Fact]
    public async Task Feature_ShouldDelete_WhenDeletingOtherUserAsAdmin()
    {
        // Arrange
        var otherUserId = 2;
        var loggedRole = "Admin";
        var request = new DeleteUserRequest(intParam);
        var command = new DeleteUserCommand(request);

        var user = GetTestUser();
        _userRepositoryMock.Setup(repo => repo.GetUserById(It.IsAny<int>()))
            .ReturnsAsync(user);

        var handler = new DeleteUserHandler(_userRepositoryMock.Object);

        // Act
        _ = await handler.Handle(command, default);

        // Assert
        Assert.True(user.IsDeleted);
    }

    [Fact]
    public async Task Feature_ShouldDelete_WhenDeletingOwnAcount()
    {
        // Arrange
        int userId = 1;
        var request = new DeleteUserRequest(userId);
        var command = new DeleteUserCommand(request);

        var user = GetTestUser();
        _userRepositoryMock.Setup(repo => repo.GetUserById(It.IsAny<int>()))
            .ReturnsAsync(user);

        var handler = new DeleteUserHandler(_userRepositoryMock.Object);

        // Act
        _ = await handler.Handle(command, default);

        // Assert
        Assert.True(user.IsDeleted);
    }
}
