﻿using Moq;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using static VirtualWallet.Features.Users.ChangeEmail.ChangeEmailFeature;

namespace VirtualWallet.Features.UnitTests.Users;

public class ChangeEmailFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;

    public ChangeEmailFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenUserNotFound()
    {
        // Arrange			
        var request = new ChangeEmailRequest(stringParam, stringParam);
        var command = new ChangeEmailCommand(request, tokenString);

        _userRepositoryMock.Setup(repo => repo.GetUserByEmailToken(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync((User)null);

        var handler = new ChangeEmailHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<InvalidTokenException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenEmailIsDuplicate()
    {
        // Arrange			
        var request = new ChangeEmailRequest(stringParam, stringParam);
        var command = new ChangeEmailCommand(request, tokenString);

        var user = GetTestUser();
        _userRepositoryMock.Setup(repo => repo.GetUserByEmailToken(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync(user);

        _userRepositoryMock.Setup(repo => repo.EmailExistsAsync(It.IsAny<string>()))
          .ReturnsAsync(true);

        var handler = new ChangeEmailHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<DuplicateUserException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldChangeEmail_WhenRequestIsValid()
    {
        // Arrange
        var newEmail = "newEmail";		
        var request = new ChangeEmailRequest(newEmail, newEmail);
        var command = new ChangeEmailCommand(request, tokenString);

        var user = GetTestUser();
        _userRepositoryMock.Setup(repo => repo.GetUserByEmailToken(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync(user);

        _userRepositoryMock.Setup(repo => repo.EmailExistsAsync(It.IsAny<string>()))
          .ReturnsAsync(false);

        var handler = new ChangeEmailHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(new ChangeEmailCommand(request, tokenString), default);

        // Assert
        Assert.Equal(newEmail, user.Email);
        Assert.True(result.Success);
    }
}
