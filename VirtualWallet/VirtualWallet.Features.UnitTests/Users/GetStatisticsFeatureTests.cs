﻿using Moq;
using static VirtualWallet.Features.GetUserFeature;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using static VirtualWallet.Features.Users.GetReceivedAmountLastMonth.GetStatisticsFeature;
using VirtualWallet.Models;

namespace VirtualWallet.Features.UnitTests.Users;

public class GetStatisticsFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;
    private readonly Mock<IExchangeRateService> _exchangeRateServiceMock;

    public GetStatisticsFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
        _exchangeRateServiceMock = new Mock<IExchangeRateService>();
    }


    [Fact]
    public async Task GetStatisticsHandler_ShouldThrow_WhenUserNotFound()
    {
        // Arrange			
        var request = new GetStatisticsRequest(intParam, dateParam, dateParam);
        var command = new GetReceivedAmountLastMonthQuery(request);

        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync((User)null);

        var handler = new GetStatisticsFeatureHandler(_exchangeRateServiceMock.Object, _userRepositoryMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task GetUserHandler_ShouldReturn_EmptyStatsWhenNoTransfersFound()
    {
        // Arrange			
        var request = new GetStatisticsRequest(intParam, dateParam, futureDate.AddDays(5));
        var command = new GetReceivedAmountLastMonthQuery(request);

        var user = GetTestUser();
        user.CreateWallet(currencyParam); 
        
        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync(user);

        var handler = new GetStatisticsFeatureHandler(_exchangeRateServiceMock.Object, _userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(0, result.TransfersReceived);
        Assert.Equal(0, result.TransfersSent);
    }

    [Fact]
    public async Task GetUserHandler_ShouldReturnStats_WhenRequestIsValid()
    {
        // Arrange			
        var request = new GetStatisticsRequest(intParam, dateParam, futureDate.AddDays(5));
        var command = new GetReceivedAmountLastMonthQuery(request);

        var user = GetTestUser();
        user.CreateWallet(currencyParam);
        var transfer = GetTestTransfer();
        transfer.SetTransferDate(futureDate);
        user.AddTransferReceived(transfer);

        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync(user);

        var eschangeResponse = GetExchangeRateResponse();
        _exchangeRateServiceMock.Setup(repo => repo.GetExchangeRateAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal>()))
            .ReturnsAsync(eschangeResponse);

        var handler = new GetStatisticsFeatureHandler(_exchangeRateServiceMock.Object, _userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(user.Id, result.UserId);
    }
}
