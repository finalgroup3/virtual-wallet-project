﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static VirtualWallet.Features.Users.GetUserProfile.GetUserProfileFeature;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Abstractions;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using static VirtualWallet.Features.Users.GetUserProfileByUsername.GetUserProfileByUsernameFeature;

namespace VirtualWallet.Features.UnitTests.Users;

public class GetUserProfileByUsernameFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;
    private readonly Mock<ILinkService> _linkServiceMock;

    public GetUserProfileByUsernameFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
        _linkServiceMock = new Mock<ILinkService>();
    }

    [Fact]
    public async Task Feature_ShouldTrown_WhenUserNotFound()
    {
        // Arrange			
        var request = new GetUserByUsernameRequest(stringParam);
        var command = new GetUserProfileByUsernameCommand(request);

        _userRepositoryMock.Setup(repo => repo.GetUserByUsername(stringParam)).ReturnsAsync((User)null);

        var handler = new GetUserProfileByUsernameHandler(_userRepositoryMock.Object, _linkServiceMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldReturnProfile_WhenRequestIsValid()
    {
        // Arrange			
        var request = new GetUserByUsernameRequest(stringParam);
        var command = new GetUserProfileByUsernameCommand(request);

        var user = GetTestUser();
        user.CreateWallet(currencyParam);
        _userRepositoryMock.Setup(repo => repo.GetUserByUsername(stringParam)).ReturnsAsync(user);

        var handler = new GetUserProfileByUsernameHandler(_userRepositoryMock.Object, _linkServiceMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.NotNull(result.BankCards);
        Assert.NotNull(result.ReceivedTransfers);
        Assert.NotNull(result.SentTransfers);
        Assert.Equal(user.Username, result.Username);
    }
}
