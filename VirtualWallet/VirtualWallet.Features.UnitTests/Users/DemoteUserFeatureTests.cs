﻿using Moq;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using static VirtualWallet.Features.Users.DemoteUser.DemoteUserFeature;

namespace VirtualWallet.Features.UnitTests.Users;

public class DemoteUserFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;

    public DemoteUserFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenTargetUserNotFound()
    {
        // Arrange			
        var request = new AdminStatusRequest(intParam, intParam);
        var command = new DemoteUserCommand(request);

        _userRepositoryMock.Setup(repo => repo.GetUserById(It.IsAny<int>())).ReturnsAsync((User)null);

        var handler = new DemoteUserHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenLoggedUserIsNotAdmin()
    {
        // Arrange			
        var request = new AdminStatusRequest(intParam, intParam);
        var command = new DemoteUserCommand(request);

        var user = GetTestUser();        
        _userRepositoryMock.Setup(repo => repo.GetUserById(It.IsAny<int>()))
            .ReturnsAsync(user);

        var handler = new DemoteUserHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<UnauthorizedOperationException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenTargetUserIsNotAdmin()
    {
        // Arrange			
        var request = new AdminStatusRequest(intParam, intParam);
        var command = new DemoteUserCommand(request);
                
        var user = GetTestUser();
        var adminUser = GetTestUser();
        adminUser.PromoteToAdmin();
        _userRepositoryMock.SetupSequence(repo => repo.GetUserById(It.IsAny<int>()))            
            .ReturnsAsync(user)
            .ReturnsAsync(adminUser);

        var handler = new DemoteUserHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<InvalidRequestException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldDemoteUser_WhenRequestIsValid()
    {
        // Arrange			
        var request = new AdminStatusRequest(intParam, intParam);
        var command = new DemoteUserCommand(request);

        var user = GetTestUser();
        user.PromoteToAdmin();
        _userRepositoryMock.Setup(repo => repo.GetUserById(It.IsAny<int>()))
            .ReturnsAsync(user);

        var handler = new DemoteUserHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.False(user.IsAdmin);
        Assert.Equal(user.Id, result.Id);
        Assert.NotNull(result);
    }

}
