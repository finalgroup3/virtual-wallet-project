﻿using Moq;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using static VirtualWallet.Features.GetUserFeature;

namespace VirtualWallet.Features.UnitTests.Users;

public class GetUserFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;

    public GetUserFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    // Test for GetUserHandler when the user is not found
    [Fact]
    public async Task GetUserHandler_InvalidId_ThrowsUserNotFoundException()
    {
        // Arrange			
        var request = new GetUserRequest(intParam);
        var command = new GetUserCommand(request);

        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync((User)null);

        var handler = new GetUserHandler(_userRepositoryMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    // Test for GetUserHandler
    [Fact]
    public async Task GetUserHandler_ValidId_ReturnsUserResponse()
    {
        // Arrange
        var request = new GetUserRequest(intParam);
        var command = new GetUserCommand(request);

        var user = GetTestUser();
        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync(user);

        var getUserHandler = new GetUserHandler(_userRepositoryMock.Object);

        // Act
        var result = await getUserHandler.Handle(command, default);

        // Assert
        Assert.NotNull(result);

        Assert.Equal(stringParam, result.Username);
        Assert.Equal(stringParam, result.Email);
        Assert.Equal(stringParam, result.PhoneNumber);

        _userRepositoryMock.Verify(repo => repo.GetUserById(intParam), Times.Once);
    }
}
