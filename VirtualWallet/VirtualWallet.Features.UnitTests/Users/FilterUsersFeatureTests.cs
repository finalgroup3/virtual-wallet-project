﻿using Moq;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using static VirtualWallet.Features.Users.FilterUsers.FilterUsersFeature;

namespace VirtualWallet.Features.UnitTests.Users;

public class FilterUsersFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;

    public FilterUsersFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldFilter_ByUsernameWhenRequested()
    {
        // Arrange	
        var byUsername = GetTestString(7);		
        var request = new FilterUsersRequest(null,byUsername,null,null,null,intParam,intParam);
        var command = new FilterUsersCommand(request);

        var user = User.CreateUser(byUsername, stringParam, stringParam, stringParam); 
        var query = GetTestUsersQuery(user);
        _userRepositoryMock.Setup(repo => repo.GetUsersQuery()).ReturnsAsync(query);

        var handler = new FilterUsersHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotEqual(query.Count(), result.Users.TotalItems);
        Assert.Equal(1, result.Users.TotalItems);
        Assert.NotNull(result);
    }

    [Fact]
    public async Task Feature_ShouldFilter_ByEmailWhenRequested()
    {
        // Arrange	
        var byEmail = GetTestString(7);
        var request = new FilterUsersRequest(null, null, byEmail, null, null, intParam, intParam);
        var command = new FilterUsersCommand(request);

        var user = User.CreateUser(stringParam, stringParam, byEmail, stringParam);
        var query = GetTestUsersQuery(user);
        _userRepositoryMock.Setup(repo => repo.GetUsersQuery()).ReturnsAsync(query);

        var handler = new FilterUsersHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotEqual(query.Count(), result.Users.TotalItems);
        Assert.Equal(1, result.Users.TotalItems);
        Assert.NotNull(result);
    }

    [Fact]
    public async Task Feature_ShouldFilter_ByPhoneNumberWhenRequested()
    {
        // Arrange	
        var byPhoneNumber = GetTestString(7);
        var request = new FilterUsersRequest(byPhoneNumber, null, null, null, null, intParam, intParam);
        var command = new FilterUsersCommand(request);

        var user = User.CreateUser(stringParam, stringParam, stringParam, byPhoneNumber);
        var query = GetTestUsersQuery(user);
        _userRepositoryMock.Setup(repo => repo.GetUsersQuery()).ReturnsAsync(query);

        var handler = new FilterUsersHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotEqual(query.Count(), result.Users.TotalItems);
        Assert.Equal(1, result.Users.TotalItems);
        Assert.NotNull(result);
    }

    [Fact]
    public async Task Feature_Should_ReturnEmptyIfNoUsersFound()
    {
        // Arrange	
        var byPhoneNumber = GetTestString(7);
        var request = new FilterUsersRequest(byPhoneNumber, null, null, null, null, intParam, intParam);
        var command = new FilterUsersCommand(request);

        var defaultUser = GetTestUser();
        var query = GetTestUsersQuery(defaultUser);
        _userRepositoryMock.Setup(repo => repo.GetUsersQuery()).ReturnsAsync(query);

        var handler = new FilterUsersHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotEqual(query.Count(), result.Users.TotalItems);
        Assert.Equal(0, result.Users.TotalItems);
        Assert.NotNull(result);
    }

    [Theory]
    [InlineData("ascending")]
    [InlineData("descending")]
    public async Task Feature_Should_OrderUsersWhenRequested(string orderBy)
    {
        // Arrange	       
        var request = new FilterUsersRequest(null, null, null, null, orderBy, intParam, intParam);
        var command = new FilterUsersCommand(request);

        var unuqueUsername = GetTestString(7);
        var user = User.CreateUser(unuqueUsername, stringParam, stringParam, stringParam);
        var query = GetTestUsersQuery(user);
        _userRepositoryMock.Setup(repo => repo.GetUsersQuery()).ReturnsAsync(query);

        var handler = new FilterUsersHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        var lastQueryUsername = query.Last().Username;
        var lastResultUsername = result.Users.Items.Last().Username;
        Assert.False(lastQueryUsername == lastResultUsername);
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenOrderByIsInvalid()
    {
        // Arrange
        var orderBy = GetTestString(5);
        var request = new FilterUsersRequest(null, null, null, null, orderBy, intParam, intParam);
        var command = new FilterUsersCommand(request);

        var defaultUser = GetTestUser();
        var query = GetTestUsersQuery(defaultUser);
        _userRepositoryMock.Setup(repo => repo.GetUsersQuery()).ReturnsAsync(query);

        var handler = new FilterUsersHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<InvalidRequestException>(() => handler.Handle(command, default));
    }

    [Theory]
    [InlineData("username")]
    [InlineData("email")]
    [InlineData("phoneNumber")]
    public async Task Feature_Should_SortUsersWhenRequested(string sortBy)
    {
        // Arrange	       
        var request = new FilterUsersRequest(null, null, null, sortBy, null, intParam, intParam);
        var command = new FilterUsersCommand(request);

        var unuqueValue = GetTestString(7);
        var user = User.CreateUser(unuqueValue, stringParam, unuqueValue, unuqueValue);
        var query = GetTestUsersQuery(user);
        _userRepositoryMock.Setup(repo => repo.GetUsersQuery()).ReturnsAsync(query);

        var handler = new FilterUsersHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        var lastQueryUsername = query.Last().Username;
        var lastResultUsername = result.Users.Items.Last().Username;
        Assert.False(lastQueryUsername == lastResultUsername);
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenSortByIsInvalid()
    {
        // Arrange
        var sortBy = GetTestString(5);
        var request = new FilterUsersRequest(null, null, null, sortBy, null, intParam, intParam);
        var command = new FilterUsersCommand(request);

        var defaultUser = GetTestUser();
        var query = GetTestUsersQuery(defaultUser);
        _userRepositoryMock.Setup(repo => repo.GetUsersQuery()).ReturnsAsync(query);

        var handler = new FilterUsersHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<InvalidRequestException>(() => handler.Handle(command, default));
    }
}
