﻿using Moq;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using static VirtualWallet.Features.Users.ChangePhoneNumber.ChangePhoneNumberFeature;

namespace VirtualWallet.Features.UnitTests.Users;

public class ChangePhoneNumberFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;

    public ChangePhoneNumberFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenUserNotFound()
    {
        // Arrange			
        var request = new ChangePhoneNumberRequest(stringParam, intParam);
        var command = new ChangePhoneNumberCommand(request);

        _userRepositoryMock.Setup(repo => repo.PhoneNumberExistsAsync(It.IsAny<string>())).ReturnsAsync(true);

        var handler = new ChangePhoneNumberHandler(_userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<DuplicateUserException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldUpdatePhoneNumber_WhenRequestIsValid()
    {
        // Arrange
        var phoneNumber = "0876543210";			
        var request = new ChangePhoneNumberRequest(phoneNumber, intParam);
        var command = new ChangePhoneNumberCommand(request);

        _userRepositoryMock.Setup(repo => repo.PhoneNumberExistsAsync(It.IsAny<string>())).ReturnsAsync(false);

        var user = GetTestUser();
        _userRepositoryMock.Setup(repo => repo.GetUserById(It.IsAny<int>())).ReturnsAsync(user);

        var handler = new ChangePhoneNumberHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(phoneNumber, result.NewPhoneNumber);
    }
}
