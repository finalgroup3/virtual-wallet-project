﻿using Moq;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using static VirtualWallet.Features.Users.ChangePhoneNumber.ChangePictureUrlFeature;

namespace VirtualWallet.Features.UnitTests.Users;

public class ChangePictureUrlFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;

    public ChangePictureUrlFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldUpdatePictureURl_WhenRequestIsValid()
    {
        // Arrange
        var newPictureUrl = GetTestString(7);
        var request = new ChangePictureUrlRequest(newPictureUrl, intParam);
        var command = new ChangePictureUrlCommand(request);

        var user = GetTestUser();
        _userRepositoryMock.Setup(repo => repo.GetUserById(It.IsAny<int>())).ReturnsAsync(user);

        var handler = new ChangePictureUrlHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(newPictureUrl, user.PhotoUrl);
    }
}
