﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static VirtualWallet.Features.Users.FilterUsers.FilterUsersFeature;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using static VirtualWallet.Features.GetAllUsersFeature;

namespace VirtualWallet.Features.UnitTests.Users;

public class GetAllUsersFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;

    public GetAllUsersFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldReturnUsers_WhenRequestIsValid()
    {
        // Arrange
        var command = new GetAllUsersQuery();

        var user = GetTestUser();
        var query = GetTestUsersQuery(user);
        _userRepositoryMock.Setup(repo => repo.GetAllUsers(It.IsAny<CancellationToken>())).ReturnsAsync(query);

        var handler = new GetAllUsersFeatureHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(query.Count(), result.Count);
    }
}
