﻿using Moq;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Abstractions;
using VirtualWallet.Features.Common.Interfaces.Authentication;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Features.Constants;
using VirtualWallet.Models.Authentication.Requests;
using static VirtualWallet.Features.Authentication.Login.LoginFeature;

namespace VirtualWallet.Features.UnitTests.Authentication;

public class LoginFeatureHandlerTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;
    private readonly Mock<IJwtProvider> _jwtProviderMock;
    private readonly Mock<ILinkService> _linkServiceMock;

    public LoginFeatureHandlerTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
        _jwtProviderMock = new Mock<IJwtProvider>();
        _linkServiceMock = new Mock<ILinkService>();
    }

    [Fact]
    public async Task Handle_Should_ThrowInvalidCredentialsException_WhenPasswordIsIncorrect()
    {
        // Arrange
        var request = new LoginRequest(stringParam, stringParam);
        var command = new LoginCommand(request);

        var hashedPassword = BCrypt.Net.BCrypt.HashPassword("correctpassword");
        var user = GetTestUser();
        user.SetPassword(hashedPassword);
        _userRepositoryMock.Setup(x => x.GetUserByEmail(It.IsAny<string>()))
            .ReturnsAsync(user);

        var handler = new LoginFeatureHandler(_userRepositoryMock.Object, _jwtProviderMock.Object, _linkServiceMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<InvalidCredentialsException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Handle_ShouldThrowInvalidCredentialsException_WhenRepositoryMethod_ReturnsNull()
    {
        // Arrange
        var request = new LoginRequest(stringParam, stringParam);

        var command = new LoginCommand(request);

        _userRepositoryMock.Setup(x => x.GetUserByEmail(It.IsAny<string>()))
            .ReturnsAsync((User)null);

        var handler = new LoginFeatureHandler(_userRepositoryMock.Object, _jwtProviderMock.Object, _linkServiceMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<InvalidCredentialsException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Handle_Should_ReturnLoginSuccessResponse_WhenCredentialsAreCorrect()
    {
        // Arrange
        var request = new LoginRequest(stringParam, stringParam);
        var command = new LoginCommand(request);

        var hashedPassword = BCrypt.Net.BCrypt.HashPassword(stringParam);
        var user = GetTestUser();
        user.SetPassword(hashedPassword);
        _userRepositoryMock.Setup(x => x.GetUserByEmail(It.IsAny<string>()))
            .ReturnsAsync(user);

        var handler = new LoginFeatureHandler(_userRepositoryMock.Object, _jwtProviderMock.Object, _linkServiceMock.Object);

        // Act & Assert
        var result = await handler.Handle(command, default);
        Assert.NotNull(result);
        Assert.Equal(AuthenticationLiterals.Success, result.Status);
    }

    [Fact]
    public async Task Handle_Should_GenerateToken_WhenUserExists()
    {
        // Arrange
        var passwordString = GetTestString(10);
        var request = new LoginRequest(stringParam, passwordString);
        var command = new LoginCommand(request);

        var hashedPassword = BCrypt.Net.BCrypt.HashPassword(passwordString);
        var user = GetTestUser();
        user.SetPassword(hashedPassword);
        _userRepositoryMock.Setup(x => x.GetUserByEmail(It.IsAny<string>()))
            .ReturnsAsync(user);

        var handler = new LoginFeatureHandler(_userRepositoryMock.Object, _jwtProviderMock.Object, _linkServiceMock.Object);

        // Act
        await handler.Handle(command, default);

        // Assert
        _jwtProviderMock.Verify(x => x.GenerateJwtToken(It.IsAny<User>()), Times.Once);
    }
}
