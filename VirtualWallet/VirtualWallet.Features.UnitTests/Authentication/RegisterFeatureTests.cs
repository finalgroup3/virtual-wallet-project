﻿using Moq;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Features.Constants;
using static VirtualWallet.Features.Authentication.Register.RegisterFeature;

namespace VirtualWallet.Features.UnitTests.Authentication;


public class RegisterFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;

    public RegisterFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Handle_Should_ThrowDuplicateEntityException_WhenEmailIsNotUnique()
    {
        // Arrange
        var request = GetRegisterRequest();
        var command = new RegisterCommand(request);

        _userRepositoryMock.Setup(x => x.EmailExistsAsync(
            It.IsAny<string>()))
            .ReturnsAsync(true);

        var handler = new RegisterFeatureHandler(
            _userRepositoryMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<DuplicateUserException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Handle_Should_ThrowDuplicateEntityException_WhenUsernameIsNotUnique()
    {
        // Arrange
        var request = GetRegisterRequest();
        var command = new RegisterCommand(request);

        _userRepositoryMock.Setup(x => x.UsernameExistsAsync(
            It.IsAny<string>()))
            .ReturnsAsync(true);

        var handler = new RegisterFeatureHandler(
            _userRepositoryMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<DuplicateUserException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Handle_Should_ThrowDuplicateEntityException_WhenPhoneNumberIsNotUnique()
    {
        // Arrange
        var request = GetRegisterRequest();
        var command = new RegisterCommand(request);

        _userRepositoryMock.Setup(x => x.PhoneNumberExistsAsync(
            It.IsAny<string>()))
            .ReturnsAsync(true);

        var handler = new RegisterFeatureHandler(
            _userRepositoryMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<DuplicateUserException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Handle_Should_ReturnRegisterSuccessResponse_WhenEmailIsUnique()
    {
        // Arrange
        var request = GetRegisterRequest();

        var command = new RegisterCommand(request);

        _userRepositoryMock.Setup(x => x.EmailExistsAsync(It.IsAny<string>()))
            .ReturnsAsync(false);

        var handler = new RegisterFeatureHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.True(result.Success);
        Assert.Equal(AuthenticationLiterals.RegisterSuccess, result.Message);
    }

    [Fact]
    public async Task Handle_Should_CallAddOnRepository_WhenEmailIsUnqiue()
    {
        // Arrange
        var request = GetRegisterRequest();

        var command = new RegisterCommand(request);

        _userRepositoryMock.Setup(x => x.EmailExistsAsync(It.IsAny<string>()))
            .ReturnsAsync(false);

        var handler = new RegisterFeatureHandler(_userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        _userRepositoryMock.Verify(x => x.AddAsync(It.Is<User>(u => u.Id == result.Data.Id)), Times.Once);
    }
}
