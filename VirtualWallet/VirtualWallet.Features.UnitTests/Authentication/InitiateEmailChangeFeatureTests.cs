﻿using Moq;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Authentication.Requests;
using static VirtualWallet.Features.Authentication.InitiateEmailChange.InitiateEmailChangeFeature;

namespace VirtualWallet.Features.UnitTests.Authentication;

public class InitiateEmailChangeFeatureTests
{    
    private readonly Mock<IUserRepository> _userRepositoryMock;
    public InitiateEmailChangeFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenUserNotFound()
    {
        // Arrange        
        var request = new InitiateEmailChangeRequest(stringParam, intParam);
        var command = new InitiateEmailChangeCommand(request);

        var user = GetTestUser();
        user.SetId(intParam);

        _userRepositoryMock.Setup(repo => repo.GetUserByEmail(
             It.IsAny<string>()))
            .Throws<UserNotFoundException>();

        var handler = new InitiateEmailChangeHandler(
            _userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenUserNotAuthorized()
    {
        // Arrange        
        var request = new InitiateEmailChangeRequest(stringParam, intParam);
        var command = new InitiateEmailChangeCommand(request);

        var user = GetTestUser();
        int newId = 2;
        user.SetId(newId);

        _userRepositoryMock.Setup(repo => repo.GetUserByEmail(It.IsAny<string>()))
            .ReturnsAsync(user);

        var handler = new InitiateEmailChangeHandler(
            _userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<UnauthorizedOperationException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_Should_GenerateToken_WhenRequestIsValid()
    {
        // Arrange        
        var request = new InitiateEmailChangeRequest(stringParam, intParam);
        var command = new InitiateEmailChangeCommand(request);

        var user = GetTestUser();
        user.SetId(intParam);

        _userRepositoryMock.Setup(repo => repo.GetUserByEmail(
             It.IsAny<string>()))
            .ReturnsAsync(user);

        var handler = new InitiateEmailChangeHandler(
            _userRepositoryMock.Object);

        // Act 
        var result = await handler.Handle(command, default);

        //Assert
        Assert.True(user.ChangeEmailToken != Guid.Empty);
        Assert.False(string.IsNullOrEmpty(result.ChangeEmailToken));
    }
}
