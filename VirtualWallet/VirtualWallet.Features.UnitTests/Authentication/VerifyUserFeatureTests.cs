﻿using Moq;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Abstractions;
using VirtualWallet.Features.Common.Interfaces.Authentication;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Features.Constants;
using static VirtualWallet.Features.Authentication.Verify.VerifyUserFeature;

namespace VirtualWallet.Features.UnitTests.Authentication.Verify;

public class VerifyUserFeatureTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;
    private readonly Mock<IJwtProvider> _jwtProviderMock;
    private readonly Mock<ILinkService> _linkServiceMock;

    public VerifyUserFeatureTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
        _jwtProviderMock = new Mock<IJwtProvider>();
        _linkServiceMock = new Mock<ILinkService>();
    }

    [Fact]
    public async Task Handle_Should_ThrowInvalidTokenException_WhenUserDoesNotExist()
    {
        // Arrange
        var command = new VerifyUserCommand(tokenString);

        _userRepositoryMock.Setup(x => x.GetUserByToken(
            It.IsAny<Guid>(),
            It.IsAny<CancellationToken>()))
            .ReturnsAsync((User)null);

        var handler = new VerifyUserFeatureHandler(_userRepositoryMock.Object, _jwtProviderMock.Object, _linkServiceMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<InvalidTokenException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Handle_Should_ThrowInvalidTokenException_WhenUserIsAlreadyVerified()
    {
        // Arrange
        var command = new VerifyUserCommand(tokenString);

        var user = GetTestUser();

        _userRepositoryMock.Setup(x => x.GetUserByToken(
                       It.IsAny<Guid>(),
                       It.IsAny<CancellationToken>()))
            .ReturnsAsync(user);
        user.Verify();


        var handler = new VerifyUserFeatureHandler(_userRepositoryMock.Object, _jwtProviderMock.Object, _linkServiceMock.Object);

        // Act & Assert
        await Assert.ThrowsAsync<InvalidTokenException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Handle_Should_CallVerifyAndUpdate_WhenTokenIsValidAndUserIsNotVerified()
    {
        // Arrange
        var user = GetTestUser();

        var command = new VerifyUserCommand(tokenString);

        _userRepositoryMock.Setup(x => x.GetUserByToken(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(user);
        var handler = new VerifyUserFeatureHandler(_userRepositoryMock.Object, _jwtProviderMock.Object, _linkServiceMock.Object);

        // Act
        await handler.Handle(command, default);

        // Assert
        Assert.True(user.IsVerified);
        _userRepositoryMock.Verify(x => x.Update(user), Times.Once);
    }

    [Fact]
    public async Task Handle_Should_ReturnVerifySuccessResponse_WhenTokenIsValidAndUserIsNotVerified()
    {
        // Arrange
        var user = GetTestUser();

        var command = new VerifyUserCommand(tokenString);

        _userRepositoryMock.Setup(x => x.GetUserByToken(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(user);
        var handler = new VerifyUserFeatureHandler(_userRepositoryMock.Object, _jwtProviderMock.Object, _linkServiceMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.True(result.Success);
        Assert.Equal(AuthenticationLiterals.VerifySuccess, result.Message);
    }
}
