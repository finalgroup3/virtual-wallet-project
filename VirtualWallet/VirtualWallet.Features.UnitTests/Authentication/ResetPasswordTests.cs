﻿using Moq;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Authentication.Requests;
using static VirtualWallet.Features.Authentication.ResetPassword.ResetPasswordFeature;

namespace VirtualWallet.Features.UnitTests.Authentication;

public class ResetPasswordTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;        
    public ResetPasswordTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Handle_ShouldThrow_WhenTokenIsInvalid()
    {        
        // Arrange
        var request = new ResetPasswordRequest(stringParam, stringParam);
        var command = new ResetPasswordCommand(request, tokenString);
                
        _userRepositoryMock.Setup(repo => repo.GetUserByResetToken(
            It.IsAny<Guid>(),
             It.IsAny<CancellationToken>()))
            .ReturnsAsync((User)null);

        var handler = new ResetPasswordFeatureHandler(
            _userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<InvalidTokenException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Handle_Should_ResetPassword_WhenTokenIsValid()
    {
        // Arrange
        var newPassword = "newPassword";
        var request = new ResetPasswordRequest(newPassword, newPassword);
        var command = new ResetPasswordCommand(request, tokenString);

        var user = GetTestUser();

        _userRepositoryMock.Setup(repo => repo.GetUserByResetToken(
            It.IsAny<Guid>(),
             It.IsAny<CancellationToken>()))
            .ReturnsAsync(user);

        var handler = new ResetPasswordFeatureHandler(
            _userRepositoryMock.Object);

        // Act 
        var result = await handler.Handle(command, default);

        //Assert
        Assert.True(BCrypt.Net.BCrypt.Verify(newPassword, user.Password));
        Assert.Equal(Guid.Empty, user.ResetPasswordToken);
        Assert.True(result.Success);
    }
}
