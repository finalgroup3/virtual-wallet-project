﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static VirtualWallet.Features.Users.BlockUser.BlockUserFeature;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using static VirtualWallet.Features.Wallets.ChangeCurrency.ChangeCurrencyFeature;
using VirtualWallet.Models.Wallets.Requests;
using VirtualWallet.Domain.Exceptions.Common;

namespace VirtualWallet.Features.UnitTests.Wallets;

public class ChangeCurrencyFeatureTests
{
    private readonly Mock<IWalletRepository> _walletRepositoryMock;
    private readonly Mock<IExchangeRateService> _exchangeRateServiceMock;
    private readonly Mock<IUserRepository> _userRepositoryMock;

    public ChangeCurrencyFeatureTests()
    {
        _walletRepositoryMock = new Mock<IWalletRepository>();
        _exchangeRateServiceMock = new Mock<IExchangeRateService>();
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenUserNotFound()
    {
        // Arrange			
        var request = new ChangeCurrencyRequest(intParam, stringParam, stringParam);
        var command = new ChangeCurrencyCommand(request);

        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync((User)null);

        var handler = new ChangeCurrencyFeatureHandler(_walletRepositoryMock.Object,
            _exchangeRateServiceMock.Object, _userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenPasswordNotCorrectFormat()
    {
        // Arrange
        var hashedPassword = BCrypt.Net.BCrypt.HashPassword(stringParam);			
        var request = new ChangeCurrencyRequest(intParam, hashedPassword, stringParam);
        var command = new ChangeCurrencyCommand(request);
        var secondHashedPassword = BCrypt.Net.BCrypt.HashPassword(tokenString);
        var user = GetTestUser();
        user.SetPassword(secondHashedPassword);
        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync(user);

        var handler = new ChangeCurrencyFeatureHandler(_walletRepositoryMock.Object,
            _exchangeRateServiceMock.Object, _userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<InvalidCredentialsException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldChangeCurrency_WhenRequestIsValid()
    {
        // Arrange	
        var newCurrency = "USD";
        var hashedPassword = BCrypt.Net.BCrypt.HashPassword(stringParam);
        var request = new ChangeCurrencyRequest(intParam, stringParam, newCurrency);
        var command = new ChangeCurrencyCommand(request);        
        var user = GetTestUser();
        user.SetPassword(hashedPassword);
        user.CreateWallet(currencyParam);
        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync(user);
        var exchangeRate = GetExchangeRateResponse();
        _exchangeRateServiceMock.Setup(repo => repo.GetExchangeRateAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal>()))
            .ReturnsAsync(exchangeRate);

        var handler = new ChangeCurrencyFeatureHandler(_walletRepositoryMock.Object,
            _exchangeRateServiceMock.Object, _userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(user.Wallet.Id, result.Id);
        Assert.Equal(user.Wallet.Currency, result.NewCurr);
        
    }
}
