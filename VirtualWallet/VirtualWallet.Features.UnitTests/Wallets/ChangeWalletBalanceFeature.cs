﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static VirtualWallet.Features.Wallets.ChangeCurrency.ChangeCurrencyFeature;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Wallets.Requests;
using static VirtualWallet.Features.Users.ChangeWalletBalanceFeature.ChangeWalletBalanceFeature;
using VirtualWallet.Domain.Exceptions.Common;

namespace VirtualWallet.Features.UnitTests.Wallets;

public class ChangeWalletBalanceFeature
{
    private readonly Mock<IWalletRepository> _walletRepositoryMock;    
    private readonly Mock<IUserRepository> _userRepositoryMock;

    public ChangeWalletBalanceFeature()
    {
        _walletRepositoryMock = new Mock<IWalletRepository>();        
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenWalletNotFound()
    {
        // Arrange			
        var request = new ChangeWalletBalanceRequest(intParam, decimalParam);
        var command = new ChangeWalletBalanceCommand(request);

        _walletRepositoryMock.Setup(repo => repo.GetWalletByUserId(intParam)).ReturnsAsync((Wallet)null);

        var handler = new ChangeWalletBalanceHandler(_userRepositoryMock.Object, _walletRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<InvalidRequestException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldChangeBalance_WhenRequestIsValid()
    {
        // Arrange	
        decimal newBalance = decimalParam * 2;		
        var request = new ChangeWalletBalanceRequest(intParam, newBalance);
        var command = new ChangeWalletBalanceCommand(request);

        var wallet = Wallet.CreateWallet(intParam, decimalParam, currencyParam);
        _walletRepositoryMock.Setup(repo => repo.GetWalletByUserId(intParam)).ReturnsAsync(wallet);

        var handler = new ChangeWalletBalanceHandler(_userRepositoryMock.Object, _walletRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(true, result.Success);
        Assert.Equal(wallet.Balance, newBalance);
    }
}
