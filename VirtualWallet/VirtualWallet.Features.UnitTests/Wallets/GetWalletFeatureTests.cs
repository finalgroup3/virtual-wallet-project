﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static VirtualWallet.Features.Wallets.ChangeCurrency.ChangeCurrencyFeature;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Wallets.Requests;
using static VirtualWallet.Features.GetWalletFeature;
using VirtualWallet.Models;
using VirtualWallet.Domain.Exceptions.Common;

namespace VirtualWallet.Features.UnitTests.Wallets;

public class GetWalletFeatureTests
{
    private readonly Mock<IWalletRepository> _walletRepositoryMock; 
    private readonly Mock<IUserRepository> _userRepositoryMock;

    public GetWalletFeatureTests()
    {
        _walletRepositoryMock = new Mock<IWalletRepository>();        
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenUserNotFound()
    {
        // Arrange			
        var request = new GetWalletRequest(intParam);
        var command = new GetWalletQuery(request);

        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync((User)null);

        var handler = new GetWalletFeatureHandler(_walletRepositoryMock.Object, _userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenWalletIsNull()
    {
        // Arrange			
        var request = new GetWalletRequest(intParam);
        var command = new GetWalletQuery(request);

        var user = GetTestUser();
        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync(user);

        var handler = new GetWalletFeatureHandler(_walletRepositoryMock.Object, _userRepositoryMock.Object);

        // Act and Assert
        await Assert.ThrowsAsync<InvalidRequestException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_ShouldReturnWallet_WhenRequestIsValid()
    {
        // Arrange			
        var request = new GetWalletRequest(intParam);
        var command = new GetWalletQuery(request);

        var user = GetTestUser();
        user.CreateWallet(currencyParam);
        _userRepositoryMock.Setup(repo => repo.GetUserById(intParam)).ReturnsAsync(user);

        var wallet = Wallet.CreateWallet(intParam, decimalParam, currencyParam);
        _walletRepositoryMock.Setup(repo => repo.GetWalletByUserId(It.IsAny<int>())).ReturnsAsync(wallet);

        var handler = new GetWalletFeatureHandler(_walletRepositoryMock.Object, _userRepositoryMock.Object);

        // Act
        var result = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(wallet.Id, result.Id);
        Assert.Equal(wallet.Currency, result.Currency);

    }
}

