﻿using MediatR;
using Moq;
using VirtualWallet.Domain.CardEntity;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.Cards.DeleteCard;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Cards.Requests;

namespace VirtualWallet.Features.UnitTests.Cards
{
    public class DeleteCardFeatureTests
	{
		private readonly Mock<ICardRepository> _cardRepositoryMock;

		public DeleteCardFeatureTests()
		{
			_cardRepositoryMock = new Mock<ICardRepository>();
		}

		[Fact]
		public async Task Feature_ShouldThrow_WhenCardIdDoesNotExist()
		{
			// Arrange	
			var deleteCardRequest = new DeleteCardRequest(intParam, intParam);
			var command = new DeleteCardFeature.DeleteCardCommand(deleteCardRequest);

			_cardRepositoryMock.Setup(repo => repo.GetCardById(intParam)).ReturnsAsync((BankCard)null);
			var handler = new DeleteCardFeature.DeleteCardHandler(_cardRepositoryMock.Object);

			// Act & Assert
			var exception = await Record.ExceptionAsync(() => handler.Handle(command, default));
			Assert.IsType<CardNotFoundException>(exception);
		}

		[Fact]
		public async Task Feature_ShouldThrow_WhenUserTriesToDeleteOtherUsersCard()
		{
			// Arrange
			var deleteCardRequest = new DeleteCardRequest(intParam, intParam + 1);
			var command = new DeleteCardFeature.DeleteCardCommand(deleteCardRequest);
			var bankCard = GetTestCard();

			_cardRepositoryMock.Setup(repo => repo.GetCardById(intParam)).ReturnsAsync(bankCard);
			_cardRepositoryMock.Setup(repo => repo.CreateCard(It.IsAny<BankCard>())).Callback<BankCard>(card => bankCard = card);
			var handler = new DeleteCardFeature.DeleteCardHandler(_cardRepositoryMock.Object);
            await _cardRepositoryMock.Object.CreateCard(bankCard);

			// Act & Assert
			var exception = await Record.ExceptionAsync(() => handler.Handle(command, default));
			Assert.IsType<UnauthorizedAccessException>(exception);
		}

		[Fact]
		public async Task Feature_ShouldDelete_WhenRequestIsValid()
		{
			// Arrange
			var deleteCardRequest = new DeleteCardRequest(intParam, intParam);
			var command = new DeleteCardFeature.DeleteCardCommand(deleteCardRequest);
			
			var bankCard = GetTestCard();

			_cardRepositoryMock.Setup(repo => repo.GetCardById(intParam)).ReturnsAsync(bankCard);
			_cardRepositoryMock.Setup(repo => repo.CreateCard(It.IsAny<BankCard>())).Callback<BankCard>(card => bankCard = card);
			var handler = new DeleteCardFeature.DeleteCardHandler(_cardRepositoryMock.Object);

			// Act - Create the card using the repository
			await _cardRepositoryMock.Object.CreateCard(bankCard);

			// Now, attempt to delete the card using the handler
			var result = await handler.Handle(command, default);

			// Assert
			Assert.Equal(Unit.Value, result);

			// Verify that DeleteCard was called on the repository
			
			_cardRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Once);
		}
	}
}
