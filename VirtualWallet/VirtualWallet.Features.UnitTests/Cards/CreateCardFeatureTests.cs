﻿using Moq;
using VirtualWallet.Domain.CardEntity;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using static VirtualWallet.Features.CreateCardFeature;

namespace VirtualWallet.Features.UnitTests.Cards
{
    public class CreateCardFeatureTests
	{
		private readonly Mock<ICardRepository> _cardRepositoryMock;
		public CreateCardFeatureTests()
		{
			_cardRepositoryMock = new Mock<ICardRepository>();
		}

		[Fact]
		public async Task Handle_ShouldThrow_WhenTheCardAlreadyExists()
		{
			// Arrange
			var request = GetCreateCardRequest();

            var command = new CreateCardCommand(request, intParam);

			_cardRepositoryMock.Setup(repo => repo.IsNumberUnique(It.IsAny<string>())).ReturnsAsync(false);
			var handler = new CreateCardHandler(_cardRepositoryMock.Object);

			// Act & Assert
			await Assert.ThrowsAsync<DuplicateCardException>(() => handler.Handle(command, default));
		}

		[Fact]
		public async Task Handle_ShouldSaveCard_WhenInputIsValid()
		{
			// Arrange
			var request = GetCreateCardRequest();
            var command = new CreateCardCommand(request, 1);

			_cardRepositoryMock.Setup(repo => repo.IsNumberUnique(It.IsAny<string>())).ReturnsAsync(true);
			var handler = new CreateCardHandler(_cardRepositoryMock.Object);

			// Act
			var response = await handler.Handle(command, default);

			// Assert
			Assert.NotNull(response);
			Assert.Equal(1, response.UserId);
			// Add other assertions as needed to check the properties of the response.

			// Verify that the card was saved in the repository
			_cardRepositoryMock.Verify(x => x.CreateCard(It.IsAny<BankCard>()), Times.Once);
		}
	}
}
