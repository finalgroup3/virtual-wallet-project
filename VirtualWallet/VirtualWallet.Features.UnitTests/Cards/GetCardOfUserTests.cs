﻿using Moq;
using VirtualWallet.Domain.CardEntity;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Cards.Requests;
using static VirtualWallet.Features.Cards.GetCardsOfUser.GetCardsOfUserFeature;

namespace VirtualWallet.Features.UnitTests.Cards;

public class GetCardOfUserTests
{
    private readonly Mock<ICardRepository> _cardRepositoryMock;
    private readonly Mock<IUserRepository> _userRepositoryMock;
    public GetCardOfUserTests()
    {
        _cardRepositoryMock = new Mock<ICardRepository>();
        _userRepositoryMock = new Mock<IUserRepository>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenUserNotFound()
    {
        // Arrange
        var request = new GetCardsOfUserRequest(intParam);

        var command = new GetCardsOfUserCommand(request);

        _userRepositoryMock.Setup(repo => repo.GetUserById(It.IsAny<int>())).ReturnsAsync((User)null);

        var handler = new GetCardsOfUserHandler(_cardRepositoryMock.Object, _userRepositoryMock.Object);

        //Act and Assert
        await Assert.ThrowsAsync<UserNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_Should_ReturnCard_WhenRequestIsValid()
    {
        // Arrange
        var request = new GetCardsOfUserRequest(intParam);

        var command = new GetCardsOfUserCommand(request);

        var user = GetTestUser();
        _userRepositoryMock.Setup(repo => repo.GetUserById(It.IsAny<int>())).ReturnsAsync(user);

        var cardsList = new List<BankCard>();
        var cardResponse = GetTestCard();
        cardsList.Add(cardResponse);
        _cardRepositoryMock.Setup(repo => repo.GetAllCardsOfUser(It.IsAny<int>())).ReturnsAsync(cardsList);

        var handler = new GetCardsOfUserHandler(_cardRepositoryMock.Object, _userRepositoryMock.Object);

        //Act
        var response = await handler.Handle(command, default);

        // Assert        
        _cardRepositoryMock.Verify(repo => repo.GetAllCardsOfUser(It.IsAny<int>()), Times.Once);
        _userRepositoryMock.Verify(repo => repo.GetUserById(It.IsAny<int>()), Times.Once);
        Assert.NotNull(response);
    }
}
