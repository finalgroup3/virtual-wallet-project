﻿using Moq;
using VirtualWallet.Domain.CardEntity;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.Common.Interfaces.Abstractions;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Cards.Requests;
using static VirtualWallet.Features.GetCardFeature;

namespace VirtualWallet.Features.UnitTests.Cards;

public class GetCardFeatureTests
{
    private readonly Mock<ICardRepository> _cardRepositoryMock;
    private readonly Mock<ILinkService> _linkService;
    public GetCardFeatureTests()
    {
        _cardRepositoryMock = new Mock<ICardRepository>();
        _linkService = new Mock<ILinkService>();
    }

    [Fact]
    public async Task Feature_ShouldThrow_WhenCardNotFound()
    {
        // Arrange
        var request = new GetCardRequest(intParam);

        var command = new GetCardCommand(request);

        var card = GetTestCard();
        _cardRepositoryMock.Setup(repo => repo.GetCardById(It.IsAny<int>())).ReturnsAsync((BankCard)null);

        var handler = new GetCardHandler(_cardRepositoryMock.Object, _linkService.Object);

        //Act and Assert
        await Assert.ThrowsAsync<CardNotFoundException>(() => handler.Handle(command, default));
    }

    [Fact]
    public async Task Feature_Should_ReturnCard_WhenRequestIsValid()
    {
        // Arrange
        var request = new GetCardRequest(intParam);

        var command = new GetCardCommand(request);

        var card = GetTestCard();
        _cardRepositoryMock.Setup(repo => repo.GetCardById(It.IsAny<int>())).ReturnsAsync(card);

        var handler = new GetCardHandler(_cardRepositoryMock.Object, _linkService.Object);

        //Act
        var response = await handler.Handle(command, default);

        // Assert
        Assert.NotNull(response);        
        _cardRepositoryMock.Verify(repo => repo.GetCardById(It.IsAny<int>()), Times.Once);
    }
}
