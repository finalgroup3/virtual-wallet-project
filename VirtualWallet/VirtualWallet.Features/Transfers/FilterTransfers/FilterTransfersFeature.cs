﻿using MediatR;
using System.Linq.Expressions;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Pagination;
using VirtualWallet.Domain.TransferEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models;
using VirtualWallet.Models.Transfer.Requests;
using VirtualWallet.Models.Transfer.Responses;


namespace VirtualWallet.Features.Transfers.FilterTransfers;

public static class FilterTransfersFeature
{
    private const string InvalidOrderByMessage = "You can only sort in \"Ascending\" or \"Descending\" order.";
    private const string InvalidSortMessage = "You can only sort the requested transfers by \"Amount\", \"Sender\", \"Recipient\" or \"Date\".";
    public record FilterTransfersCommand(FilterTransfersRequest RequestModel) : IRequest<FilterTransfersResponse>;

    public class FilterTransfersHandler : IRequestHandler<FilterTransfersCommand, FilterTransfersResponse>
    {
        private readonly ITransferRepository _transferRepository;

        public FilterTransfersHandler(ITransferRepository transferRepository)
        {
            _transferRepository = transferRepository;
        }

        public async Task<FilterTransfersResponse> Handle(FilterTransfersCommand request, CancellationToken cancellationToken)
        {
            await Task.CompletedTask;

            IQueryable<Transfer> transfers = await _transferRepository.GetTransfersQuery();

            var requestModel = request.RequestModel;

            if (requestModel.TransferedAfter != DateTime.MinValue)
            {
                transfers = transfers.Where(transfer => transfer.TransferDate > requestModel.TransferedAfter);
            }
            if (requestModel.TransferedBefore != DateTime.MinValue)
            {
                transfers = transfers.Where(transfer => transfer.TransferDate < requestModel.TransferedBefore);
            }
            if (!string.IsNullOrEmpty(requestModel.BySender))
            {
                transfers = transfers.Where(transfer => transfer.Sender.Username == requestModel.BySender);
            }
            if (!string.IsNullOrEmpty(requestModel.ByRecipient))
            {
                transfers = transfers.Where(transfer => transfer.Recipient.Username == requestModel.ByRecipient);
            }
            if (requestModel.MinimumAmount > 0)
            {
                transfers = transfers.Where(transfer => transfer.TargetAmount > requestModel.MinimumAmount);
            }
            if (requestModel.MaximumAmount > 0)
            {
                transfers = transfers.Where(transfer => transfer.TargetAmount < requestModel.MaximumAmount);
            }

            if (!transfers.Any())
            {
                var emptyResponse = PagedList<TransferResponse>
                .CreateEmpty(requestModel.Page,
                requestModel.PageSize);

                return new FilterTransfersResponse(emptyResponse);
            }

            var sortProperty = GetSortProperty(requestModel);

            if (!string.IsNullOrEmpty(requestModel.OrderBy?.ToLower()))
            {
                switch (requestModel.OrderBy)
                {
                    case "ascending":
                        transfers.OrderBy(sortProperty);
                        break;
                    case "descending":
                        transfers.OrderByDescending(sortProperty);
                        break;
                    default:
                        throw new InvalidRequestException(InvalidOrderByMessage);
                };
            }
            int page = requestModel.Page == 0 ? 1 : requestModel.Page;
            int transfersPerPage = requestModel.PageSize;

            var transferResponsesQuery = transfers
                .Select(transfer => new TransferResponse(
                        transfer.Id,
                        transfer.SenderId,
                        transfer.Sender.Username,
                        transfer.RecipientId,
                        transfer.Recipient.Username,
                        transfer.BaseAmount,
                        transfer.BaseCurrency,
                        transfer.TargetAmount,
                        transfer.TargetCurrency,
                        transfer.TotalTaken,
                        transfer.Fees
                    ));
            var response = PagedList<TransferResponse>
                .Create(transferResponsesQuery,
                page,
                transfersPerPage);

            return new FilterTransfersResponse(response);
        }
    }
    private static Expression<Func<Transfer, object>> GetSortProperty(FilterTransfersRequest request)
    {
        if (string.IsNullOrEmpty(request.SortBy))
        {
            return transfer => transfer.Id;
        }
        return request.SortBy?.ToLower() switch
        {
            "amount" => transfer => transfer.TargetAmount,
            "sender" => transfer => transfer.Sender.Username,
            "recipient" => transfer => transfer.Recipient.Username,
            "transferdate" => transfer => transfer.TransferDate,
            _ => throw new InvalidRequestException(InvalidSortMessage)
        };
    }
}
