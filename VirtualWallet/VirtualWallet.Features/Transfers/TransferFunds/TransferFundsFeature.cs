﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.TransferEntity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Transfer.Requests;
using VirtualWallet.Models.Transfer.Responses;

namespace VirtualWallet.Features.Transfers.TransferFunds;

public static class TransferFundsFeature
{
    private const string SelfTransferMessage = "You are unable to transfer funds to yourself. We would have still gotten the transfer fee if we had allowed you to though, so make sure you tell all your friends what nice guys we are.";
    private const string InsufficientFundsMessage = "Your ballance is insufficient to make this transfer.";

    public static readonly decimal CommissionPercentage = 0.005m;
    public record TransferFundsCommand(TransferFundsRequest RequestModel) : IRequest<TransferFundsResponse>;

    public class TransferFundsHandler : IRequestHandler<TransferFundsCommand, TransferFundsResponse>
    {
        private readonly IUserRepository _userRepository;
        private readonly ITransferRepository _transferRepository;
        private readonly IExchangeRateService _exchangeRateService;

        public TransferFundsHandler(
            IUserRepository userRepository, ITransferRepository transferRepository,
            IExchangeRateService exchangeRateService)
        {
            _exchangeRateService = exchangeRateService;
            _userRepository = userRepository;
            _transferRepository = transferRepository;
        }

        public async Task<TransferFundsResponse> Handle(TransferFundsCommand request, CancellationToken cancellationToken)
        {
            int senderId = request.RequestModel.SenderId;
            int recipientId = request.RequestModel.RecipientId;

            if (senderId == recipientId)
            {
                throw new InvalidRequestException(SelfTransferMessage);
            }

            if (await _userRepository.GetUserById(recipientId) is not User recipient)
            {
                throw new UserNotFoundException("Id", recipientId.ToString());
            }

            else if (!recipient.IsVerified)
            {
                throw new UnverifiedRecipinetException();
            }

            var sender = await _userRepository.GetUserById(senderId);
            if (!sender.IsVerified)
            {
                throw new UnverifiedAccountException();
            }

            decimal amount = request.RequestModel.Amount;
            // TODO: Move this to domain invariant validation
            if (sender.Wallet.Balance < amount)
            {
                throw new InvalidRequestException(InsufficientFundsMessage);
            }

            var exchangeRes = await _exchangeRateService.GetExchangeRateAsync(sender.Wallet.Currency, recipient.Wallet.Currency, amount);
            var recAmount = exchangeRes.Conversion_result;
            decimal fees = amount / 100 * CommissionPercentage;
            var totalTaken = amount + fees;

            var transfer = Transfer.CreateTransfer(
                senderId, recipientId, amount, sender.Wallet.Currency, recAmount, recipient.Wallet.Currency, totalTaken,
                fees); // ! Fees are in the sender's currency
            await _transferRepository.CreateTransfer(transfer);

            sender.AddTransferSent(transfer);
            sender.DeductFunds(totalTaken);
            recipient.AddTransferReceived(transfer);
            recipient.AddFunds(recAmount);
            await _userRepository.SaveChanges();

            return new TransferFundsResponse(
                transfer.Id, senderId, sender.Username, recipientId, recipient.Username, amount, sender.Wallet.Currency,
                recAmount, recipient.Wallet.Currency, totalTaken, fees, exchangeRes);
        }
    }
}
