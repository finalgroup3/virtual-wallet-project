﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.TransferEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models;
using VirtualWallet.Models.Transfer.Requests;

namespace VirtualWallet.Features.Transfers.GetTransfer
{
    public static class GetTransferFeature
    {
        public record GetTransferCommand(TransferRequest RequestModel) : IRequest<TransferResponse>;

        public class GetTransferHandler : IRequestHandler<GetTransferCommand, TransferResponse>
        {
            private readonly ITransferRepository _transferRepository;

            public GetTransferHandler(ITransferRepository transferRepository)
            {
                _transferRepository = transferRepository;
            }

            public async Task<TransferResponse> Handle(GetTransferCommand request, CancellationToken cancellationToken)
            {
                await Task.CompletedTask;

                var requestModel = request.RequestModel;

                if (await _transferRepository.GetTransferById(requestModel.Id) is not Transfer transfer)
                {
                    throw new TransferNotFoundException(nameof(requestModel.Id), requestModel.Id.ToString());
                }

                return new TransferResponse(
                  transfer.Id,
                    transfer.SenderId,
                    transfer.Sender.Username,
                    transfer.RecipientId,
                    transfer.Recipient.Username,
                    transfer.BaseAmount,
                    transfer.BaseCurrency,
                    transfer.TargetAmount,
                    transfer.TargetCurrency,
                    transfer.TotalTaken,
                    transfer.Fees);
            }
        }
    }
}
