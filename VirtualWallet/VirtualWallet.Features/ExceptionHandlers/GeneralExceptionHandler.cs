﻿using Microsoft.AspNetCore.Http;
using System.Text.Json;
using VirtualWallet.Features.Common.Interfaces.Abstractions;

namespace VirtualWallet.Features.ExceptionHandlers;

public class GeneralExceptionHandler : IExceptionHandler
{
    public async Task HandleAsync(HttpContext context, Exception exception)
    {
        context.Response.StatusCode = StatusCodes.Status500InternalServerError;

        var errorResponse = new
        {
            type = "errors/internal-server-error",
            title = "Internal Server Error",
            status = context.Response.StatusCode,
            detail = "An unexpected error occurred on the server.",
            instance = context.Request.Path
        };

        var errorResponseJson = JsonSerializer.Serialize(errorResponse);

        context.Response.ContentType = "application/problem+json";

        await context.Response.WriteAsync(errorResponseJson);
    }
}
