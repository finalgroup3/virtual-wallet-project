using System.Text.Json;
using Microsoft.AspNetCore.Http;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Features.Common.Interfaces.Abstractions;

namespace VirtualWallet.Features.ExceptionHandlers;

public class EntityNotFoundExceptionHandler : IExceptionHandler
{
    public async Task HandleAsync(HttpContext context, Exception exception)
    {
        var ex = (EntityNotFoundException)exception;
        var entity = ex.Entity.ToLower();

        context.Response.StatusCode = StatusCodes.Status404NotFound;

        var errorResponse = new
        {
            type = $"errors/{entity}-not-found",
            title = ex.ErrorCode,
            status = context.Response.StatusCode,
            detail = ex.Message,
            entity = ex.Entity,
            property = ex.Property,
            notFoundValue = ex.NotFoundValue,
            instance = context.Request.Path
        };

        var errorResponseJson = JsonSerializer.Serialize(errorResponse);

        context.Response.ContentType = "application/problem+json";
        await context.Response.WriteAsync(errorResponseJson);
    }
}