﻿using Microsoft.AspNetCore.Http;
using System.Text.Json;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Features.Common.Interfaces.Abstractions;

namespace VirtualWallet.Features.ExceptionHandlers;

public class DuplicateEntityExceptionHandler : IExceptionHandler
{
    public async Task HandleAsync(HttpContext context, Exception exception)
    {
        var ex = (DuplicateEntityException)exception;
        context.Response.StatusCode = StatusCodes.Status409Conflict;
        var entity = ex.Entity.ToLower();

        var errorResponse = new
        {
            type = $"errors/{entity}-exists",
            title = ex.ErrorCode,
            status = context.Response.StatusCode,
            detail = ex.Message,
            entity = ex.Entity,
            property = ex.Property,
            duplicateValue = ex.DuplicateValue,
            instance = context.Request.Path
        };

        var errorResponseJson = JsonSerializer.Serialize(errorResponse);

        context.Response.ContentType = "application/problem+json";
        await context.Response.WriteAsync(errorResponseJson);
    }
}