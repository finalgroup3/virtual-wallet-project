﻿using Microsoft.Extensions.DependencyInjection;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Features.Common.Interfaces.Abstractions;

namespace VirtualWallet.Features.ExceptionHandlers;

public class ExceptionHandlerFactory
{
    private readonly IServiceProvider _serviceProvider;

    public ExceptionHandlerFactory(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }
    public IExceptionHandler GetHandler(Exception ex)
    {
        var handlerType = ex switch
        {
            EntityNotFoundException => typeof(EntityNotFoundExceptionHandler),
            DuplicateEntityException => typeof(DuplicateEntityExceptionHandler),            
            _ => typeof(GeneralExceptionHandler)
        };

        return (IExceptionHandler)_serviceProvider.GetRequiredService(handlerType);
    }
}