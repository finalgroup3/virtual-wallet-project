﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using VirtualWallet.Models.User.Responses;

namespace VirtualWallet.Features;

public static class GetUserFeature
{
    public record GetUserCommand(GetUserRequest RequestModel) : IRequest<UserResponse>;

    public class GetUserHandler : IRequestHandler<GetUserCommand, UserResponse>
    {
        private readonly IUserRepository _userRepository;

        public GetUserHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<UserResponse> Handle(GetUserCommand request, CancellationToken cancellationToken)
        {
            await Task.CompletedTask;

            var requestModel = request.RequestModel;

            if (await _userRepository.GetUserById(requestModel.Id) is not User user)
            {
                throw new UserNotFoundException("Id", requestModel.Id.ToString());
            }

            return new UserResponse(
                user.Id,
                user.Username,
                user.Email,
                user.PhoneNumber,
                user.PhotoUrl,
                user.IsAdmin,
                user.IsBlocked,
                user.IsVerified);
        }
    }
}
