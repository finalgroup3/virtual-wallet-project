﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using VirtualWallet.Models.User.Responses;

namespace VirtualWallet.Features.Users.DemoteUser;

public static class DemoteUserFeature
{
    public record DemoteUserCommand(AdminStatusRequest RequestModel) : IRequest<AdminStatusResponse>;

    public class DemoteUserHandler : IRequestHandler<DemoteUserCommand, AdminStatusResponse>
    {
        private readonly IUserRepository _userRepository;

        public DemoteUserHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<AdminStatusResponse> Handle(DemoteUserCommand request, CancellationToken cancellationToken)
        {
            await Task.CompletedTask;

            var requestModel = request.RequestModel;

            if (await _userRepository.GetUserById(requestModel.Id) is not User user)
            {
                throw new UserNotFoundException("Id", requestModel.Id.ToString());
            }
            if (await _userRepository.GetUserById(requestModel.LoggedUserId) is User loggedUser && !loggedUser.IsAdmin)
            {
                throw new UnauthorizedOperationException();
            }
            else if (!user.IsAdmin)
            {
                string errorMessage = $"The specified user ID number '{user.Id}' is not an Admin!";
                throw new InvalidRequestException(errorMessage);
            }
            user.DemoteFromAdmin();
            await _userRepository.SaveChanges();

            return new AdminStatusResponse(
            user.Id,
            user.IsAdmin);
        }
    }
}
