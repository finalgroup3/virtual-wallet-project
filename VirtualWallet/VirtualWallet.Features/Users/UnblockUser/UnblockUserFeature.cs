﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using VirtualWallet.Models.User.Responses;

namespace VirtualWallet.Features.Users.UnblockUser;

public static class UnblockUserFeature
{
    public record UnblockUserCommand(BlockStatusRequest RequestModel) : IRequest<BlockStatusResponse>;

    public class UnblockUserHandler : IRequestHandler<UnblockUserCommand, BlockStatusResponse>
    {
        private readonly IUserRepository _userRepository;

        public UnblockUserHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<BlockStatusResponse> Handle(UnblockUserCommand request, CancellationToken cancellationToken)
        {
            var requestModel = request.RequestModel;

            if (await _userRepository.GetUserById(requestModel.LoggedUserId) is not User loggedUser)
            {
                throw new UserNotFoundException("Id", requestModel.LoggedUserId.ToString());
            }

            if (!loggedUser.IsAdmin)
            {
                throw new UnauthorizedOperationException();
            }

            if (await _userRepository.GetUserById(requestModel.Id) is not User user)
            {
                throw new UserNotFoundException("Id", requestModel.Id.ToString());
            }
            if (!user.IsBlocked)
            {
                string errorMessage = $"The specified user ID number '{user.Id}' has not been blocked!";
                throw new InvalidRequestException(errorMessage);
            }

            user.Unblock();
            await _userRepository.SaveChanges();

            return new BlockStatusResponse(
            user.Id,
            user.IsBlocked);
        }
    }
}
