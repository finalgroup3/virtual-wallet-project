﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using VirtualWallet.Models.User.Responses;

namespace VirtualWallet.Features.Users.ChangePhoneNumber;

public static class ChangePhoneNumberFeature
{
	public record ChangePhoneNumberCommand(ChangePhoneNumberRequest Request) : IRequest<ChangePhoneNumberResponse>;

	public sealed class ChangePhoneNumberHandler : IRequestHandler<ChangePhoneNumberCommand, ChangePhoneNumberResponse>
	{
		private readonly IUserRepository _userRepository;

		public ChangePhoneNumberHandler(IUserRepository userRepository)
		{
			_userRepository = userRepository;
		}

		public async Task<ChangePhoneNumberResponse> Handle(ChangePhoneNumberCommand command, CancellationToken cancellationToken)
		{
			var newPhoneNumber = command.Request.PhoneNumber;
			if (await _userRepository.PhoneNumberExistsAsync(newPhoneNumber))
			{
				throw new DuplicateUserException(nameof(newPhoneNumber), newPhoneNumber);
			}

			var user = await _userRepository.GetUserById(command.Request.LoggedUserId);
			user.SetPhoneNumber(newPhoneNumber);	
			
			await _userRepository.Update(user);

			return new ChangePhoneNumberResponse(user.Username, user.PhoneNumber);
		}
	}
}
