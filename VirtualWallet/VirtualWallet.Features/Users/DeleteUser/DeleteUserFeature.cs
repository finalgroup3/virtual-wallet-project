﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;

namespace VirtualWallet.Features.Users.DeleteUser;

public static class DeleteUserFeature
{
    public record DeleteUserCommand(DeleteUserRequest RequestModel) : IRequest<Unit>;

    public class DeleteUserHandler : IRequestHandler<DeleteUserCommand, Unit>
    {
        private readonly IUserRepository _userRepository;

        public DeleteUserHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var requestModel = request.RequestModel;

            if (await _userRepository.GetUserById((int)requestModel.LoggedId) is not User user)
            {
                throw new UserNotFoundException("Id", requestModel.LoggedId.ToString());
            }

            user.DeleteUser();
            await _userRepository.SaveChanges();
            return Unit.Value;
        }
    }
}
