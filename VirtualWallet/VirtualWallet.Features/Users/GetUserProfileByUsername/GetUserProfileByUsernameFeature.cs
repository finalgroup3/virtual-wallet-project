﻿using MediatR;
using VirtualWallet.Api.Constants;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Abstractions;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using VirtualWallet.Models.User.Responses;

namespace VirtualWallet.Features.Users.GetUserProfileByUsername;

public static class GetUserProfileByUsernameFeature
{
    public record GetUserProfileByUsernameCommand(GetUserByUsernameRequest RequestModel) : IRequest<UserProfileResponse>;

    public class GetUserProfileByUsernameHandler : IRequestHandler<GetUserProfileByUsernameCommand, UserProfileResponse>
    {
        private readonly IUserRepository _userRepository;
        private readonly ILinkService _linkService;

        public GetUserProfileByUsernameHandler(IUserRepository userRepository, ILinkService linkService)
        {
            _userRepository = userRepository;
            _linkService = linkService;
        }

        public async Task<UserProfileResponse> Handle(GetUserProfileByUsernameCommand request, CancellationToken cancellationToken)
        {
            var requestModel = request.RequestModel;

            if (await _userRepository.GetUserByUsername(requestModel.Username) is not User user)
            {
                throw new UserNotFoundException(nameof(requestModel.Username), requestModel.Username);
            }

            var userBankCards = GetUserBankCards(user);
            var userTransfersSent = GetUserTransfersSent(user);
            var userTransfersReceived = GetUserTransfersReceived(user);

            var wallet = new ProfileWallet(user.Wallet.Id, new WalletBalance(user.Wallet.Balance, user.Wallet.Currency));

            var response = new UserProfileResponse(
                user.Id,
                user.Username,
                user.Email,
                user.PhoneNumber,
                user.PhotoUrl!,
                user.IsAdmin,
                user.IsBlocked,
                user.IsVerified,
                userBankCards,
                userTransfersReceived,
                userTransfersSent,
                wallet);


            await GetLinksForUser(response);

            return response;
        }

        private async Task GetLinksForUser(UserProfileResponse response)
        {
            await Task.CompletedTask;

            response.Links.Add(_linkService.Generate(
                HttpMethodNames.GetUserProfile,
                new { id = response.Id },
                "self",
                HttpMethodNames.Get));

            response.Links.Add(_linkService.Generate(
                HttpMethodNames.DeleteUser,
                new { id = response.Id },
                "delete_user",
                HttpMethodNames.Delete));

            response.AdminLinks.Add(_linkService.Generate(
                HttpMethodNames.BlockUser,
                new { id = response.Id },
                "block_user",
                HttpMethodNames.Put));

            response.AdminLinks.Add(_linkService.Generate(
                HttpMethodNames.UnblockUser,
                new { id = response.Id },
                "unblock_user",
                HttpMethodNames.Put));

            response.AdminLinks.Add(_linkService.Generate(
                HttpMethodNames.PromoteUser,
                new { id = response.Id },
                "promote_user",
                HttpMethodNames.Put));

            response.AdminLinks.Add(_linkService.Generate(
                HttpMethodNames.DemoteUser,
                new { id = response.Id },
                "demote_user",
                HttpMethodNames.Put));

            response.Links.Add(_linkService.Generate(
                HttpMethodNames.Logout,
                null,
                "logout",
                HttpMethodNames.Post));
        }

        private static List<UserBankCards> GetUserBankCards(User user)
        {
            return user.BankCards.Select(bc => new UserBankCards(
                bc.Number,
                bc.ExpirationDate.ToString("MM/yy"),
                bc.Name,
                bc.Type)).ToList();
        }

        private static List<UserTransfersReceived> GetUserTransfersReceived(User user)
        {
            return user.TransfersReceived.Select(rt => new UserTransfersReceived(
                rt.Id,
                rt.Sender.Username,
                rt.TargetAmount,
                rt.TargetCurrency,
                rt.BaseAmount,
                rt.BaseCurrency,
                rt.TransferDate.ToString())).ToList();
        }

        private static List<UserTransfersSent> GetUserTransfersSent(User user)
        {
            return user.TransfersSent.Select(st => new UserTransfersSent(
                st.Id,
                st.Recipient.Username,
                st.TargetAmount,
                st.TargetCurrency,
                st.BaseAmount,
                st.BaseCurrency,
                st.TransferDate.ToString())).ToList();
        }
    }
}
