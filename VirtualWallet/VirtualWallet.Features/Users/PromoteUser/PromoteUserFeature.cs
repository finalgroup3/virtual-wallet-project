﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using VirtualWallet.Models.User.Responses;

namespace VirtualWallet.Features.Users.PromoteUser;

public class PromoteUserFeature
{
    public record PromoteUserCommand(AdminStatusRequest RequestModel) : IRequest<AdminStatusResponse>;

    public class PromoteUserHandler : IRequestHandler<PromoteUserCommand, AdminStatusResponse>
    {
        private readonly IUserRepository _userRepository;

        public PromoteUserHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<AdminStatusResponse> Handle(PromoteUserCommand request, CancellationToken cancellationToken)
        {
            var requestModel = request.RequestModel;

            if (await _userRepository.GetUserById(requestModel.Id) is not User user)
            {
                throw new UserNotFoundException("Id", requestModel.Id.ToString());
            }

            if (await _userRepository.GetUserById(requestModel.LoggedUserId) is User loggedUser && !loggedUser.IsAdmin)
            {
                throw new UnauthorizedOperationException();
            }

            if (user.IsAdmin)
            {
                string errorMessage = $"The specified user ID number '{user.Id}' is already an Admin!";
                throw new InvalidRequestException(errorMessage);
            }

            user.PromoteToAdmin();
            await _userRepository.SaveChanges();

            return new AdminStatusResponse(
            user.Id,
            user.IsAdmin);
        }
    }
}