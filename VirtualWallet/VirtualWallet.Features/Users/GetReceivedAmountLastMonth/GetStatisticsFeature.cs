﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using VirtualWallet.Models.User.Responses;

namespace VirtualWallet.Features.Users.GetReceivedAmountLastMonth;

public static class GetStatisticsFeature
{
    public record GetReceivedAmountLastMonthQuery(GetStatisticsRequest Request) : IRequest<GetStatisticsResponse>;

    public class GetStatisticsFeatureHandler : IRequestHandler<GetReceivedAmountLastMonthQuery, GetStatisticsResponse>
    {
        private readonly IUserRepository _userRepository;
        private readonly IExchangeRateService _exchangeRateService;

        public GetStatisticsFeatureHandler(IExchangeRateService exchangeRateService, IUserRepository userRepository)
        {
            _exchangeRateService = exchangeRateService;
            _userRepository = userRepository;
        }

        public async Task<GetStatisticsResponse> Handle(GetReceivedAmountLastMonthQuery request, CancellationToken cancellationToken)
        {
            var model = request.Request;
            if (await _userRepository.GetUserById(model.UserId) is not User user)
            {
                throw new UserNotFoundException("Id", model.UserId.ToString());
            }

            var transfersReceived = user.TransfersReceived.Where(t => t.TransferDate >= model.StartDate && t.TransferDate <= model.EndDate).ToList();
            var transfersSent = user.TransfersSent.Where(t => t.TransferDate >= model.StartDate && t.TransferDate <= model.EndDate).ToList();
            decimal amountSent = transfersSent.Select(t => t.BaseAmount).Sum();

            var transfersSentCount = transfersSent.Count;
            var transfersReceivedCount = transfersReceived.Count;

            if (transfersSentCount == 0 && transfersReceivedCount == 0)
            {
                return new GetStatisticsResponse(user.Id, model.StartDate, model.EndDate,
                    user.Wallet!.Currency, 0, 0, 0, 0);
            }

            Dictionary<string, decimal> transfersByCurr = new();
            foreach (var transfer in transfersReceived)
            {
                if (!transfersByCurr.ContainsKey(transfer.TargetCurrency))
                {
                    transfersByCurr[transfer.TargetCurrency] = 0;
                }

                transfersByCurr[transfer.TargetCurrency] += transfer.TargetAmount;
            }

            decimal amountReceived = 0;
            var userCurr = user.Wallet!.Currency;
            foreach (var (curr, amount) in transfersByCurr)
            {
                var response = await _exchangeRateService.GetExchangeRateAsync(curr, userCurr, amount);
                amountReceived += response.Conversion_result;
            }

            return new GetStatisticsResponse(user.Id, model.StartDate, model.EndDate, userCurr, amountSent, amountReceived, transfersSentCount, transfersReceivedCount);
        }
    }
}
