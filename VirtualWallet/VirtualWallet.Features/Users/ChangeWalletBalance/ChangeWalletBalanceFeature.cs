﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Wallets.Requests;
using VirtualWallet.Models.Wallets.Responses;

namespace VirtualWallet.Features.Users.ChangeWalletBalanceFeature
{
    public static class ChangeWalletBalanceFeature
	{
		public record ChangeWalletBalanceCommand(ChangeWalletBalanceRequest Request) : IRequest<ChangeWalletBalanceResponse>;

		public sealed class ChangeWalletBalanceHandler : IRequestHandler<ChangeWalletBalanceCommand, ChangeWalletBalanceResponse>
		{
			private readonly IUserRepository _userRepository;
			private readonly IWalletRepository _walletRepository;

			public ChangeWalletBalanceHandler(IUserRepository userRepository, IWalletRepository walletRepository)
			{
				_userRepository = userRepository;
				_walletRepository = walletRepository;
			}

			public async Task<ChangeWalletBalanceResponse> Handle(ChangeWalletBalanceCommand command, CancellationToken cancellationToken)
			{
				var wallet = await _walletRepository.GetWalletByUserId(command.Request.UserId)
					?? throw new InvalidRequestException("Wallet doesn't exists");

                wallet.ChangeMoney(command.Request.Balance);
				await _walletRepository.UpdateAsync(wallet);

				return new ChangeWalletBalanceResponse(true, "You have successfully updated your balance.");
			}
		}
	}
}
