﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using VirtualWallet.Models.User.Responses;

namespace VirtualWallet.Features.Users.ChangeEmail;

public static class ChangeEmailFeature
{
    public record ChangeEmailCommand(ChangeEmailRequest Request, string Token) : IRequest<ChangeEmailResponse>;

    public sealed class ChangeEmailHandler : IRequestHandler<ChangeEmailCommand, ChangeEmailResponse>
    {
        private readonly IUserRepository _userRepository;

        public ChangeEmailHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<ChangeEmailResponse> Handle(ChangeEmailCommand command, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetUserByEmailToken(Guid.Parse(command.Token), cancellationToken)
                ?? throw new InvalidTokenException();

            var newEmail = command.Request.Email;
            if (await _userRepository.EmailExistsAsync(newEmail))
            {
                throw new DuplicateUserException(nameof(newEmail), newEmail);
            }

            user.SetEmail(newEmail);
            user.ResetChangeEmailToken();
            await _userRepository.Update(user);

            return new ChangeEmailResponse(true, "You have successfully updated your e-mail address. Please login again with your new credentials.");
        }
    }
}
