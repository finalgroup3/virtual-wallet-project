﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Authentication.Requests;
using VirtualWallet.Models.User.Responses;

namespace VirtualWallet.Features.Users.ForgotPassword;

public static class ForgotPasswordFeature
{
    public record ForgotPasswordCommand(ForgotPasswordRequest Request) : IRequest<ForgotPasswordResponse>;

    public class ForgotPasswordHandler : IRequestHandler<ForgotPasswordCommand, ForgotPasswordResponse>
    {
        private const string PasswordChangeURL = "http://localhost:3006/change-password?token=";

        private readonly IUserRepository _userRepository;

        public ForgotPasswordHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<ForgotPasswordResponse> Handle(ForgotPasswordCommand request, CancellationToken cancellationToken)
        {
            var model = request.Request;

            var user = await _userRepository.GetUserByEmail(model.Email) 
                ?? throw new UserNotFoundException(nameof(model.Email), model.Email);

            var token = user.GenerateResetPasswordToken();

            await _userRepository.Update(user);

            var link = PasswordChangeURL + token;

            return new ForgotPasswordResponse(user.Email, link, token);
        }
    }
}
