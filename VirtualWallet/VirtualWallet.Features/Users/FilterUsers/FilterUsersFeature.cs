﻿using MediatR;
using System.Linq.Expressions;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Pagination;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using VirtualWallet.Models.User.Responses;

namespace VirtualWallet.Features.Users.FilterUsers;

public static class FilterUsersFeature
{
    private const string InvalidOrderByMessage = "You can only sort in \"Ascending\" or \"Descending\" order.";
    private const string InvalidSortMessage = "You can only sort the requested users by \"Username\", \"E-mail\" or \"Phone Number\".";

    public record FilterUsersCommand(FilterUsersRequest RequestModel) : IRequest<UserFilterResponse>;

    public class FilterUsersHandler : IRequestHandler<FilterUsersCommand, UserFilterResponse>
    {
        private readonly IUserRepository _userRepository;

        public FilterUsersHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<UserFilterResponse> Handle(FilterUsersCommand request, CancellationToken cancellationToken)
        {
            await Task.CompletedTask;

            IQueryable<User> users = await _userRepository.GetUsersQuery();

            var requestModel = request.RequestModel;

            if (!string.IsNullOrEmpty(requestModel.SearchTerm))
            {
                users = users.Where(user =>
                    user.Username.Contains(requestModel.SearchTerm) ||
                    user.Email.Contains(requestModel.SearchTerm) ||
                    user.PhoneNumber.Contains(requestModel.SearchTerm)
                );
            }

            if (!users.Any())
            {
                var emptyResponse = PagedList<UserResponse>
                .CreateEmpty(requestModel.Page,
                requestModel.PageSize);

                return new UserFilterResponse(emptyResponse);
            }

            var sortProperty = GetSortProperty(requestModel);

            if (!string.IsNullOrEmpty(requestModel.OrderBy?.ToLower()))
            {
                switch (requestModel.OrderBy)
                {
                    case "ascending":
                        users.OrderBy(sortProperty);
                        break;
                    case "descending":
                        users.OrderByDescending(sortProperty);
                        break;
                    default:
                        throw new InvalidRequestException(InvalidOrderByMessage);
                };
            }
            else
            {
                users.OrderBy(sortProperty);
            }

            int page = requestModel.Page == 0 ? 1 : requestModel.Page;
            int pageSize = requestModel.PageSize == 0 ? 5 : requestModel.PageSize;

            var userResponsesQuery = users
                .Select(user => new UserResponse(
                    user.Id,
                    user.Username,
                    user.Email,
                    user.PhoneNumber,
                    user.PhotoUrl,
                    user.IsAdmin,
                    user.IsBlocked,
                    user.IsVerified));

            var response = PagedList<UserResponse>
                .Create(userResponsesQuery,
                page,
                pageSize);

            return new UserFilterResponse(response);
        }
    }
    private static Expression<Func<User, object>> GetSortProperty(FilterUsersRequest request)
    {
        if (string.IsNullOrEmpty(request.SortBy))
        {
            return user => user.Username;
        }

        return request.SortBy.ToLower() switch
        {
            "username" => user => user.Username,
            "email" => user => user.Email,
            "phonenumber" => user => user.PhoneNumber,
            _ => throw new InvalidRequestException(InvalidSortMessage)
        };
    }
}
