﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models;

namespace VirtualWallet.Features;

public static class GetWalletFeature
{
    public record GetWalletQuery(GetWalletRequest RequestModel) : IRequest<CreateWalletResponse>;

    public class GetWalletFeatureHandler : IRequestHandler<GetWalletQuery, CreateWalletResponse>
    {
        private readonly IUserRepository _userRepository;
        private readonly IWalletRepository _walletRepository;

        public GetWalletFeatureHandler(IWalletRepository walletRepository, IUserRepository userRepository)
        {
            _walletRepository = walletRepository;
            _userRepository = userRepository;
        }

        public async Task<CreateWalletResponse> Handle(GetWalletQuery request, CancellationToken cancellationToken)
        {
            var userId = int.Parse(request.RequestModel.UserId.ToString());
            var user = await _userRepository.GetUserById(userId) ?? throw new UserNotFoundException("Id", userId.ToString());
            if (user.Wallet is null)
            {
                throw new InvalidRequestException("Wallet doesn't exists");
            }
            var wallet = await _walletRepository.GetWalletByUserId(request.RequestModel.UserId);
            return new CreateWalletResponse(
                wallet!.Id,
                wallet.Currency,
                wallet.Balance,
                userId);
        }
    }
}