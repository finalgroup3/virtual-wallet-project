﻿using MediatR;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Responses;

namespace VirtualWallet.Features;

public static class GetAllUsersFeature
{
    public record GetAllUsersQuery() : IRequest<List<UserResponse>>;

    public class GetAllUsersFeatureHandler : IRequestHandler<GetAllUsersQuery, List<UserResponse>>
    {
        private readonly IUserRepository _userRepository;

        public GetAllUsersFeatureHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<List<UserResponse>> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
        {
            var userList = await _userRepository.GetAllUsers(cancellationToken);

            var response = userList.Select(u => new UserResponse(
                u.Id,
                u.Username,
                u.Email,
                u.PhoneNumber,
                u.PhotoUrl,
                u.IsAdmin,
                u.IsBlocked,
                u.IsVerified))
                .OrderBy(user=>user.Username)
                .ToList();

            return response;
        }
    }
}
