﻿using MediatR;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.User.Requests;
using VirtualWallet.Models.User.Responses;

namespace VirtualWallet.Features.Users.ChangePhoneNumber;

public static class ChangePictureUrlFeature
{
    public record ChangePictureUrlCommand(ChangePictureUrlRequest Request) : IRequest<ChangePictureUrlResponse>;

    public sealed class ChangePictureUrlHandler : IRequestHandler<ChangePictureUrlCommand, ChangePictureUrlResponse>
    {
        private readonly IUserRepository _userRepository;

        public ChangePictureUrlHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<ChangePictureUrlResponse> Handle(ChangePictureUrlCommand command, CancellationToken cancellationToken)
        {
            var newPictureUrl = command.Request.NewPictureUrl;           

            var user = await _userRepository.GetUserById(command.Request.LoggedUserId);
            user.SetPhotoUrl(newPictureUrl);

            await _userRepository.Update(user);

            return new ChangePictureUrlResponse(user.Username, user.PhotoUrl);
        }
    }
}
