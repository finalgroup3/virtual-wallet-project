﻿namespace VirtualWallet.Api.Constants;

public static class EmailMessages
{
    public const string RegistrationSubject = "Welcome to Virtual Wallet";
    public const string ResetPasswordSubject = "Reset your password";
	public const string ChangeEmailSubject = "Change your E-mail";

	public const string ResetPasswordBody = 
        @"<html>
        <head>
            <style>
                body {
                    font-family: Arial, sans-serif;
                    line-height: 1.6;
                    max-width: 600px;
                    margin: 0 auto;
                    padding: 20px;
                    background-color: #f7f7f7;
                }
                h1 {
                    color: #007bff;
                }
                p {
                    color: #444;
                }
                a {
                    color: #007bff;
                    text-decoration: none;
                }
            </style>
        </head>
        <body>
            <h1>Forgotten Password Reset</h1>
            <p>
                You have requested to change your password. To continue with the reset, please click the following link:
                <br>
                <a href=""[link]"">Reset your Password</a>
            </p>
            <p>If you did request this email, please change your password immediately.</p>
            <p>Thank you,</p>
            <p>The Virtual Wallet Team</p>
        </body>
        </html>";

    public const string RegistrationBody =
        @"<html>
        <head>
            <style>
                body {
                    font-family: Arial, sans-serif;
                    line-height: 1.6;
                    max-width: 600px;
                    margin: 0 auto;
                    padding: 20px;
                    background-color: #f7f7f7;
                }
                h1 {
                    color: #007bff;
                }
                p {
                    color: #444;
                }
                a {
                    color: #007bff;
                    text-decoration: none;
                }
            </style>
        </head>
        <body>
            <h1>Thank you for registering with Virtual Wallet!</h1>
            <p>
                You have successfully registered to Virtual Wallet. To complete your registration, please click the following link to verify your email:
                <br>
                <a href=""[link]"">Verify Email</a>
            </p>
            <p>If you did not sign up for an account, you can safely ignore this email.</p>
            <p>Thank you,</p>
            <p>The Virtual Wallet Team</p>
        </body>
        </html>";

	public const string ChangeEmailBody =
        @"<html>
        <head>
            <style>
                body {
                    font-family: Arial, sans-serif;
                    line-height: 1.6;
                    max-width: 600px;
                    margin: 0 auto;
                    padding: 20px;
                    background-color: #f7f7f7;
                }
                h1 {
                    color: #007bff;
                }
                p {
                    color: #444;
                }
                a {
                    color: #007bff;
                    text-decoration: none;
                }
            </style>
        </head>
        <body>
            <h1>Changing your E-mail</h1>
            <p>
                You have requested to change your E-mail. To continue this process, please click the following link:
                <br>
                <a href=""[link]"">Change your E-mail</a>
            </p>
            <p>If you did request this change, please update your login information immediately.</p>
            <p>Thank you,</p>
            <p>The Virtual Wallet Team</p>
        </body>
        </html>";
}
