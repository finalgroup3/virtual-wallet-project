﻿namespace VirtualWallet.Features.Constants;

public static class ApiRoutes
{
    public const string Base = "api/virtual-wallet";

    // Authentication
    public const string AuthBase = Base + "/auth";
    public const string Register = "register";
    public const string Login = "login";
    public const string Logout = "logout";
    public const string Verify = "verify-email";
    public const string ForgotPassword = "forgot-password";


    // Users
    public const string UserBase = Base + "/users";
    public const string IdIdentifier = "{id}";
    public const string UsernameIdentifier = "{username}";
    public const string FilterUsers = "filter";
    public const string InitiateEmailChange = "initiate-email-change";
    public const string ChangeEmail = "change-email";
    public const string ChangePhoneNumber = "change-phone-number";
    public const string ChangePictureUrl = "change-picture-url";
    public const string BlockUser = "block/" + IdIdentifier;
    public const string UnblockUser = "unblock/" + IdIdentifier;
    public const string PromoteUser = "promote/" + IdIdentifier;
    public const string DemoteUser = "demote/" + IdIdentifier;
    public const string DeleteUser = "delete";
    public const string GetUserProfile = IdIdentifier + "/profile";
    public const string GetUserProfileByUsername = "profile/" + UsernameIdentifier;
    public const string ResetPassword = "reset-password";
    public const string GetAllUsers = "all";
    public const string GetStatistiscs = IdIdentifier + "/statistics";

    // Cards
    public const string CardBase = Base + "/cards";
    public const string NumberIdentifier = "{id}";
    public const string CardsOfUser = "api/virtual-wallet/users/{userId}/cards";

    //Transfers
    public const string TransferBase = Base + "/transfers";
    public const string TransferIdentifier = "{id}";
    public const string FilterTransfers = "filter";
    public const string MakeTransfer = TransferBase + "/make-transfer";
    public const string GetTransfer = TransferBase + TransferIdentifier;
    public const string GetSentTransfers = TransferBase + "/sent";
    public const string GetReceivedTransfers = TransferBase + "/received";

    //Wallet
    public const string ChangeWalletBalance = "change-balance";
    public const string ChangeWalletCurrency = "change-currency";

}
