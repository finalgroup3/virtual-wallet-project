﻿namespace VirtualWallet.Api.Constants;

public static class HttpMethodNames
{
    // AUTH
    public const string Register = "Register";
    public const string Login = "Login";
    public const string Logout = "Logout";
    public const string ForgotPassword = "ForgotPassword";
    public const string ResetPassword = "ResetPassword";
    public const string VerifyEmail = "VerifyEmail";

    // USER
    public const string GetUser = "GetUserById";
    public const string FilterUsers = "FilterUsers";
    public const string InitiateEmailChange = "InitiateEmailChange";
    public const string ChangeEmail = "ChangeEmail";
    public const string ChangePhoneNumber = "ChangePhoneNumber";
    public const string ChangePictureUrl = "ChangePictureUrl";
    public const string DeleteUser = "DeleteUser";
    public const string BlockUser = "BlockUser";
    public const string UnblockUser = "UnblockUser";
    public const string PromoteUser = "PromoteUser";
    public const string DemoteUser = "DemoteUser";
    public const string GetUserProfile = "GetUserProfile";
    public const string GetUserProfileByUsername = "GetUserProfileByUsername";
    public const string GetAllUsers = "GetAllUsers";
    public const string GetStatistics = "GetStatistics";

    //CARD
    public const string CreateCard = "CreateCard";
    public const string GetCard = "GetCardById";
    public const string GetCardsOfUser = "GetCardsByUserId";
    public const string DeleteCard = "DeleteCard";

    // REST
    public const string Get = "GET";
    public const string Post = "POST";
    public const string Delete = "DELETE";
    public const string Put = "PUT";

    //TRANSFER
    public const string TransferFunds = "TransferFunds";
    public const string FilterTransfers = "FilterTransfers";
    public const string Transfer = "GetTransferById";

    //WALLET
    public const string ChangeWalletBalance = "ChangeWalletBalance";
    public const string ChangeWalletCurrency = "ChangeWalletCurrency";
}
