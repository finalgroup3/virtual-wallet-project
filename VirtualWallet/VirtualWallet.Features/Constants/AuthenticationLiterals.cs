﻿namespace VirtualWallet.Features.Constants;

public static class AuthenticationLiterals
{
    public const string Success = "success";
    public const string LoginSuccess = "Authentication successfull";
    public const string LogoutSuccess = "Logged out successfully";
    public const string VerifySuccess = "User verified successfully.";
    public const string VerifyFailed = "User verification failed.";
    public const string RegisterSuccess = "User registered successfull.";
    public const string TokenType = "bearer";
    public static string GetEmailVerificationLink(Guid token)
    {
        return $"http://localhost:3006/verify-email?token={token}";
    }
}
