﻿namespace VirtualWallet.Features.Constants;

public static class Validation
{
    public static readonly string Password = "Password";
    public static readonly int PasswordMinLength = 6;
    public static readonly int PasswordMaxLength = 20;

    public static readonly string PhoneNumber = "PhoneNumber";
    public static readonly int PhoneNumberLength = 10;

    public static readonly string Email = "Email";

    public static readonly string Username = "Username";
    public const int UsernameMinLength = 2;
    public const int UsernameMaxLength = 20;

	public const string PasswordRegex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,20}$";
    public const string PhoneNumberRegex = @"^[0-9]{10}$";
    public const string CardNumberRegex = @"^[0-9]{16}$";
	public const string ExpiryRegex = @"^(0?[1-9]|1[0-2])/(2[3-9]|3[0-9]|40)$";
	public const string CVVRegex = @"^[0-9]{3}$";
	public const string ExpirationDateRegex = @"^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})$";
    public const string EmailRegex = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";

    public static string ErrorRequired(string field) => $"{field} is required";

    public static string ErrorLength(string field, int minLength, int maxLength) => $"{field} must be at least {minLength} and no more than {maxLength} characters long";

    public static string ErrorFormat(string field) => $"Invalid {field}";
	public static string CardNumberLengthError() => $"{CardNumber} must be {CardNumberLength} digits long";
	public static string CardHolderLengthError() => $"{CardHolder} must be between {CardHolderMinLength} and {CardHolderMaxLength} symbols long";
	public static string ExpiryLengthError() => $"{Expiry} must be {ExpiryLength} digits long";
	public static string CVVLengthError() => $"{CVV} must be {CVVLength} digits long";
    public static string PhoneNumberDigitError() => "Invalid phone number. Must be all digits.";
    public static string PasswordMatchError() => "Passwords must match";
    public static string PhoneNumberLengthError() => $"{PhoneNumber} must be {PhoneNumberLength} digits long";
	public static string PasswordFormatError() => $"{Password} must contain at least one uppercase letter, one lowercase letter and one digit";
	public static string CardNumberFormatError() => $"{CardNumber} must contain exactly 16 digits";
	public static string ExpiryFormatError() => $"{Expiry} must be between the current moment and 20 years from now";
	public static string CVVFormatError() => $"{CVV} must contain exactly 3 digits";

	public static readonly string CVV = "CVV";
	public static readonly int CVVLength = 3;

	public static readonly string CardNumber = "CardNumber";
	public static readonly int CardNumberLength = 16;

	public static readonly string Expiry = "Expiry";
	public static readonly int ExpiryLength = 5;

	public static readonly string CardHolder = "CardHolder";
	public static readonly int CardHolderMinLength = 5;
	public static readonly int CardHolderMaxLength = 20;

	public const string Amount = "Amount";
    public const string RecipientUsername = "RecipientUsername";
    public const string SenderUsername = "SenderUsername";
    public const string VerificationToken = "VerificationToken";
}
