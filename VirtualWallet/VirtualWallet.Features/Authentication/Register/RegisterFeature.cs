using MediatR;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Features.Constants;
using VirtualWallet.Models;
using VirtualWallet.Models.Authentication.Responses;

namespace VirtualWallet.Features.Authentication.Register;

public static class RegisterFeature
{
    public record RegisterCommand(RegisterRequest Request) : IRequest<RegisterSuccessResponse>;

    public sealed class RegisterFeatureHandler : IRequestHandler<RegisterCommand, RegisterSuccessResponse>
    {
        private const string DefaultPhotoUrl = "https://i.imgur.com/R7vnue3.jpeg";
        private readonly IUserRepository _userRepository;

        public RegisterFeatureHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<RegisterSuccessResponse> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {

            var requestModel = request.Request;

            var hashedPassword = BCrypt.Net.BCrypt.HashPassword(requestModel.Password);
            // TODO: Combine these checks into one method
            string email = requestModel.Email;
            if (await _userRepository.EmailExistsAsync(email))
            {
                throw new DuplicateUserException(nameof(email), requestModel.Email);
            }

			string username = requestModel.Username;
			if (await _userRepository.UsernameExistsAsync(username))
			{
				throw new DuplicateUserException(nameof(username), requestModel.Username);
			}

            string phoneNumber = requestModel.PhoneNumber;

            if (await _userRepository.PhoneNumberExistsAsync(phoneNumber))
            {
                throw new DuplicateUserException(nameof(phoneNumber), requestModel.PhoneNumber);
            }

            var user = User.CreateUser(
                username,
                hashedPassword,
                email,
                requestModel.PhoneNumber);
            user.SetPhotoUrl(DefaultPhotoUrl);

            await _userRepository.AddAsync(user);

            user.CreateWallet("EUR");

            await _userRepository.SaveChanges();

            var verificationLink = AuthenticationLiterals.GetEmailVerificationLink(user.VerificationToken);

            return new RegisterSuccessResponse(
                               true,
                               AuthenticationLiterals.RegisterSuccess,
                               new Data(
                               user.Id,
                               user.Email,
                               user.Username,
                               user.VerificationToken.ToString(),
                               verificationLink));
        }
    }
}