﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Authentication.Requests;
using VirtualWallet.Models.Authentication.Responses;

namespace VirtualWallet.Features.Authentication.InitiateEmailChange;

public static class InitiateEmailChangeFeature
{
	

    public record InitiateEmailChangeCommand(InitiateEmailChangeRequest Request) : IRequest<InitiateEmailChangeResponse>;

	public class InitiateEmailChangeHandler : IRequestHandler<InitiateEmailChangeCommand, InitiateEmailChangeResponse>
	{
        private const string EmailChangeURL = "http://localhost:3006/update-email?token=";
        private readonly IUserRepository _userRepository;

		public InitiateEmailChangeHandler(IUserRepository userRepository)
		{
			_userRepository = userRepository;
		}

		public async Task<InitiateEmailChangeResponse> Handle(InitiateEmailChangeCommand request, CancellationToken cancellationToken)
		{
			var requestModel = request.Request;

			var user = await _userRepository.GetUserByEmail(requestModel.Email) 
				?? throw new UserNotFoundException(nameof(requestModel.Email), requestModel.Email);

			if (user.Id != requestModel.LoggedId)
			{
				throw new UnauthorizedOperationException();
			}

			var token = user.GenerateChangeEmailToken();

			await _userRepository.Update(user);

			var link = EmailChangeURL + token;

			return new InitiateEmailChangeResponse(user.Email, link, token);
		}
	}
}
