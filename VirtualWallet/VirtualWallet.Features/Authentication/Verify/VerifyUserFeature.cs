﻿using MediatR;
using VirtualWallet.Api.Constants;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Features.Common.Interfaces.Abstractions;
using VirtualWallet.Features.Common.Interfaces.Authentication;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Features.Constants;
using VirtualWallet.Models.Authentication.Responses;

namespace VirtualWallet.Features.Authentication.Verify;

public static class VerifyUserFeature
{
    public record VerifyUserCommand(string Token) : IRequest<VerificationResponse>;

    public class VerifyUserFeatureHandler : IRequestHandler<VerifyUserCommand, VerificationResponse>
    {
        private readonly IUserRepository _userRepository;
        private readonly ILinkService _linkService;

        public VerifyUserFeatureHandler(IUserRepository userRepository, IJwtProvider jwtProvider, ILinkService linkService)
        {
            _linkService = linkService;
            _userRepository = userRepository;
        }

        public async Task<VerificationResponse> Handle(VerifyUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetUserByToken(Guid.Parse(request.Token), cancellationToken) ?? throw new InvalidTokenException();

            if (user.IsVerified)
            {
                throw new InvalidTokenException();
            }

            user.Verify();

            await _userRepository.Update(user);

            var response = new VerificationResponse(
                true,
                AuthenticationLiterals.VerifySuccess);

            await AddLinksForUser(response);

            return response;
        }

        private async Task AddLinksForUser(VerificationResponse response)
        {
            await Task.CompletedTask;

            response.Links.Add(
                _linkService.Generate(
                    HttpMethodNames.Login,
                    null,
                    "login",
                    HttpMethodNames.Post));
        }
    }
}
