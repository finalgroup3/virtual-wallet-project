using MediatR;
using VirtualWallet.Api.Constants;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Features.Common.Interfaces.Abstractions;
using VirtualWallet.Features.Common.Interfaces.Authentication;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Features.Constants;
using VirtualWallet.Models;
using VirtualWallet.Models.Authentication.Requests;

namespace VirtualWallet.Features.Authentication.Login;

public static class LoginFeature
{
    public record LoginCommand(
        LoginRequest Request) : IRequest<AuthenticationResponse>;

    public class LoginFeatureHandler : IRequestHandler<LoginCommand, AuthenticationResponse>
    {
        private readonly IUserRepository _userRepository;
        private readonly IJwtProvider _jwtProvider;
        private readonly ILinkService _linkService;

        public LoginFeatureHandler(IUserRepository userRepository, IJwtProvider jwtProvider, ILinkService linkService)
        {
            _linkService = linkService;
            _userRepository = userRepository;
            _jwtProvider = jwtProvider;
        }

        public async Task<AuthenticationResponse> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            await Task.CompletedTask;

            var requestModel = request.Request;

            var user = await _userRepository.GetUserByEmail(requestModel.Email);

            if (user is null || !BCrypt.Net.BCrypt.Verify(
               requestModel.Password,
               user.Password))
            {
                throw new InvalidCredentialsException();
            }

            var token = _jwtProvider.GenerateJwtToken(user);

            decimal? balance = user.Wallet?.Balance ?? 0;

            var response = new AuthenticationResponse(
                AuthenticationLiterals.Success,
                AuthenticationLiterals.LoginSuccess,
                new UserData(
                user.Id,
                user.Username,
                user.Email,
                user.PhoneNumber,
                balance,
                user.IsAdmin,
                user.IsBlocked,
                user.IsVerified,
                token,
                AuthenticationLiterals.TokenType));

            await AddLinksForUser(response);

            return response;
        }

        private async Task AddLinksForUser(AuthenticationResponse response)
        {
            await Task.CompletedTask;

            response.Links.Add(_linkService.Generate(
                HttpMethodNames.GetUser,
                new { id = response.Data.Id },
                "self",
                HttpMethodNames.Get));

            response.Links.Add(_linkService.Generate(
                "GetUserProfile",
                new { id = response.Data.Id },
                "user_profile",
                HttpMethodNames.Get));

            response.Links.Add(_linkService.Generate(
                HttpMethodNames.Logout,
                null,
                "logout",
                HttpMethodNames.Post));

            response.Links.Add(_linkService.Generate(
                HttpMethodNames.Register,
                null,
                "register_user",
                HttpMethodNames.Post));

            response.Links.Add(_linkService.Generate(
                "DeleteUser",
                new { username = response.Data.Username },
                "delete_user",
                HttpMethodNames.Delete));
        }
    }
}