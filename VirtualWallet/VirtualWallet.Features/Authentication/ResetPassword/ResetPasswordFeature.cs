﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Authentication.Requests;
using VirtualWallet.Models.Authentication.Responses;

namespace VirtualWallet.Features.Authentication.ResetPassword;

public static class ResetPasswordFeature
{
    public record ResetPasswordCommand(ResetPasswordRequest Request, string Token) : IRequest<ResetPasswordResponse>;

    public sealed class ResetPasswordFeatureHandler : IRequestHandler<ResetPasswordCommand, ResetPasswordResponse>
    {
        private readonly IUserRepository _userRepository;

        public ResetPasswordFeatureHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<ResetPasswordResponse> Handle(ResetPasswordCommand command, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetUserByResetToken(Guid.Parse(command.Token), cancellationToken) ?? throw new InvalidTokenException();

            var newPassword = BCrypt.Net.BCrypt.HashPassword(command.Request.Password);

            user.SetPassword(newPassword);

            user.InvalidateResetPasswordToken();

            await _userRepository.Update(user);

            return new ResetPasswordResponse(true, "Password reset successfully.");
        }
    }
}
