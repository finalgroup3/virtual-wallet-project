﻿using VirtualWallet.Domain.TransferEntity;

namespace VirtualWallet.Features.Common.Interfaces.Persistence
{
    public interface ITransferRepository
	{
		Task CreateTransfer(Transfer transfer);
        Task<IQueryable<Transfer>> GetTransfersQuery();
		Task<Transfer> GetTransferById(int id);
        Task SaveChanges();
	}
}
