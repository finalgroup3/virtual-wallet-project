﻿using VirtualWallet.Domain.CardEntity;

namespace VirtualWallet.Features.Common.Interfaces.Persistence
{
    public interface ICardRepository
    {
        Task CreateCard(BankCard card);
        Task<BankCard> GetCardById(int id);
        Task<List<BankCard>> GetAllCardsOfUser(int userId);
        Task<bool> IsNumberUnique(string number);
        Task<BankCard> GetCardByNumber(string name);
        Task SaveChanges();
    }
}
