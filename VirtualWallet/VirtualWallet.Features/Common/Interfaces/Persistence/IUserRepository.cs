﻿using VirtualWallet.Domain.UserEntity;

namespace VirtualWallet.Features.Common.Interfaces.Persistence
{
    public interface IUserRepository
    {
        Task<IEnumerable<User>> GetAllUsers(CancellationToken token);
        Task AddAsync(User user);
        Task<User> GetUserById(int id);
        Task<User> GetUserByUsername(string username);
        Task<User> GetUserByEmail(string email);
        Task SaveChanges();
        Task<User> GetUserByToken(Guid token, CancellationToken cancellationToken);
        Task<IQueryable<User>> GetUsersQuery();
        Task Update(User user);
        Task<bool> EmailExistsAsync(string email);
        Task<bool> UsernameExistsAsync(string username);
        Task<bool> PhoneNumberExistsAsync(string phoneNumber);
        Task<User> GetUserByResetToken(Guid guid, CancellationToken cancellationToken);
        Task<User> GetUserByEmailToken(Guid guid, CancellationToken cancellationToken);
    }
}
