using VirtualWallet.Domain.UserEntity;

namespace VirtualWallet.Features.Common.Interfaces.Persistence;

public interface IWalletRepository
{
    Task AddAsync(Wallet wallet);
    Task<Wallet?> GetWalletById(int walletId);
	Task<Wallet?> GetWalletByUserId(int userId);
	Task UpdateAsync(Wallet wallet);
	Task SaveChangesAsync();
}