namespace VirtualWallet.Features.Common.Interfaces.Abstractions;

public interface IEmailSender
{
    Task<bool> SendEmailAsync(string email, string subject, string message);
}