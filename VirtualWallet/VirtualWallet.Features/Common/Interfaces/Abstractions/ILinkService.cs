using VirtualWallet.Models;

namespace VirtualWallet.Features.Common.Interfaces.Abstractions;

public interface ILinkService
{
    Link Generate(string routeName, object? routeValue = null, string? rel = null, string? method = null);
}