﻿using Microsoft.AspNetCore.Http;

namespace VirtualWallet.Features.Common.Interfaces.Abstractions;

public interface IExceptionHandler<in TException> : IExceptionHandler where TException : Exception
{
    Task HandleAsync(HttpContext context, TException exception);
}

public interface IExceptionHandler
{
    Task HandleAsync(HttpContext context, Exception exception);
}

