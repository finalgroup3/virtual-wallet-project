﻿using VirtualWallet.Models;

namespace VirtualWallet.Features;
public interface IExchangeRateService
{
    Task<ExchangeRateResponse> GetExchangeRateAsync(string baseCurrency, string targetCurrency, decimal amount);

    Task<List<ExchangeRateResponse>> GetBestExchangeRatesAsync(string baseCurrency, string[] targetCurrencies, decimal amount);
}

