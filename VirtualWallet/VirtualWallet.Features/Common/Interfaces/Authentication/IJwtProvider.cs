using VirtualWallet.Domain.UserEntity;

namespace VirtualWallet.Features.Common.Interfaces.Authentication;

public interface IJwtProvider
{
    string GenerateJwtToken(User user);
}