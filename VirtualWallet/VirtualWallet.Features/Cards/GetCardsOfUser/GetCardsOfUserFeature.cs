﻿using MediatR;
using VirtualWallet.Domain.CardEntity;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Cards.Requests;
using VirtualWallet.Models.Cards.Responses;

namespace VirtualWallet.Features.Cards.GetCardsOfUser
{
    public static class GetCardsOfUserFeature
	{
		public record GetCardsOfUserCommand(GetCardsOfUserRequest RequestModel) : IRequest<List<CardResponse>>;

		public class GetCardsOfUserHandler : IRequestHandler<GetCardsOfUserCommand, List<CardResponse>>
		{
			private readonly ICardRepository _cardRepository;
			private readonly IUserRepository _userRepository;			

			public GetCardsOfUserHandler(ICardRepository cardRepository, IUserRepository userRepository)
			{
				_cardRepository = cardRepository;				
				_userRepository = userRepository;
			}

			public async Task<List<CardResponse>> Handle(GetCardsOfUserCommand request, CancellationToken cancellationToken)
			{
				await Task.CompletedTask;

				var requestModel = request.RequestModel;

				if (await _userRepository.GetUserById(requestModel.UserId) == null)
				{
					throw new UserNotFoundException("Id", requestModel.UserId.ToString());
				}

				List<BankCard> cards = await _cardRepository.GetAllCardsOfUser(requestModel.UserId);
				List<CardResponse> responses = new List<CardResponse>();
				foreach (var card in cards)
				{
					responses.Add(new CardResponse(

					card.Id,
					card.Name,
					card.Number,
					card.ExpirationDate.ToString("MM/yy"),
					card.SecurityCode,
					card.Type,
					card.UserId));
				}
				return responses;
			}
		}
	}
}
