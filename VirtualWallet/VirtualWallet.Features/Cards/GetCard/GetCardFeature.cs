﻿using MediatR;
using VirtualWallet.Api.Constants;
using VirtualWallet.Domain.CardEntity;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.Common.Interfaces.Abstractions;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Cards.Requests;
using VirtualWallet.Models.Cards.Responses;

namespace VirtualWallet.Features;

public static class GetCardFeature
{
    public record GetCardCommand(GetCardRequest RequestModel) : IRequest<CardResponse>;

    public class GetCardHandler : IRequestHandler<GetCardCommand, CardResponse>
    {
        private readonly ICardRepository _cardRepository;
        private readonly ILinkService _linkService;

        public GetCardHandler(ICardRepository cardRepository, ILinkService linkService)
        {
            _cardRepository = cardRepository;
            _linkService = linkService;
        }

        public async Task<CardResponse> Handle(GetCardCommand request, CancellationToken cancellationToken)
        {
            await Task.CompletedTask;

            var requestModel = request.RequestModel;

            if (await _cardRepository.GetCardById(requestModel.Id) is not BankCard card)
            {
                throw new CardNotFoundException(nameof(requestModel.Id), requestModel.Id.ToString());
            }

            var response = new CardResponse(

            card.Id,
            card.Name,
            card.Number,
            card.ExpirationDate.ToString("MM/yy"),
            card.SecurityCode,
            card.Type,
            card.UserId);

            await AddLinksToCard(response);

            return response;
        }

        private async Task AddLinksToCard(CardResponse response)
        {
            await Task.CompletedTask;

            response.Links.Add(_linkService.Generate(
                HttpMethodNames.GetCard,
                new { id = response.Id },
                "self",
                HttpMethodNames.Get));

            response.Links.Add(_linkService.Generate(
            HttpMethodNames.DeleteCard,
                new { id = response.Id },
                "delete_card",
                HttpMethodNames.Delete));
        }
    }
}
