﻿using MediatR;
using VirtualWallet.Domain.CardEntity;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Cards.Requests;
using VirtualWallet.Models.Cards.Responses;

namespace VirtualWallet.Features;

public static class CreateCardFeature
{
    public record CreateCardCommand(CreateCardRequest CreateCardModel, int UserId) : IRequest<CardResponse>;

    public class CreateCardHandler : IRequestHandler<CreateCardCommand, CardResponse>
    {
        private readonly ICardRepository _cardRepository;
        public CreateCardHandler(ICardRepository cardRepository)
        {
            _cardRepository = cardRepository;
        }
        public async Task<CardResponse> Handle(CreateCardCommand request, CancellationToken cancellationToken)
        {
            var createModel = request.CreateCardModel;
            string cardNumber = createModel.Number;

            if (!await _cardRepository.IsNumberUnique(cardNumber))
            {
                // mask the card number
                cardNumber = string.Concat(cardNumber.AsSpan()[..4], "****", cardNumber.AsSpan(12, 4));
                throw new DuplicateCardException(cardNumber);
            }
			string format = "MM/yy";
            DateTime result;
            DateTime.TryParseExact(createModel.ExpirationDate, format, null, System.Globalization.DateTimeStyles.None,out result);

            var card = BankCard.CreateCard(
                createModel.Name,
                createModel.Number,
                result,
                createModel.SecurityCode,
                createModel.Type,
                request.UserId);

            await _cardRepository.CreateCard(card);

            return (new CardResponse(
                card.Id,
                card.Name,
                card.Number,
                card.ExpirationDate.ToString("MM/yy"),
                card.SecurityCode,
                card.Type,
                card.UserId));
        }
    }
}