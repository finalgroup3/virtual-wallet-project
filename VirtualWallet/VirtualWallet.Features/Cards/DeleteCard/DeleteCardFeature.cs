﻿using MediatR;
using VirtualWallet.Domain.CardEntity;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Cards.Requests;

namespace VirtualWallet.Features.Cards.DeleteCard
{
    public static class DeleteCardFeature
    {
        public record DeleteCardCommand(DeleteCardRequest RequestModel) : IRequest<Unit>;

        public class DeleteCardHandler : IRequestHandler<DeleteCardCommand, Unit>
        {
            private readonly ICardRepository _cardRepository;

            public DeleteCardHandler(ICardRepository cardRepository)
            {
                _cardRepository = cardRepository;
            }

            public async Task<Unit> Handle(DeleteCardCommand request, CancellationToken cancellationToken)
            {
                await Task.CompletedTask;

                var requestModel = request.RequestModel;

                if (await _cardRepository.GetCardById(requestModel.Id) is not BankCard card)
                {
                    throw new CardNotFoundException("Id", requestModel.Id.ToString());
                }
                if (card.UserId != requestModel.UserId)
                {
                    //Placeholder exception
                    throw new UnauthorizedAccessException(requestModel.Id.ToString());
                }
                card.DeleteCard();
                await _cardRepository.SaveChanges();
                return Unit.Value;
            }
        }
    }
}
