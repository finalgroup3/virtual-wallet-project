﻿using MediatR;
using VirtualWallet.Domain.Exceptions.Common;
using VirtualWallet.Domain.Exceptions.Entity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Features.Common.Interfaces.Persistence;
using VirtualWallet.Models.Wallets.Requests;
using VirtualWallet.Models.Wallets.Responses;

namespace VirtualWallet.Features.Wallets.ChangeCurrency;

public static class ChangeCurrencyFeature
{
    public record ChangeCurrencyCommand(ChangeCurrencyRequest RequestModel) : IRequest<ChangeCurrencyResponse>;

    public class ChangeCurrencyFeatureHandler : IRequestHandler<ChangeCurrencyCommand, ChangeCurrencyResponse>
    {
        private readonly IWalletRepository _walletRepository;
        private readonly IExchangeRateService _exchangeRateService;
        private readonly IUserRepository _userRepository;
        private readonly decimal Fee = 0.005m;

        public ChangeCurrencyFeatureHandler(IWalletRepository walletRepository, IExchangeRateService exchangeRateService, IUserRepository userRepository)
        {
            _walletRepository = walletRepository;
            _exchangeRateService = exchangeRateService;
            _userRepository = userRepository;
        }

        public async Task<ChangeCurrencyResponse> Handle(ChangeCurrencyCommand request, CancellationToken cancellationToken)
        {
            var model = request.RequestModel;
            if (await _userRepository.GetUserById((int)model.UserId!) is not User user)
            {
                throw new UserNotFoundException("Id", model.UserId.ToString());
            }

            if (!BCrypt.Net.BCrypt.Verify(
               model.Password,
               user.Password))
            {
                throw new InvalidCredentialsException();
            }

            var wallet = user.Wallet;

            var oldCurr = wallet!.Currency;
            var newCurr = model.NewCurr;

            var oldBalancePreFee = wallet.Balance;
            var fees = oldBalancePreFee / 100 * Fee;
            var oldBalancePostFee = oldBalancePreFee - fees;

            var exchangeResponse = await _exchangeRateService.GetExchangeRateAsync(oldCurr, newCurr, oldBalancePostFee);

            var newBalance = exchangeResponse.Conversion_result;
            wallet.ConvertBalance(newBalance, newCurr);

            await _walletRepository.SaveChangesAsync();

            return new ChangeCurrencyResponse(
                wallet.Id,
                oldCurr,
                newCurr,
                oldBalancePreFee,
                newBalance,
                fees);
        }
    }
}
