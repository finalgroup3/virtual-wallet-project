﻿namespace VirtualWallet.Models.User.Responses;

public record ChangePhoneNumberResponse(
	string Username,
	string NewPhoneNumber);
