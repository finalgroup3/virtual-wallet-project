﻿namespace VirtualWallet.Models.User.Responses;

public record ChangePictureUrlResponse(
    string Username,
    string NewPictureUrl);
