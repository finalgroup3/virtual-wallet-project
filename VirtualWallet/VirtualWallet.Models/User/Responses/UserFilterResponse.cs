﻿using VirtualWallet.Domain.Pagination;

namespace VirtualWallet.Models.User.Responses;

public record UserFilterResponse(PagedList<UserResponse> Users);
