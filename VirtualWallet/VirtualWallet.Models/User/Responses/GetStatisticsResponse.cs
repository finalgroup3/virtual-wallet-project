﻿namespace VirtualWallet.Models.User.Responses;

public record GetStatisticsResponse(
    int UserId,
    DateTime StartDate,
    DateTime EndDate,
    string Currency,
    decimal AmountSent,
    decimal AmountReceived,
    int TransfersSent,
    int TransfersReceived);
