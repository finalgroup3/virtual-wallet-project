﻿namespace VirtualWallet.Models.User.Responses;

public record BlockStatusResponse(int Id, bool BlockedStatus);
