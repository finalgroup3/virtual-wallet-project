﻿namespace VirtualWallet.Models.User.Responses;

public record ForgotPasswordResponse(
    string To,
    string ResetPasswordLink,
    string ResetPasswordToken);
