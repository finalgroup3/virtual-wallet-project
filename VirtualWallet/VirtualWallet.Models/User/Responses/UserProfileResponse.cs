﻿namespace VirtualWallet.Models.User.Responses;
public record UserProfileResponse(
    int Id,
    string Username,
    string Email,
    string PhoneNumber,
    string PhotoUrl,
    bool IsAdmin,
    bool IsBlocked,
    bool IsVerified,
    List<UserBankCards> BankCards,
    List<UserTransfersReceived> ReceivedTransfers,
    List<UserTransfersSent> SentTransfers,
    ProfileWallet? Wallet)

{
    public List<Link> Links { get; set; } = new();
    public List<Link> AdminLinks { get; set; } = new();
}

public record UserBankCards(
    string CardNumber,
    string ExpirationDate,
    string CardHolderName,
    string Type);

public record UserTransfersSent(
    int Id,
    string RecipientUsername,
    decimal Amount,
    string Currency,
    decimal BaseAmount,
    string BaseCurrency,
    string Timestamp);

public record UserTransfersReceived(
    int Id,
    string SenderUsername,
    decimal Amount,
    string Currency,
    decimal BaseAmount,
    string BaseCurrency,
    string Timestamp);

public record ProfileWallet(
    int Id,
    WalletBalance Balance
);

public record WalletBalance(
    decimal Balance,
    string Currency
);