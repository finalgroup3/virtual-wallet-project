﻿namespace VirtualWallet.Models.User.Responses;
public record ChangeEmailResponse(
    bool Success,
    string Message);
