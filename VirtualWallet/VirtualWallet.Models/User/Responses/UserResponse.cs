﻿namespace VirtualWallet.Models.User.Responses;

public record UserResponse(
    int Id,
    string Username,
    string Email,
    string PhoneNumber,
    string PhotoUrl,
    bool IsAdmin,
    bool IsBlocked,
    bool IsVerified);