﻿namespace VirtualWallet.Models.User.Responses;

public record AdminStatusResponse(int Id, bool AdminStatus);
