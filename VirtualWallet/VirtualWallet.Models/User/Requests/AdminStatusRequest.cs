﻿namespace VirtualWallet.Models.User.Requests;

public record AdminStatusRequest(int Id, int LoggedUserId);
