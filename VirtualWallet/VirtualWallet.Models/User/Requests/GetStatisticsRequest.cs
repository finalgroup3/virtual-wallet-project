﻿namespace VirtualWallet.Models.User.Requests;

public record GetStatisticsRequest(
    int UserId,
    DateTime StartDate,
    DateTime EndDate);
