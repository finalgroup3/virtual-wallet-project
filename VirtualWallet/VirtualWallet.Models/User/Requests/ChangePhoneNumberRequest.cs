﻿namespace VirtualWallet.Models.User.Requests;

public record ChangePhoneNumberRequest(
	string PhoneNumber,
	int LoggedUserId);
