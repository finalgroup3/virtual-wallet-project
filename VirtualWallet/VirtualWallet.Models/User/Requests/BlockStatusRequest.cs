﻿namespace VirtualWallet.Models.User.Requests;

public record BlockStatusRequest(int Id, int LoggedUserId);

