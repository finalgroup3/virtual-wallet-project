﻿namespace VirtualWallet.Models.User.Requests;

public record FilterUsersRequest(
    string? SearchTerm,
    string? SortBy,
    string? OrderBy,
    int Page,
    int PageSize);
