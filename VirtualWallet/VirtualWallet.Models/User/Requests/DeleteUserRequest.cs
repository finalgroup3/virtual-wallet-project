﻿namespace VirtualWallet.Models.User.Requests;

public record DeleteUserRequest(int? LoggedId);