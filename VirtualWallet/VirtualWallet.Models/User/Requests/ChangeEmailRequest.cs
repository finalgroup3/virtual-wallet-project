﻿namespace VirtualWallet.Models.User.Requests;

public record ChangeEmailRequest(
    string Email,
    string ConfirmEmail);
