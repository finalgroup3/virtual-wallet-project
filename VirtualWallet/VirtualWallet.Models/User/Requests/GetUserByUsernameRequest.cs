﻿namespace VirtualWallet.Models.User.Requests;

public record GetUserByUsernameRequest(string Username);
