﻿namespace VirtualWallet.Models.User.Requests;

public record ChangePictureUrlBody(
    string NewPictureUrl);
