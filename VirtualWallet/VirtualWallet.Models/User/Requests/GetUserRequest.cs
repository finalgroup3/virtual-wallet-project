﻿namespace VirtualWallet.Models.User.Requests;

public record GetUserRequest(int Id);
