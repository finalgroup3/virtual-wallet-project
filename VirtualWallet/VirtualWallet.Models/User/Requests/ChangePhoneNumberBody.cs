﻿namespace VirtualWallet.Models.User.Requests;

public record ChangePhoneNumberBody(
    string NewPhoneNumber);
