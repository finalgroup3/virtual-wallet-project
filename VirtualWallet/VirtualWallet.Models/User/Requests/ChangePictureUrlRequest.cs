﻿namespace VirtualWallet.Models.User.Requests;

public record ChangePictureUrlRequest(
    string NewPictureUrl,
    int LoggedUserId);
