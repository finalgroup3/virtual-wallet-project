﻿namespace VirtualWallet.Models.Wallets.Requests;

public record ChangeCurrencyRequest(
    int? UserId,
    string Password,
    string NewCurr);