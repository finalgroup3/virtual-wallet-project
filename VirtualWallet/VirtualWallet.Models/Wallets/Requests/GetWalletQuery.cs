﻿namespace VirtualWallet.Models;

public record GetWalletRequest(
   int UserId);
