﻿namespace VirtualWallet.Models;

public record CreateWalletRequest(
    int? UserId,
    string Currency);
