﻿namespace VirtualWallet.Models.Wallets.Requests
{
    public record ChangeWalletBalanceRequest(
        int UserId,
        decimal Balance);
}
