﻿namespace VirtualWallet.Models.Wallets.Responses
{
    public record ChangeWalletBalanceResponse(
	bool Success,
	string Message);
}
