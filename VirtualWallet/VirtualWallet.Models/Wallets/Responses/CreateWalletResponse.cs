﻿namespace VirtualWallet.Models;

public record CreateWalletResponse(
    int Id,
    string Currency,
    decimal Balance,
    int UserId);