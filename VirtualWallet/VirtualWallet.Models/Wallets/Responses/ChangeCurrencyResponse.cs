﻿namespace VirtualWallet.Models.Wallets.Responses;

public record ChangeCurrencyResponse(
    int Id,
    string OldCurr,
    string NewCurr,
    decimal OldBalance,
    decimal NewBalance,
    decimal Fees);