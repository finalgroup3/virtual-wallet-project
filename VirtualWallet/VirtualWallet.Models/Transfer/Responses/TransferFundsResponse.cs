﻿namespace VirtualWallet.Models.Transfer.Responses;

public record TransferFundsResponse(
    int Id,
    int SenderId,
    string SenderUsername,
    int RecipientId,
    string RecipientUsername,
    decimal BaseAmount,
    string BaseCurrency,
    decimal TargetAmount,
    string TargetCurrency,
    decimal TotalTaken,
    decimal Fees,
    ExchangeRateResponse ExchangeData);

