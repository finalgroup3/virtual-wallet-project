﻿using VirtualWallet.Domain.Pagination;

namespace VirtualWallet.Models.Transfer.Responses;

public record FilterTransfersResponse(PagedList<TransferResponse> Transfers);
