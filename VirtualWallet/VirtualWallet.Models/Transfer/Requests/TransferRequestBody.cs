﻿namespace VirtualWallet.Models.Transfer.Requests;

public record TransferRequestBody(
    int RecipientId,
    decimal Amount);
