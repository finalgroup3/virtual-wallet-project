﻿namespace VirtualWallet.Models.Transfer.Requests;

public record FilterTransfersRequest(
	DateTime TransferedAfter,
	DateTime TransferedBefore,
	string? BySender,
	string? ByRecipient,
	decimal MinimumAmount,
	decimal MaximumAmount,
	string? SortBy,
	string? OrderBy,
	int Page,
	int PageSize);
