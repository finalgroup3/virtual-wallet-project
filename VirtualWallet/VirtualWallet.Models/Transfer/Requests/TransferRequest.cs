﻿namespace VirtualWallet.Models.Transfer.Requests
{
    public record TransferRequest(int Id);
}
