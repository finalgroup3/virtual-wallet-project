﻿namespace VirtualWallet.Models.Transfer.Requests;

public record TransferFundsRequest(
    int SenderId,
    int RecipientId,
    decimal Amount);
