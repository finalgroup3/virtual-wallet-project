﻿namespace VirtualWallet.Models;

public record ExchangeRateResponse(
    string Result,
    string Documentation,
    string Terms_of_use,
    long Time_last_update_unix,
    string Time_last_update_utc,
    long Time_next_update_unix,
    string Time_next_update_utc,
    string Base_code,
    string Target_code,
    double Conversion_rate,
    decimal Conversion_result);