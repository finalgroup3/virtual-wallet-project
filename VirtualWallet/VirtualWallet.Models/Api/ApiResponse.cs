﻿namespace VirtualWallet.Models.Api;

public class ApiResponse<TResponse> where TResponse : class
{
    public TResponse? Model { get; set; }
    public bool IsSuccess { get; set; }
    public ApiError? Errors { get; set; }
}