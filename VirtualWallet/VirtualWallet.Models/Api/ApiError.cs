﻿namespace VirtualWallet.Models.Api;

public class ApiError
{
    public string? ErrorMessage { get; set; }
    public Dictionary<string, string[]>? Errors { get; set; }
}
