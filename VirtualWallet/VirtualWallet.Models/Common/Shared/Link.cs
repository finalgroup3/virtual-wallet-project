namespace VirtualWallet.Models;

public record Link(string Href, string Rel, string Method);