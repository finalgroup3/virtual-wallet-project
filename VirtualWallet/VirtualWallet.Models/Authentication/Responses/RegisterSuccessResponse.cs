﻿namespace VirtualWallet.Models.Authentication.Responses;

public record RegisterSuccessResponse(
        bool Success,
        string Message,
        Data Data);

public record Data(
    int Id,
    string Email,
    string Username,
    string VerificationToken,
    string VerificationLink);
