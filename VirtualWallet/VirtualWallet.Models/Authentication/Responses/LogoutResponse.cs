﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualWallet.Models.Authentication.Responses;

public record LogoutResponse(
    bool Succes,
    string? Message);