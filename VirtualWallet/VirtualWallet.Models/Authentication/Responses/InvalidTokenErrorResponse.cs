﻿namespace VirtualWallet.Models.Authentication.Responses;

public record InvalidTokenErrorResponse(
    string Type,
    bool Success,
    string Message);
