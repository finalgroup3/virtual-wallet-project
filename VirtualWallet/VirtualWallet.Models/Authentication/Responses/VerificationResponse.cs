﻿namespace VirtualWallet.Models.Authentication.Responses;

public record VerificationResponse(
    bool Success,
    string Message)
{
   public List<Link> Links { get; init; } = new();
}
