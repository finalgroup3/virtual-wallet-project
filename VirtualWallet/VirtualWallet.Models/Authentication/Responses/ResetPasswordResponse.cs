﻿namespace VirtualWallet.Models.Authentication.Responses;

public record ResetPasswordResponse(
    bool Success,
    string Message);
