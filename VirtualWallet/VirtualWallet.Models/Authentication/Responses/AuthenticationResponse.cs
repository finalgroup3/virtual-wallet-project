namespace VirtualWallet.Models;

public record AuthenticationResponse(
    string Status,
    string Message,
    UserData Data)
{
    public List<Link> Links { get; set; } = new List<Link>();
}

public record UserData(
    int Id,
    string Username,
    string Email,
    string PhoneNumber,
    decimal? Balance,
    bool IsAdmin,
    bool IsBlocked,
    bool IsVerified,
    string Token,
    string TokenType);