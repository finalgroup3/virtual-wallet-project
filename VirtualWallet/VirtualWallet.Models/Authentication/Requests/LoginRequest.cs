namespace VirtualWallet.Models.Authentication.Requests;

public record LoginRequest(
    string Email,
    string Password);