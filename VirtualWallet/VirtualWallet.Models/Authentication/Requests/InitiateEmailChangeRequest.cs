﻿namespace VirtualWallet.Models.Authentication.Requests;

public record InitiateEmailChangeRequest(
	string Email,
	int LoggedId);
