﻿namespace VirtualWallet.Models.Authentication.Requests;

public record ResetPasswordRequest(
    string Password,
    string ConfirmPassword);