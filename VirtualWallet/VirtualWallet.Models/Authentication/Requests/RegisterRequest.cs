namespace VirtualWallet.Models;

public record RegisterRequest(
    string Username,
    string Password,
    string Email,
    string PhoneNumber);