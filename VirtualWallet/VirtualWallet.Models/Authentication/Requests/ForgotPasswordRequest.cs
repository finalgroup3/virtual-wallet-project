﻿namespace VirtualWallet.Models.Authentication.Requests;

public record ForgotPasswordRequest(
    string Email);
