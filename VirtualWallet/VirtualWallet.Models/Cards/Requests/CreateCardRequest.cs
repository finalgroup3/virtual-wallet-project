﻿namespace VirtualWallet.Models.Cards.Requests;

public record CreateCardRequest(

    string Name,
    string Number,
    string ExpirationDate,
    string SecurityCode,
    string Type);
