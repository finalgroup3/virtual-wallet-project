﻿namespace VirtualWallet.Models.Cards.Requests
{
    public record GetCardsOfUserRequest(int UserId);
}
