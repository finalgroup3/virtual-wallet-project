﻿namespace VirtualWallet.Models.Cards.Requests;

public record GetCardRequest(int Id);
