﻿namespace VirtualWallet.Models.Cards.Requests;

public record DeleteCardRequest(int Id, int UserId);

