﻿namespace VirtualWallet.Models.Cards.Responses;

public record CardResponse(
	int Id,
	string Name,
	string Number,
	string ExpirationDate,
	string SecurityCode,
	string Type,
	int UserId)
{
	public List<Link> Links { get; set; } = new();
}