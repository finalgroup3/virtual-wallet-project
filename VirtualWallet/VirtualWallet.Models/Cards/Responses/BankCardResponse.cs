namespace VirtualWallet.Models.Cards.Responses;

public record BankCardResponse(
    int Id,
    string Name,
    string Number,
    string ExpirationDate,
    string SecurityCode,
    string Type,
    string Brand);