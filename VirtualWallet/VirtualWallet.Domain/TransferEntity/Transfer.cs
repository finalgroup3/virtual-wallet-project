using VirtualWallet.Domain.UserEntity;

namespace VirtualWallet.Domain.TransferEntity;

public sealed class Transfer
{
    public int Id { get; private set; }
    public DateTime TransferDate { get; private set; } = DateTime.UtcNow;
    public int SenderId { get; private set; }
    public User Sender { get; private set; } = null!;
    public int RecipientId { get; private set; }
    public User Recipient { get; private set; } = null!;
    public decimal BaseAmount { get; private set; }
    public string BaseCurrency { get; private set; }
    public decimal TargetAmount { get; private set; }
    public string TargetCurrency { get; private set; }
    public decimal TotalTaken { get; private set; }
    public decimal Fees { get; private set; }

    private Transfer(
        int senderId,
        int recipientId,
        decimal baseAmount,
        string baseCurrency,
        decimal targetAmount,
        string targetCurrency,
        decimal totalTaken,
        decimal fees)
    {
        SenderId = senderId;
        RecipientId = recipientId;
        BaseCurrency = baseCurrency;
        TargetCurrency = targetCurrency;
        TargetAmount = targetAmount;
        BaseAmount = baseAmount;
        TotalTaken = totalTaken;
        Fees = fees;
    }

    public static Transfer CreateTransfer(
        int senderId,
        int recepientId,
        decimal baseAmount,
        string baseCurrency,
        decimal targetAmount,
        string targetCurrency,
        decimal totalTaken,
        decimal fees)
    {
        return new Transfer(
            senderId,
            recepientId,
            baseAmount,
            baseCurrency,
            targetAmount,
            targetCurrency,
            totalTaken,
            fees);
    }

    public void SetTransferDate(DateTime date)
    { 
       TransferDate = date;
    }

    public void SetSender(User sender)
    {
        Sender = sender;
    }

    public void SetRecipient(User recipient)
    {
        Recipient = recipient;
    }

    public void SetId(int id)
    {
        Id = id;
    }
}