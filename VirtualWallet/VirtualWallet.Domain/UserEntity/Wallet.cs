using VirtualWallet.Domain.EntityTypes;
using VirtualWallet.Domain.Exceptions.Common;

namespace VirtualWallet.Domain.UserEntity;
public sealed class Wallet
{
    public int Id { get; private set; }
    public User User { get; private set; } = default!;
    public int UserId { get; private set; }
    public decimal Balance { get; private set; }
    public string Currency { get; private set; }
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
    private Wallet()
    {
    }
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
    private Wallet(decimal amount, string currency, int userId)
    {
        UserId = userId;
        Balance = amount;
        Currency = currency;
    }
    public static Wallet CreateWallet(int userId, decimal amount, string currency)
    {
        if (!Enum.IsDefined(typeof(CurrencyType), currency))
        {
            throw new InvalidRequestException("Invalid currency");
        }
        return new Wallet(amount, currency, userId);
    }
    public void ConvertBalance(decimal amount, string currency)
    {
        Balance = amount;
        Currency = currency;
    }

    public void AddMoney(decimal amount)
    {
        Balance += amount;
    }
	public void ChangeMoney(decimal amount)
	{
		Balance = amount;
	}
	public void SubtractMoney(decimal amount)
    {
        Balance -= amount;
    }

    public void SetId(int id)
    {
        Id = id;
    }

}