﻿using VirtualWallet.Domain.CardEntity;
using VirtualWallet.Domain.TransferEntity;

namespace VirtualWallet.Domain.UserEntity;

public sealed class User
{
    private readonly List<Transfer> _transfersReceived = new();
    private readonly List<Transfer> _transfersSent = new();
    private readonly List<BankCard> _bankCards = new();
    public int Id { get; private set; }
    public string Username { get; private set; }
    public string Password { get; private set; }
    public string Email { get; private set; }
    public string PhoneNumber { get; private set; }
    public string? PhotoUrl { get; private set; }
    public bool IsAdmin { get; private set; } = false;
    public bool IsBlocked { get; private set; } = false;
    public bool IsDeleted { get; private set; } = false;
    public bool IsVerified { get; private set; } = false;
    public Guid VerificationToken { get; private set; }
    public Guid? ResetPasswordToken { get; private set; }
    public Guid? ChangeEmailToken { get; private set; }
    public Wallet? Wallet { get; private set; }
    public IReadOnlyList<BankCard> BankCards => _bankCards.AsReadOnly();
    public IReadOnlyList<Transfer> TransfersReceived => _transfersReceived.AsReadOnly();
    public IReadOnlyList<Transfer> TransfersSent => _transfersSent.AsReadOnly();

    private User(
        string username,
        string password,
        string email,
        string phoneNumber
        )
    {
        Username = username;
        Password = password;
        Email = email;
        PhoneNumber = phoneNumber;
        VerificationToken = Guid.NewGuid();
    }

    public static User CreateUser(
        string username,
        string password,
        string email,
        string phoneNumber)
    {
        var user = new User(
        username,
        password,
        email,
        phoneNumber);

        return user;
    }
    public void ChangeWalletCurrency(decimal amount, string curr)
    {
        Wallet.ConvertBalance(amount, curr);
    }
    public Wallet CreateWallet(string curr)
    {
        Wallet = Wallet.CreateWallet(this.Id, 0, curr);
        return Wallet;
    }

    public void SetPassword(string password)
    {
        this.Password = password;
    }

    public void SetEmail(string email)
    {
        this.Email = email;
    }

    public void SetPhoneNumber(string phoneNumber)
    {
        this.PhoneNumber = phoneNumber;
    }

    public void SetPhotoUrl(string photoUrl)
    {
        this.PhotoUrl = photoUrl;
    }

    public string GenerateResetPasswordToken()
    {
        this.ResetPasswordToken = Guid.NewGuid();
        return this.ResetPasswordToken.ToString();
    }

    public void InvalidateResetPasswordToken()
    {
        this.ResetPasswordToken = Guid.Empty;
    }

    public string GenerateChangeEmailToken()
    {
        ChangeEmailToken = Guid.NewGuid();
        return ChangeEmailToken.ToString();
    }

    public void ResetChangeEmailToken()
    {
        this.ChangeEmailToken = Guid.Empty;
    }

    public void AddTransferReceived(Transfer transfer)
    {
        _transfersReceived.Add(transfer);
    }

    public void AddTransferSent(Transfer transfer)
    {
        _transfersSent.Add(transfer);
    }

    public void DeleteUser()
    {
        this.IsDeleted = true;
        this.Username = $"Deleted_{this.Id}";
        this.Email = $"Deleted_{this.Id}";
        this.PhoneNumber = $"Deleted_{this.Id}";
    }
    public void AddBankCard(BankCard card)
    {
        _bankCards.Add(card);
    }

    public void Block()
    {
        this.IsBlocked = true;
    }

    public void Unblock()
    {
        this.IsBlocked = false;
    }

    public void SetId(int id)
    {
        this.Id = id;
    }

    public void PromoteToAdmin()
    {
        this.IsAdmin = true;
    }

    public void DemoteFromAdmin()
    {
        this.IsAdmin = false;
    }

    public void Verify()
    {
        this.IsVerified = true;
    }

    public void AddFunds(decimal amount)
    {
        Wallet.AddMoney(amount);
    }

    public void DeductFunds(decimal amount)
    {
        Wallet.SubtractMoney(amount);
    }
}