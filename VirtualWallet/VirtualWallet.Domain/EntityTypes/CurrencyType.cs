namespace VirtualWallet.Domain.EntityTypes;

public enum CurrencyType
{
    USD = 1,
    EUR,
    BGN,
    GBP,
    JPY,
    CAD,
    AUD,
    CHF,
    CNY,
    INR,
    BRL,
    RUB,
    ZAR,
    SGD,
    NZD,
    MXM
}