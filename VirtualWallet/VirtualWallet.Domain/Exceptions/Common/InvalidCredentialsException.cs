﻿namespace VirtualWallet.Domain.Exceptions.Common;

public sealed class InvalidCredentialsException : Exception
{
    public InvalidCredentialsException() : base()
    {
    }

    public InvalidCredentialsException(string? message) : base(message)
    {
    }

    public InvalidCredentialsException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}
