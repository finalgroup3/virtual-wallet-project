namespace VirtualWallet.Domain.Exceptions.Common;

public class ErrorDetails
{
    public string Action { get; set; } = null!;
    public string Description { get; set; } = null!;
}