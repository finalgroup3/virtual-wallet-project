﻿namespace VirtualWallet.Domain.Exceptions.Common;

public class InvalidRequestException : Exception
{
    protected static int _autoIncrementCounter = 1;
    public string ErrorCode { get; }

    public InvalidRequestException(string message) :
    base(message)
    {
        ErrorCode = GenerateNextErrorCode();
    }

    protected virtual string GenerateNextErrorCode()
    {
        return $"INVALID_REQUEST_{_autoIncrementCounter++:D3}";
    }
}
