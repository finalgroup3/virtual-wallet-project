﻿namespace VirtualWallet.Domain.Exceptions.Common;

public class UnauthorizedOperationException : Exception
{	
	private const string ErrorMessage = "You are not authorized to perform this operation. Your location has been backtraced and the cyber police are on their way. The consequences will never be the same.";

	protected static int _autoIncrementCounter = 1;
	public string ErrorCode { get; }

	public UnauthorizedOperationException(string? errorCode = null) :
	base(ErrorMessage)
	{
		ErrorCode = errorCode ?? GenerateNextErrorCode();
	}

	private static string GenerateNextErrorCode()
	{
		return $"UNAUTHORIZED_OPERATION_{_autoIncrementCounter++:000}";
	}
}
