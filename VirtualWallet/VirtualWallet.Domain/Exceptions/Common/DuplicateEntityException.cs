﻿namespace VirtualWallet.Domain.Exceptions.Common;

public class DuplicateEntityException : Exception
{
    protected static int _autoIncrementCounter = 1;

    public string Entity { get; }
    public string Property { get; }
    public string DuplicateValue { get; }
    public string ErrorCode { get; }

    public DuplicateEntityException(string entity, string property, string duplicateValue) :
        base($"The specified {entity} with {property} '{duplicateValue}' is already registered.")
    {
        Entity = entity;
        Property = property;
        DuplicateValue = duplicateValue;
        ErrorCode = GenerateNextErrorCode();
    }

    protected virtual string GenerateNextErrorCode()
    {
        return $"ENTITY_EXISTS_{_autoIncrementCounter++:D3}";
    }
}
