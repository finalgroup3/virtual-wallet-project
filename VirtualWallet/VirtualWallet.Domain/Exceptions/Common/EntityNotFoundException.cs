namespace VirtualWallet.Domain.Exceptions.Common;

public class EntityNotFoundException : Exception
{
    protected static int _autoIncrementCounter = 1;

    public string? Entity { get; }
    public string? Property { get; }
    public string? NotFoundValue { get; }
    public string? ErrorCode { get; }

    public EntityNotFoundException(string entity, string property, string notFoundValue) :
    base($"The requested {entity} with {property} '{notFoundValue}' does not exist.")
    {
        Entity = entity;
        Property = property;
        NotFoundValue = notFoundValue;
        ErrorCode = GenerateNextErrorCode();
    }

    public EntityNotFoundException() : base()
    {
    }

    protected virtual string GenerateNextErrorCode()
    {
        return $"ENTITY_NOT_FOUND_{_autoIncrementCounter++:D3}";
    }
}