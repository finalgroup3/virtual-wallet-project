using VirtualWallet.Domain.Exceptions.Common;

namespace VirtualWallet.Domain.Exceptions.Entity;

public class CardNotFoundException : EntityNotFoundException
{
    private const string EntityType = "Bank Card";
    public CardNotFoundException(string property, string notFoundValue) : base(EntityType, property, notFoundValue)
    {
    }

    public CardNotFoundException() : base()
    {
    }

    protected override string GenerateNextErrorCode()
    {
        return $"CARD_NOT_FOUND_{_autoIncrementCounter++:D3}";
    }
}