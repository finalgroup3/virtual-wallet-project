﻿using VirtualWallet.Domain.Exceptions.Common;

namespace VirtualWallet.Domain.Exceptions.Entity;

public class DuplicateCardException : DuplicateEntityException
{
    private const string EntityType = "Bank Card";
    private const string PropertyName = "Card Number";
    public DuplicateCardException(string duplicateValue)
        : base(EntityType, PropertyName, duplicateValue)
    {
    }

    protected override string GenerateNextErrorCode() => $"CARD_ALREADY_EXISTS_{_autoIncrementCounter++:D3}";
}
