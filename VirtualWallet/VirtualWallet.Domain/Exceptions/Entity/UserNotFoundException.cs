﻿using VirtualWallet.Domain.Exceptions.Common;

namespace VirtualWallet.Domain.Exceptions.Entity;

public class UserNotFoundException : EntityNotFoundException
{
    private const string EntityType = "User";
    public UserNotFoundException(string property, string notFoundValue) : base(EntityType, property, notFoundValue)
    {
    }

    public UserNotFoundException() : base()
    {
    }
    protected override string GenerateNextErrorCode()
    {
        return $"USER_NOT_FOUND_{_autoIncrementCounter++:000}";
    }
}
