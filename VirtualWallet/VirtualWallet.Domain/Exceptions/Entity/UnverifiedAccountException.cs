﻿namespace VirtualWallet.Domain.Exceptions.Entity;

public class UnverifiedAccountException: Exception
{	
	private const string ErrorMessage = "You have not verified your account yet and can therefore not perform this operation.";

	protected static int _autoIncrementCounter = 1;
	public string ErrorCode { get; }

	public UnverifiedAccountException(string? errorCode = null) :
	base(ErrorMessage)
	{
		ErrorCode = errorCode ?? GenerateNextErrorCode();
	}

	private static string GenerateNextErrorCode()
	{
		return $"UNVERIFIED_ACCOUNT_{_autoIncrementCounter++:000}";
	}
}
