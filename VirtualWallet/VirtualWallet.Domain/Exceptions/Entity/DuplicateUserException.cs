﻿using VirtualWallet.Domain.Exceptions.Common;

namespace VirtualWallet.Domain.Exceptions.Entity;

public class DuplicateUserException : DuplicateEntityException
{
    private const string EntityType = "User";
    public DuplicateUserException(string property, string duplicateValue) : base(EntityType, property, duplicateValue)
    {
    }

    protected override string GenerateNextErrorCode()
    {
        return $"USER_ALREADY_EXISTS_{_autoIncrementCounter++:000}";
    }
}
