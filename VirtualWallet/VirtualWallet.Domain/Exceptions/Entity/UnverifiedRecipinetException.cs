﻿namespace VirtualWallet.Domain.Exceptions.Entity;

public class UnverifiedRecipinetException : Exception
{	
	private const string ErrorMessage = "The user you are tryin got transfer money to has not verified his account yet and can therefore not receive any payments.";

	protected static int _autoIncrementCounter = 1;
	public string ErrorCode { get; }

	public UnverifiedRecipinetException(string? errorCode = null) :
	base(ErrorMessage)
	{
		ErrorCode = errorCode ?? GenerateNextErrorCode();
	}

	private static string GenerateNextErrorCode()
	{
		return $"UNVERIFIED_RECIPIENT_{_autoIncrementCounter++:000}";
	}
}
