﻿using VirtualWallet.Domain.Exceptions.Common;

namespace VirtualWallet.Domain.Exceptions.Entity;

public class TransferNotFoundException : EntityNotFoundException
{
    public static string EntityType => "Transfer";

    public TransferNotFoundException(string property, string notFoundValue) :
    base(EntityType, property, notFoundValue)
    {
    }



    protected override string GenerateNextErrorCode()
    {
        return $"TRANSFER_NOT_FOUND{_autoIncrementCounter++:000}";
    }
}
