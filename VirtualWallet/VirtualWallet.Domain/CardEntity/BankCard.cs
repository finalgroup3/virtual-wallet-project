﻿using VirtualWallet.Domain.UserEntity;

namespace VirtualWallet.Domain.CardEntity;

public sealed class BankCard
{
	public int Id { get; set; }
	public string Name { get; set; }
	public string Number { get; set; }
	public DateTime ExpirationDate { get; set; }
	public string SecurityCode { get; set; }
	public string Type { get; set; }
	public bool IsDeleted { get; set; } = false;
	public int UserId { get; set; }
	public User User { get; set; } = null!;

	private BankCard(
		string name,
		string number,
		DateTime expirationDate,
		string securityCode,
		string type,
		int userId)
	{
		Name = name;
		Number = number;
		ExpirationDate = expirationDate;
		SecurityCode = securityCode;
		Type = type;
		UserId = userId;
	}

	public static BankCard CreateCard(
		string name,
		string number,
		DateTime expirationDate,
		string securityCode,
		string type,
		int userId)
	{
		return new BankCard(
			name,
			number,
			expirationDate,
			securityCode,
			type,
			userId);
	}


	public void DeleteCard()
	{
		this.IsDeleted = true;
	}
}
