﻿namespace VirtualWallet.Domain.Pagination;

public class PagedList<T>
{	
	public List<T> Items { get;}
	public int Page { get;}
	public int PageSize { get;}
	public int TotalItems { get;}
	public bool HasNextPage =>Page*PageSize < TotalItems;
	public bool HasPreviousPage => Page > 1;
	private PagedList(List<T> items, int page, int pageSize, int totalItems)
	{
		Items = items;
		Page = page;
		PageSize = pageSize;
		TotalItems = totalItems;
	}

	public static PagedList<T> Create(IQueryable<T> query, int page, int pageSize)
	{
		int totalItems = query.Count();
		var items = query.Skip((page - 1) * pageSize).Take(pageSize).ToList();

		return new (items, page, pageSize, totalItems);
	}

	public static PagedList<T> CreateEmpty(int page, int pageSize)
	{
		int totalItems = 0;
		var items = new List<T>();

		return new(items, page, pageSize, totalItems);
	}
}
