﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using VirtualWallet.Api.Controllers;
using VirtualWallet.Features.Common.Interfaces.Abstractions;
using VirtualWallet.Features.Constants;
using VirtualWallet.Models;
using VirtualWallet.Models.Authentication.Requests;
using VirtualWallet.Models.Authentication.Responses;
using VirtualWallet.Models.User.Responses;
using static VirtualWallet.Features.Authentication.InitiateEmailChange.InitiateEmailChangeFeature;
using static VirtualWallet.Features.Authentication.Login.LoginFeature;
using static VirtualWallet.Features.Authentication.Register.RegisterFeature;
using static VirtualWallet.Features.Authentication.ResetPassword.ResetPasswordFeature;
using static VirtualWallet.Features.Authentication.Verify.VerifyUserFeature;
using static VirtualWallet.Features.Users.ForgotPassword.ForgotPasswordFeature;


namespace VirtualWallet.Api.UnitTests.Controllers;

public class AuthenticationControllerTests
{
    private readonly Mock<IMediator> _mediatorMock;
    private readonly Mock<IEmailSender> _emailSenderMock;
    private readonly AuthenticationController _controller;

    private string stringParam = GetTestString();

    public AuthenticationControllerTests()
    {
        _mediatorMock = new Mock<IMediator>();
        _emailSenderMock = new Mock<IEmailSender>();
        _controller = new AuthenticationController(_mediatorMock.Object, _emailSenderMock.Object);
    }

    [Fact]
    public async Task RegisterAsync_Should_SendRegisterCommandAndSendEmail()
    {
        // Arrange        
        var request = new RegisterRequest(
            stringParam,
            stringParam,
            stringParam,
            stringParam);
        
        var token = Guid.NewGuid();
        var verificationLink = AuthenticationLiterals.GetEmailVerificationLink(token);

        var response = new RegisterSuccessResponse(
            true,
            stringParam,
            new Models.Authentication.Responses.Data(
                1,
                stringParam,
                stringParam,
                stringParam,
                verificationLink));

        _mediatorMock.Setup(x => x.Send(It.IsAny<RegisterCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        // Act
        var result = await _controller.RegisterAsync(request);

        // Assert
        _mediatorMock.Verify(x => x.Send(It.Is<RegisterCommand>(c => c.Request == request), It.IsAny<CancellationToken>()), Times.Once);
        _emailSenderMock.Verify(x => x.SendEmailAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task LoginAsync_ShouldSendLoginCommandAndSetCookie()
    {
        // Arrange        
        var request = new LoginRequest(stringParam, stringParam);

        var response = new AuthenticationResponse(
            stringParam,
            stringParam,
            new UserData(
                1,
                stringParam,
                stringParam,
                stringParam,
                0.00m,
                false,
                false,
                true,
                stringParam,
                stringParam));

        _mediatorMock.Setup(x => x.Send(It.IsAny<LoginCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        var responseMock = new Mock<HttpResponse>();
        var cookiesMock = new Mock<IResponseCookies>();
        responseMock.SetupGet(r => r.Cookies).Returns(cookiesMock.Object);

        var httpContextMock = new Mock<HttpContext>();
        httpContextMock.SetupGet(a => a.Response).Returns(responseMock.Object);

        var controllerContext = new ControllerContext
        {
            HttpContext = httpContextMock.Object
        };

        _controller.ControllerContext = controllerContext;

        // Act
        var result = await _controller.LoginAsync(request);

        // Assert
        _mediatorMock.Verify(x => x.Send(It.Is<LoginCommand>(c => c.Request == request), It.IsAny<CancellationToken>()), Times.Once);
        cookiesMock.Verify(c => c.Append(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CookieOptions>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task VerifyEmailAsync_ShouldSendVerifyUserCommand()
    {
        // Arrange            
        var response = new VerificationResponse(true, stringParam);

        _mediatorMock.Setup(x => x.Send(It.IsAny<VerifyUserCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);
                
        _controller.ControllerContext = GetControllerContext();

        // Act
        var result = await _controller.VerifyEmailAsync(stringParam);

        // Assert
        _mediatorMock.Verify(x => x.Send(It.Is<VerifyUserCommand>(c => c.Token == stringParam), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public void Logout_ShouldUpdateJWTCookie()
    {
        // Arrange
        var responseMock = new Mock<HttpResponse>();
        var requestMock = new Mock<HttpRequest>();

        var requestCookiesMock = new Mock<IRequestCookieCollection>();
        var validToken = GetValidToken();
        requestCookiesMock.Setup(cookies => cookies[It.IsAny<String>()]).Returns(validToken);
        requestMock.SetupGet(r => r.Cookies).Returns(requestCookiesMock.Object);

        var responseCookiesMock = new Mock<IResponseCookies>();
        responseMock.SetupGet(r => r.Cookies).Returns(responseCookiesMock.Object);

        var httpContextMock = new Mock<HttpContext>();
        httpContextMock.SetupGet(context => context.Request).Returns(requestMock.Object);
        httpContextMock.SetupGet(context => context.Response).Returns(responseMock.Object);

        var controllerContext = new ControllerContext
        {
            HttpContext = httpContextMock.Object
        };

        _controller.ControllerContext = controllerContext;

        // Act
        var result = _controller.Logout();

        // Assert        
        requestCookiesMock.Verify(cookies => cookies[It.IsAny<String>()], Times.Once);
        responseCookiesMock.Verify(cookies => cookies.Append(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CookieOptions>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public void Logout_ShouldReturnBadRequest_WhenCookieNotFound()
    {
        // Arrange        
        var requestMock = new Mock<HttpRequest>();

        var requestCookiesMock = new Mock<IRequestCookieCollection>();
        string? nullCookie = null;
        requestCookiesMock.Setup(cookies => cookies[It.IsAny<String>()]).Returns(nullCookie);
        requestMock.SetupGet(r => r.Cookies).Returns(requestCookiesMock.Object);        

        var httpContextMock = new Mock<HttpContext>();
        httpContextMock.SetupGet(context => context.Request).Returns(requestMock.Object);       

        var controllerContext = new ControllerContext
        {
            HttpContext = httpContextMock.Object
        };

        _controller.ControllerContext = controllerContext;

        // Act
        var result = _controller.Logout();

        // Assert        
        requestCookiesMock.Verify(cookies => cookies[It.IsAny<String>()], Times.Once);
        
        Assert.IsType<BadRequestObjectResult>(result);
    }

    [Fact]
    public void Logout_ShouldReturnBadRequest_WhenTokenNotValid()
    {
        // Arrange
        var responseMock = new Mock<HttpResponse>();
        var requestMock = new Mock<HttpRequest>();

        var requestCookiesMock = new Mock<IRequestCookieCollection>();
        string token = GetInvalidToken();
        requestCookiesMock.Setup(cookies => cookies[It.IsAny<String>()]).Returns(token);
        requestMock.SetupGet(r => r.Cookies).Returns(requestCookiesMock.Object);

        var httpContextMock = new Mock<HttpContext>();
        httpContextMock.SetupGet(context => context.Request).Returns(requestMock.Object);

        var controllerContext = new ControllerContext
        {
            HttpContext = httpContextMock.Object
        };

        _controller.ControllerContext = controllerContext;

        // Act
        var result = _controller.Logout();

        // Assert        
        requestCookiesMock.Verify(cookies => cookies[It.IsAny<String>()], Times.Once);

        Assert.IsType<BadRequestObjectResult>(result);
    }

    [Fact]
    public async Task ForgotPassword_Should_SendEmail()
    {
        // Arrange 
        var request = new ForgotPasswordRequest(stringParam);        
        var response = new ForgotPasswordResponse(stringParam, stringParam, stringParam);       

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<ForgotPasswordCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.ForgotPassword(request);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<ForgotPasswordCommand>(c => c.Request == request), It.IsAny<CancellationToken>()), Times.Once);
        _emailSenderMock.Verify(mailSender => mailSender.SendEmailAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task ResetPasswordAsync_Should_SendEmail()
    {
        // Arrange
        var request = new ResetPasswordRequest(stringParam, stringParam);
       
        var response = new ResetPasswordResponse(true, stringParam);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<ResetPasswordCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.ResetPasswordAsync(stringParam, request);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<ResetPasswordCommand>(c => c.Request == request), It.IsAny<CancellationToken>()), Times.Once);        
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task InitiateEmailChange_Should_SendEmail()
    {
        // Arrange
        var intParam = 1;
        var request = new InitiateEmailChangeRequest(stringParam, intParam);
        var response = new InitiateEmailChangeResponse(stringParam, stringParam, stringParam);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<InitiateEmailChangeCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContextWithIdentity();

        //Act        
        var result = await _controller.InitiateEmailChange();

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<InitiateEmailChangeCommand>(c => c.Request == request), It.IsAny<CancellationToken>()), Times.Once);
        _emailSenderMock.Verify(mailSender => mailSender.SendEmailAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }
}