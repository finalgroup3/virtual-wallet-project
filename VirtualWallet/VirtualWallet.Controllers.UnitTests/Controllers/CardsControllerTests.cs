﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using VirtualWallet.Api.Controllers;
using VirtualWallet.Models.Cards.Requests;
using VirtualWallet.Models.Cards.Responses;
using static VirtualWallet.Features.Cards.DeleteCard.DeleteCardFeature;
using static VirtualWallet.Features.Cards.GetCardsOfUser.GetCardsOfUserFeature;
using static VirtualWallet.Features.CreateCardFeature;
using static VirtualWallet.Features.GetCardFeature;

namespace VirtualWallet.Api.UnitTests.Controllers;

public class CardsControllerTests
{
    private readonly Mock<IMediator> _mediatorMock;    
    private readonly CardsController _controller;

    public CardsControllerTests()
    {
        _mediatorMock = new Mock<IMediator>();       
        _controller = new CardsController(_mediatorMock.Object);
    }

    [Fact]
    public async Task CreateCardAsync_Should_CreateCard()
    {
        // Arrange    
        var request = new CreateCardRequest(
            stringParam, 
            stringParam,
			stringParam,
            stringParam,
            stringParam
            );
        var response = GetCardResponse();

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<CreateCardCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContextWithIdentity();

        //Act        
        var result = await _controller.CreateCardAsync(request);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<CreateCardCommand>(c => c.CreateCardModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<CreatedAtActionResult>(result);
    }

    [Fact]
    public async Task GetCardsOfUserAsync_Should_GetCards()
    {
        // Arrange    
        var request = new GetCardsOfUserRequest(intParam);
        var response = new List<CardResponse>();

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<GetCardsOfUserCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.GetCardsOfUserAsync(intParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<GetCardsOfUserCommand>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task GetCardByNumberAsync_Should_GetCard()
    {
        // Arrange    
        var request = new GetCardRequest(intParam);
        var response = GetCardResponse();

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<GetCardCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.GetCardByNumberAsync(intParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<GetCardCommand>(c => c.RequestModel== request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task DeleteCardAsync_Should_DeleteCard()
    {
        // Arrange    
        var request = new DeleteCardRequest(intParam, intParam);
          
        _controller.ControllerContext = GetControllerContextWithIdentity();

        //Act        
        var result = await _controller.DeleteCardAsync(intParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<DeleteCardCommand>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<NoContentResult>(result);
    }
}
