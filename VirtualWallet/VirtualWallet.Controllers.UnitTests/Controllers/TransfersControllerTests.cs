﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using VirtualWallet.Api.Controllers;
using VirtualWallet.Domain.Pagination;
using VirtualWallet.Models;
using VirtualWallet.Models.Transfer.Requests;
using VirtualWallet.Models.Transfer.Responses;
using static VirtualWallet.Features.Transfers.FilterTransfers.FilterTransfersFeature;
using static VirtualWallet.Features.Transfers.GetTransfer.GetTransferFeature;
using static VirtualWallet.Features.Transfers.TransferFunds.TransferFundsFeature;

namespace VirtualWallet.Api.UnitTests.Controllers;

public class TransfersControllerTests
{
    private readonly Mock<IMediator> _mediatorMock;
    private readonly TransfersController _controller;

    public TransfersControllerTests()
    {
        _mediatorMock = new Mock<IMediator>();
        _controller = new TransfersController(_mediatorMock.Object);
    }

    [Fact]
    public async Task TransferFundsAsync_Should_TransferFunds()
    {
        // Arrange    
        var requestBody = new TransferRequestBody(intParam, decimalParam);
        var request = new TransferFundsRequest(
            intParam,
            intParam,
            decimalParam
            );
        var response = GetTransferFundsResponse();

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<TransferFundsCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContextWithIdentity();

        //Act        
        var result = await _controller.TransferFundsAsync(requestBody);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<TransferFundsCommand>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task FilterTransfersAsync_Should_FilterTransfers()
    {
        // Arrange            
        var request = new FilterTransfersRequest(
            dateParam,
            dateParam,
            stringParam,
            stringParam,
            decimalParam,
            decimalParam,
            stringParam,
            stringParam,
            intParam,
            intParam
            );

        var query = Enumerable.Empty<TransferResponse>().AsQueryable();
        var responseList = PagedList<TransferResponse>.Create(query, intParam, intParam);
        var response = new FilterTransfersResponse(responseList);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<FilterTransfersCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.FilterTransfersAsync(request);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<FilterTransfersCommand>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task GetTransferAsync_Should_GetTransfer()
    {
        // Arrange            
        var request = new TransferRequest(intParam);
        var response = GetTransferResponse();

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<GetTransferCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.GetTransferAsync(intParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<GetTransferCommand>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

}
