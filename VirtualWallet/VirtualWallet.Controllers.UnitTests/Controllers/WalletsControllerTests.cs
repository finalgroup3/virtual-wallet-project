﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using VirtualWallet.Models;
using VirtualWallet.Models.Wallets.Requests;
using VirtualWallet.Models.Wallets.Responses;
using static VirtualWallet.Features.GetWalletFeature;
using static VirtualWallet.Features.Users.ChangeWalletBalanceFeature.ChangeWalletBalanceFeature;
using static VirtualWallet.Features.Wallets.ChangeCurrency.ChangeCurrencyFeature;

namespace VirtualWallet.Api.UnitTests.Controllers;

public class WalletsControllerTests
{
    private readonly Mock<IMediator> _mediatorMock;
    private readonly WalletsController _controller;

    public WalletsControllerTests()
    {
        _mediatorMock = new Mock<IMediator>();
        _controller = new WalletsController(_mediatorMock.Object);
    }

    [Fact]
    public async Task ChangeWalletBalaceAsync_Should_ChangeBalance()
    {
        // Arrange
        var request = new ChangeWalletBalanceRequest(intParam, decimalParam);

        var response = new ChangeWalletBalanceResponse(boolParam, stringParam);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<ChangeWalletBalanceCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.ChangeWalletBalaceAsync(request, intParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<ChangeWalletBalanceCommand>(c => c.Request == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task ChangeWalletCurrency_Should_ChangeCurrency()
    {
        // Arrange
        var request = new ChangeCurrencyRequest(intParam, stringParam, stringParam);

        var response = new ChangeCurrencyResponse(
            intParam,
            stringParam,
            stringParam,
            decimalParam,
            decimalParam,
            decimalParam);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<ChangeCurrencyCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.ChangeWalletCurrency(intParam, request);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<ChangeCurrencyCommand>(c => c.RequestModel == request), 
            It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<CreatedAtActionResult>(result);
    }

    [Fact]
    public async Task GetWalletAsync_Should_ReturnWallet()
    {
        // Arrange
        var request = new GetWalletRequest(intParam);

        var response = new CreateWalletResponse(intParam, stringParam, decimalParam, intParam);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<GetWalletQuery>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.GetWalletAsync(intParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<GetWalletQuery>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }
}
