﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using VirtualWallet.Api.Controllers;
using VirtualWallet.Domain.Pagination;
using VirtualWallet.Models.User.Requests;
using VirtualWallet.Models.User.Responses;
using static VirtualWallet.Features.GetAllUsersFeature;
using static VirtualWallet.Features.GetUserFeature;
using static VirtualWallet.Features.Users.BlockUser.BlockUserFeature;
using static VirtualWallet.Features.Users.ChangeEmail.ChangeEmailFeature;
using static VirtualWallet.Features.Users.ChangePhoneNumber.ChangePhoneNumberFeature;
using static VirtualWallet.Features.Users.ChangePhoneNumber.ChangePictureUrlFeature;
using static VirtualWallet.Features.Users.DeleteUser.DeleteUserFeature;
using static VirtualWallet.Features.Users.DemoteUser.DemoteUserFeature;
using static VirtualWallet.Features.Users.FilterUsers.FilterUsersFeature;
using static VirtualWallet.Features.Users.GetReceivedAmountLastMonth.GetStatisticsFeature;
using static VirtualWallet.Features.Users.GetUserProfile.GetUserProfileFeature;
using static VirtualWallet.Features.Users.GetUserProfileByUsername.GetUserProfileByUsernameFeature;
using static VirtualWallet.Features.Users.PromoteUser.PromoteUserFeature;
using static VirtualWallet.Features.Users.UnblockUser.UnblockUserFeature;

namespace VirtualWallet.Api.UnitTests.Controllers;

public class UsersControllerTests
{
    private readonly Mock<IMediator> _mediatorMock;
    private readonly UsersController _controller;

    public UsersControllerTests()
    {
        _mediatorMock = new Mock<IMediator>();
        _controller = new UsersController(_mediatorMock.Object);
    }

    [Fact]
    public async Task GetUserAsync_Should_ReturnUser()
    {
        // Arrange            
        var request = new GetUserRequest(intParam);
        var response = GetUserResponse();

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<GetUserCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContextWithIdentity();

        //Act        
        var result = await _controller.GetUserAsync(intParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<GetUserCommand>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task GetUserProfileAsync_Should_ReturnUser()
    {
        // Arrange            
        var request = new GetUserRequest(intParam);
        var response = GetUserProfileResponse();

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<GetUserProfileCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.GetUserProfileAsync(intParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<GetUserProfileCommand>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task GetAllUsersAsync_Should_ReturnUser()
    {
        // Arrange 
        var response = new List<UserResponse>();

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<GetAllUsersQuery>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.GetAllUsersAsync();

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.IsAny<GetAllUsersQuery>(), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task GetUserProfileByUsernameAsync_Should_ReturnUserProfile()
    {
        // Arrange 
        var request = new GetUserByUsernameRequest(stringParam);
        var response = GetUserProfileResponse();

        _mediatorMock.Setup(mediator => mediator.Send(It.Is<GetUserProfileByUsernameCommand>(c => c.RequestModel == request),
            It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.GetUserProfileByUsernameAsync(stringParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.IsAny<GetUserProfileByUsernameCommand>(), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task FilterUsersAsync_Should_ReturnUsers()
    {
        // Arrange            
        var request = new FilterUsersRequest(
            stringParam,
            stringParam,
            stringParam,
            stringParam,
            stringParam,
            intParam,
            intParam
            );

        var query = Enumerable.Empty<UserResponse>().AsQueryable();
        var responseList = PagedList<UserResponse>.Create(query, intParam, intParam);
        var response = new UserFilterResponse(responseList);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<FilterUsersCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.FilterUsersAsync(request);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<FilterUsersCommand>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task ChangeEmailAsync_Should_ChangeEmail()
    {
        // Arrange            
        var request = new ChangeEmailRequest(
            stringParam,
            stringParam            
            );
        var response = new ChangeEmailResponse(boolParam, stringParam);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<ChangeEmailCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.ChangeEmailAsync(stringParam, request);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<ChangeEmailCommand>(c => c.Request == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task ChangePhoneNumberAsync_Should_ChangePhoneNumber()
    {
        // Arrange            
        var requestBody = new ChangePhoneNumberBody(stringParam);
        var request = new ChangePhoneNumberRequest(
            stringParam,
            intParam
            );
        var response = new ChangePhoneNumberResponse(stringParam, stringParam);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<ChangePhoneNumberCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContextWithIdentity();

        //Act        
        var result = await _controller.ChangePhoneNumberAsync(requestBody);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<ChangePhoneNumberCommand>(c => c.Request == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task ChangePictureUrlAsync_Should_ChangePictureUrl()
    {
        // Arrange            
        var requestBody = new ChangePictureUrlBody(stringParam);
        var request = new ChangePictureUrlRequest(
            stringParam,
            intParam
            );
        var response = new ChangePictureUrlResponse(stringParam, stringParam);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<ChangePictureUrlCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContextWithIdentity();

        //Act        
        var result = await _controller.ChangePictureUrlAsync(requestBody);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<ChangePictureUrlCommand>(c => c.Request == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task GetStatisticsAsync_Should_GetStats()
    {
        // Arrange                    
        var request = new GetStatisticsRequest(
            intParam,
            dateParam,
            dateParam
            );
        var response = new GetStatisticsResponse(
            intParam,
            dateParam,
            dateParam,
            stringParam,
            decimalParam,
            decimalParam,
            intParam,
            intParam);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<GetReceivedAmountLastMonthQuery>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContext();

        //Act        
        var result = await _controller.GetStatisticsAsync(intParam, dateParam, dateParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<GetReceivedAmountLastMonthQuery>(c => c.Request == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task BlockUserAsync_Should_BlockUser()
    {
        // Arrange                    
        var request = new BlockStatusRequest(
            intParam,
            intParam
            );
        var response = new BlockStatusResponse(
            intParam,
            boolParam);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<BlockUserCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContextWithIdentity();

        //Act        
        var result = await _controller.BlockUserAsync(intParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<BlockUserCommand>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task UnblockUserAsync_Should_UnblockUser()
    {
        // Arrange                    
        var request = new BlockStatusRequest(
            intParam,
            intParam
            );
        var response = new BlockStatusResponse(
            intParam,
            boolParam);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<UnblockUserCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContextWithIdentity();

        //Act        
        var result = await _controller.UnblockUserAsync(intParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<UnblockUserCommand>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task DemoteUserAsync_Should_DemoteUser()
    {
        // Arrange                    
        var request = new AdminStatusRequest(
            intParam,
            intParam
            );
        var response = new AdminStatusResponse(
            intParam,
            boolParam);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<DemoteUserCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContextWithIdentity();

        //Act        
        var result = await _controller.DemoteUserAsync(intParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<DemoteUserCommand>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task PromoteUserAsync_Should_PromoteUser()
    {
        // Arrange                    
        var request = new AdminStatusRequest(
            intParam,
            intParam
            );
        var response = new AdminStatusResponse(
            intParam,
            boolParam);

        _mediatorMock.Setup(mediator => mediator.Send(It.IsAny<PromoteUserCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

        _controller.ControllerContext = GetControllerContextWithIdentity();

        //Act        
        var result = await _controller.PromoteUserAsync(intParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<PromoteUserCommand>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public async Task DeleteUserAsync_Should_DeleteUser()
    {
        // Arrange                    
        var request = new DeleteUserRequest(
            intParam
            ); 
        
        _controller.ControllerContext = GetControllerContextWithIdentity();

        //Act        
        var result = await _controller.DeleteUserAsync(intParam);

        //Assert
        _mediatorMock.Verify(mediator => mediator.Send(It.Is<DeleteUserCommand>(c => c.RequestModel == request), It.IsAny<CancellationToken>()), Times.Once);
        Assert.IsType<NoContentResult>(result);
    }
}
