﻿using VirtualWallet.Models.Api;

namespace VirtualWallet.Client.Abstractions;

public interface IApiClient
{
    Task<ApiResponse<TResponse>> LoginAsync<TRequest, TResponse>(TRequest model)
        where TRequest : class
        where TResponse : class;
    Task<ApiResponse<TResponse>> RegisterAsync<TRequest, TResponse>(TRequest model)
        where TRequest : class
        where TResponse : class;
}
