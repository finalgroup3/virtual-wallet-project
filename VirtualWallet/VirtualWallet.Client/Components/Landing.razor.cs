﻿namespace VirtualWallet.Client.Components;

public partial class Landing
{
    public List<string> Images = new List<string>
    {
        "img/cellphone-5459713.svg",
        "img/laptop-5459712.svg",
        "img/lock-5459714.svg",
    };
}
