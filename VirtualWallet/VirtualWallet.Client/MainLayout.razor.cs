﻿using Microsoft.AspNetCore.Components;
using MudBlazor;
using VirtualWallet.Client.NewFolder;

namespace VirtualWallet.Client;

public partial class MainLayout : IDisposable
{
    [Inject] private AppSettings AppSettings { get; set; } = default!;

    private MudTheme Theme => AppSettings.Theme;
    private bool _isDarkMode;

    protected override void OnInitialized()
    {
        _isDarkMode = AppSettings.IsDarkMode;
        AppSettings.OnChange += UpdateTheme;
        base.OnInitialized();
    }

    private void UpdateTheme()
    {
        _isDarkMode = AppSettings.IsDarkMode;
        InvokeAsync(StateHasChanged);
    }

    public void Dispose() => AppSettings.OnChange -= UpdateTheme;
}
