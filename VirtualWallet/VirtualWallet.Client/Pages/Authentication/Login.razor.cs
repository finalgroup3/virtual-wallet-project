﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using System.ComponentModel.DataAnnotations;
using VirtualWallet.Client.Services.Api.ApiClient;
using VirtualWallet.Models;
using VirtualWallet.Models.Authentication.Requests;

namespace VirtualWallet.Client.Pages.Authentication;

public partial class Login
{
    [Inject] ApiClient ApiClient { get; set; } = default!;
    [Inject] NavigationManager NavigationManager { get; set; } = default!;
    private readonly LoginData model = new();
    private bool success;

    private async Task LoginAsync()
    {
        var loginRequest = new LoginRequest
        (
            model.Email,
            model.Password
        );

        var response = await ApiClient.LoginAsync<LoginRequest, AuthenticationResponse>(loginRequest);

        if (response.IsSuccess)
        {
            NavigationManager.NavigateTo("/");
        }
        else
        {

        }
    }

    private async Task OnValidSubmit(EditContext context)
    {
        success = true;
        await LoginAsync();
        StateHasChanged();
    }

    private sealed class LoginData
    {
        [EmailAddress]
        [StringLength(100, ErrorMessage = "Email is too long.")]
        public string Email { get; set; } = default!;
        [StringLength(100, ErrorMessage = "Password is too long.")]
        public string Password { get; set; } = default!;
    }

}
