﻿using MudBlazor;

namespace VirtualWallet.Client.NewFolder;

public class AppSettings
{
    public MudTheme Theme { get; set; } = new MudTheme();
    public bool IsDarkMode { get; set; } = false;
    public event Action OnChange;

    public void SetDarkMode(bool isDarkMode)
    {
        IsDarkMode = isDarkMode;
        NotifyStateChanged();
        Console.WriteLine("Theme changed.");
    }
    private void NotifyStateChanged() => OnChange?.Invoke();
}
