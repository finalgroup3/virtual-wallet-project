﻿using System.Text;
using System.Text.Json;
using VirtualWallet.Client.Abstractions;
using VirtualWallet.Models.Api;

namespace VirtualWallet.Client.Services.Api.ApiClient;

public class ApiClient : IApiClient
{
    private readonly HttpClient _httpClient;
    private readonly JsonSerializerOptions _jsonSerializerOptions;
    private const string _baseUrl = "http://localhost:5120/";

    public ApiClient(HttpClient httpClient)
    {
        _httpClient = httpClient;
        var handler = new HttpClientHandler
        {
            ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
        };
        _httpClient = new HttpClient(handler) { BaseAddress = new Uri(_baseUrl) };
        _jsonSerializerOptions = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true
        };
    }

    public async Task<ApiResponse<TResponse>> RegisterAsync<TRequest, TResponse>(TRequest model)
        where TRequest : class
        where TResponse : class
    {
        return await PostAsync<TRequest, TResponse>("api/virtual-wallet/auth/register", model);
    }

    public async Task<ApiResponse<TResponse>> LoginAsync<TRequest, TResponse>(TRequest model)
        where TRequest : class
        where TResponse : class
    {
        return await PostAsync<TRequest, TResponse>("api/virtual-wallet/auth/login", model);
    }

    private async Task<ApiResponse<TResponse>> DeleteAsync<TResponse>(string url) where TResponse : class
    {
        return await SendAsync<TResponse>(HttpMethod.Delete, url, null);
    }

    private async Task<ApiResponse<TResponse>> PutAsync<TRequest, TResponse>(string url, TRequest model)
        where TRequest : class
        where TResponse : class
    {
        var content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
        return await SendAsync<TResponse>(HttpMethod.Put, url, content);
    }

    private async Task<ApiResponse<TResponse>> PostAsync<TRequest, TResponse>(string url, TRequest model)
        where TRequest : class
        where TResponse : class
    {
        var content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
        return await SendAsync<TResponse>(HttpMethod.Post, url, content);
    }

    private async Task<ApiResponse<TResponse>> GetAsync<TResponse>(string url)
        where TResponse : class
    {
        return await SendAsync<TResponse>(HttpMethod.Get, url, null);
    }


    private async Task<ApiResponse<TResponse>> SendAsync<TResponse>(HttpMethod method, string url, HttpContent? content)
        where TResponse : class
    {
        var request = new HttpRequestMessage(method, url)
        {
            Content = content
        };

        var response = await _httpClient.SendAsync(request);

        var responseContent = await response.Content.ReadAsStringAsync();

        var apiResponse = new ApiResponse<TResponse>();
        if (response.IsSuccessStatusCode)
        {
            apiResponse.Model = JsonSerializer.Deserialize<TResponse>(responseContent, _jsonSerializerOptions);
            apiResponse.IsSuccess = true;
        }
        else
        {
            apiResponse.Errors = JsonSerializer.Deserialize<ApiError>(responseContent, _jsonSerializerOptions);
        }
        return apiResponse;
    }
}
