using MudBlazor.Services;
using VirtualWallet.Client.Abstractions;
using VirtualWallet.Client.NewFolder;
using VirtualWallet.Client.Services.Api.ApiClient;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddMudServices();
builder.Services.AddSingleton<AppSettings>();
builder.Services.AddSingleton<IApiClient, ApiClient>();

builder.Services.AddHttpClient<ApiClient>();


var app = builder.Build();


app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();
