﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using VirtualWallet.Domain.CardEntity;
using VirtualWallet.Domain.TransferEntity;
using VirtualWallet.Domain.UserEntity;
using VirtualWallet.Models;
using VirtualWallet.Models.Cards.Requests;
using VirtualWallet.Models.Cards.Responses;
using VirtualWallet.Models.Transfer.Responses;
using VirtualWallet.Models.User.Responses;

namespace VirtualWallet.Tests.Helpers;

public static class TestHelpers
{
    public static string currencyParam = "EUR";
    public static int intParam = 1;
    public static long longParam = 2;
    public static double doubleParam = 2;
    public static decimal decimalParam = 2.0m;
    public static string stringParam = GetTestString();
    public static string tokenString = Guid.NewGuid().ToString();
    public static bool boolParam = true;
    public static DateTime dateParam = DateTime.Now;
    public static DateTime futureDate = DateTime.Now.AddDays(1);
    public static DateTime pastDate = DateTime.Now.AddDays(-1);
    public static DateTime minDate = DateTime.MinValue;
    private static string defaultIdString = "1";
    private static string testCardNumber = GetTestString(16);
    private static string securityKey = "defaultSecurityKey";
    private static string issuer = "issuer";
    private static string audience = "audience";

    public static string GetValidToken()
    {
        var claims = new Claim[]
        {
            new Claim(JwtRegisteredClaimNames.Sub, defaultIdString)            
        };

        var signingCredentials = new SigningCredentials(
            new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(securityKey)),
            SecurityAlgorithms.HmacSha256);

        var token = new JwtSecurityToken(
            issuer,
            audience,
            claims,
            null,
            futureDate,
            signingCredentials);

        return new JwtSecurityTokenHandler().WriteToken(token);
    }

    public static string GetInvalidToken()
    {
        var claims = new Claim[]
        {
            new Claim(JwtRegisteredClaimNames.Sub, defaultIdString)
        };

        var signingCredentials = new SigningCredentials(
            new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(securityKey)),
            SecurityAlgorithms.HmacSha256);

        var token = new JwtSecurityToken(
            issuer,
            audience,
            claims,
            null,
            pastDate,
            signingCredentials);

        return new JwtSecurityTokenHandler().WriteToken(token);
    }

    public static ControllerContext GetControllerContext()
    {
        var responseMock = new Mock<HttpResponse>();
        var cookiesMock = new Mock<IResponseCookies>();
        responseMock.SetupGet(r => r.Cookies).Returns(cookiesMock.Object);

        var httpContextMock = new Mock<HttpContext>();
        httpContextMock.SetupGet(context => context.Response).Returns(responseMock.Object);

        var controllerContext = new ControllerContext
        {
            HttpContext = httpContextMock.Object
        };
        return controllerContext;
    }

    public static ControllerContext GetControllerContextWithIdentity()
    {
        var responseMock = new Mock<HttpResponse>();
        var cookiesMock = new Mock<IResponseCookies>();
        responseMock.SetupGet(r => r.Cookies).Returns(cookiesMock.Object);

        var httpContextMock = new Mock<HttpContext>();
        httpContextMock.SetupGet(context => context.Response).Returns(responseMock.Object);

        var identity = new ClaimsIdentity();
        identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, defaultIdString));
        identity.AddClaim(new Claim(ClaimTypes.Email, GetTestString()));
        identity.AddClaim(new Claim(ClaimTypes.Role, GetTestString()));
        var principal = new ClaimsPrincipal(identity);
        httpContextMock.SetupGet(context => context.User).Returns(principal);

        var controllerContext = new ControllerContext
        {
            HttpContext = httpContextMock.Object
        };
        return controllerContext;
    }

    public static CardResponse GetCardResponse()
    {
        return new CardResponse(
            intParam,
            stringParam,
            stringParam,
            stringParam,
            stringParam,
            stringParam,
            intParam
            );
    }

    public static TransferFundsResponse GetTransferFundsResponse()
    {
        var exchangeData = GetExchangeRateResponse();
        return new TransferFundsResponse(
            intParam,
            intParam,
            stringParam,
            intParam,
            stringParam,
            decimalParam,
            stringParam,
            decimalParam,
            stringParam,
            decimalParam,
            decimalParam,
            exchangeData
            );
    }

    public static ExchangeRateResponse GetExchangeRateResponse()
    {         
        return new ExchangeRateResponse(
            stringParam,
            stringParam,
            stringParam,
            longParam,
            stringParam,
            longParam,
            stringParam,
            stringParam,
            stringParam,
            doubleParam,
            decimalParam
            );
    }

    public static TransferResponse GetTransferResponse()
    {
        return new TransferResponse(
            intParam,
            intParam,
            stringParam,
            intParam,
            stringParam,
            decimalParam,
            stringParam,
            decimalParam,
            stringParam,
            decimalParam,
            decimalParam
            );
    }

    public static UserResponse GetUserResponse()
    {
        return new UserResponse(
            intParam,
            stringParam,
            stringParam,
            stringParam,
            stringParam,
            boolParam,
            boolParam,
            boolParam
            );
    }

    public static UserProfileResponse GetUserProfileResponse()
    {
        var bankCarts = new List<UserBankCards>();
        var transfersSent = new List<UserTransfersSent>();
        var transfersReceived = new List<UserTransfersReceived>();
        var balance = new WalletBalance(decimalParam, stringParam);
        var wallets = new ProfileWallet(intParam, balance);
        return new UserProfileResponse(
            intParam,
            stringParam,
            stringParam,
            stringParam,
            stringParam,
            boolParam,
            boolParam,
            boolParam,
            bankCarts,
            transfersReceived,
            transfersSent,
            wallets
            );
    }

    public static RegisterRequest GetRegisterRequest()
    {
        return new RegisterRequest(
            stringParam,
            stringParam,
            stringParam,
            stringParam);
    }

    public static User GetTestUser()
    {
        return User.CreateUser(
            stringParam,
            stringParam,
            stringParam,
            stringParam);
    }

    public static CreateCardRequest GetCreateCardRequest()
    {        
        return new CreateCardRequest(
            stringParam,
            testCardNumber,
            stringParam, 
            stringParam,
            stringParam);
    }

    public static BankCard GetTestCard()
    {
        return BankCard.CreateCard
            (
                stringParam,
                testCardNumber,
                futureDate,
                stringParam,
                stringParam,
                intParam
            );
    }

    public static Transfer GetTestTransfer()
    {
        var transfer = Transfer.CreateTransfer
            (
                intParam,
                intParam,
                decimalParam,
                stringParam,
                decimalParam,
                stringParam,
                decimalParam,
                decimalParam
            );
        var user = GetTestUser();
        transfer.SetSender(user);
        transfer.SetRecipient(user);

        return transfer;
    }

    public static IQueryable<User> GetTestUsersQuery(User user)
    {
        var testQuery = new List<User>
        {
            GetTestUser(),
            GetTestUser(),
            GetTestUser(),
            GetTestUser(),
            GetTestUser(),
            user
        };

        return testQuery.AsQueryable();
    }

    public static IQueryable<Transfer> GetTestTransfersQuery(Transfer transfer)
    {
        var testQuery = new List<Transfer>
        {
            GetTestTransfer(),
            GetTestTransfer(),
            GetTestTransfer(),
            GetTestTransfer(),
            GetTestTransfer(),            
            transfer
        };

        return testQuery.AsQueryable();
    }

    public static string GetTestString(int length)
    {
        return new string('y', length);
    }
    public static string GetTestString()
    {
        return new string('x', 10);
    }
}
