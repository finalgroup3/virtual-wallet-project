## Backend Repository for Virtual Wallet Project

Welcome to the backend repository of Virtual Wallet Project! 
This repository contains the backend code responsible for handling the core logic, data processing, and communication with databases or external services.

If you're looking for the frontend code that powers the user interface and user experience, it's hosted in a separate GitLab repository. 
You can find the frontend repository [here](https://gitlab.com/finalgroup3/virtual-wallet-react).


- [Backend Repository for Virtual Wallet Project](#backend-Repository-for-virtual-wallet-project)
  - [Description](#description)
  - [How to Run](#how-to-run)
  - [Technologies](#technologies)
  - [HATEOAS (Hyerpmedia as the Engine of Application State)](#hateoas-hyerpmedia-as-the-engine-of-application-state)
    - [Key Points](#key-points)
    - [Benefits](#benefits)
  - [MediatR Implementation and Benefits](#mediatr-implementation-and-benefits)
    - [Implementation](#implementation)


### Description
This project is a virtual wallet that allows users to create an account, login, and add funds to their account. Users can also send money to other users and view their transaction history. This project was created using C#, and Microsoft SQL Server.

### How to Run
1. Clone the repository
2. Open the project in Visual Studio
3. Run the project

### Technologies
- C#
- Microsoft SQL Server
- Entity Framework Core
- ASP.NET Core MVC

### HATEOAS (Hyerpmedia as the Engine of Application State)
Is a fundamental principle of RESTful APIs. It allows the client to interact with the server through hypermedia links. This allows the client to navigate the API without having to know the URL structure beforehand. This is useful because it allows the server to change the URL structure without breaking the client.

  - Example:
    - Request: `POST /api/auth/register`
    - Response:
```json
{
  "id": 41,
  "username": "14dfff",
  "email": "test22@example.com",
  "phoneNumber": "string1234",
  "balance": 0,
  "isAdmin": false,
  "isBlocked": false,
  "isVerified": true,
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0MSIsIm5hbWUiOiIxNGRmZmYiLCJlbWFpbCI6InRlc3QyMkBleGFtcGxlLmNvbSIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IlVzZXIiLCJleHAiOjE2OTAyMjk2OTIsImlzcyI6IlRlYW04LlZpcnR1YWxXYWxsZXQiLCJhdWQiOiJUZWFtOC5WaXJ0dWFsV2FsbGV0In0.6Bu-qvvVNREbEbT7cFYTQnecWo-9ZDv_pX1UHnLNEbc",
  "links": [
    {
      "href": "http://localhost:5120/api/virtual-wallet/auth/verify-email",
      "rel": "self",
      "method": "GET"
    },
    {
      "href": "http://localhost:5120/api/virtual-wallet/users/14dfff",
      "rel": "profile",
      "method": "GET"
    },
    {
      "href": "http://localhost:5120/api/virtual-wallet/users/14dfff",
      "rel": "delete-user",
      "method": "DELETE"
    },
    {
      "href": "http://localhost:5120/api/virtual-wallet/auth/login",
      "rel": "login",
      "method": "POST"
    },
    {
      "href": "http://localhost:5120/api/virtual-wallet/auth/logout",
      "rel": "logout",
      "method": "POST"
    }
  ]
}
```

#### Key Points
HATEOAS encourages a self-descriptive nature in APIs, making them more intuitive and easier to use.
Instead of relying on prior knowledge or fixed URLs, clients interact with the API by following links embedded in the responses.
The API acts as a "hypermedia" guide, providing navigation options to clients and reducing the coupling between the client and server.
With HATEOAS, APIs can evolve over time without breaking client implementations, as clients adapt dynamically based on the provided links.

#### Benefits
Reduced client-server coupling: Clients become less dependent on hardcoded URLs and can adapt to changes more gracefully.
Enhanced discoverability: HATEOAS makes it easier for clients to explore the API and understand available actions.
Future-proofing: API changes and additions can be made without breaking existing client implementations.

### MediatR Implementation and Benefits
MediatR is a popular library in the .NET ecosystem that enables the implementation of the Mediator pattern. 
It enhances the separation of concerns by promoting the concept of "single responsibility" for classes and components within a software application. 
In the context of your Virtual Wallet project, you've utilized MediatR to manage the interactions between different parts of your application.

#### Implementation
In the Virtual Wallet project, MediatR has been employed to facilitate communication between various application components. 
Instead of components directly invoking methods on each other, they send requests through the MediatR pipeline. 
These requests are then handled by corresponding request handler classes, which encapsulate the logic required to fulfill the requests.

#### Created By
Konstantin L. Stoianov, Iliyan Tsvetkov, Diman Stamatov